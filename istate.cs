public string InstrumentState
{
get
{
if ( Instrument.SDPCRProcessor.Connected )
{
	bool r = Instrument.Rotor.GetMotorState(out string rState);
	bool x = Instrument.XStage.GetMotorState(out string xState);
	bool y = Instrument.YStage.GetMotorState(out string yStage);
	bool f = Instrument.FilterWheel.GetMotorState(out string fState);
	if ( !r)
		rState = "1_Controller:Off";
	else
		rState = String.Format("1_Controller:On,{0}",rState);
	if ( !x)
		xState = "2_Controller:Off";
	else
		xState = String.Format("2_Controller:On,{0}",xState);
	if ( !y)
		yState = "3_Controller:Off";
	else
		yState = String.Format("3_Controller:On,{0}",yState);
	if ( !f)
		fState = "4_Controller:Off";
	else
		fState = String.Format("4_Controller:On,{0}",fState);

	string tState = String.Format("PID_Program_State:{0}",
		Instrument.TempProgramController.CurrentState);
	string vState = String.Format("Velocity_Program_State:{0}",
		Instrument.RotorVelocitySequencer.CurrentState);

	return String.Format("Processor:Connected,{0},{1},{2},{3},{4},{5}",
		rState, xState, yState, fState, tState, vState);

}
else
{
return "Processor:NotConnected";
}
}
}
