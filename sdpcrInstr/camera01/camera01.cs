﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Drawing;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.IO;
using System.IO.Ports;
using System.Diagnostics;
using System.Windows.Forms;
using TIS.Imaging;

namespace camera01
{

    class Program
    {
        internal static bool StringToXDoc(string XDocString, out XDocument XDoc)
        {
            try { XDoc = XDocument.Parse(XDocString); }
            catch (System.Xml.XmlException)
            {
                XDoc = new XDocument();
                return false;
            }
            return true;
        }
        static internal Stopwatch stopW = new Stopwatch();
        static void Main(string[] args)
        {
            ICImagingControl control = new ICImagingControl();
            TIS.Imaging.Device[] devs = control.Devices;
            TIS.Imaging.Device dev;
            Console.WriteLine(String.Format("{0} devices found", devs.Length));
            if (devs.Length > 0)
            {
                stopW.Start();
                dev = devs[0];
                Console.WriteLine(String.Format("Device Version: {0}", dev.DeviceVersion));
                Console.WriteLine(String.Format("Driver Version: {0}", dev.DriverVersion));
                Console.WriteLine(String.Format("Name: {0}", dev.Name));
                string numb;
                if (dev.GetSerialNumber(out numb))
                {
                    Console.WriteLine(String.Format("Serial Number: {0}", numb));
                    // DevState creates the string which needs to be loaded when starting camera.  Most of the string is the
                    // same for all cameras, but the camera name and serial number needs to be added.  Otherwise, the camera
                    // will reject the string
                    Console.WriteLine(DevState(dev.Name, numb));  
                    control.LoadDeviceState(DevState(dev.Name, numb), true);
                    // Get current state of camera and write it out for all to see.
                    string currentProperties = control.SaveDeviceState();
                    Console.WriteLine("============================================");
                    Console.WriteLine(currentProperties);
                    int numbRetrys;
                    bool retry;
                    if (StringToXDoc(currentProperties, out XDocument xDoc))
                    {
                        // Use DeviceState() to collect particular values from currentProperties and write them to console
                        string[] cState = DeviceState(xDoc);
                        Console.WriteLine("==========");
                        for (int n = 0; n < cState.Length; n++)
                            Console.WriteLine(cState[n]);
                        Console.WriteLine("==========");
                        if (control.VideoFormats.Length > 0)
                        {
                            for (int m = 0; (m < control.VideoFormats.Length && m <= 30); m++)
                                Console.WriteLine(String.Format("Video Format {0}: {1}", m, control.VideoFormats[m].ToString()));
                        }
                        Console.WriteLine("==========");
                        control.VideoFormat = control.VideoFormats[28];  // Sets the video format to Y16 (3872x2764) - 16 bit monochrome

                        // This is to change the value of the gain and exposure in xDoc 
                        // This works only if we know the names of the elements in the xml document.
                        if (SetOne(xDoc, "Gain", "Value", "2.00") && SetOne(xDoc, "Exposure", "Value", "0.500"))
                        {
                            Console.WriteLine(xDoc.ToString());   // write the xDoc to console for people to look at
                            control.LoadDeviceState(xDoc.ToString(), true);  // load xDoc into the camera to change the values on the camera

                            string newProperties = control.SaveDeviceState();  // check to see if the camera state has changed
                            if (StringToXDoc(newProperties, out XDocument xDoc2))
                            {
                                string[] nState = DeviceState(xDoc2);
                                Console.WriteLine("========== new State");
                                for (int n = 0; n < nState.Length; n++)
                                    Console.WriteLine(nState[n]);
                                Console.WriteLine("==========");
                            }
                        }
                        // The camera will stream images to a FrameSnapSink.  So set up the sink and tell control about it
                        FrameSnapSink fsSink = new FrameSnapSink();
                        IFrameQueueBuffer frame;
                        control.Sink = fsSink;
                        control.LivePrepare();
                        Console.WriteLine("Live Prepared " + control.IsLivePrepared.ToString());
                        // Start streaming images to sink
                        control.LiveStart();
                        Console.WriteLine("Time: " + stopW.ElapsedMilliseconds);
                        numbRetrys = 0;
                        retry = true;
                        while (retry)
                        {
                            try
                            {
                                frame = fsSink.SnapSingle(TimeSpan.FromSeconds(4));
                                Console.WriteLine("Took Picture 1 - Exposure 0.5, Time: " + stopW.ElapsedMilliseconds);
                                frame.SaveAsTiff("c:\\instrument\\testF28G2E05.tiff");
                                retry = false;
                            }
                            catch (Exception _)
                            {
                                numbRetrys++;
                                Console.WriteLine("Problem with SnapSingle, Try = " + numbRetrys);
                                if (numbRetrys > 2)
                                    retry = false;
                            }
                        }
                        if (numbRetrys <= 2)
                            Console.WriteLine("Picture taken");
                        else
                            Console.WriteLine("Picture not taken");
                        control.LiveStop();
                        // Next command sets the format to 16 bits monochrome 1280x1024
                        control.VideoFormat = control.VideoFormats[23];
                        Console.WriteLine("Current Video Format: " + control.VideoFormatCurrent.ToString());
                        control.LivePrepare();
                        Console.WriteLine("Live Prepared " + control.IsLivePrepared.ToString());
                        control.LiveStart();
                        Console.WriteLine("Time: " + stopW.ElapsedMilliseconds);
                        //Console.WriteLine("Press enter to snap picture");
                        //Console.ReadLine();
                        numbRetrys = 0;
                        retry = true;
                        while (retry)
                        {
                            try
                            {
                                frame = fsSink.SnapSingle(TimeSpan.FromSeconds(4));
                                Console.WriteLine("Took Picture 2 - Exposure 0.5, Time: " + stopW.ElapsedMilliseconds);
                                frame.SaveAsTiff("c:\\instrument\\testF28G2E05_Y23.tiff");
                                retry = false;
                            }
                            catch (Exception _)
                            {
                                numbRetrys++;
                                Console.WriteLine("Problem with SnapSingle, Try = " + numbRetrys);
                                if (numbRetrys > 2)
                                    retry = false;
                            }
                        }
                        if (numbRetrys <= 2)
                            Console.WriteLine("Picture taken");
                        else
                            Console.WriteLine("Picture not taken");
                        control.LiveStop();

                        control.VideoFormat = control.VideoFormats[28];
                        SetOne(xDoc, "Exposure", "Value", "1.000");
                        control.LoadDeviceState(xDoc.ToString(), true);
                        System.Threading.Thread.Sleep(1000);
                        control.LivePrepare();
                        Console.WriteLine("Live Prepared " + control.IsLivePrepared.ToString());
                        control.LiveStart();
                        Console.WriteLine("Time: " + stopW.ElapsedMilliseconds);
                        numbRetrys = 0;
                        retry = true;
                        while (retry)
                        {
                            try
                            {
                                frame = fsSink.SnapSingle(TimeSpan.FromSeconds(4));
                                Console.WriteLine("Took Picture 3 - Exposure 1.0, Time: " + stopW.ElapsedMilliseconds);
                                frame.SaveAsTiff("c:\\instrument\\testF28G2E1.tiff");
                                retry = false;
                            }
                            catch (Exception _)
                            {
                                numbRetrys++;
                                Console.WriteLine("Problem with SnapSingle, Try = " + numbRetrys);
                                if (numbRetrys > 2)
                                    retry = false;
                            }
                        }
                        if (numbRetrys <= 2)
                            Console.WriteLine("Picture taken");
                        else
                            Console.WriteLine("Picture not taken");
                        control.LiveStop();

                        SetOne(xDoc, "Exposure", "Value", "2.000");
                        control.LoadDeviceState(xDoc.ToString(), true);
                        System.Threading.Thread.Sleep(1000);
                        control.LivePrepare();
                        Console.WriteLine("Live Prepared " + control.IsLivePrepared.ToString());
                        control.LiveStart();
                        Console.WriteLine("Time: " + stopW.ElapsedMilliseconds);
                        numbRetrys = 0;
                        retry = true;
                        while (retry)
                        {
                            try
                            {
                                frame = fsSink.SnapSingle(TimeSpan.FromSeconds(5));
                                Console.WriteLine("Took Picture 4 - Exposure 2.0, Time: " + stopW.ElapsedMilliseconds);
                                frame.SaveAsTiff("c:\\instrument\\testF28G2E2.tiff");
                                retry = false;
                            }
                            catch (Exception _)
                            {
                                numbRetrys++;
                                Console.WriteLine("Problem with SnapSingle, Try = " + numbRetrys);
                                if (numbRetrys > 2)
                                    retry = false;
                            }
                        }
                        if (numbRetrys <= 2)
                            Console.WriteLine("Picture taken");
                        else
                            Console.WriteLine("Picture not taken");
                        control.LiveStop();

                        SetOne(xDoc, "Exposure", "Value", "1.000");
                        SetOne(xDoc, "Gain", "Value", "3.00");
                        control.LoadDeviceState(xDoc.ToString(), true);
                        System.Threading.Thread.Sleep(1000);
                        control.LivePrepare();
                        Console.WriteLine("Live Prepared " + control.IsLivePrepared.ToString());
                        control.LiveStart();
                        Console.WriteLine("Time: " + stopW.ElapsedMilliseconds);
                        numbRetrys = 0;
                        retry = true;
                        while (retry)
                        {
                            try
                            {
                                frame = fsSink.SnapSingle(TimeSpan.FromSeconds(4));
                                Console.WriteLine("Took Picture 5 - Exposure 1.0, Time: " + stopW.ElapsedMilliseconds);
                                frame.SaveAsTiff("c:\\instrument\\testF28G3E1.tiff");
                                retry = false;
                            }
                            catch (Exception _)
                            {
                                numbRetrys++;
                                Console.WriteLine("Problem with SnapSingle, Try = " + numbRetrys);
                                if (numbRetrys > 2)
                                    retry = false;
                            }
                        }
                        if (numbRetrys <= 2)
                            Console.WriteLine("Picture taken");
                        else
                            Console.WriteLine("Picture not taken");
                        control.LiveStop();
                        SetOne(xDoc, "Gain", "Value", "4.00");
                        control.LoadDeviceState(xDoc.ToString(), true);
                        System.Threading.Thread.Sleep(1000);
                        control.LivePrepare();
                        Console.WriteLine("Live Prepared " + control.IsLivePrepared.ToString());
                        control.LiveStart();
                        Console.WriteLine("Time: " + stopW.ElapsedMilliseconds);
                        numbRetrys = 0;
                        retry = true;
                        while (retry)
                        {
                            try
                            {
                                frame = fsSink.SnapSingle(TimeSpan.FromSeconds(4));
                                Console.WriteLine("Took Picture 6 - Exposure 1.0, Time: " + stopW.ElapsedMilliseconds);
                                frame.SaveAsTiff("c:\\instrument\\testF28G4E1.tiff");
                                retry = false;
                            }
                            catch (Exception _)
                            {
                                numbRetrys++;
                                Console.WriteLine("Problem with SnapSingle, Try = " + numbRetrys);
                                if (numbRetrys > 2)
                                    retry = false;
                            }
                        }
                        if (numbRetrys <= 2)
                            Console.WriteLine("Picture taken");
                        else
                            Console.WriteLine("Picture not taken");
                        control.LiveStop();
                        SetOne(xDoc, "Gain", "Value", "2.00");
                        control.LoadDeviceState(xDoc.ToString(), true);
                        System.Threading.Thread.Sleep(1000);
                        control.LivePrepare();
                        Console.WriteLine("Set up to take multiple images with out LiveStart() before each");
                        Console.WriteLine("Live Prepared " + control.IsLivePrepared.ToString());
                        control.LiveStart();
                        Console.WriteLine("Time: " + stopW.ElapsedMilliseconds);
                        numbRetrys = 0;
                        retry = true;
                        while (retry)
                        {
                            try
                            {
                                frame = fsSink.SnapSingle(TimeSpan.FromSeconds(4));
                                Console.WriteLine("Took Picture 7.1 - Exposure 1.0, Time: " + stopW.ElapsedMilliseconds);
                                frame.SaveAsTiff("c:\\instrument\\testF28G2E1S1.tiff");
                                retry = false;
                            }
                            catch (Exception _)
                            {
                                numbRetrys++;
                                Console.WriteLine("Problem with SnapSingle, Try = " + numbRetrys);
                                if (numbRetrys > 2)
                                    retry = false;
                            }
                        }
                        if (numbRetrys <= 2)
                            Console.WriteLine("Picture taken");
                        else
                            Console.WriteLine("Picture not taken");
                        Console.WriteLine("Time: " + stopW.ElapsedMilliseconds);
                        numbRetrys = 0;
                        retry = true;
                        while (retry)
                        {
                            try
                            {
                                frame = fsSink.SnapSingle(TimeSpan.FromSeconds(4));
                                Console.WriteLine("Took Picture 7.2 - Exposure 1.0, Time: " + stopW.ElapsedMilliseconds);
                                frame.SaveAsTiff("c:\\instrument\\testF28G2E1S2.tiff");
                                retry = false;
                            }
                            catch (Exception _)
                            {
                                numbRetrys++;
                                Console.WriteLine("Problem with SnapSingle, Try = " + numbRetrys);
                                if (numbRetrys > 2)
                                    retry = false;
                            }
                        }
                        if (numbRetrys <= 2)
                            Console.WriteLine("Picture taken");
                        else
                            Console.WriteLine("Picture not taken");
                        Console.WriteLine("Time: " + stopW.ElapsedMilliseconds);
                        numbRetrys = 0;
                        retry = true;
                        while (retry)
                        {
                            try
                            {
                                frame = fsSink.SnapSingle(TimeSpan.FromSeconds(4));
                                Console.WriteLine("Took Picture 7.3 - Exposure 1.0, Time: " + stopW.ElapsedMilliseconds);
                                frame.SaveAsTiff("c:\\instrument\\testF28G2E1S3.tiff");
                                retry = false;
                            }
                            catch (Exception _)
                            {
                                numbRetrys++;
                                Console.WriteLine("Problem with SnapSingle, Try = " + numbRetrys);
                                if (numbRetrys > 2)
                                    retry = false;
                            }
                        }
                        if (numbRetrys <= 2)
                            Console.WriteLine("Picture taken");
                        else
                            Console.WriteLine("Picture not taken");
                        Console.WriteLine("Time: " + stopW.ElapsedMilliseconds);
                        numbRetrys = 0;
                        retry = true;
                        while (retry)
                        {
                            try
                            {
                                frame = fsSink.SnapSingle(TimeSpan.FromSeconds(4));
                                Console.WriteLine("Took Picture 7.4 - Exposure 1.0, Time: " + stopW.ElapsedMilliseconds);
                                frame.SaveAsTiff("c:\\instrument\\testF28G2E1S4.tiff");
                                retry = false;
                            }
                            catch (Exception _)
                            {
                                numbRetrys++;
                                Console.WriteLine("Problem with SnapSingle, Try = " + numbRetrys);
                                if (numbRetrys > 2)
                                    retry = false;
                            }
                        }
                        if (numbRetrys <= 2)
                            Console.WriteLine("Picture taken");
                        else
                            Console.WriteLine("Picture not taken");
                        Console.WriteLine("After Last Picture");
                        control.LiveStop();
                    }
                    else
                    {
                        Console.WriteLine("No Serial Number");
                    }
                    Console.ReadLine();

                }
            }
        }
        // The examples for Get/Set from the XDocument assume the item being accessed is 
        //"<item guid=\"{284C0E0F-010B-45BF-8291-09D90A459B28}\" name=\"Gain\">" +
        //    "<element guid=\"{B57D3001-0AC6-4819-A609-272A33140ACA}\" name=\"Auto\">" +
        //    "<itf guid=\"{99B44940-BFE1-4083-ADA1-BE703F4B8E04}\" value=\"0\" />" +
        //    "</element>" +
        //    "<element guid=\"{B57D3000-0AC6-4819-A609-272A33140ACA}\" name=\"Value\">" +
        //    "<itf guid=\"{99B44940-BFE1-4083-ADA1-BE703F4B8E08}\" value=\"3.010000\" />" +
        //    "</element>" +
        //    "</item>"
        // This gets the value of one item from XDoc
        //   So GetOne(XDoc, "Gain", "Value", out string Result) will set Result to "3.010000"
        static bool GetOne(XDocument XDoc, string Item, string Element, out string Value)
        {
            //var x0 = XDoc
            //    .Elements("vcdpropertyitems");
            Value = "";
            //if (x0.Count() == 1)
            if ( GetVCDProp(XDoc, out XElement x0))
            {
                var x1 = x0
                    .Elements("item")
                    .Where(e => e.Attribute("name").Value == Item);
                if (x1.Count() == 1)
                {
                    var x2 = x1
                    .Elements("element")
                    .Where(a => a.Attribute("name").Value == Element);
                    if (x2.Count() == 1)
                    {
                        var x3 = x2
                        .Elements("itf");
                        if (x3.Count() == 1)
                            Value = x3.Single().Attribute("value").Value;
                        else
                            return false;
                    }
                    else
                        return false;

                }
                else
                    return false;
            }
            else
                return false;

            return true;
        }
        // This sets the value of one item from XDoc
        //   So SettOne(XDoc, "Gain", "Value", "2.00") will set the Gain Value to "2.00"
        static bool SetOne(XDocument XDoc, string Item, string Element, string Value)
        {
            //var x0 = XDoc
            //    .Elements("vcdpropertyitems");
            //if (x0.Count() == 1)
            if (GetVCDProp(XDoc, out XElement x0))
            {
                var x1 = x0
                    .Elements("item")
                    .Where(e => e.Attribute("name").Value == Item);
                if (x1.Count() == 1)
                {
                    var x2 = x1
                    .Elements("element")
                    .Where(a => a.Attribute("name").Value == Element);
                    if (x2.Count() == 1)
                    {
                        var x3 = x2
                        .Elements("itf");
                        if (x3.Count() == 1)
                            x3.Single().Attribute("value").Value = Value;
                        else
                            return false;
                    }
                    else
                        return false;

                }
                else
                    return false;
            }
            else
                return false;

            return true;
        }
        // This gets two values of one item from XDoc
        //   So GetTwo(XDoc, "Gain", "Value", "Auto", out string Result1, out string Result2) will set Result1 to "3.010000" and Result2 to "0"
        static bool GetTwo(XDocument XDoc, string Item, string Element1, string Element2, out string Value1, out string Value2)
        {
            Value1 = "";
            Value2 = "";
            //var x0 = XDoc
            //    .Elements("vcdpropertyitems");
            //if (x0.Count() == 1)
            if (GetVCDProp(XDoc, out XElement x0))
            {
                var x1 = x0
                    .Elements("item")
                    .Where(e => e.Attribute("name").Value == Item);
                if (x1.Count() == 1)
                {
                    var x2 = x1
                    .Elements("element")
                    .Where(a => a.Attribute("name").Value == Element1);
                    if (x2.Count() == 1)
                    {
                        var x3 = x2
                        .Elements("itf");
                        if (x3.Count() == 1)
                            Value1 = x3.Single().Attribute("value").Value;
                        else
                            return false;
                    }
                    else
                        return false;

                    var x4 = x1
                        .Elements("element")
                        .Where(a => a.Attribute("name").Value == Element2);
                    if ( x4.Count() == 1)
                    {
                        var x5 = x4
                            .Elements("itf");
                        if (x5.Count() == 1)
                            Value2 = x5.Single().Attribute("value").Value;
                        else
                            return false;
                    }
                    else
                        return false;

                }
                else
                    return false;
            }
            else
                return false;

            return true;
        }

        //static bool SetTwo(XDocument XDoc, string Item, string Element1, string Element2, string Value1, string Value2)
        //{
        //    //var x0 = XDoc
        //    //    .Elements("vcdpropertyitems");
        //    //if (x0.Count() == 1)
        //    if (GetVCDProp(XDoc, out XElement x0))
        //    {
        //        var x1 = x0
        //            .Elements("item")
        //            .Where(e => e.Attribute("name").Value == Item);
        //        if (x1.Count() == 1)
        //        {
        //            var x2 = x1
        //            .Elements("element")
        //            .Where(a => a.Attribute("name").Value == Element1);
        //            if (x2.Count() == 1)
        //            {
        //                var x3 = x2
        //                .Elements("itf");
        //                if (x3.Count() == 1)
        //                    x3.Single().Attribute("value").Value = Value1;
        //                else
        //                    return false;
        //            }
        //            else
        //                return false;

        //            var x4 = x1
        //                .Elements("element")
        //                .Where(a => a.Attribute("name").Value == Element2);
        //            if (x4.Count() == 1)
        //            {
        //                var x5 = x4
        //                    .Elements("itf");
        //                if (x5.Count() == 1)
        //                    x5.Single().Attribute("value").Value = Value2;
        //                else
        //                    return false;
        //            }
        //            else
        //                return false;

        //        }
        //        else
        //            return false;
        //    }
        //    else
        //        return false;

        //    return true;
        //}

        // The structure of XDoc is
        //"<device_state libver=\"3.5\" filemajor=\"1\" fileminor=\"0\">" +
        //    "<device name=\"" + DevName + "\" base_name=\"" + DevName + "\" unique_name=\"" + DevName + " " + SN + "\">" +
        //    "<videoformat>Y16 (3872x2764)</videoformat>" +
        //    "<fps>1.453000</fps>" +
        //    "<vcdpropertyitems>"
        //         [All the VCD Prooperties]
        //    "</vcdpropertyitems>" +
        //    "</device>" +
        //    "</device_state>"
        // This returns an XElement containing all the VCD Properties
        static bool GetVCDProp(XDocument XDoc, out XElement XE)
        {
            try
            {
                var xd = XDoc
                    .Elements("device_state")
                    .Single()
                    .Elements("device")
                    .Single()
                    .Elements("vcdpropertyitems")
                    .Single();
                XE = (XElement)xd;
                return true;
            }
            catch (Exception _)
            {
                XE = new XElement("None");
                return false;
            }
        }
        static string[] DeviceState(XDocument XDoc)
        {
            List<string> state = new List<string>(0);
            if (GetOne(XDoc, "Brightness", "Value", out string bV))
                state.Add("Brightness - Value: " + bV);
            else
                state.Add("Brightness - Value: Not found");

            if (GetOne(XDoc, "Contrast", "Value", out string cV))
                state.Add("Contrast - Value: " + cV);
            else
                state.Add("Contrast - Value: Not found");

            if (GetOne(XDoc, "Sharpness", "Value", out string sV))
                state.Add("Sharpness - Value: " + cV);
            else
                state.Add("Sharpness - Value: Not found");

            if (GetOne(XDoc, "Gamma", "Value", out string gV))
                state.Add("Gamma - Value: " + gV);
            else
                state.Add("Gamma - Value: Not found");

            if (GetTwo(XDoc, "Gain", "Value", "Auto", out string gV1, out string gV2))
            {
                state.Add("Gain - Value: " + gV1);
                state.Add("Gain - Auto: " + gV2);
            }
            else
            {
                state.Add("Gain - Value: Not found");
                state.Add("Gain - Auto: Not found");
            }

            if (GetTwo(XDoc, "Exposure", "Value", "Auto Reference", out string eV1, out string eV2))
            {
                state.Add("Exposure - Value: " + eV1);
                state.Add("Exposure - Auto Reference: " + eV2);
            }
            else
            {
                state.Add("Exposure - Value: Not found");
                state.Add("Exposure - Auto Reference: Not found");
            }

            if (GetTwo(XDoc, "Trigger", "Enable", "Polarity", out string tV1, out string tV2))
            {
                state.Add("Trigger - Enable: " + tV1);
                state.Add("Trigger - Polarity: " + tV2);
            }
            else
            {
                state.Add("Trigger - Enable: Not found");
                state.Add("Trigger - Polarity: Not found");
            }
            if (GetTwo(XDoc, "Trigger", "Delay", "Operation", out string tV3, out string tV4))
            {
                state.Add("Trigger - Delay: " + tV3);
                state.Add("Trigger - Operation: " + tV4);
            }
            else
            {
                state.Add("Trigger - Delay: Not found");
                state.Add("Trigger - Operation: Not found");
            }


            if (GetOne(XDoc, "Denoise", "Value", out string dV))
                state.Add("Denoise - Value: " + dV);
            else
                state.Add("Denoise - Value: Not found");

            if (GetOne(XDoc, "GPIO", "GP Out", out string gpV))
                state.Add("GPIO - Value: " + gpV);
            else
                state.Add("GPIO - Value: Not found");

            if (GetOne(XDoc, "Binning factor", "Value", out string bfV))
                state.Add("Binning factor - Value: " + bfV);
            else
                state.Add("Binning factor - Value: Not found");

            if (GetOne(XDoc, "Highlight reduction", "Enable", out string hrV))
                state.Add("Highlight reduction - Enable: " + hrV);
            else
                state.Add("Highlight reduction - Enable: Not found");

            if (GetOne(XDoc, "Tone Mapping", "Enable", out string tmV))
                state.Add("Tone Mapping - Enable: " + tmV);
            else
                state.Add("Tone Mapping - Enable: Not found");

            if (GetOne(XDoc, "Partial scan", "Auto-center", out string acV))
                state.Add("Partial scan - Auto-center: " + acV);
            else
                state.Add("Partial scan - Auto-center: Not found");

            if (GetOne(XDoc, "Strobe", "Enable", out string stV))
                state.Add("Strobe - Enable: " + stV);
            else
                state.Add("Strobe - Enable: Not found");

            return state.ToArray();
        }

        static string DevState(string DevName, string SN)
        {
            return "<device_state libver=\"3.5\" filemajor=\"1\" fileminor=\"0\">" +
            "<device name=\"" + DevName + "\" base_name=\"" + DevName + "\" unique_name=\"" + DevName + " " + SN + "\">" +
            "<videoformat>Y16 (3872x2764)</videoformat>" +
            "<fps>1.453000</fps>" +
            "<vcdpropertyitems>" +
            "<item guid=\"{284C0E06-010B-45BF-8291-09D90A459B28}\" name=\"Brightness\">" +
            "<element guid=\"{B57D3000-0AC6-4819-A609-272A33140ACA}\" name=\"Value\">" +
            "<itf guid=\"{99B44940-BFE1-4083-ADA1-BE703F4B8E03}\" value=\"168\" />" +
            "</element>" +
            "</item>" +
            "<item guid=\"{284C0E07-010B-45BF-8291-09D90A459B28}\" name=\"Contrast\">" +
            "<element guid=\"{B57D3000-0AC6-4819-A609-272A33140ACA}\" name=\"Value\">" +
            "<itf guid=\"{99B44940-BFE1-4083-ADA1-BE703F4B8E03}\" value=\"0\" />" +
            "</element>" +
            "</item>" +
            "<item guid=\"{284C0E0A-010B-45BF-8291-09D90A459B28}\" name=\"Sharpness\">" +
            "<element guid=\"{B57D3000-0AC6-4819-A609-272A33140ACA}\" name=\"Value\">" +
            "<itf guid=\"{99B44940-BFE1-4083-ADA1-BE703F4B8E03}\" value=\"0\" />" +
            "</element>" +
            "</item>" +
            "<item guid=\"{284C0E0B-010B-45BF-8291-09D90A459B28}\" name=\"Gamma\">" +
            "<element guid=\"{B57D3000-0AC6-4819-A609-272A33140ACA}\" name=\"Value\">" +
            "<itf guid=\"{99B44940-BFE1-4083-ADA1-BE703F4B8E03}\" value=\"100\" />" +
            "</element>" +
            "</item>" +
            "<item guid=\"{284C0E0F-010B-45BF-8291-09D90A459B28}\" name=\"Gain\">" +
            "<element guid=\"{B57D3001-0AC6-4819-A609-272A33140ACA}\" name=\"Auto\">" +
            "<itf guid=\"{99B44940-BFE1-4083-ADA1-BE703F4B8E04}\" value=\"0\" />" +
            "</element>" +
            "<element guid=\"{B57D3000-0AC6-4819-A609-272A33140ACA}\" name=\"Value\">" +
            "<itf guid=\"{99B44940-BFE1-4083-ADA1-BE703F4B8E08}\" value=\"3.010000\" />" +
            "</element>" +
            "</item>" +
            "<item guid=\"{90D5702E-E43B-4366-AAEB-7A7A10B448B4}\" name=\"Exposure\">" +
            "<element guid=\"{B57D3001-0AC6-4819-A609-272A33140ACA}\" name=\"Auto\">" +
            "<itf guid=\"{99B44940-BFE1-4083-ADA1-BE703F4B8E04}\" value=\"0\" />" +
            "</element>" +
            "<element guid=\"{B57D3000-0AC6-4819-A609-272A33140ACA}\" name=\"Value\">" +
            "<itf guid=\"{99B44940-BFE1-4083-ADA1-BE703F4B8E08}\" value=\"1.000000\" />" +
            "</element>" +
            "<element guid=\"{65190390-1AD8-4E91-9021-66D64090CC85}\" name=\"Auto\">" +
            "<itf guid=\"{99B44940-BFE1-4083-ADA1-BE703F4B8E04}\" value=\"0\" />" +
            "</element>" +
            "<element guid=\"{6519038F-1AD8-4E91-9021-66D64090CC85}\" name=\"Auto Max Value\">" +
            "<itf guid=\"{99B44940-BFE1-4083-ADA1-BE703F4B8E08}\" value=\"1.000000\" />" +
            "</element>" +
            "<element guid=\"{6519038C-1AD8-4E91-9021-66D64090CC85}\" name=\"Auto Reference\">" +
            "<itf guid=\"{99B44940-BFE1-4083-ADA1-BE703F4B8E03}\" value=\"128\" />" +
            "</element>" +
            "</item>" +
            "<item guid=\"{90D57031-E43B-4366-AAEB-7A7A10B448B4}\" name=\"Trigger\">" +
            "<element guid=\"{B57D3000-0AC6-4819-A609-272A33140ACA}\" name=\"Enable\">" +
            "<itf guid=\"{99B44940-BFE1-4083-ADA1-BE703F4B8E04}\" value=\"0\" />" +
            "</element>" +
            "<element guid=\"{6519038D-1AD8-4E91-9021-66D64090CC85}\" name=\"Polarity\">" +
            "<itf guid=\"{99B44940-BFE1-4083-ADA1-BE703F4B8E04}\" value=\"0\" />" +
            "</element>" +
            "<element guid=\"{C337CFB8-EA08-4E69-A655-586937B6AFEC}\" name=\"Delay\">" +
            "<itf guid=\"{99B44940-BFE1-4083-ADA1-BE703F4B8E08}\" value=\"15.000000\" />" +
            "</element>" +
            "<element guid=\"{859A76B9-E289-472B-AB05-C9B3F26DCAA0}\" name=\"Operation\">" +
            "<itf guid=\"{99B44940-BFE1-4083-ADA1-BE703F4B8E06}\" value=\"0\" />" +
            "</element>" +
            "</item>" +
            "<item guid=\"{C3C9944A-E6F6-4E25-A0BE-53C066AB65D8}\" name=\"Denoise\">" +
            "<element guid=\"{B57D3000-0AC6-4819-A609-272A33140ACA}\" name=\"Value\">" +
            "<itf guid=\"{99B44940-BFE1-4083-ADA1-BE703F4B8E03}\" value=\"0\" />" +
            "</element>" +
            "</item>" +
            "<item guid=\"{86D89D69-9880-4618-9BF6-DED5E8383449}\" name=\"GPIO\">" +
            "<element guid=\"{7D006621-761D-4B88-9C5F-8B906857A501}\" name=\"GP Out\">" +
            "<itf guid=\"{99B44940-BFE1-4083-ADA1-BE703F4B8E03}\" value=\"0\" />" +
            "</element>" +
            "</item>" +
            "<item guid=\"{4F95A06D-9C15-407B-96AB-CF3FED047BA4}\" name=\"Binning factor\">" +
            "<element guid=\"{B57D3000-0AC6-4819-A609-272A33140ACA}\" name=\"Value\">" +
            "<itf guid=\"{99B44940-BFE1-4083-ADA1-BE703F4B8E06}\" value=\"1\" />" +
            "</element>" +
            "</item>" +
            "<item guid=\"{546541AD-C815-4D82-AFA9-9D59AF9F399E}\" name=\"Highlight reduction\">" +
            "<element guid=\"{B57D3000-0AC6-4819-A609-272A33140ACA}\" name=\"Enable\">" +
            "<itf guid=\"{99B44940-BFE1-4083-ADA1-BE703F4B8E04}\" value=\"0\" />" +
            "</element>" +
            "</item>" +
            "<item guid=\"{3D505AC4-1A28-428B-83E5-85AA8EB441C1}\" name=\"Tone Mapping\">" +
            "<element guid=\"{B57D3000-0AC6-4819-A609-272A33140ACA}\" name=\"Enable\">" +
            "<itf guid=\"{99B44940-BFE1-4083-ADA1-BE703F4B8E04}\" value=\"0\" />" +
            "</element>" +
            "</item>" +
            "<item guid=\"{2CED6FD6-AB4D-4C74-904C-D682E53B9CC5}\" name=\"Partial scan\">" +
            "<element guid=\"{36EAA683-3321-44BE-9D73-E1FD4C3FDB87}\" name=\"Auto-center\">" +
            "<itf guid=\"{99B44940-BFE1-4083-ADA1-BE703F4B8E04}\" value=\"1\" />" +
            "</element>" +
            "</item>" +
            "<item guid=\"{DC320EDE-DF2E-4A90-B926-71417C71C57C}\" name=\"Strobe\">" +
            "<element guid=\"{B57D3000-0AC6-4819-A609-272A33140ACA}\" name=\"Enable\">" +
            "<itf guid=\"{99B44940-BFE1-4083-ADA1-BE703F4B8E04}\" value=\"0\" />" +
            "</element>" +
            "</item>" +
            "</vcdpropertyitems>" +
            "</device>" +
            "</device_state>";
        }

    }
}
