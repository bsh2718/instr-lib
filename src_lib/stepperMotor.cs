using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
//using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Timers;
using System.IO;
using System.IO.Ports;

namespace sdpcrInstrLib
{
    using BoardComm;
    using Hardware;
    using OneResponse;
    // public delegate void EnqueueResponse(ResponseItem Resp);
    // public delegate void EnqueueInfo(string str);
    namespace Stepper
    {
        static class SIdx
        {
            public const int Initialize = 0;
            public const int Direction = 1;
            public const int HighImped = 2;
            public const int Backout = 3;
            public const int Profile = 4;
            public const int Velocity = 5;
            public const int Terminate = 6;
            public const int Home = 7;
            public const int CheckFlag = 8;
            public const int VModePos = 9;
            public const int VModeNeg = 10;
            public const int Absolute = 11;
            public const int QueryBusy = 12;
            public const int ReadRegister = 13;
            // Count must be set to the number of items above
            public const int Count = 14;
        }
        public enum LastCommand
        {
            Home,
            Positive,
            Negative,
            None,
            Other
        }
        
        public class MotorProfile
        {
            public bool Used;
            public int MinimumVelocity;
            public int MaximumVelocity;

            public MotorProfile()
            {
                Used = false;
                MinimumVelocity = 0;
                MaximumVelocity = 0;
            }
            public override string ToString()
            {
                return String.Format("Used: {0} / Min Velocity: {1} / Max Velocity: {2}", 
                    Used.ToString(), MinimumVelocity, MaximumVelocity);
            }
        }
        public class StepperMotor : HardwareBase
        {
            //int boardWaitTime;
            readonly int _motorNumber;
            readonly bool _direction;
            protected System.Timers.Timer responseTimer;
            protected System.Timers.Timer homeTimer;
            string[] CmdList;
            SingleResponse startup;
            int _motorWait = 200;
            private readonly object fieldLock = new object();
            private readonly object homeLock = new object();
            private readonly object busyLock = new object();
            private readonly object registerLock = new object();
            bool _initialized;
            int _profile;
            int _velocity;
            bool _homed;
            bool _homedUpdated;
            bool _busy;
            bool _busyUpdated;
            int _backoutSteps;
            string _register;
            bool _registerUpdated;
            List<MotorProfile> _motorProfiles;
            private readonly object lastCommandLock = new object();
            LastCommand _lastCommand;

            List<string> _errorMsg;

            public bool Initialized
            {
                get
                {
                    lock (fieldLock) { return _initialized; }
                }
            }
            public int Profile
            {
                get
                {
                    lock (fieldLock) { return _profile; }
                }
            }
            public int Velocity
            {
                get
                {
                    lock (fieldLock) { return _velocity; }
                }
            }
            public int MaxPosition { get; }
            public bool Homed
            {
                get
                {
                    bool res = false;
                    if (_connected)
                    {
                        CheckHomeFlag();
                        for (int n = 0; n < 10; n++)
                        {
                            System.Threading.Thread.Sleep(BoardWaitTime);
                            lock (homeLock)
                            {
                                if (_homedUpdated)
                                {
                                    res = _homed;
                                    break;
                                }
                            }
                        }
                    }
                    return res;
                }
            }
            public int MotorNumber
            {
                get
                {
                    return _motorNumber;
                }
            }
            public bool Busy
            {
                get
                {
                    bool res = false;
                    if (_connected)
                    {  
                        CheckIfBusy();
                        for (int n = 0; n < 10; n++)
                        {
                            System.Threading.Thread.Sleep(BoardWaitTime);
                            lock (busyLock)
                            {
                                if (_busyUpdated)
                                {
                                    res = _busy;
                                    break;
                                }
                            }
                        }   
                    }
                    return res;
                }
            }
            public bool Wait_N_Or_Until_NotBusy(int N=10)
            {
                for (int n = 0; n <= N; n++)
                {
                    //Console.WriteLine(String.Format("Wait10OrUntilNotBusy - {0}", n));
                    if (!Busy)
                        return true;
                    else
                        System.Threading.Thread.Sleep(_motorWait);
                }
                return false;
            }
            public string Register
            {
                get
                {
                    string res = "Timed Out";
                    if (_connected)
                    {
                        ReadRegister();
                        for (int n = 0; n < 10; n++)
                        {
                            System.Threading.Thread.Sleep(BoardWaitTime);
                            lock (registerLock)
                            {
                                if (_registerUpdated)
                                {
                                    res = _register;
                                    break;
                                }
                            }
                        }
                    }
                    return res;
                }
            }
            public bool MinMaxVelocity(int Profile, out int MinVelocity, out int MaxVelocity)
            {
                if (_initialized)
                {
                    if (Profile >= 0 && Profile < _motorProfiles.Count && _motorProfiles[Profile].Used)
                    {
                        MinVelocity = _motorProfiles[Profile].MinimumVelocity;
                        MaxVelocity = _motorProfiles[Profile].MaximumVelocity;
                        return true;
                    }
                    else
                    {
                        MinVelocity = 0;
                        MaxVelocity = 0;
                        return false;
                    }
                }
                else
                {
                    MinVelocity = 0;
                    MaxVelocity = 0;
                    return false;
                }
            }
            public bool ProfileUsed(int Profile)
            {
                if (_initialized)
                {
                    if (Profile >= 0 && Profile < _motorProfiles.Count && _motorProfiles[Profile].Used)
                        return true;
                    else
                        return false;
                }
                else
                    return false;
            }
            public List<string> ErrorMessage
            {
                get { return _errorMsg; }
            }

            public StepperMotor(ControlBoard Board, int MotorNumber, int DirectionCode, List<MotorProfile> MotorProfilesIn,
                int InitProfile, int InitVelocity, int Backout, int MaximumPosition, out bool IsConnected) : base()
            {   
                _errorMsg = new List<string>();
                IsConnected = false;
                if (MotorNumber > 0 && MotorNumber < 5
                    && MotorProfilesIn.Count > 1 && InitProfile < MotorProfilesIn.Count  
                    && (DirectionCode == 0 || DirectionCode == 1)
                    && Backout > 10 && Board != null )
                {
                    if (MotorProfilesIn[InitProfile].Used)
                    {
                        if (InitVelocity >= MotorProfilesIn[InitProfile].MinimumVelocity
                            && InitVelocity <= MotorProfilesIn[InitProfile].MaximumVelocity)
                        {
                            _motorProfiles = MotorProfilesIn;
                            IsConnected = true;
                        }
                    }
                }
                if ( IsConnected )
                {
                    _motorNumber = MotorNumber;
                    if (DirectionCode == 0)
                        _direction = true;
                    else
                        _direction = false;
                    sdPCRBoard = Board;
                    MaxPosition = MaximumPosition;
                    CmdList = new string[SIdx.Count];
                    CmdList[SIdx.Initialize] = String.Format("/{0}IR", _motorNumber);           //  0 - Initialize
                    if (_direction)
                        CmdList[SIdx.Direction] = String.Format("/{0}f0R", _motorNumber);   //  1 - Set Direction
                    else
                        CmdList[SIdx.Direction] = String.Format("/{0}f1R", _motorNumber);
                    CmdList[SIdx.HighImped] = String.Format("/{0}hR", _motorNumber);        //  2 - High Impedance
                    CmdList[SIdx.Backout] = String.Format("/{0}B", _motorNumber);           //  3 - Back out steps (stub, requires number of steps to back out and trailing R)
                    CmdList[SIdx.Profile] = String.Format("/{0}V", _motorNumber);           //  4 - Profile (stub, requires profile number and trailing R)
                    CmdList[SIdx.Velocity] = String.Format("/{0}v", _motorNumber);          //  5 - Velocity (stub, requires velocity and trailing R)	
                    CmdList[SIdx.Terminate] = String.Format("/{0}TR", _motorNumber);        //  6 - Terminate
                    CmdList[SIdx.Home] = String.Format("/{0}ZR", _motorNumber);             //  7 - Home	
                    CmdList[SIdx.CheckFlag] = String.Format("/6{0}ER", _motorNumber);       //  8 - Check flag
                    CmdList[SIdx.VModePos] = String.Format("/{0}P0R", _motorNumber);         //  9 - Velocity mode, positive direction
                    CmdList[SIdx.VModeNeg] = String.Format("/{0}D0R", _motorNumber);         // 10 - Velocity mode, negative direction
                    CmdList[SIdx.Absolute] = String.Format("/{0}A", _motorNumber);          // 11 - Absolute Move (stub, requires number position and trailing R)
                    CmdList[SIdx.QueryBusy] = String.Format("/{0}QR", _motorNumber);        // 12 - Query, is motor busy?
                    CmdList[SIdx.ReadRegister] = String.Format("/{0}rR", _motorNumber);     // 13 - Read Motor Register
                    _lastCommand = LastCommand.None;
                    IsConnected = true;
                }
                else
                {
                    IsConnected = false;
                    _errorMsg.Add(String.Format("Motor Number: {0}", MotorNumber));
                    _errorMsg.Add(String.Format("Direction Code: {0}", DirectionCode));
                    if ( MotorProfilesIn.Count > 0 )
                    {
                        _errorMsg.Add("Motor Profiles");
                        for ( int n = 0; n < MotorProfilesIn.Count; n++)
                        {
                            _errorMsg.Add(String.Format("     {0}: {1}", n, MotorProfilesIn[n].ToString()));
                        }
                    }
                    else
                    {
                        _errorMsg.Add("No Motor Profiles");
                    }
                    _errorMsg.Add(String.Format("Init Profile: {0}", InitProfile));
                    _errorMsg.Add(String.Format("Init Velocity: {0}", InitVelocity));
                    _errorMsg.Add(String.Format("Backout: {0}", Backout));
                    if (Board != null)
                        _errorMsg.Add(String.Format("Board != null"));
                    else
                        _errorMsg.Add(String.Format("Board == null"));

                }
                if (IsConnected)
                {
                    startup = new SingleResponse(sdPCRBoard);
                    BoardWaitTime = sdPCRBoard.BoardWaitTime;
                    //Console.WriteLine(String.Format("Board Wait Time = {0}", boardWaitTime));
                    if (TestConnection(Backout, InitProfile, InitVelocity, ref _errorMsg))
                    {
                        responseTimer = new System.Timers.Timer(TimeStep);
                        // hook up the elapsed event for the timer. 
                        responseTimer.AutoReset = false;
                        responseTimer.Enabled = false;
                        responseTimer.Elapsed += CheckBoardResponseList;
                    }
                    else
                        IsConnected = false;
                }
                else
                {
                    if (Board != null)
                        InternalLog.Enqueue(String.Format("Time: {0} - Stepper motor controller did not connect", Board.BoardTime()));
                    else
                        InternalLog.Enqueue("Board == null");
                }
                _connected = IsConnected;
            }
            bool useStartup(int K, int Wait, string Cmd, ref List<string> ErrMsg, out string StartTime)
            {
                ResponseItem resp;
                startup.AddCommand(Cmd);
                ErrMsg.Add(String.Format("Command: {0}", Cmd));
                StartTime = "";
                bool cont = false;
                for (int n = 0; n < 8; n++)
                {
                    if (startup.GetResponse(Wait, out resp))
                    {
                        if (resp.Response.IndexOf(DefaultResponses[0]) >= 0)
                        {
                            cont = true;
                            StartTime = String.Format("{0}:{1}", K, n);
                            break;
                        }
                        else
                        {
                            ErrMsg.Add(String.Format("{0}:{1}, Cmd: {2}, Resp: {3}", K, n, resp.Command, resp.Response));
                        }
                    }
                    else
                        ErrMsg.Add(String.Format("{0}:{1} - No response", K, n));
                }
                return cont;
            }
            bool TestConnection(int Backout, int InitProfile, int InitVelocity, ref List<string> ErrMsg)
            {
                List<string> _startupTimes = new List<string>();
                int wait = 2 * BoardWaitTime;
                ErrMsg = new List<string>();
                string startTime;
                bool cont = true;
                while (cont)
                {
                    cont = useStartup(0, wait, CmdList[SIdx.Initialize], ref ErrMsg, out startTime);
                    if (cont)
                        _startupTimes.Add(startTime);
                    else
                        break;

                    cont = useStartup(1, wait, CmdList[SIdx.Direction], ref ErrMsg, out startTime);
                    if (cont)
                        _startupTimes.Add(startTime);
                    else
                        break;

                    cont = useStartup(2, wait, CmdList[SIdx.HighImped], ref ErrMsg, out startTime);
                    if (cont)
                        _startupTimes.Add(startTime);
                    else
                        break;

                    string cm = String.Format("{0}{1}R", CmdList[SIdx.Backout], Backout);
                    cont = useStartup(3, wait, cm, ref ErrMsg, out startTime);
                    if (cont)
                        _startupTimes.Add(startTime);
                    else
                        break;

                    cm = String.Format("{0}{1}R", CmdList[SIdx.Profile], InitProfile);
                    cont = useStartup(4, wait, cm, ref ErrMsg, out startTime);
                    if (cont)
                        _startupTimes.Add(startTime);
                    else
                        break;

                    cm = String.Format("{0}{1}R", CmdList[SIdx.Velocity], InitVelocity);
                    cont = useStartup(5, wait, cm, ref ErrMsg, out startTime);
                    if (cont)
                        _startupTimes.Add(startTime);

                    break;
                }
                if (!cont)
                {
                    ErrMsg.Add("     Start up Times");
                    for (int n = 0; n < _startupTimes.Count; n++)
                    {
                        ErrMsg.Add(_startupTimes[n]);
                    }
                    return false;
                }

                ErrMsg = new List<string>();
                lock (fieldLock)
                {
                    _initialized = true;
                    _velocity = InitVelocity;
                    _profile = InitProfile;
                    _backoutSteps = Backout;
                    double mTmp = 30000.0 / (double)_velocity;
                    if (mTmp < 200.0)
                        _motorWait = 200;
                    else
                        _motorWait = (int)mTmp;

                }
                ErrMsg.Add("     Start up Times");
                for (int n = 0; n < _startupTimes.Count; n++)
                {
                    ErrMsg.Add(_startupTimes[n]);
                }
                return true;
            }

            public bool Terminate()
            {
                if (_connected)
                {
                    sdPCRBoard.AddCommand(CmdList[SIdx.Terminate], Notification.OnError, LogResponses);
                    CmdsSent.Enqueue(CmdList[SIdx.Terminate]);
                    lock (lastCommandLock) { _lastCommand = LastCommand.None; }
                    return true;
                }
                return false;
            }
            public bool HighImpedance()
            {
                if (_connected)
                {
                    if (Wait_N_Or_Until_NotBusy())
                    {
                        sdPCRBoard.AddCommand(CmdList[SIdx.HighImped], Notification.OnError, LogResponses);
                        CmdsSent.Enqueue(CmdList[SIdx.HighImped]);
                        lock (lastCommandLock) { _lastCommand = LastCommand.None; }
                        return true;
                    }
                    else
                        return false;
                }
                return false;
            }
            public bool SetProfile(int NewProfile)
            {
                if (_connected && NewProfile >= 0 && NewProfile <= _motorProfiles.Count && _motorProfiles[NewProfile].Used)
                {
                    if (NewProfile != Profile)
                    {
                        double spd;
                        bool term = true;
                        if (GetMotorSpeed(out spd))
                        {
                            if (spd < 1.0)
                                term = false;
                        }
                        if (term)
                        {
                            sdPCRBoard.AddCommand(CmdList[SIdx.Terminate], Notification.OnError, LogResponses);
                            CmdsSent.Enqueue(CmdList[SIdx.Terminate]);
                        }
                        if (Wait_N_Or_Until_NotBusy())
                        {
                            sdPCRBoard.AddCommand(CmdList[SIdx.HighImped], Notification.OnError, LogResponses);
                            CmdsSent.Enqueue(CmdList[SIdx.HighImped]);

                            string cmd = String.Format("{0}{1}R", CmdList[SIdx.Profile], NewProfile);
                            if (Wait_N_Or_Until_NotBusy())
                            {
                                sdPCRBoard.AddCommand(cmd, Notification.OnError, LogResponses);
                                CmdsSent.Enqueue(cmd);
                                lock (lastCommandLock) { _lastCommand = LastCommand.None; }
                                _profile = NewProfile;
                            }
                            else
                                return false;
                        }
                        else
                            return false;
                    }
                    return true;
                }
                return false;
            }
            public bool SetVelocity(int NewVelocity)
            {
                if (_connected && NewVelocity >= _motorProfiles[_profile].MinimumVelocity 
                    && NewVelocity <= _motorProfiles[_profile].MaximumVelocity)
                {
                    string cmd = String.Format("{0}{1}R", CmdList[SIdx.Velocity], NewVelocity);
                    if (Wait_N_Or_Until_NotBusy())
                    {
                        sdPCRBoard.AddCommand(cmd, Notification.OnError, LogResponses);
                        CmdsSent.Enqueue(cmd);
                        //lock (lastCommandLock) { _lastCommand = LastCommand.None; }
                        return true;
                    }
                    else
                        return false;
                }
                return false;
            }

            public bool Home()
            {
                if (_connected)
                {
                    Wait_N_Or_Until_NotBusy();
                    sdPCRBoard.AddCommand(CmdList[SIdx.Home], Notification.OnError, LogResponses);
                    CmdsSent.Enqueue(CmdList[SIdx.Home]);
                    lock (lastCommandLock) { _lastCommand = LastCommand.Home; }
                    return true;
                }
                return false;

            }

            bool CheckHomeFlag()
            {
                if (_connected)
                {
                    lock (homeLock) { _homedUpdated = false; }
                    Wait_N_Or_Until_NotBusy();
                    sdPCRBoard.AddCommand(CmdList[SIdx.CheckFlag], Notification.OnError, LogResponses);
                    CmdsSent.Enqueue(CmdList[SIdx.CheckFlag]);
                    return true;
                }
                return false;
            }

            public bool VelocityModePositive()
            {
                if (_connected)
                {
                    bool last = false;
                    lock (lastCommandLock)
                    {
                        last = (_lastCommand == LastCommand.Negative || _lastCommand == LastCommand.Other || _lastCommand == LastCommand.Home);
                    }
                    if (last)
                    {
                        if (Wait_N_Or_Until_NotBusy())
                        {
                            sdPCRBoard.AddCommand(CmdList[SIdx.Terminate], Notification.OnError, LogResponses);
                            CmdsSent.Enqueue(CmdList[SIdx.Terminate]);
                        }
                        else
                        {
                            sdPCRBoard.AddCommand(CmdList[SIdx.Terminate], Notification.OnError, LogResponses);
                            CmdsSent.Enqueue(CmdList[SIdx.Terminate]);
                            return false;
                        }
                    }
                    if (Wait_N_Or_Until_NotBusy())
                    {
                        sdPCRBoard.AddCommand(CmdList[SIdx.VModePos], Notification.OnError, LogResponses);
                        CmdsSent.Enqueue(CmdList[SIdx.VModePos]);
                        lock (lastCommandLock) { _lastCommand = LastCommand.Positive; }
                        return true;
                    }
                    else
                        return false;
                }
                return false;
            }

            public bool VelocityModeNegative()
            {
                if (_connected)
                {
                    bool last = false;
                    lock (lastCommandLock)
                    {
                        last = (_lastCommand == LastCommand.Positive || _lastCommand == LastCommand.Other);
                    }
                    if (last)
                    {
                        if (Wait_N_Or_Until_NotBusy())
                        {
                            sdPCRBoard.AddCommand(CmdList[SIdx.Terminate], Notification.OnError, LogResponses);
                            CmdsSent.Enqueue(CmdList[SIdx.Terminate]);
                        }
                        else
                        {
                            sdPCRBoard.AddCommand(CmdList[SIdx.Terminate], Notification.OnError, LogResponses);
                            CmdsSent.Enqueue(CmdList[SIdx.Terminate]);
                            return false;
                        }
                    }
                    if (Wait_N_Or_Until_NotBusy())
                    {
                        sdPCRBoard.AddCommand(CmdList[SIdx.VModeNeg], Notification.OnError, LogResponses);
                        CmdsSent.Enqueue(CmdList[SIdx.VModeNeg]);
                        lock (lastCommandLock) { _lastCommand = LastCommand.Negative; }
                        return true;
                    }
                    else
                        return false;
                }
                return false;
            }
            public bool AbsolutePosition(int NewPosition)
            {
                if (_connected && NewPosition >= 0 && NewPosition <= MaxPosition)
                {
                    if (Wait_N_Or_Until_NotBusy())
                    {
                        string cmd = String.Format("{0}{1}R", CmdList[SIdx.Absolute], NewPosition);
                        sdPCRBoard.AddCommand(cmd, Notification.OnError, LogResponses);
                        CmdsSent.Enqueue(cmd);
                        //Console.WriteLine(cmd);
                        lock (lastCommandLock) { _lastCommand = LastCommand.Other; }
                        //System.Threading.Thread.Sleep(BoardWaitTime * 2);
                        return true;
                    }
                    else
                        return false;
                }
                return false;
            }
            bool CheckIfBusy()
            {
                if (_connected)
                {
                    lock (busyLock) { _busyUpdated = false; }
                    sdPCRBoard.AddCommand(CmdList[SIdx.QueryBusy], Notification.OnError, LogResponses);
                    CmdsSent.Enqueue(CmdList[SIdx.QueryBusy]);
                    return true;
                }
                return false;
            }
            bool ReadRegister()
            {
                if (_connected)
                {
                    lock (registerLock) { _registerUpdated = false; }
                    Wait_N_Or_Until_NotBusy();
                    sdPCRBoard.AddCommand(CmdList[SIdx.ReadRegister], Notification.OnError, LogResponses);
                    CmdsSent.Enqueue(CmdList[SIdx.ReadRegister]);
                    return true;
                }
                return false;
            }
            public bool GetMotorSpeed(out double Speed)
            {
                string regString = Register;
                if (regString.IndexOf("Timed ") < 0)
                {
                    if (ParseCurrentSpeed(regString, out Speed))
                        return true;
                    else
                    {
                        Speed = -1.0;
                        return false;
                    }
                }
                else
                {
                    Speed = -1.0;
                    return false;
                }
            }

            public bool GetMotorDirection(out int Direction)
            {
                string regString = Register;
                if (regString.IndexOf("Timed ") < 0)
                {
                    if (ParseCurrentDirection(regString, out Direction))
                        return true;
                    else
                    {
                        Direction = -1;
                        return false;
                    }
                }
                else
                {
                    Direction = -1;
                    return false;
                }
            }

            public bool GetMotorMotion(out int Motion)
            {
                string regString = Register;
                if (regString.IndexOf("Timed ") < 0)
                {
                    if (ParseCurrentMotion(regString, out Motion))
                        return true;
                    else
                    {
                        Motion = -1;
                        return false;
                    }
                }
                else
                {
                    Motion = -1;
                    return false;
                }
            }

            public bool GetMotorPosition(out int Position)
            {
                string regString = Register;
                if (regString.IndexOf("Timed ") < 0)
                {
                    if (ParseCurrentPosition(regString, out Position))
                        return true;
                    else
                    {
                        Position = -1;
                        return false;
                    }
                }
                else
                {
                    Position = -1;
                    return false;
                }
            }
            public bool GetMotorState(out string State)
            {
                string regString = Register;
                if (regString.IndexOf("Timed ") < 0)
                {
                    if (ParseMotorState(regString, out State))
                        return true;
                    else
                    {
                        State = "";
                        return false;
                    }
                }
                else
                {
                    State = "";
                    return false;
                }
            }
            bool ParseCurrentSpeed(string RegisterString, out double Speed)
            {
                int idx = RegisterString.IndexOf("SPEED:");
                int idx1 = RegisterString.IndexOf("_SPEED");
                Speed = -1;
                if (idx >= 0 && idx != idx1)
                {
                    string sTmp = RegisterString.Substring(idx + 5);
                    int startChar = 0;
                    while (sTmp[startChar] != ' ' && startChar < sTmp.Length - 7)
                    {
                        startChar++;
                    }
                    if (sTmp[startChar] == ' ')
                    {
                        int endChar = startChar + 1;
                        while (sTmp[endChar] == ' ' && endChar < sTmp.Length - 3)
                            endChar++;
                        while (sTmp[endChar] != ' ' && startChar < sTmp.Length - 2)
                            endChar++;

                        if (sTmp[endChar] == ' ')
                        {
                            int len = endChar - startChar + 1;
                            string spd = sTmp.Substring(startChar, len);
                            if (Double.TryParse(spd, out Speed))
                                return true;
                            else
                                return false;
                        }
                        else
                            return false;
                    }
                    else
                        return false;
                }
                else
                    return false;
            }

            bool ParseCurrentDirection(string RegisterString, out int Direction)
            {
                Direction = -1;
                char[] _trim_hex = new char[] { ' ', '0', 'x' };
                int idx = RegisterString.IndexOf("STATUS");
                if (idx >= 0)
                {
                    string sTmp = RegisterString.Substring(idx + 8);
                    int startChar = 0;
                    while (sTmp[startChar] != ':' && startChar < sTmp.Length - 6)
                    {
                        startChar++;
                    }
                    while (sTmp[startChar] != ' ' && startChar < sTmp.Length - 6)
                    {
                        startChar++;
                    }
                    if (sTmp[startChar] == ' ')
                    {
                        int endChar = startChar + 1;
                        while (sTmp[endChar] == ' ' && endChar < sTmp.Length - 3)
                            endChar++;
                        while (sTmp[endChar] != ' ' && endChar < sTmp.Length - 2)
                            endChar++;

                        if (sTmp[endChar] == ' ')
                        {
                            int len = endChar - startChar + 1;
                            string spd = sTmp.Substring(startChar, len);
                            int hexN;
                            if (Int32.TryParse(spd.TrimStart(_trim_hex), System.Globalization.NumberStyles.HexNumber, null, out hexN))
                            {
                                int mask = 0x10;
                                int d = mask & hexN;
                                if (d != 0)
                                {
                                    Direction = 1;
                                    return true;
                                }
                                else
                                {
                                    Direction = 0;
                                    return true;
                                }
                            }
                            else
                                return false;
                        }
                        else
                            return false;
                    }
                    else
                        return false;
                }
                else
                    return false;
            }
            bool ParseCurrentMotion(string RegisterString, out int Motion)
            {
                Motion = 0;
                char[] _trim_hex = new char[] { ' ', '0', 'x' };
                int idx = RegisterString.IndexOf("STATUS");
                if (idx >= 0)
                {
                    string sTmp = RegisterString.Substring(idx + 8);
                    int startChar = 0;
                    while (sTmp[startChar] != ':' && startChar < sTmp.Length - 6)
                    {
                        startChar++;
                    }
                    while (sTmp[startChar] != ' ' && startChar < sTmp.Length - 6)
                    {
                        startChar++;
                    }
                    if (sTmp[startChar] == ' ')
                    {
                        int endChar = startChar + 1;
                        while (sTmp[endChar] == ' ' && endChar < sTmp.Length - 3)
                            endChar++;
                        while (sTmp[endChar] != ' ' && endChar < sTmp.Length - 2)
                            endChar++;

                        if (sTmp[endChar] == ' ')
                        {
                            int len = endChar - startChar + 1;
                            string spd = sTmp.Substring(startChar, len);
                            int hexN;
                            if (Int32.TryParse(spd.TrimStart(_trim_hex), System.Globalization.NumberStyles.HexNumber, null, out hexN))
                            {
                                int mask = 0x60;
                                int d = mask & hexN;
                                if (d != 0)
                                {
                                    Motion = 1;
                                    return true;
                                }
                                else
                                {
                                    Motion = 0;
                                    return true;
                                }
                            }
                            else
                                return false;
                        }
                        else
                            return false;
                    }
                    else
                        return false;
                }
                else
                    return false;
            }
            bool ParseCurrentPosition(string RegisterString, out int Position)
            {
                int idx = RegisterString.IndexOf("ABS_POS:");
                Position = -1;
                if (idx >= 0)
                {
                    string sTmp = RegisterString.Substring(idx + 7);
                    int startChar = 0;
                    while (sTmp[startChar] != ' ' && startChar < sTmp.Length - 7)
                    {
                        startChar++;
                    }
                    if (sTmp[startChar] == ' ')
                    {
                        int endChar = startChar + 1;
                        while (sTmp[endChar] == ' ' && endChar < sTmp.Length - 3)
                            endChar++;
                        while (sTmp[endChar] != ' ' && startChar < sTmp.Length - 2)
                            endChar++;

                        if (sTmp[endChar] == ' ')
                        {
                            int len = endChar - startChar + 1;
                            string spd = sTmp.Substring(startChar, len);
                            if (Int32.TryParse(spd, out Position))
                                return true;
                            else
                                return false;
                        }
                        else
                            return false;
                    }
                    else
                        return false;
                }
                else
                    return false;
            }

            bool ParseMotorState(string RegisterString, out string State)
            {
                int position;
                double speed;
                int direction;
                int motion;
                string dir;
                string mot;
                if (ParseCurrentSpeed(RegisterString, out speed)
                    && ParseCurrentPosition(RegisterString, out position)
                    && ParseCurrentMotion(RegisterString, out motion)
                    && ParseCurrentDirection(RegisterString, out direction))
                {
                    if (motion > 0)
                        mot = "Moving";
                    else
                        mot = "Stopped";
                    if (direction > 0)
                        dir = "Positive";
                    else
                        dir = "Negative";
                    State = String.Format("{0}_AbsPosition:{1}, {0}_Speed:{2}, {0}_Motion:{3}, {0}_Direction:{4}",
                        _motorNumber, position, speed, mot, dir);
                    return true;
                }
                else
                {
                    State = "";
                    return false;
                }
            }


            public bool Oscillate(int Pos1, int Pos2)
            {
                if (_connected)
                {
                    if (Pos1 >= 0 && Pos2 >= 0 && Pos1 != Pos2)
                    {
                        string str0 = String.Format("/1A{0}A{1}G0R", Pos1, Pos2);
                        Wait_N_Or_Until_NotBusy();
                        sdPCRBoard.AddCommand(str0, Notification.OnError, LogResponses);
                        CmdsSent.Enqueue(str0);
                        return true;
                    }
                    else
                        InternalLog.Enqueue(String.Format("Time: {0} - Position values must be >= 0 and unequal to each other",
                            sdPCRBoard.BoardTime()));
                    return false;
                }
                return false;
            }
            public override string ToString()
            {
                StringBuilder ret = new StringBuilder();
                if (_direction)
                    ret.AppendFormat("Motor: {0}, Direction: 0\n", _motorNumber);
                else
                    ret.AppendFormat("Motor: {0}, Direction: 1\n", _motorNumber);
                if (_initialized)
                    ret.AppendFormat("Initialized: True\nCurrent Profile: {0}\n", _profile);
                else
                    ret.AppendFormat("Initialized: False\n");

                for ( int n = 0; n < _motorProfiles.Count; n++)
                {
                    if ( _motorProfiles[n].Used)
                        ret.AppendFormat("Motor Profile {0}: {1}", n, _motorProfiles[n].ToString());
                }
                
                ret.AppendFormat("Current Velocity: {0}", _velocity);
                if (_homed)
                    ret.Append("Homed\n");
                else
                    ret.Append("Not Homed\n");
                if (_busy)
                    ret.Append("Motor is busy\n");
                else
                    ret.Append("Motor is not busy\n");

                return base.ToString() + ret.ToString();
            }
            protected override void LogResponses(ResponseItem Resp)
            {
                //Console.WriteLine(String.Format("LogResponses: {0} - {1}", Resp.Command, Resp.Response));
                ResponsesFromBoard.Enqueue(Resp.Clone());
                responseTimer.AutoReset = true;
                responseTimer.Enabled = true;
            }
            void CheckBoardResponseList(object sender, ElapsedEventArgs e)
            {
                ResponseItem resp;
                //InternalLog.Enqueue(String.Format("Motor {0}: CheckBoardResponse Count = {1}", _motorNumber, ResponsesFromBoard.Count));
                while (ResponsesFromBoard.TryDequeue(out resp))
                {
                    //InternalLog.Enqueue(String.Format("In Stepper Motor {0}, resp.Response.Length = {1}", _motorNumber, resp.Response.Length));
                    //InternalLog.Enqueue(String.Format("                      resp.Response.Length = {0}", resp.Response.Length));
                    //Console.WriteLine(String.Format("resp {0}\n        CmdsSent.Count {1}", resp, CmdsSent.Count));
                    if (resp.Response.Length < 3)
                    {
                        InternalLog.Enqueue(String.Format("Time: {0}", sdPCRBoard.BoardTime()));
                        InternalLog.Enqueue(String.Format("In Stepper Motor {0}, short response\n{1}", _motorNumber, resp.ToString()));
                        InternalLog.Enqueue(String.Format("Message |{0}|, length = {1}", resp.Message, resp.Message.Length));
                        InternalLog.Enqueue(String.Format("Command |{0}|, length = {1}", resp.Command, resp.Command.Length));
                        InternalLog.Enqueue(String.Format("Response |{0}|, length = {1}", resp.Response, resp.Response.Length));
                    }
                    else if (resp.InstrumentSignal)
                    {
                        ResponsesToCaller.Enqueue(resp.Clone());
                    }
                    else if (CmdsSent.Count < 1)
                    {
                        resp.InstrumentSignal = true;
                        ResponsesToCaller.Enqueue(resp.Clone());
                    }
                    else
                    {
                        string cmd;
                        if (CmdsSent.TryDequeue(out cmd))
                        {
                            int iCmd = cmd.IndexOf(resp.Command);
                            if (iCmd < 0)
                                iCmd = resp.Command.IndexOf(cmd);
                            if (iCmd >= 0)
                            {
                                int cmdIdx = -1;
                                for (int n = 0; n < CmdList.Length; n++)
                                {
                                    if (cmd.IndexOf(CmdList[n]) == 0)
                                    {
                                        cmdIdx = n;
                                        break;
                                    }
                                }
                                int resIdx = -1;
                                for (int n = 0; n < DefaultResponses.Count; n++)
                                {
                                    if (resp.Response.IndexOf(DefaultResponses[n]) >= 0)
                                    {
                                        resIdx = n;
                                        break;
                                    }
                                }
                                bool error;
                                if (resIdx == 0)
                                    error = false;
                                else
                                    error = true;
                                switch (cmdIdx)
                                {
                                    case SIdx.Initialize:
                                        break;
                                    case SIdx.Direction:
                                        break;
                                    case SIdx.HighImped:
                                        break;
                                    case SIdx.Backout:
                                        break;
                                    case SIdx.Profile:
                                        if (resIdx == 0)
                                        {
                                            int itmp = cmd.IndexOf(CmdList[SIdx.Profile]);
                                            if (itmp < cmd.Length - 2)
                                            {
                                                int res;
                                                string stmp = cmd.Substring(itmp + 3, 1);
                                                Int32.TryParse(stmp, out res);
                                                if (res >= 0 && res < _motorProfiles.Count && _motorProfiles[res].Used)
                                                {
                                                    lock (fieldLock)
                                                    {
                                                        _profile = res;
                                                    }
                                                }
                                                else
                                                    error = true;
                                            }
                                        }
                                        break;
                                    case SIdx.Velocity:
                                        if (resIdx == 0)
                                        {
                                            int itmp = cmd.IndexOf(CmdList[SIdx.Velocity]);
                                            if (itmp < cmd.Length - 2)
                                            {
                                                int res;
                                                string stmp = cmd.Substring(itmp + 3, 1);
                                                Int32.TryParse(stmp, out res);
                                                if (res >= _motorProfiles[_profile].MinimumVelocity && res <= _motorProfiles[_profile].MaximumVelocity)
                                                {
                                                    lock (fieldLock)
                                                    {
                                                        _velocity = res;
                                                    }
                                                }
                                                else
                                                    error = true;
                                            }
                                        }
                                        break;
                                    case SIdx.Terminate:
                                        break;
                                    case SIdx.Home:
                                        break;
                                    case SIdx.CheckFlag:
                                        if (resIdx == -1)
                                        {
                                            lock (homeLock)
                                            {
                                                if (resp.Response.IndexOf("/00") >= 0)
                                                {
                                                    _homed = false;
                                                    error = false;
                                                    _homedUpdated = true;
                                                }
                                                else if (resp.Response.IndexOf("/01") >= 0)
                                                {
                                                    _homed = true;
                                                    error = false;
                                                    _homedUpdated = true;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            error = true;
                                        }
                                        break;
                                    case SIdx.VModePos:
                                        break;
                                    case SIdx.VModeNeg:
                                        break;
                                    case SIdx.Absolute:
                                        break;
                                    case SIdx.QueryBusy:
                                        if (resIdx == -1)
                                        {
                                            lock (busyLock)
                                            {
                                                if (resp.Response.IndexOf("/00") >= 0)
                                                {
                                                    _busy = false;
                                                    error = false;
                                                    _busyUpdated = true;
                                                }
                                                else if (resp.Response.IndexOf("/01") >= 0)
                                                {
                                                    _busy = true;
                                                    error = false;
                                                    _busyUpdated = true;
                                                }
                                            }
                                        }
                                        else
                                            error = true;
                                        break;
                                    case SIdx.ReadRegister:
                                        lock (registerLock)
                                        {
                                            _register = resp.Response;
                                            _registerUpdated = true;
                                        }
                                        break;
                                    case -1:

                                        break;
                                    default:

                                        break;
                                }
                                if (error)
                                {
                                    InternalLog.Enqueue(String.Format("{0}: In Stepper Motor {1}, error == true, resp - {2}, Response = |{3}|",
                                        sdPCRBoard.BoardTime(), _motorNumber, resp.ToString(), resp.Response));
                                }
                                else
                                {
                                    //InternalLog.Enqueue(String.Format("In Stepper Motor {0}, error == false, resp - {1}, Response = |{2}|",
                                    //    _motorNumber, resp.ToString(), resp.Response));
                                }

                                if (resp.Notify == Notification.Always)
                                    ResponsesToCaller.Enqueue(resp.Clone());
                                else if (error && resp.Notify == Notification.OnError)
                                    ResponsesToCaller.Enqueue(resp.Clone());
                            }
                        }
                    }
                }
                if (ResponsesFromBoard.Count <= 0)
                {
                    responseTimer.AutoReset = false;
                    responseTimer.Enabled = false;
                }
            }


        }

    } // namespaceStepper
}
