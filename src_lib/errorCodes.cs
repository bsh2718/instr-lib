﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace sdpcrInstrLib
{
    namespace ErrorCodes
    {
        enum ErrorCode : int
        {
            /* General / Parse Errors */
            DEV_ERR_PARSER_ERROR = 1,
            DEV_ERR_PARSER_BAD_CMD = 2,

            /* Interlock Errors */
            DEV_ERR_INTERLOCK_PARSE_ERROR = 10,

            /* Fan Errors */
            DEV_ERR_FAN_PARSE_ERROR = 20,
            DEV_ERR_FAN_PID_IS_ON = 21,
            DEV_ERR_FAN_MANUAL_DISABLED = 22,

            /* Shell Errors */
            DEV_ERR_SHELL_PARSE_ERROR = 40,
            DEV_ERR_SHELL_UNKNOWN_CMD = 41,
            DEV_ERR_SHELL_TOO_MANY_ARGS = 42,

            /* PID Related Errors */
            DEV_ERR_PID_TEMP_SPREAD = 30,
            DEV_ERR_PID_TEMP_BOUNDS = 31,

            /* Motor Errors */
            DEV_ERR_MOTOR_OPERAND_OUT_OF_RANGE = 50,
            DEV_ERR_MOTOR_BAD_COMMAND = 51,
            DEV_ERR_MOTOR_LIN_VELOCITY_DIR = 52,
            DEV_ERR_MOTOR_LIN_VELOCITY_SET = 53,
            DEV_ERR_MOTOR_LIN_HOME_CHECK = 54,
            DEV_ERR_MOTOR_LIN_VELOCITY_PROFILE = 55
        }
        public class ProcessErrorCode
        {
            int _numberOfMotors = 4;
            public int NumberOfMotors
            {
                get
                {
                    return _numberOfMotors;
                }
            }
            public ProcessErrorCode(int numb)
            {
                _numberOfMotors = numb;
            }
            public string ToString(string Code)
            {
                if (Code.Length < 2)
                    return "";
                string description;
                int idx = Code.IndexOf("0E");
                if (idx < 0)
                    return "Not an Error Message";
                if (Code.Length <= idx + 2)
                    return "Not an Error Message";

                string sCode = Code.Substring(idx + 2); 
                int iCode;
                if (Int32.TryParse(sCode, out iCode))
                {
                    switch (iCode)
                    {
                        case (int)ErrorCode.DEV_ERR_PARSER_ERROR:
                            description = "DEV_ERR_PARSER_ERROR";
                            break;
                        case (int)ErrorCode.DEV_ERR_PARSER_BAD_CMD:
                            description = "DEV_ERR_PARSER_BAD_CMD";
                            break;

                        /* Interlock Errors */
                        case (int)ErrorCode.DEV_ERR_INTERLOCK_PARSE_ERROR:
                            description = "DEV_ERR_INTERLOCK_PARSE_ERROR";
                            break;

                        /* Fan Errors */
                        case (int)ErrorCode.DEV_ERR_FAN_PARSE_ERROR:
                            description = "DEV_ERR_FAN_PARSE_ERROR";
                            break;
                        case (int)ErrorCode.DEV_ERR_FAN_PID_IS_ON:
                            description = "DEV_ERR_FAN_PID_IS_ON";
                            break;
                        case (int)ErrorCode.DEV_ERR_FAN_MANUAL_DISABLED:
                            description = "DEV_ERR_FAN_MANUAL_DISABLED";
                            break;

                        /* Shell Errors */
                        case (int)ErrorCode.DEV_ERR_SHELL_PARSE_ERROR:
                            description = "DEV_ERR_SHELL_PARSE_ERROR";
                            break;
                        case (int)ErrorCode.DEV_ERR_SHELL_UNKNOWN_CMD:
                            description = "DEV_ERR_SHELL_UNKNOWN_CMD";
                            break;
                        case (int)ErrorCode.DEV_ERR_SHELL_TOO_MANY_ARGS:
                            description = "DEV_ERR_SHELL_TOO_MANY_ARGS";
                            break;

                        /* PID Related Errors */
                        case (int)ErrorCode.DEV_ERR_PID_TEMP_SPREAD:
                            description = "DEV_ERR_PID_TEMP_SPREAD";
                            break;
                        case (int)ErrorCode.DEV_ERR_PID_TEMP_BOUNDS:
                            description = "DEV_ERR_PID_TEMP_BOUNDS";
                            break;
                        default:
                            description = "Invalid Error Code";
                            break;
                    }
                }
                else if (sCode.IndexOf('M') == 2 && sCode.Length > 3 )
                {
                    int mCode;
                    if (Int32.TryParse(sCode.Substring(0, 2), out iCode)
                        && Int32.TryParse(sCode.Substring(3, 1), out mCode))
                    {
                        if (iCode >= 50 && iCode <= 59)
                        {
                            if (mCode < 1 || mCode > NumberOfMotors)
                                description = String.Format("Error Code {0} refers to nonexistent motor", sCode);
                            else
                            {
                                switch (iCode)
                                {
                                    /* Motor Errors */
                                    case (int)ErrorCode.DEV_ERR_MOTOR_OPERAND_OUT_OF_RANGE:
                                        description = String.Format("DEV_ERR_MOTOR_OPERAND_OUT_OF_RANGE : Motor {0}", mCode);
                                        break;
                                    case (int)ErrorCode.DEV_ERR_MOTOR_BAD_COMMAND:
                                        description = String.Format("DEV_ERR_MOTOR_BAD_COMMAND : Motor {0}", mCode);
                                        break;
                                    case (int)ErrorCode.DEV_ERR_MOTOR_LIN_VELOCITY_DIR:
                                        description = String.Format("DEV_ERR_MOTOR_LIN_VELOCITY_DIR : Motor {0}", mCode);
                                        break;
                                    case (int)ErrorCode.DEV_ERR_MOTOR_LIN_VELOCITY_SET:
                                        description = String.Format("DEV_ERR_MOTOR_LIN_VELOCITY_SET : Motor {0}", mCode);
                                        break;
                                    case (int)ErrorCode.DEV_ERR_MOTOR_LIN_HOME_CHECK:
                                        description = String.Format("DEV_ERR_MOTOR_LIN_HOME_CHECK : Motor {0}", mCode);
                                        break;
                                    case (int)ErrorCode.DEV_ERR_MOTOR_LIN_VELOCITY_PROFILE:
                                        description = "DEV_ERR_MOTOR_LIN_VELOCITY_PROFILE";
                                        break;
                                    default:
                                        description = "Invalid Error Code";
                                        break;
                                }
                            }
                        }
                        else
                            description = "Invalid Error Code";
                    }
                    else
                        description = "Invalid Error Code";
                }
                else
                {
                    description = "Invalid Error Code";
                }
                return description;
            }
            public bool IsResponse(string Code)
            {
                bool res = true;
                int idx = Code.IndexOf("/0E");
                if ( idx >= 0 )
                {
                    string sCode = Code.Substring(idx + 2);
                    if ( Int32.TryParse(sCode, out int iCode))
                    {
                        switch(iCode)
                        {
                            case (int)ErrorCode.DEV_ERR_PARSER_ERROR:
                            case (int)ErrorCode.DEV_ERR_PARSER_BAD_CMD:
                            case (int)ErrorCode.DEV_ERR_SHELL_PARSE_ERROR:
                            case (int)ErrorCode.DEV_ERR_SHELL_UNKNOWN_CMD:
                            case (int)ErrorCode.DEV_ERR_SHELL_TOO_MANY_ARGS:
                                break;
                            default:
                                res = true;
                                break;
                        }
                    }
                }
                return res;
            }
        }
    }
}
