﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace sdpcrInstrLib
{
    using BoardComm;
    using Hardware;

    using PIDController;
    using Therm;
    using Stepper;
    using System.Security.Permissions;
    using System.IO;
    public struct TempProgUnit
    {
        public double Time;
        public double Temperature;
        public string Description;

        public override string ToString()
        {
            return String.Format("Temperature: {0} C, Time: {1} sec, Description: {2}", Temperature, Time, Description);
        }
    }
    public struct VelocityProgUnit
    {
        public double Time;
        public int Velocity;
        public string Description;

        public override string ToString()
        {
            return String.Format("Velocity: {0} rpm, Time: {1} sec, Description: {2}", Velocity, Time, Description);
        }
    }
    namespace Sequencers
    {
        public abstract class SequencerBase
        {
            protected static List<string> Errors = new List<string> { "/0B", "/0C", "/0E", "/0S" };
            protected static string MotorBusy = "/0S";
            protected ConcurrentQueue<ResponseItem> ErrorResponses;
            protected static char[] quotes = { '"', '\'', ' ' };
            protected readonly object outputLock = new object();
            protected List<string> output = new List<string>();
            protected ConcurrentQueue<string> InternalLog;
            protected EnqueueInfo LogInfo;
            protected bool timer1On = false;
            protected System.Timers.Timer timer1;
            protected int timeStep;

            protected GetBoardTime BoardTime;

            protected readonly object statusLock = new object();
            bool _scriptRunning;
            bool _scriptLoaded;
            bool _connected;
            public SequencerBase()
            {
                _scriptLoaded = false;
                _scriptRunning = false;
                _connected = false;
                ErrorResponses = new ConcurrentQueue<ResponseItem>();
                InternalLog = new ConcurrentQueue<string>();
            }

            protected virtual void LogErrors(ResponseItem Resp)
            {
                ErrorResponses.Enqueue(Resp.Clone());
            }

            public bool CheckErrorList(out ResponseItem Resp)
            {
                bool res = ErrorResponses.TryDequeue(out ResponseItem resp);
                if (res)
                    Resp = resp.Clone();
                else
                    Resp = new ResponseItem();
                return res;
            }

            public bool ProgramLoaded
            {
                get
                {
                    lock (statusLock)
                    {
                        return _scriptLoaded;
                    }
                }
                protected set
                {
                    lock (statusLock)
                    {
                        _scriptLoaded = value;
                    }
                }
            }

            public bool ProgramRunning
            {
                get
                {
                    lock (statusLock)
                    {
                        return _scriptRunning;
                    }
                }
                protected set
                {
                    lock (statusLock)
                    {
                        _scriptRunning = value;
                    }
                }
            }

            public bool Connected 
            {
                get
                {
                    lock (statusLock)
                    {
                        return _connected;
                    }
                }
                protected set
                {
                    lock (statusLock)
                    {
                        _connected = value;
                    }
                }
            }

            public bool GetOutput(out string[] Lines)
            {
                lock (outputLock)
                {
                    if (output.Count > 0)
                    {
                        Lines = new string[output.Count];
                        output.CopyTo(Lines, 0);
                        return true;
                    }
                    else
                    {
                        Lines = new string[0];
                        return false;
                    }
                }
            }
            public int GetInternalLog(out string[] Log)
            {
                if (InternalLog.Count == 0)
                {
                    Log = new string[0];
                    return 0;
                }
                else
                {
                    Log = InternalLog.ToArray();
                    return Log.Length;
                }
            }
        }
        public class TempMonitoring : SequencerBase
        {
            static Thermistors therm;
            //static PIDCtl tempCtl;
            ResponseItem resp = new ResponseItem();
            //public TempMonitoring(Thermistors Therm24, PIDCtl TemperatureCtl, out bool IsConnected )
            public TempMonitoring(Thermistors Therm24, out bool IsConnected) : base()
            {
                ProgramLoaded = false;
                ProgramRunning = false;
                timeStep = 2000;
                //if ( !Therm24.Connected || !TemperatureCtl.Connected)
                if (!Therm24.Connected)
                {
                    IsConnected = false;
                    Connected = IsConnected;
                }
                else
                {
                    therm = Therm24;
                    //tempCtl = TemperatureCtl;
                    IsConnected = true;
                    Connected = IsConnected;

                }
            }
            public bool StartMonitoring(int TimeInterval)
            {
                if (Connected && !ProgramRunning && TimeInterval > 299)
                {
                    timeStep = TimeInterval;
                    timer1On = true;
                    while (ErrorResponses.TryDequeue(out resp)) { }

                    DateTime date = DateTime.Now;
                    string biases = String.Format(",{0}", therm.ThermistorBiases);
                    StartTimer1(timeStep);
                    lock (outputLock)
                    {
                        output.Clear();
                        output.Add(String.Format("Day: {0:d} Time: {1:g}", date.Date, date.TimeOfDay));
                        output.Add("Time,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23");
                        output.Add(biases);
                    }
                    ProgramLoaded = true;
                    ProgramRunning = true;
                    return true;
                }
                else
                    return false;
            }
            private void StartTimer1(int TimeStep)
            {
                if (timer1 != null)
                {
                    timer1.Stop();
                    timer1.Dispose();
                }
                // Create a timer with a two second interval.
                timer1 = new System.Timers.Timer(TimeStep);
                // Hook up the Elapsed event for the timer. 
                timer1.Elapsed += OnTimer1Event;
                timer1.AutoReset = true;
                timer1.Enabled = true;
            }
            private void OnTimer1Event(Object source, ElapsedEventArgs e)
            {
                string str24 = therm.CalibratedTemperatures;
                long tim = therm.MeasurementTime;
                lock (outputLock)
                {
                    output.Add(string.Format("{0:0.00},{1}", (double)tim * 0.001, str24));
                }
                System.Threading.Thread.Sleep(therm.BoardWaitTime);
                while (therm.CheckResponseList(out resp))
                {
                    for (int n = 0; n < Errors.Count; n++)
                    {
                        if (resp.Response.IndexOf(Errors[n]) >= 0)
                            LogErrors(resp.Clone());
                    }
                }
            }
            public void StopMonitoring()
            {
                if (timer1 != null)
                {
                    timer1.Stop();
                    timer1.Dispose();
                    timer1On = false;
                }
                ProgramRunning = false;
            }
        }

        
        public class PIDSequencer : SequencerBase
        {
            PIDCtl ctl = null;
            static System.Timers.Timer tempProgTimer;
            static bool _tempTimerOn = false;

            static int nextStep = -1;
            static bool offWhenDone;
            static bool firstStep = false;

            private static readonly object fieldLock = new object();
            int step = 0;
            int _timeStep;

            ResponseItem resp = new ResponseItem();

            TempProgUnit newTempProgUnit;

            public string CurrentStep
            {
                get
                {
                    lock (fieldLock)
                    {
                        return TempProgram[step].Description;
                    }
                }
            }
            public string CurrentState
            {
                get
                {
                    double pTemp = ctl.ProcessTemperature;
                    if (ProgramRunning)
                    {
                        lock (fieldLock)
                        {
                            return String.Format("{0},{1:0.00},{2:0.00},{3:0.00},{4}",
                                step, ctl.CurrentTime * 0.001, ctl.TargetTemperature, pTemp, TempProgram[step].Description);
                        }
                    }
                    else if (ProgramLoaded)
                    {
                        return String.Format("-,{0:0.00},-,{1:0.00},Not Running", ctl.CurrentTime * 0.001, pTemp);
                    }
                    else
                    {
                        return String.Format("-,{0:0.00},-,{1:0.00},Program Not Loaded", ctl.CurrentTime * 0.001, pTemp);
                    }
                }
            }
            public List<TempProgUnit> TempProgram
            {
                get; private set;
            }
            public PIDSequencer(PIDCtl Controller, out bool IsConnected) : base()
            {
                ProgramLoaded = false;
                ProgramRunning = false;
                TempProgram = new List<TempProgUnit>(0);
                newTempProgUnit = new TempProgUnit();
                newTempProgUnit.Time = 1.0;
                newTempProgUnit.Temperature = 40.0;
                newTempProgUnit.Description = "None";
                TempProgram.Add(newTempProgUnit);
                if (!Controller.Connected)
                {
                    IsConnected = false;
                    Connected = IsConnected;
                    //Console.WriteLine(String.Format("TimeInterval: {0}, ProgramIn.Count: {1}, Controller.Connected: {2}",
                    //    TimeInterval, ProgramIn.Count, Controller.Connected.ToString()));
                }
                else
                {
                    ctl = Controller;
                    IsConnected = true;
                    Connected = IsConnected;
                }
            }
            
            public bool LoadProgram(List<string> ProgramIn, out string Err)
            {
                Err = "";
                if (ProgramRunning)
                {
                    Err = "Program is already running";
                    return false;
                }
                else if ( ProgramIn.Count < 2)
                {
                    Err = "Input Program must have at least 2 steps";
                    return false;
                }
                List<TempProgUnit> newCycle = new List<TempProgUnit>();
                newCycle.Clear();

                double w1;
                double w2;
                bool progOK = true;
                for (int n = 0; n < ProgramIn.Count; n++)
                {
                    string[] ws = ProgramIn[n].Split(',');
                    //Console.WriteLine(ProgramIn[n]);
#if DEBUG
                for (int mm = 0; mm < ws.Length; mm++)
                {
                    if (mm < ws.Length - 1)
                        InternalLog.Enqueue(String.Format("{0} , ", ws[mm]));
                    else
                        InternalLog.Enqueue(ws[mm]);
                }
#endif
                    if (ws.Length > 2)
                    {
                        if (Double.TryParse(ws[0], out w1))
                        {
                            if (w1 > 10.0 && w1 < 115.0 && Double.TryParse(ws[1], out w2))
                            {
                                if (w2 > 0.0)
                                {
                                    newTempProgUnit.Temperature = w1;
                                    newTempProgUnit.Time = w2;
                                    newTempProgUnit.Description = ws[2].TrimEnd(quotes).TrimStart(quotes);
                                    newCycle.Add(newTempProgUnit);
                                }
                                else
                                    progOK = false;
                            }
                            else
                                progOK = false;
                        }
                        else
                            progOK = false;
                    }
                    else
                        break;
                    if (!progOK)
                        break;
                }
                TempProgram.Clear();
                if (progOK)
                {
                    while (ErrorResponses.TryDequeue(out resp)) { }
                    for (int n = 0; n < newCycle.Count; n++)
                    {
                        TempProgram.Add(newCycle[n]);
#if DEBUG
                    InternalLog.Enqueue(String.Format("{0} / {1} / {2}",
                        TempProgram[n].Temperature, TempProgram[n].Time, TempProgram[n].Description));
#endif
                    }
                    progOK = true;
                }
                else
                {
                    newTempProgUnit = new TempProgUnit();
                    newTempProgUnit.Time = 1.0;
                    newTempProgUnit.Temperature = 40.0;
                    newTempProgUnit.Description = "None";
                    TempProgram.Add(newTempProgUnit);
                    Err = "There is a problem with the parametes of the input program";
                    progOK = false;
                }
                ProgramLoaded = progOK;
                return progOK;
            }
            public bool StartProgram(int InitStep, int TimerInterval, bool Off)
            {
                if (!Connected || !ProgramLoaded || ctl == null)
                {
                    ProgramRunning = false;
                    return false;
                }
                if (!ProgramRunning)
                {
                    _timeStep = TimerInterval;

                    ProgramRunning = true;
                    DateTime date = DateTime.Now;
                    StartTimer1(_timeStep);
                    StartTempProg(InitStep, Off);
                    lock (fieldLock) { step = InitStep; }
                    lock (outputLock)
                    {
                        output.Clear();
                        output.Add(String.Format("Day: {0:d} Time: {1:g}", date.Date, date.TimeOfDay));
                        output.Add("Step,Time,Target T,Process T,Description");
                    }
                    return true;
                }
                else
                    return false;
            }

            public void StopProgram()
            {
                
                StopTimer1();
                if (_tempTimerOn)
                {
                    StopTempProg();
                    double tim = ctl.CurrentTime * 0.001;
                    lock (outputLock)
                    {
                        output.Add(String.Format("-,{0:0.00},-,-,Done", tim));
                    }
                }
                ProgramRunning = false;
                if (offWhenDone)
                {
                    ctl.ManualControlOff();
                    ctl.PIDControllerOff();
                }

            }

            public void StopProgram(bool TurnOff)
            {

                StopTimer1();
                if (_tempTimerOn)
                {
                    StopTempProg();
                    double tim = ctl.CurrentTime * 0.001;
                    lock (outputLock)
                    {
                        output.Add(String.Format("-,{0:0.00},-,-,Done", tim));
                    }
                }

                ProgramRunning = false;
                if (TurnOff)
                {
                    ctl.ManualControlOff();
                    ctl.PIDControllerOff();
                }

            }
            private void StartTimer1(int TimeStep)
            {
                if (timer1 != null)
                {
                    timer1.Stop();
                    timer1.Dispose();
                }
                // Create a timer with TimeStep interval.
                timer1 = new System.Timers.Timer(TimeStep);
                // Hook up the Elapsed event for the timer. 
                timer1.Elapsed += OnTimer1Event;
                timer1.AutoReset = true;
                timer1.Enabled = true;
                timer1On = true;
            }
            private void OnTimer1Event(Object source, ElapsedEventArgs e)
            {
                lock (outputLock)
                {
                    output.Add(CurrentState);
                }
                //System.Threading.Thread.Sleep(ctl.BoardWaitTime);
                while (ctl.CheckResponseList(out resp))
                {
                    for (int n = 0; n < Errors.Count; n++)
                    {
                        if (resp.Response.IndexOf(Errors[n]) >= 0)
                            LogErrors(resp.Clone());
                    }
                }
                if (!ProgramRunning)
                {
                    StopTimer1();
                }
            }

            private void StopTimer1()
            {
                if (timer1 != null)
                {
                    timer1.Stop();
                    timer1.Dispose();
                }
                timer1On = false;
            }

            private bool StartTempProg(int InitStep, bool Off)
            {
                if (InitStep < TempProgram.Count)
                {
                    if (tempProgTimer != null)
                    {
                        tempProgTimer.Stop();
                        tempProgTimer.Dispose();
                    }
                    nextStep = InitStep;
                    offWhenDone = Off;
                    firstStep = true;
                    // Create a timer with a ten second interval.
                    tempProgTimer = new System.Timers.Timer(500);
                    // Hook up the Elapsed event for the timer. 
                    tempProgTimer.Elapsed += OnTempProgEvent;
                    tempProgTimer.AutoReset = false;
                    tempProgTimer.Enabled = true;
                    _tempTimerOn = true;
                    return true;
                }
                else
                {
#if DEBUG
                    InternalLog.Enqueue(String.Format("tempProgOn = {0}, InitStep = {1}, TempProgram.Count = {2}",
                        _tempTimerOn.ToString(), InitStep, TempProgram.Count));
#endif
                    return false;
                }
            }

            private void OnTempProgEvent(Object source, ElapsedEventArgs e)
            {
                if (tempProgTimer != null)
                {
                    string str;
                    double tim = ctl.CurrentTime * 0.001;
                    int newInterval;
                    if (nextStep < TempProgram.Count)
                    {
                        newInterval = (int)(TempProgram[nextStep].Time * 1000);
                        
                        str = CurrentState;
                        if (firstStep)
                        {
                            firstStep = false;
                            if (!ctl.PIDStatus)
                            {
                                ctl.PIDControllerOn();
                            }
                            ctl.ManualControlOn();    
                        }
                        ctl.SetManualTemperature(TempProgram[nextStep].Temperature);
                        lock (fieldLock) { step = nextStep; }
                        nextStep++;
                        tempProgTimer.Interval = newInterval; // restarts timer
                    }
                    else // Done with program and turn controller off
                    {
                        if (offWhenDone)
                        {
                            ctl.ManualControlOff();
                            ctl.PIDControllerOff();
                        }
                        else
                        {

                        }
                        str = String.Format("-,{0:0.00},-,-,Done", tim);
                        tempProgTimer.Stop();
                        tempProgTimer.Dispose();
                        _tempTimerOn = false;
                        ProgramRunning = false;
                        System.Threading.Thread.Sleep(5000);
                        StopTimer1();
                    }
                    lock (outputLock)
                    {
                        output.Add(str);
                    }
                }
            }

            private void StopTempProg()
            {
                if (tempProgTimer != null)
                {
                    tempProgTimer.Stop();
                    tempProgTimer.Dispose();
                }
                _tempTimerOn = false;
            }
        }

        public class PositionSequencer : SequencerBase
        {
            List<int> positions = new List<int>(0);
            int motorTimerCount;
            int _numbSteps;
            public int Modulous { get; protected set; }
            public StepperMotor Motor { get; protected set; }
            public int Profile { get; protected set; }
            public int Velocity { get; protected set; }
            ResponseItem resp = new ResponseItem();
            public PositionSequencer(StepperMotor Stepper, out bool IsConnected) : base()
            {
                ProgramRunning = false;
                ProgramLoaded = false;
                motorTimerCount = 0;
                _numbSteps = 0;
                Modulous = 2;
                if (Stepper.Connected )
                {
                    IsConnected = true;
                   
                        Motor = Stepper;
                        positions.Clear();
                        while (ErrorResponses.TryDequeue(out resp)) { }
                }
                else
                {
                    IsConnected = false;
                }
                Connected = IsConnected;
            }

            public bool LoadPositionProgram(List<int> PositionsIn, int ProfileIn, int VelocityIn, out string Err)
            {
                Err = "";
                int minV;
                int maxV;
                bool isLoaded = false;
                if (ProgramRunning)
                    Err = "Program is already running";
                else if (PositionsIn.Count < 2)
                    Err = "Input program must have at least 2 positions";
                if ( Motor.MinMaxVelocity(ProfileIn, out minV, out maxV))
                {
                    if (VelocityIn < minV || VelocityIn > maxV)
                        Err = "Velocity out of range";
                    else
                        isLoaded = true;
                }
                else
                {
                    Err = "Invalid Profile";
                }

                if (isLoaded)
                {
                    for (int n = 0; n < PositionsIn.Count; n++)
                    {
                        if (PositionsIn[n] < 0 || PositionsIn[n] > Motor.MaxPosition)
                        {
                            isLoaded = false;
                            Err = String.Format("Position {0} out of range", n);
                            break;
                        }
                    }
                }
                if (isLoaded)
                {
                    positions.Clear();
                    while (ErrorResponses.TryDequeue(out resp)) { }
                    for (int n = 0; n < PositionsIn.Count; n++)
                        positions.Add(PositionsIn[n]);

                    bool rehome = (ProfileIn != Motor.Profile);
                    double spd;
                    bool term = true;
                    if ( Motor.GetMotorSpeed(out spd) )
                    {
                        if (spd < 1.0)
                            term = false;
                    }
                    if ( term)
                        Motor.Terminate();
                    //Motor.HighImpedance();
                    Motor.SetProfile(ProfileIn);
                    Motor.SetVelocity(VelocityIn);
                    if (rehome)
                        Motor.Home();
                    Velocity = VelocityIn;
                    Profile = ProfileIn;
                    Modulous = positions.Count;
                    while (Motor.CheckResponseList(out resp))
                    {
                        for (int n = 0; n < Errors.Count; n++)
                        {
                            if (resp.Response.IndexOf(Errors[n]) >= 0)
                            {

                                isLoaded = false;
                                Err = String.Format("Error in response to motor setup commands, {0}\n     VelocityIn: {1}, ProfileIn: {2}",
                                    resp.ToString(), Velocity, Profile);
                                LogErrors(resp.Clone());
                            }
                        }
                    }
                }
                else
                {
                    isLoaded = false;
                }
                ProgramLoaded = isLoaded;
                return ProgramLoaded;
            }
            public bool GetPositionProgram(out List<string> ProgramOut)
            {
                ProgramOut = new List<string>();
                if (ProgramLoaded && positions.Count > 1)
                {
                    ProgramOut.Add(String.Format("Profile: {0}", Profile));
                    ProgramOut.Add(String.Format("Velocity: {0}", Velocity));
                    ProgramOut.Add(String.Format("Modulous: {0}", positions.Count));
                    ProgramOut.Add("=====");
                    for (int n = 0; n < positions.Count; n++)
                        ProgramOut.Add(positions[n].ToString());

                    return true;
                }
                else
                    return false;
            }
            

            public bool GoToPosition(int P)
            {
                bool res = false;
                if (ProgramLoaded && P >= 0 && P < positions.Count)
                {
                    if (Motor.AbsolutePosition(positions[P]))
                        res = true;

                    System.Threading.Thread.Sleep(Motor.BoardWaitTime);
                    while (Motor.CheckResponseList(out resp))
                    {
                        for (int n = 0; n < Errors.Count; n++)
                        {
                            if (resp.Response.IndexOf(Errors[n]) >= 0)
                            {
                                res = false;
                                LogErrors(resp.Clone());
                            }
                        }
                    }
                }
                return res;
            }

            public bool StartPositionProgram(int TimeStep, int NumbSteps)
            {
                if (timer1 != null)
                {
                    timer1.Stop();
                    timer1.Dispose();
                }
                timer1On = false;
                if (ProgramLoaded && TimeStep > 1999 && NumbSteps > 0)
                {
                    timeStep = TimeStep;
                    motorTimerCount = 0;
                    _numbSteps = NumbSteps;
                    timer1 = new System.Timers.Timer(200);
                    // Hook up the Elapsed event for the timer. 
                    timer1.Elapsed += OnPositionTimerEvent;
                    timer1.AutoReset = false;
                    timer1.Enabled = true;
                    timer1On = true;
                    ProgramRunning = true;
                }
                return timer1On;
            }

            private void OnPositionTimerEvent(Object source, ElapsedEventArgs e)
            {
                if (ProgramLoaded)
                {
                    if (motorTimerCount < _numbSteps)
                    {
                        int idx = (motorTimerCount % Modulous);
                        GoToPosition(idx);
                        if (motorTimerCount == 0)
                        {
                            timer1.Interval = timeStep;
                            timer1.AutoReset = true;
                        }
                        motorTimerCount++;
                    }
                    else
                    {
                        timer1.Stop();
                        timer1.Dispose();
                        timer1On = false;
                        //Console.WriteLine("motor program has finished");
                        ProgramRunning = false;
                    }
                }
            }

            public void StopPositionProgram()
            {
                if (timer1 != null)
                {
                    timer1.Stop();
                    timer1.Dispose();
                    timer1On = false;
                    //Console.WriteLine("motor program was aborted");
                }
                ProgramRunning = false;
            }
        }

        public class VelocitySequencer : SequencerBase
        {
            List<VelocityProgUnit> velocityProgram;
            VelocityProgUnit newVelocityProgUnit;
            int _numbSteps;
            int nextStep;
            bool reportStatus;
            public StepperMotor Motor { get; protected set; }
            public int Profile { get; protected set; }
            ResponseItem resp = new ResponseItem();
            public string CurrentState
            {
                get
                {
                    if (ProgramRunning && nextStep > 0)
                    {
                            return String.Format("{0},{1}",
                                nextStep-1, velocityProgram[nextStep-1].ToString());
                    }
                    else
                    {
                        return String.Format("-,Velocity Program Not Running");
                    }
                }
            }
            public VelocitySequencer(StepperMotor Stepper, out bool IsConnected, GetBoardTime BT = null) : base()
            {
                ProgramRunning = false;
                ProgramLoaded = false;
                velocityProgram = new List<VelocityProgUnit>(0);
                newVelocityProgUnit = new VelocityProgUnit();
                _numbSteps = 0;
                reportStatus = true;
                if (BT != null)
                    BoardTime = BT;
                else
                    BoardTime = null;
                if (Stepper.Connected)
                {
                    IsConnected = true;

                    Motor = Stepper;
                    while (ErrorResponses.TryDequeue(out resp)) { }
                }
                else
                {
                    IsConnected = false;
                }
                Connected = IsConnected;
            }
            public bool LoadVelocityProgram(List<string> ProgramIn, int ProfileIn, out string Err)
            {
                Err = "";
                int minVelocity;
                int maxVelocity;
                if (ProgramRunning)
                {
                    Err = "Program is already running";
                    return false;
                }
                else if (ProgramIn.Count < 1)
                {
                    Err = "Input Program must have at least 1 step";
                    return false;
                }
                if (!Motor.MinMaxVelocity(ProfileIn, out minVelocity, out maxVelocity))
                {
                    Err = "Invalid Profile";
                    return false;
                }
                List<VelocityProgUnit> newVelocityStep = new List<VelocityProgUnit>();
                newVelocityStep.Clear();

                int w1;
                double w2;
                bool progOK = true;
                for (int n = 0; n < ProgramIn.Count; n++)
                {
                    string[] ws = ProgramIn[n].Split(',');
                    //Console.WriteLine(ProgramIn[n]);
#if DEBUG
                for (int mm = 0; mm < ws.Length; mm++)
                {
                    if (mm < ws.Length - 1)
                        InternalLog.Enqueue(String.Format("{0} , ", ws[mm]));
                    else
                        InternalLog.Enqueue(ws[mm]);
                }
#endif
                    if (ws.Length > 2)
                    {
                        if (Int32.TryParse(ws[0], out w1))
                        {
                            if (w1 >= minVelocity && w1 <= maxVelocity && Double.TryParse(ws[1], out w2))
                            {
                                if (w2 > 0.0)
                                {
                                    newVelocityProgUnit.Velocity = w1;
                                    newVelocityProgUnit.Time = w2;
                                    newVelocityProgUnit.Description = ws[2].TrimEnd(quotes).TrimStart(quotes);
                                    newVelocityStep.Add(newVelocityProgUnit);
                                }
                                else
                                    progOK = false;
                            }
                            else
                                progOK = false;
                        }
                        else
                            progOK = false;
                    }
                    else
                        break;
                    if (!progOK)
                        break;
                }
                velocityProgram.Clear();
                if (progOK)
                {
                    Profile = ProfileIn;
                    while (ErrorResponses.TryDequeue(out resp)) { }
                    for (int n = 0; n < newVelocityStep.Count; n++)
                    {
                        velocityProgram.Add(newVelocityStep[n]);
#if DEBUG
                    InternalLog.Enqueue(String.Format("{0} / {1} / {2}",
                        newVelocityStep[n].Velocity, newVelocityStep[n].Time, newVelocityStep[n].Description));
#endif
                    }
                    progOK = true;
                }
                else
                {
                    newVelocityProgUnit.Time = 1.0;
                    newVelocityProgUnit.Velocity = 40;
                    newVelocityProgUnit.Description = "None";
                    velocityProgram.Add(newVelocityProgUnit);
                    Err = "There is a problem with the parameters of the input program";
                    progOK = false;
                }
                ProgramLoaded = progOK;
                return progOK;
            }

            public bool GetVelocityProgram(out List<string> ProgramOut)
            {
                ProgramOut = new List<string>();
                if (ProgramLoaded && velocityProgram.Count > 1)
                {
                    ProgramOut.Add(String.Format("Profile: {0}", Profile));
                    ProgramOut.Add("=====");
                    for (int n = 0; n < velocityProgram.Count; n++)
                        ProgramOut.Add(velocityProgram[n].ToString());

                    return true;
                }
                else
                    return false;
            }

            internal bool ChangeVelocity(int P)
            {
                bool res = false;
                if (ProgramLoaded && P >= 0 && P < velocityProgram.Count)
                {
                    //Console.WriteLine(String.Format("Changing Velocity: Step {0}, speed: {1}", P, velocityProgram[P].Velocity));
                    if (Motor.SetVelocity(velocityProgram[P].Velocity))
                    {
                        if (Motor.VelocityModePositive())   
                            res = true;

                        System.Threading.Thread.Sleep(Motor.BoardWaitTime);
                        while (Motor.CheckResponseList(out resp))
                        {
                            for (int n = 0; n < Errors.Count; n++)
                            {
                                if (resp.Response.IndexOf(Errors[n]) >= 0)
                                {
                                    res = false;
                                    LogErrors(resp.Clone());
                                }
                            }
                        }
                    }
                }
                return res;
            }

            public bool StartVelocityProgram(EnqueueInfo LI = null)
            {
                if (timer1 != null)
                {
                    timer1.Stop();
                    timer1.Dispose();
                }
                timer1On = false;
                if (ProgramLoaded )
                {
                    if (LI != null)
                    {
                        reportStatus = true;
                        LogInfo = (EnqueueInfo)LI;
                    }
                    else
                    {
                        reportStatus = false;
                        LogInfo = null;
                    }
                    Motor.SetProfile(Profile);
                    _numbSteps = velocityProgram.Count;
                    nextStep = 0;
                    timer1 = new System.Timers.Timer(200);
                    // Hook up the Elapsed event for the timer. 
                    timer1.Elapsed += OnVelocityTimerEvent;
                    timer1.AutoReset = false;
                    timer1.Enabled = true;
                    timer1On = true;
                    ProgramRunning = true;
                }
                return timer1On;
            }

            private void OnVelocityTimerEvent(Object source, ElapsedEventArgs e)
            {
                if (ProgramLoaded)
                {
                    bool terminate = false;
                    int newInterval;
                    if (nextStep < _numbSteps)
                    {
                        newInterval = (int)(velocityProgram[nextStep].Time * 1000.0);
                        if (!ChangeVelocity(nextStep))
                        {
                            Console.WriteLine("Problem reported from ChangeVelocity()");
                            terminate = true;
                        }
                        else
                        {
                            timer1.Interval = newInterval;
                        }

                        if (reportStatus)
                        {
                            if (BoardTime != null)
                                LogInfo(String.Format("{0}: {1}", BoardTime(), velocityProgram[nextStep].ToString()));
                            else
                                LogInfo(String.Format("{0}", velocityProgram[nextStep].ToString()));
                            if (terminate)
                            {
                                LogInfo("!!!!! Problem changing velocity, program terminated");
                            }
                        }
                        nextStep++;
                    }
                    else
                    {
                        terminate = true;
                    }
                    if ( terminate)
                    {
                        Motor.Terminate();
                        timer1.Stop();
                        timer1.Dispose();
                        timer1On = false;
                        if (reportStatus)
                        {
                            if (BoardTime != null)
                                LogInfo(String.Format("{0}: Motor velocity program has finished", BoardTime()));
                            else
                                LogInfo(String.Format("Motor velocity program has Finished"));
                        }
                        ProgramRunning = false;
                    }
                }
            }

            public void StopVelocityProgram()
            {
                if (timer1 != null)
                {
                    Motor.Terminate();
                    timer1.Stop();
                    timer1.Dispose();
                    timer1On = false;
                    //Console.WriteLine("motor program was aborted");
                }
                ProgramRunning = false;
            }
            public string CurrentVelocityState()
            {
                if (ProgramLoaded && ProgramRunning)
                {
                    if (nextStep > 0)
                    {
                        if (BoardTime != null)
                            return String.Format("{0}: {1}", BoardTime(), velocityProgram[nextStep - 1].ToString());
                        else
                            return String.Format("{0}", velocityProgram[nextStep-1].ToString());
                    }
                    else
                    {
                        if (BoardTime != null)
                            return String.Format("{0}: Velocity Program just starting", BoardTime());
                        else
                            return "Velocity Program just starting";
                    }
                }
                else
                    return "";
            }
        }
    }
}
