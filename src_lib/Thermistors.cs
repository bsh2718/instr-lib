using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
//using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Timers;
using System.IO;
using System.IO.Ports;

namespace sdpcrInstrLib
{
    using BoardComm;
    using Hardware;
    //using PIDController;
    using OneResponse;
    // public delegate void EnqueueResponse(ResponseItem Resp);
    // public delegate void EnqueueInfo(string str);
    namespace Therm
    {
        static class TIdx
        {
            // Count must be set to the number of items below
            public const int GetBias = 0;
            public const int ReadAllCalibrated = 1;
            public const int GetBiasCalibTime = 2;
            public const int Calibrate = 3;
            public const int CalibrationBusy = 4;
            public const int ClearBias = 5;
            public const int ClearWeights = 6;
            public const int Thermistor00 = 7;
            public const int Thermistor01 = 8;
            public const int Thermistor02 = 9;
            public const int Thermistor03 = 10;
            public const int Thermistor04 = 11;
            public const int Thermistor05 = 12;
            public const int Thermistor06 = 13;
            public const int Thermistor07 = 14;
            public const int Thermistor08 = 15;
            public const int Thermistor09 = 16;
            public const int Thermistor10 = 17;
            public const int Thermistor11 = 18;
            public const int Thermistor12 = 19;
            public const int Thermistor13 = 20;
            public const int Thermistor14 = 21;
            public const int Thermistor15 = 22;
            public const int Thermistor16 = 23;
            public const int Thermistor17 = 24;
            public const int Thermistor18 = 25;
            public const int Thermistor19 = 26;
            public const int Thermistor20 = 27;
            public const int Thermistor21 = 28;
            public const int Thermistor22 = 29;
            public const int Thermistor23 = 30;
            // This must The number ofitems above
            public const int Count = 31;

        }
        public class Thermistors : HardwareBase
        {
            //readonly char[] whiteSpace = { ' ', '\n', '\r', '\t' };  // space, new line, carriage return, tab
            //readonly char[] whiteSpaceAndComma = { ' ', '\n', '\r', '\t', ',' };  // space, new line, carriage return, tab, comma
            readonly char[] commaDelimiters = { ',' };
            protected System.Timers.Timer responseTimer;

            private static readonly object fieldLock = new object();
            string _thermistorBiases;
            string _thermistors;
            long _measurementTime;
            double[] _weightsArray = new double[24];
            int _calibrationTime;
            bool _busyCalibrating;

            bool _tempUpdated;
            bool _biasUpdated;
            //bool _weightsUpdated;
            bool _calibrationTimeChecked;
            bool _busyStatusChecked;

            //System.Timers.Timer tempTimer;
            //SynchronizedCollection<string> listTemp = new SynchronizedCollection<string>();
            //string _logFilePath;
            //FileStream FS;
            //bool tempTimerOn;
            //int _timeStep = 2000; // milliseconds, for temperature monitoring

            //int responseTimerCount;

            readonly string[] CmdList;
            SingleResponse startup;
            public bool BusyCalibrating
            {
                get
                {
                    //long cmdTime = sdPCRBoard.BoardTime();
                    CheckIfBusyCalibrating();
                    bool res = false;
                    for (int n = 0; n < 10; n++)
                    {
                        System.Threading.Thread.Sleep(BoardWaitTime);
                        lock (fieldLock)
                        {
                            if (_busyStatusChecked)
                            {
                                res = _busyCalibrating;
                                break;
                            }
                        }
                    }
                    return res;
                }
            }
            public string ThermistorBiases
            {
                get
                {
                    FetchThermistorBiases();
                    string res = "";
                    for (int n = 0; n < 10; n++)
                    {
                        System.Threading.Thread.Sleep(BoardWaitTime);
                        lock (fieldLock)
                        {
                            if (_biasUpdated)
                            {
                                res = _thermistorBiases;
                                break;
                            }
                        }
                    }
                    return res;
                }
            }
            public long BiasCalibrationTime
            {
                get
                {
                    FetchBiasCalibrationTime();
                    long res = 0;
                    for (int n = 0; n < 10; n++)
                    {
                        System.Threading.Thread.Sleep(BoardWaitTime);
                        lock (fieldLock)
                        {
                            if (_calibrationTimeChecked)
                            {
                                res = _calibrationTime;
                                break;
                            }
                        }
                    }
                    return res;
                }
            }
            public string CalibratedTemperatures
            {
                get
                {
                    //lock (fieldLock)
                    //{
                    //    InternalLog.Enqueue(String.Format("\nT - 1: {0} / {1}", sdPCRBoard.BoardTime(), _tempUpdated.ToString()));
                    //    InternalLog.Enqueue(String.Format("     {0}", _thermistors));
                    //}
                    //InternalLog.Enqueue(String.Format(">>>>> {0}: Called FetchCalibratedTemperatures", sdPCRBoard.BoardTime()));
                    FetchCalibratedTemperatures();
                    string res = "";
                   // bool updated = false;
                    for (int n = 0; n < 20; n++)
                    {
                        System.Threading.Thread.Sleep(BoardWaitTime);
                        lock (fieldLock)
                        {
                            if (_tempUpdated)
                            {
                                res = _thermistors;
                                //updated = true;
                                //InternalLog.Enqueue("<<<<<");
                                //InternalLog.Enqueue(String.Format("CalibratedTemperatures Accessed: {0}/{1}", sdPCRBoard.BoardTime(), n));
                                //InternalLog.Enqueue(String.Format("          _thermistors: {0}", _thermistors));
                                //InternalLog.Enqueue(String.Format("                   res: {0}", res));
                                break;
                            }
                            //else if (n > 5)
                            //{
                            //    InternalLog.Enqueue(String.Format("{0}: _tempUpdated == false", sdPCRBoard.BoardTime()));
                            //}
                        }
                    }
                    return res;
                }
            }

            public string TimeAndCalibratedTemperatures
            {
                get
                {
                    //lock (fieldLock)
                    //{
                    //    InternalLog.Enqueue(String.Format("\nTAndC - 1: {0} / {1}", sdPCRBoard.BoardTime(), _tempUpdated.ToString()));
                    //    InternalLog.Enqueue(String.Format("     {0}", _thermistors));
                    //}
                    FetchCalibratedTemperatures();
                    string res = "";
                    double tim = 0.0;
                    bool updated = false;
                    for (int n = 0; n < 20; n++)
                    {
                        System.Threading.Thread.Sleep(BoardWaitTime);
                        lock (fieldLock)
                        {
                            if (_tempUpdated)
                            {
                                res = _thermistors;
                                tim = (double)(_measurementTime) * 0.001;
                                //InternalLog.Enqueue(String.Format("T - 2: {0} / {1} / {2}", sdPCRBoard.BoardTime(), n, _tempUpdated.ToString()));
                                //InternalLog.Enqueue(String.Format("     {0}", _thermistors));
                                updated = true;
                                break;
                            }
                        }
                    }
                    if ( updated)
                        return String.Format("{0},{1}", tim, res);
                    else
                    {
                        lock (fieldLock)
                        {
                            return String.Format("0.0,{0}", _thermistors);
                        }
                    }
                }
            }
            public long MeasurementTime
            {
                get
                {
                    lock (fieldLock)
                    {
                        return _measurementTime;
                    }
                }
            }
            public Thermistors(ControlBoard Board, out bool IsConnected) : base()
            {
                sdPCRBoard = Board;

                CmdList = new string[TIdx.Count];
                CmdList[TIdx.GetBias] = "/100DR";           //  0 - Get Thermistor Biases
                CmdList[TIdx.ReadAllCalibrated] = "/100AR"; //  1 - Reads all thermistors (calibrated output)
                CmdList[TIdx.GetBiasCalibTime] = "/100S";    //  2 - Set/Get Calibration Times (stub)
                                                            //	    Set: requires time in seconds followed by R
                                                            //	    Get: requires R
                CmdList[TIdx.Calibrate] = "/100T";          //  3 - Sets Reference temperature (stub). Requires
                                                            //	    the reference temperature used to compute
                                                            //	    biases (double XX.X) followed by R
                CmdList[TIdx.CalibrationBusy] = "/100QR";   //  4 - Check to see if bias calculation is finished
                CmdList[TIdx.ClearBias] = "/100ZR";         //  5 - Clears thermistor biases
                CmdList[TIdx.ClearWeights] = "/100XR";      //  6 - Clears all thermistor weights
                CmdList[TIdx.Thermistor00] = "/100";        //  7 to 30 - References for Thermistors 00 to 23 (stubs)
                CmdList[TIdx.Thermistor01] = "/101";
                CmdList[TIdx.Thermistor02] = "/102";
                CmdList[TIdx.Thermistor03] = "/103";
                CmdList[TIdx.Thermistor04] = "/104";
                CmdList[TIdx.Thermistor05] = "/105";
                CmdList[TIdx.Thermistor06] = "/106";
                CmdList[TIdx.Thermistor07] = "/107";
                CmdList[TIdx.Thermistor08] = "/108";
                CmdList[TIdx.Thermistor09] = "/109";
                CmdList[TIdx.Thermistor10] = "/110";
                CmdList[TIdx.Thermistor11] = "/111";
                CmdList[TIdx.Thermistor12] = "/112";
                CmdList[TIdx.Thermistor13] = "/113";
                CmdList[TIdx.Thermistor14] = "/114";
                CmdList[TIdx.Thermistor15] = "/115";
                CmdList[TIdx.Thermistor16] = "/116";
                CmdList[TIdx.Thermistor17] = "/117";
                CmdList[TIdx.Thermistor18] = "/118";
                CmdList[TIdx.Thermistor19] = "/119";
                CmdList[TIdx.Thermistor20] = "/120";
                CmdList[TIdx.Thermistor21] = "/121";
                CmdList[TIdx.Thermistor22] = "/122";
                CmdList[TIdx.Thermistor23] = "/123";

                //Console.WriteLine("Finished Loading CmdList");
                //Console.Out.Flush();
                //Console.WriteLine(String.Format("Thermistor Cstr Time: {0}", Board.BoardTime()));
                _tempUpdated = false;
                _biasUpdated = false;
                //_weightsUpdated = false;
                _calibrationTimeChecked = false;
                _busyStatusChecked = false;
                using (startup = new SingleResponse(sdPCRBoard))
                {
                    BoardWaitTime = sdPCRBoard.BoardWaitTime;
                    if (TestConnection())
                    {
                        responseTimer = new System.Timers.Timer(BoardWaitTime);
                        // hook up the elapsed event for the timer.  
                        responseTimer.AutoReset = false;
                        responseTimer.Enabled = false;
                        responseTimer.Elapsed += CheckBoardResponseList;

                        //Console.WriteLine(String.Format("Board Wait Time: {0}", boardWaitTime));
                        IsConnected = true;
                    }
                    else
                        IsConnected = false;
                }
                //Console.WriteLine(String.Format("Thermistor Cstr Time: {0}", Board.BoardTime()));

                _connected = IsConnected;
            }
            void FetchThermistorBiases()
            {
                sdPCRBoard.AddCommand(CmdList[TIdx.GetBias], Notification.OnError, LogResponses);
                CmdsSent.Enqueue(CmdList[TIdx.GetBias]);
                lock (fieldLock) { _biasUpdated = false; }
            }
            void FetchCalibratedTemperatures()
            {
                //Console.WriteLine(String.Format("FCT - 1: {0}", sdPCRBoard.BoardTime()));
                //InternalLog.Enqueue(String.Format("{0}: FetchCalibratedTemperatures/sdPCRBoard.AddCommand", sdPCRBoard.BoardTime()));
                sdPCRBoard.AddCommand(CmdList[TIdx.ReadAllCalibrated], Notification.OnError, LogResponses);
                CmdsSent.Enqueue(CmdList[TIdx.ReadAllCalibrated]);
                lock (fieldLock) { _tempUpdated = false; }
            }
            public bool GetThermistorWeights(out string Weights)
            {
                lock (fieldLock)
                {
                    _weightsArray[23] = -10.0;
                    //_weightsUpdated = false;
                }
                for (int n = 0; n < 24; n++)
                {
                    string stmp = String.Format("{0}WR", CmdList[TIdx.Thermistor00 + n]);
                    sdPCRBoard.AddCommand(stmp, Notification.OnError, LogResponses);
                    CmdsSent.Enqueue(stmp);
                    //Console.WriteLine(String.Format("n = {0}", n));
                }
                System.Threading.Thread.Sleep(24 * BoardWaitTime);
                bool done = true;
                for (int n = 0; n < 10; n++)
                {
                    done = true;
                    if (_weightsArray[23] > -9.0)
                    {
                        //Console.WriteLine(String.Format("n = {0}, _w[{1}] = {2}", n, k, _weightsArray[k]));
                        break;
                    }
                    else
                        System.Threading.Thread.Sleep(24 * BoardWaitTime);
#if DEBUG
                    InternalLog.Enqueue(String.Format("Time: {0} - n={1},  _w[23] = {2}", sdPCRBoard.BoardTime(), n, _weightsArray[23]));
#endif
                }
                if (done)
                {
                    
                    StringBuilder sb = new StringBuilder();
                    lock (fieldLock)
                    {
                        //_weightsUpdated = true;
                        sb.Append(String.Format("{0}", _weightsArray[0]));
                        for (int k = 1; k < 24; k++)
                        {
                            sb.Append(String.Format(",{0}", _weightsArray[k]));
                        }
                        //_lastFieldLock = sdPCRBoard.BoardTime();
                    }
                    Weights = sb.ToString();
                    return true;
                }
                else
                {
                    Weights = "";
                    return false;
                }
            }
            void CheckIfBusyCalibrating()
            {
                sdPCRBoard.AddCommand(CmdList[TIdx.CalibrationBusy], Notification.OnError, LogResponses);
                CmdsSent.Enqueue(CmdList[TIdx.CalibrationBusy]);
                _busyStatusChecked = false;
            }


            public void ClearAllTemperatureBiases()
            {
                sdPCRBoard.AddCommand(CmdList[TIdx.ClearBias], Notification.OnError, LogResponses);
                CmdsSent.Enqueue(CmdList[TIdx.ClearBias]);            
                //return false;
            }
            public bool SetReferenceTemperature(double Temp)
            {
                if (Temp > 10.0 && Temp < 40.0)
                {
                    string cmd = String.Format("{0}{1}R", CmdList[TIdx.Calibrate], Temp);
                    // CmdList[TIdx.] = "/100T");	//  3 - Sets Reference temperature (stub).
                    sdPCRBoard.AddCommand(cmd, Notification.OnError, LogResponses);
                    CmdsSent.Enqueue(cmd);
                    return true;
                }
                else
                    return false;
            }
            public bool Check24Weights(string testString)
            {
                string[] words = testString.Split(',');
                //for (int n = 0; n < words.Length; n++)
                //    Console.WriteLine(String.Format("{0} : {1}", n, words[n]));
                bool weightsOK = true;
                int numbNonZero = 0;
                if (words.Length == 24)
                {
                    for (int k = 0; k < 24; k++)
                    {
                        if (words[k].Length > 0)
                        {
                            double arr;
                            if (!Double.TryParse(words[k], out arr))
                            {
                                //Console.WriteLine(String.Format("{0} : TryParse Failed on {1}", k, words[k]));
                                weightsOK = false;
                                break;
                            }
                            else if (arr > 0.001)
                            {
                                numbNonZero++;
                                break;
                            }
                        }
                        else
                        {
                           // Console.WriteLine(String.Format("{0} : Short string for {1}", k, words[k]));
                            weightsOK = false;
                            break;
                        }
                    }
                    if (numbNonZero == 0)
                        weightsOK = false;
                }
                else
                {
                    weightsOK = false;
                }
                return weightsOK;
            }

            bool CheckFor24(ResponseItem Resp, out string StringOut, out long Tim)
            {
                int idx = Resp.Response.IndexOf("/0");
                StringOut = Resp.Response.Substring(idx + 2);
                string[] words = StringOut.Split(commaDelimiters);
                bool cont = true;
                //Console.WriteLine("CheckFor24");
                //Console.WriteLine(String.Format("Resp.Response: {0}", Resp.Response));
                //Console.WriteLine(String.Format("Resp.Response(2): {0}", Resp.Response.Substring(2)));
                //Console.WriteLine(String.Format("words.Length = {0}", words.Length));
                //for (int n = 0; n < words.Length; n++)
                //    Console.WriteLine(String.Format("     {0}", words[n]));

                if (words.Length == 24)
                {
                    for (int k = 0; k < 24; k++)
                    {
                        if (words[k].Length > 2)
                        {
                            double arr;
                            if (!Double.TryParse(words[k], out arr))
                            {
                                cont = false;
                                break;
                            }
                        }
                        else
                        {
                            cont = false;
                            break;
                        }
                    }
                }
                else
                {
                    cont = false;
                }
                if (cont)
                    Tim = Resp.Time;
                else
                    Tim = 0;

                return cont;
            }
            bool TestConnection()
            {
                ResponseItem respTC;
                bool cont = false;
                string outString0 = "";
                string outString1 = "";
                //long time0 = 0;
                long time1 = 0;
                int wait = 5 * BoardWaitTime;
                startup.AddCommand(CmdList[TIdx.GetBias]);
                for (int n = 0; n < 8; n++)
                {
                    if (startup.GetResponse(wait, out respTC))
                    {
                        long time0;
                        if (CheckFor24(respTC, out outString0, out time0))
                        {
#if DEBUG
                        InternalLog.Enqueue(String.Format("Time {0}: {1} - {2}", sdPCRBoard.BoardTime(), n, outString0));
#endif
                            cont = true;
                            break;
                        }
                        else
                        {
#if DEBUG
                        InternalLog.Enqueue(String.Format("Time {0}: {1} - not 24 numbers - {2}", sdPCRBoard.BoardTime(), n, outString0));
#endif
                        }
                    }
                    else
                    {
#if DEBUG
                    InternalLog.Enqueue(String.Format("Time {0}: no response", sdPCRBoard.BoardTime(), n));
#endif
                    }
                }
                if (!cont)
                    return false;
                cont = false;

                startup.AddCommand(CmdList[TIdx.ReadAllCalibrated]);
                for (int n = 0; n < 8; n++)
                {
                    if (startup.GetResponse(wait, out respTC))
                    {
                        if (CheckFor24(respTC, out outString1, out time1))
                        {
#if DEBUG
                        InternalLog.Enqueue(String.Format("ReadAllCalibrated - {0}: {1}: {2}", n, time1, outString1));
#endif
                            cont = true;
                            break;
                        }
                        else
                        {
#if DEBUG
                        InternalLog.Enqueue(String.Format("0 - {0}: {1} - not 24 numbers", n, outString1));
#endif
                        }
                    }
                    else
                    {
#if DEBUG
                    InternalLog.Enqueue(String.Format("1 - {0}: no response", n));
#endif
                    }
                }
                if (!cont)
                    return false;
                else
                {
                    lock (fieldLock)
                    {
                        _thermistorBiases = outString0;
                        _thermistors = outString1;
                        _measurementTime = time1;
                        _biasUpdated = true;
                        _tempUpdated = true;
                        //InternalLog.Enqueue(String.Format("ReadAllCalibrated(fieldLock) - {0}: {1}", time1, outString1));
                    }
                }
                return cont;
            }

            public void FetchBiasCalibrationTime()
            {
                string stmp = String.Format("{0}R", CmdList[TIdx.GetBiasCalibTime]);
                lock (fieldLock)
                {
                    _calibrationTimeChecked = false;
                }
                sdPCRBoard.AddCommand(stmp, Notification.OnError, LogResponses);
                CmdsSent.Enqueue(stmp);
            }
            public bool SetCalibrationTime(int Tim)
            {
                if (Tim > 0 && Tim < 10000)
                {
                    string stmp = String.Format("{0}{1}R", CmdList[TIdx.GetBiasCalibTime], Tim);
                    lock (fieldLock)
                    {
                        _calibrationTimeChecked = false;
                    }
                    sdPCRBoard.AddCommand(stmp, Notification.OnError, LogResponses);
                    CmdsSent.Enqueue(stmp);
                    return true;
                }
                else
                    return false;
            }
            public bool SetWeight(int ThermIdx, int Weight)
            {
                if (ThermIdx >= 0 && ThermIdx < 24 && Weight >= 0 && Weight < 1000)
                {
                    string cmd = String.Format("{0}W{1}R", CmdList[TIdx.Thermistor00 + ThermIdx], Weight);
                    sdPCRBoard.AddCommand(cmd, Notification.OnError, LogResponses);
                    CmdsSent.Enqueue(cmd);
                    return true;
                }
                return false;
            }
            //public bool GetWeight(int ThermIdx)
            //{
            //    if (ThermIdx >= 0 && ThermIdx < 24)
            //    {
            //        string cmd = String.Format("{0}WR", CmdList[TIdx.Thermistor00 + ThermIdx]);
            //        sdPCRBoard.AddCommand(cmd, Notification.OnError, LogResponses);
            //        CmdsSent.Enqueue(cmd);
            //        return true;
            //    }
            //    return false;
            //}

            public override string ToString()
            {
                StringBuilder ret = new StringBuilder();
                lock (fieldLock)
                {
                    ret.AppendFormat("Biases: {0}\n", _thermistorBiases);
                    //ret.AppendFormat("Weights: {0}\n", _thermistorWeights);
                    ret.AppendFormat("Measurement Time: {0}\n", _measurementTime);
                    ret.AppendFormat("Temperatures: {0}\n", _thermistors);
                }
                return base.ToString() + ret.ToString();
            }
            protected override void LogResponses(ResponseItem Resp)
            {
                //Console.WriteLine(String.Format("LogResponses: {0} - {1}", Resp.Command, Resp.Response));
                ResponsesFromBoard.Enqueue(Resp.Clone());
                //InternalLog.Enqueue(String.Format("Time {0}: ResponsesFromBoard: {1}", sdPCRBoard.BoardTime(), Resp.ToString() ));
                responseTimer.AutoReset = true;
                responseTimer.Enabled = true;
            }
            void CheckBoardResponseList(object sender, ElapsedEventArgs e)
            {
                ResponseItem resp;
                while (ResponsesFromBoard.TryDequeue(out resp))
                {
                    //InternalLog.Enqueue("-----");
                    //InternalLog.Enqueue(String.Format("CRB Time {0} : {1}", sdPCRBoard.BoardTime(), resp.ToString()));
                    if (resp.InstrumentSignal)
                    {
                        //InternalLog.Enqueue(String.Format("Instrument Signal: {0}", resp.ToString()));
                        ResponsesToCaller.Enqueue(resp.Clone());
                    }
                    else if (CmdsSent.Count < 1)
                    {
                        //InternalLog.Enqueue(String.Format("CmdsSent.Count < 1: {0}", resp.ToString()));
                        //resp.InstrumentSignal = true;
                        ResponsesToCaller.Enqueue(resp.Clone());
                    }
                    else
                    {
                        //InternalLog.Enqueue(String.Format("Comparing commands: {0}", resp.ToString()));
                        string cmd;
                        if (CmdsSent.TryDequeue(out cmd))
                        {
                            //int itmp = cmd.IndexOf(resp.Command);
                            //int jtmp = resp.Command.IndexOf(cmd);
                            //InternalLog.Enqueue(String.Format("Inside CheckBoardResponseList: {0} - {1}: itmp: {2}, jtmp: {3}",
                            //    cmd, resp.Command, itmp, jtmp));
                            int iCmd = cmd.IndexOf(resp.Command);
                            if (iCmd < 0)
                                iCmd = resp.Command.IndexOf(cmd);
                            if (iCmd >= 0)
                            {
                                int cmdIdx = -1;
                                for (int n = 0; n < CmdList.Length; n++)
                                {
                                    if (cmd.IndexOf(CmdList[n]) == 0)
                                    {
                                        cmdIdx = n;
                                        break;
                                    }
                                }
                                //InternalLog.Enqueue(String.Format("          {0} : {1}", cmdIdx, CmdList[cmdIdx]));
                                int resIdx = -1;
                                int respStart = 2;
                                for (int n = 0; n < DefaultResponses.Count; n++)
                                {
                                    int idx = resp.Response.IndexOf(DefaultResponses[n]);

                                    if (idx >= 0)
                                    {
                                        resIdx = n;
                                        respStart = idx + 2;
                                        break;
                                    }
                                }
                                bool error;
                                if (resIdx == 0)
                                    error = false;
                                else
                                    error = true;
                                string outString;
                                long outTime;
                                switch (cmdIdx)
                                {
                                    case TIdx.GetBias: //  0 - Get Thermistor Biases
                                        if (resIdx == -1)
                                        {
                                            if (CheckFor24(resp, out outString, out outTime))
                                            {
                                                lock (fieldLock)
                                                {
                                                    _thermistorBiases = outString;
                                                    _biasUpdated = true;
                                                    //_lastFieldLock = sdPCRBoard.BoardTime();
                                                }
                                                error = false;
                                            }
                                            else
                                                error = true;
                                        }
                                        else
                                            error = true;
                                        break;
                                    case TIdx.ReadAllCalibrated: //  1 - Get Calibrated temperatures
                                        //InternalLog.Enqueue(String.Format("ReadAllCalibrated: {0} : {1} - {2}: {3}",
                                        //                             resIdx, resp.Command, resp.Response, sdPCRBoard.BoardTime()));
                                        if (resIdx == -1)
                                        {
                                            if (CheckFor24(resp, out outString, out outTime))
                                            {
                                                lock (fieldLock)
                                                {
                                                    _thermistors = outString;
                                                    _measurementTime = outTime;
                                                    _tempUpdated = true;
                                                    //InternalLog.Enqueue(String.Format("     Measurement Time: {0}", _measurementTime));
                                                    //InternalLog.Enqueue(String.Format("     _thermistors: {0}", _thermistors));
                                                }
                                                error = false;
                                            }
                                            else
                                                error = true;
                                        }
                                        else
                                            error = true;
                                        break;
                                    case TIdx.GetBiasCalibTime: //  2 - Set/Get Calibration Times
                                        if (resIdx == -1)
                                        {
                                            if (resp.Command.Length > 6)
                                            {
                                                string stmp = resp.Response.Substring(respStart, resp.Response.Length - 3);
                                                int ktmp;
                                                if (Int32.TryParse(stmp, out ktmp))
                                                {
                                                    lock (fieldLock)
                                                    {
                                                        _calibrationTime = ktmp;
                                                        _calibrationTimeChecked = true;
                                                    }
                                                    error = false;
                                                }
                                                else
                                                    error = true;
                                            }
                                        }
                                        break;
                                    case TIdx.Calibrate: //  3 - Sets Reference temperature
                                        break;
                                    case TIdx.CalibrationBusy: //  4 - Check to see if bias calculation is finished
                                        if (resIdx == -1)
                                        {
                                            lock (fieldLock)
                                            {
                                                if (resp.Response.IndexOf("/00") >= 0)
                                                {
                                                    lock (fieldLock)
                                                    {
                                                        _busyCalibrating = false;
                                                        _busyStatusChecked = true;
                                                    }
                                                    error = false;
                                                }
                                                else if (resp.Response.IndexOf("/01") >= 0)
                                                {
                                                    lock (fieldLock)
                                                    {
                                                        _busyCalibrating = true;
                                                        _busyStatusChecked = true;
                                                    }
                                                    error = false;
                                                }
                                            }
                                        }
                                        else
                                            error = true;
                                        break;
                                    case TIdx.ClearBias: //  5 - Clears thermistor biases
                                        break;
                                    case TIdx.ClearWeights: //  6 - Clears all thermistor weights
                                        break;
                                    case TIdx.Thermistor00:
                                    case TIdx.Thermistor01:
                                    case TIdx.Thermistor02:
                                    case TIdx.Thermistor03:
                                    case TIdx.Thermistor04:
                                    case TIdx.Thermistor05:
                                    case TIdx.Thermistor06:
                                    case TIdx.Thermistor07:
                                    case TIdx.Thermistor08:
                                    case TIdx.Thermistor09:
                                    case TIdx.Thermistor10:
                                    case TIdx.Thermistor11:
                                    case TIdx.Thermistor12:
                                    case TIdx.Thermistor13:
                                    case TIdx.Thermistor14:
                                    case TIdx.Thermistor15:
                                    case TIdx.Thermistor16:
                                    case TIdx.Thermistor17:
                                    case TIdx.Thermistor18:
                                    case TIdx.Thermistor19:
                                    case TIdx.Thermistor20:
                                    case TIdx.Thermistor21:
                                    case TIdx.Thermistor22:
                                    case TIdx.Thermistor23:
                                        //Console.WriteLine(String.Format("Substring(4) = {0}", resp.Command.Substring(4)));
                                        if (resp.Command.Substring(4).IndexOf("WR") == 0)  // get weight
                                        {
                                            double dtmp;
                                            if (Double.TryParse(resp.Response.Substring(respStart), out dtmp))
                                            {
                                                lock (fieldLock)
                                                {
                                                    _weightsArray[cmdIdx - TIdx.Thermistor00] = dtmp;
                                                    //Console.WriteLine(String.Format("[{0}]: {1}",
                                                    //    cmdIdx - TIdx.Thermistor00, _weightsArray[cmdIdx - TIdx.Thermistor00]));
                                                }
                                                error = false;
                                            }
                                        }
                                        else if (resp.Command.Substring(4).IndexOf("W") == 0) // set weight
                                        {
                                            error = false;
                                        }
                                        //else if (resp.Command.Substring(4).IndexOf("CR") == 0)  // get one calibrated
                                        //{
                                        //    double dtmp;
                                        //    if (Double.TryParse(resp.Response.Substring(respStart), out dtmp))
                                        //    {
                                        //        lock (fieldLock)
                                        //        {
                                        //            _temperaturesArray[cmdIdx - TIdx.Thermistor00] = dtmp;
                                        //        }
                                        //        error = false;
                                        //    }
                                        //}
                                        else
                                            error = true;
                                        break;
                                    default:
                                        break;
                                }

                                if (resp.Notify == Notification.Always)
                                    ResponsesToCaller.Enqueue(resp.Clone());
                                else if (error && resp.Notify == Notification.OnError)
                                    ResponsesToCaller.Enqueue(resp.Clone());
                            }
                        }
                    } 
                }
                if (ResponsesFromBoard.Count <= 0)
                {
                    responseTimer.AutoReset = false;
                    responseTimer.Enabled = false;
                }
            }
            public void Dispose()
            {
                if (responseTimer.Enabled)
                    responseTimer.Dispose();
            }
        }



    } // namespace Therm
}
