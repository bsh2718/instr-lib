using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
//using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Timers;
using System.IO;
using System.IO.Ports;

namespace sdpcrInstrLib
{
    using BoardComm;
    // public delegate void EnqueueResponse(ResponseItem Resp);
    // public delegate void EnqueueInfo(string str);
    using ErrorCodes;
    namespace Hardware
    {
        public abstract class HardwareBase
        {
            public static List<string> DefaultResponses = new List<string> { "/0'", "/0B", "/0C", "/0S", "/0E" };
            protected object gen;
            //protected List<string> _commandList;
            //protected string _catenatedCommandList;
            protected ConcurrentQueue<string> CmdsSent;
            protected ConcurrentQueue<ResponseItem> ResponsesFromBoard;
            protected ConcurrentQueue<ResponseItem> ResponsesToCaller;
            protected ConcurrentQueue<string> InternalLog;
            //private int waitTime = 5;
            protected bool _connected = false;
            protected int TimeStep = 50;
            protected ControlBoard sdPCRBoard;
            public bool Connected
            {
                get
                {
                    return _connected;
                }
            }
            public int NumbCommandsWaiting
            {
                get
                {
                    return CmdsSent.Count;
                }
            }

            //public bool LogIO
            //{
            //    get;
            //    set;
            //}
            public int BoardWaitTime
            {
                get; protected set;
            }
            protected HardwareBase()
            {
                CmdsSent = new ConcurrentQueue<string>();
                ResponsesFromBoard = new ConcurrentQueue<ResponseItem>();
                ResponsesToCaller = new ConcurrentQueue<ResponseItem>();
                InternalLog = new ConcurrentQueue<string>();
            }

            protected virtual void LogResponses(ResponseItem Resp)
            {
                ResponsesFromBoard.Enqueue(Resp.Clone());
            }

            protected void PostCommand(string Command, Notification Notify)
            {
                sdPCRBoard.AddCommand(Command, Notify, LogResponses);
                CmdsSent.Enqueue(Command);
            }

            public bool CheckResponseList(out ResponseItem Resp)
            {
                bool res = ResponsesToCaller.TryDequeue(out ResponseItem resp);
                if (res)
                    Resp = resp.Clone();
                else
                    Resp = new ResponseItem();
                return res;
            }

            public void EmptyResponseLists()
            {
                //ResponseItem resp;
                while (ResponsesToCaller.TryDequeue(out _ )){ }
                while (ResponsesFromBoard.TryDequeue(out _ )){ }
            }
            public override string ToString()
            {
                return "";
            }

            public bool GetInternalLog(out string[] Log)
            {
                if (InternalLog.Count == 0 )
                {
                    Log = new string[0];
                    return false;
                }
                else
                {
                    Log = InternalLog.ToArray();
                    return true;
                }
            }
        }

        public class Processor : HardwareBase
        {
            private static readonly object fieldLock = new object();
            public bool BoardWaiting
            {
                get
                {
                    return sdPCRBoard.WaitingForResponse;
                }
            }
            public void ClearBoardWaiting()
            {
                sdPCRBoard.ClearWaiting();
            }
            ConcurrentQueue<string> logInfo;
            //ConcurrentQueue<ResponseItem> statusResponses;
            //ConcurrentQueue<ResponseItem> errorMessages;
            //ConcurrentQueue<ResponseItem> statusMessages;
            ConcurrentQueue<ResponseItem> startUpResponses;
            List<string> defaultList = new List<string> { "/0FR", "/0E1R", "/0E0R" };
            //string defaultString = "/0FR/0E1R/0E0R";
            // int timeStep = 100;
            private static readonly object processorLock = new object();
            string _firmware;
            bool _firmwareUpdated;
            string currPortName;
            string[] _portInfo;
            private readonly bool consoleOutput;
            ResponseItem resp;
            static System.Timers.Timer responseTimer;
            // static System.Timers.Timer statusTimer;
            int responseTimerCount;
            //int statusTimerCount;
            bool disposed;
            // public delegate void EnqueueInfo(string str);
            public void LogQueue(string str)
            {
                logInfo.Enqueue(str);
            }
           
            public void LogStartUpResponses(ResponseItem Resp)
            {
                startUpResponses.Enqueue(Resp.Clone());
            }
            protected override void LogResponses(ResponseItem Resp)
            {
                ResponsesFromBoard.Enqueue(Resp.Clone());
                responseTimerCount = 0;
                responseTimer.AutoReset = true;
                responseTimer.Start();
            }
            public void Response(ResponseItem Resp)
            {
                ResponsesFromBoard.Enqueue(Resp.Clone());
            }
            public string FirmwareVersion
            {
                get
                {
                    string res = "0.0.0";
                    if (_connected)
                    {
                        FetchFirmwareVersion();
                        for (int n = 0; n < 10; n++)
                        {
                            System.Threading.Thread.Sleep(BoardWaitTime);
                            lock (fieldLock)
                            {
                                if (_firmwareUpdated)
                                {
                                    res = _firmware;
                                    break;
                                }
                            }
                            //Console.WriteLine(String.Format("FirmwareVersion, n: {0}, BoardWaitTime: {1}", n, BoardWaitTime));
                        }
                        return res;
                    }
                    else
                        return "NotConnected";
                }
            }
            public string[] PortInformation
            {
                get
                {
                    lock (processorLock)
                    {
                        return _portInfo;
                    }
                }
            }
            public Processor(string PrevPortName, bool UseConsole, bool TrackConnect, bool TrackComm, bool TrackResp) : base()
            {
                disposed = false;
                //_commandList = defaultList;
                //_catenatedCommandList = defaultString;
                _firmwareUpdated = true;
                _firmware = "1.0.0";
                _connected = false;
                responseTimerCount = 0;
                //statusTimerCount = 0;
                List<string> portNames;
                // ResponsesFromBoard = new ConcurrentQueue<ResponseItem>();
                // ResponsesToCaller = new ConcurrentQueue<ResponseItem>();
                logInfo = new ConcurrentQueue<string>();
                //statusResponses = new ConcurrentQueue<ResponseItem>();
                startUpResponses = new ConcurrentQueue<ResponseItem>();

                // Timers for response and status queues
                responseTimer = new System.Timers.Timer(TimeStep);  // Timer for ResponsesFromBoard Queue
                                                                    // get timer, but don't start it yet.
                responseTimer.AutoReset = false;
                responseTimer.Stop();
                // hook up the elapsed event for the timer. 
                responseTimer.Elapsed += CheckBoardResponseList;
                consoleOutput = UseConsole;
                sdPCRBoard = new ControlBoard(LogResponses, LogQueue, TrackConnect, TrackComm, TrackResp, out portNames);
                if (UseConsole)
                {
                    Console.WriteLine("Port Names");
                    for (int n = 0; n < portNames.Count; n++)
                        Console.WriteLine(portNames[n]);
                }
                int prevPortIdx = -1;
                if (PrevPortName.Length > 0)
                {
                    for (int n = 0; n < portNames.Count; n++)
                    {
                        if (portNames[n].IndexOf(PrevPortName) == 0)
                        {
                            prevPortIdx = n;
                            break;
                        }
                    }
                }
                if (prevPortIdx >= 0)
                {
                    _connected = sdPCRBoard.ConnectTo(portNames[prevPortIdx], out _portInfo);
                    if (_connected)
                    {
                        _connected = TestConnection(out _firmware);
                        if (UseConsole)
                            Console.WriteLine(String.Format("1 _connected = {0}", _connected));
                    }
                    if (_connected)
                        currPortName = portNames[prevPortIdx];
                }
                if (!_connected)
                {
                    if (UseConsole)
                        Console.WriteLine(String.Format("2 _connected = {0}", _connected));
                    for (int n = 0; n < portNames.Count; n++)
                    {
                        if (UseConsole)
                            Console.WriteLine(String.Format("testing {0}", portNames[n]));
                        if (n != prevPortIdx)
                        {
                            _connected = sdPCRBoard.ConnectTo(portNames[n], out _portInfo);
                            if (UseConsole)
                                Console.WriteLine(String.Format("3 _connected = {0}", _connected));
                            if (_connected)
                            {
                                _connected = TestConnection(out _firmware);
                                if (UseConsole)
                                    Console.WriteLine(String.Format("4 _connected = {0}", _connected));
                            }
                            else
                            if (_connected)
                            {
                                currPortName = portNames[n];
                                break;
                            }
                        }
                    }
                }

                if (_connected)
                {
                    BoardWaitTime = sdPCRBoard.BoardWaitTime;
                    if (UseConsole)
                        Console.WriteLine(" port is connected");
                    sdPCRBoard.SetupErrorParser(4);
                    // sdPCRBoard.StartUp(); // uncomment this if there are any additional startup commands
                }
                else // if not connected dispose of timers
                {
                    if (UseConsole)
                        Console.WriteLine(" port is NOT connected");
                    if (responseTimer != null)
                        responseTimer.Dispose();
                    //if (statusTimer != null)
                    //    statusTimer.Dispose();
                }
            }
            public void Dispose()
            {
                string log;
                if (!disposed)
                {
                    if (consoleOutput)
                        Console.WriteLine("Disposing of Processor");
                    if (responseTimer != null)
                    {
                        responseTimer.Stop();
                        responseTimer.Dispose();
                    }

                    if (logInfo.Count > 0)
                    {
                        if (consoleOutput)
                            Console.WriteLine("logInfo contents");
                        while (logInfo.Count > 0)
                        {
                            if (logInfo.TryDequeue(out log) && consoleOutput)
                                Console.WriteLine(log);
                        }
                        if (consoleOutput)
                            Console.ReadLine();
                    }
                    sdPCRBoard.Disconnect();
                }
                disposed = true;
                //GC.SuppressFinalize(this);
            }
            ~Processor()
            {
                Dispose();
            }

            bool TestConnection(out string Version)
            {
                if (consoleOutput)
                    Console.WriteLine("Inside Test Connection 1");
                if (_connected)
                {
                    sdPCRBoard.AddCommand("/0FR", Notification.Always, Response);
                    if (consoleOutput)
                        Console.WriteLine("Inside Test Connection 2");
                    for (int n = 0; n < 8; n++)
                    {
                        if (n == 7)
                        {
                            _connected = false;
                            break;
                        }
                        if (ResponsesFromBoard.TryDequeue(out resp))
                            break;
                        else
                        {
                            if (consoleOutput)
                                Console.WriteLine(String.Format("n = {0}", n));
                            System.Threading.Thread.Sleep(200);
                        }
                    }
                    if (_connected)
                    {
                        if (consoleOutput)
                            Console.WriteLine("Inside Test Connection 3");
                        if (resp.Command.IndexOf("/0FR") >= 0 && resp.Response.Length > 6)
                        {
                            string[] words = resp.Response.Substring(2).Split('.');
                            if (consoleOutput)
                            {
                                Console.WriteLine(String.Format("Inside Test Connection 3 - {0} : {1}", resp.Response, words[0]));
                                Console.Out.Flush();
                            }

                            if (words.Length == 3)
                            {
                                if (consoleOutput)
                                    Console.WriteLine(String.Format("Inside Test Connection 5 - {0} : {1} : {2}", words[0], words[1], words[2]));
                                for (int n = 0; n < 3; n++)
                                {
                                    for (int m = 0; m < words[n].Length; m++)
                                    {
                                        if ("012345679".IndexOf(words[n].Substring(m, 1)) < 0)
                                        {
                                            _connected = false;
                                            break;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                _connected = false;
                            }
                        }
                        else
                            _connected = false;
                    }
                }
                else
                {
                    if (consoleOutput)
                        Console.WriteLine("Test Connection called with _connected == false");
                }
                if (_connected)
                {
                    Version = resp.Response.Substring(2);
                }
                else
                    Version = "";
                if (consoleOutput)
                    Console.WriteLine("Inside Test Connection - End");

                EmptyResponseLists();
                return _connected;
            }

            public bool GetBoard(out ControlBoard Board) // whenever this is used, OBoard must be cast to ControlBoard before use
            {
                if (_connected)
                {
                    Board = sdPCRBoard;
                    return true;
                }
                else
                {
                    Board = null;
                    return false;
                }
            }
            private void FetchFirmwareVersion()
            {
                //Console.WriteLine("Inside FetchFirmwareVersion");
                if (_connected)
                {
                    //Console.WriteLine("Inside FetchFirmwareVersion, _connected == true");
                    lock (fieldLock) { _firmwareUpdated = false; }
                    sdPCRBoard.AddCommand("/0FR", Notification.OnError, LogResponses);
                    CmdsSent.Enqueue("/0FR");
                }
            }

            public bool ReadLogInfo(out string[] LogText)
            {
                List<string> tmpList = new List<string>(0);
                string text;
                while (logInfo.TryDequeue(out text))
                    tmpList.Add(text);
                LogText = tmpList.ToArray();
                if (LogText.Length > 0)
                    return true;
                else
                    return false;
            }
            public bool ReadResponses(out ResponseItem Resp)
            {
                bool res = ResponsesToCaller.TryDequeue(out ResponseItem resp);
                if (res)
                    Resp = resp.Clone();
                else
                    Resp = new ResponseItem();
                return res;
            }
            public bool ReadStatusMessages(out ResponseItem Resp)
            {
                bool res = ResponsesFromBoard.TryDequeue(out ResponseItem resp);
                if (res)
                    Resp = resp.Clone();
                else
                    Resp = new ResponseItem();
                return res;
            }
            void CheckBoardResponseList(object sender, ElapsedEventArgs e)
            {
                if (responseTimerCount % 5 == 0 && consoleOutput)
                    Console.WriteLine(String.Format("CheckBoardResponse running - {0}", responseTimerCount));

                responseTimerCount++;
                while (ResponsesFromBoard.TryDequeue(out resp))
                {
                    if (resp.InstrumentSignal)
                    {
                        ResponsesFromBoard.Enqueue(resp.Clone());
                    }
                    else if (CmdsSent.Count < 1)
                    {
                        resp.InstrumentSignal = true;
                        ResponsesFromBoard.Enqueue(resp.Clone());
                    }
                    else
                    {
                        string cmd;
                        if (CmdsSent.TryDequeue(out cmd))
                        {
                            if (cmd.IndexOf(resp.Command) >= 0)
                            {
                                int idx = -1;
                                for (int n = 0; n < defaultList.Count; n++)
                                {
                                    if (cmd.IndexOf(defaultList[n]) >= 0)
                                    {
                                        idx = n;
                                        break;
                                    }
                                }
                                if (idx == 0)
                                {
                                    lock (processorLock)
                                    {
                                        _firmware = resp.Response.Substring(2);
                                        _firmwareUpdated = true;
                                    }
                                }
                                if (resp.Notify == Notification.Always)
                                    ResponsesToCaller.Enqueue(resp.Clone());
                                else if (resp.Notify == Notification.OnError)
                                {
                                    if (resp.Response.IndexOf("/0B") >= 0 
                                        || resp.Response.IndexOf("/0C") >= 0 
                                        || resp.Response.IndexOf("/0E") >= 0)
                                        ResponsesToCaller.Enqueue(resp.Clone());
                                }
                                else if (resp.Notify != Notification.Never)
                                    ResponsesToCaller.Enqueue(resp.Clone());
                            }
                        }
                    }
                }
                if (ResponsesFromBoard.Count <= 0)
                {
                    responseTimer.AutoReset = false;
                    responseTimer.Enabled = false;
                }
            }
        }
    } // namespace Hardware
}
