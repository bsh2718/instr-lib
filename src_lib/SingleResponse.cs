﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
//using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Timers;
using System.IO;
using System.IO.Ports;

namespace sdpcrInstrLib
{
    using BoardComm;

    namespace OneResponse
    {
        class SingleResponse : IDisposable
        {
            ConcurrentQueue<ResponseItem> responseList;
            ControlBoard sdPCRBoard;
            internal SingleResponse(ControlBoard Board)
            {
                sdPCRBoard = Board;
                responseList = new ConcurrentQueue<ResponseItem>();
            }

            internal void LogResponse(ResponseItem Resp)
            {
                responseList.Enqueue(Resp.Clone());
            }

            internal bool GetResponse(out ResponseItem Resp)
            {
                bool res = responseList.TryDequeue(out ResponseItem resp);
                if (res)
                    Resp = resp.Clone();
                else
                    Resp = new ResponseItem();
                return res;
            }

            internal bool GetResponse(int Delay, out ResponseItem Resp)
            {
                if (Delay > 0)
                    System.Threading.Thread.Sleep(Delay);

                bool res = responseList.TryDequeue(out ResponseItem resp);
                if (res)
                    Resp = resp.Clone();
                else
                    Resp = new ResponseItem();
                return res;
            }

            internal bool AddCommand(string Cmd)
            {
                return sdPCRBoard.AddCommand(Cmd, Notification.Always, LogResponse);
            }

            public void Dispose()
            {
            }
        }
    }
}