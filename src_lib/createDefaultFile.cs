﻿using System;
//using System.Collections.Generic;
//using System.Collections.Concurrent;
//using System.Linq;
//using System.Xml;
using System.Xml.Linq;
//using System.Text;
//using System.Threading;
//using System.Threading.Tasks;
//using System.Diagnostics;
//using System.Timers;
//using System.IO;
//using System.IO.Ports;

namespace sdpcrInstrLib
{
    namespace CreateDefaultFile
    {
        public struct DyeInformation
        {
            public string Name;
            public string LEDName;
            public string FilterName;
            public int LEDPosition;
            public int FilterPosition;
            public int Gain;
            public int Exposure;
        }

        public static class WriteDefaultXML
        {
            public static void Config(string OutputDir, string BaseName)
            {
                CreateDefaultXML cxml = new CreateDefaultXML();
                XDocument xd = cxml.DefaultXML;
                string path = String.Format("{0}\\{1}.xml", OutputDir, BaseName);
                using (System.IO.StreamWriter file = new System.IO.StreamWriter(@path))
                { file.WriteLine(xd.ToString()); }
            }
        }

        public class CreateDefaultXML
        {
            public XDocument DefaultXML { get; }
            public CreateDefaultXML()
            {
                DefaultXML = new XDocument();
                var rootElement = new XElement("Configuration");
                DefaultXML.Add(rootElement);
               
                XElement xe = DefaultRotorNode();
                rootElement.Add(xe);
                xe = DefaultXStageNode();
                rootElement.Add(xe);
                xe = DefaultYStageNode();
                rootElement.Add(xe);
                xe = DefaultFilterWheelNode();
                rootElement.Add(xe);
                xe = DefaultThermistorNode();
                rootElement.Add(xe);
                xe = DefaultPIDNode();
                rootElement.Add(xe);
                xe = DefaultLEDNode();
                rootElement.Add(xe);
                xe = DefaultFilterNode();
                rootElement.Add(xe);
                xe = DefaultDyeNode();
                rootElement.Add(xe);
            }
            public void WriteDefaultXML(string OutputDir, string BaseName)
            {
                string path = String.Format("{0}\\{1}.xml", OutputDir, BaseName);
                using (System.IO.StreamWriter file = new System.IO.StreamWriter(@path))
                { file.WriteLine(DefaultXML.ToString()); }
            }
            internal XElement DefaultRotorNode()
            {
                XElement xe;
                StepperMotorNode(out xe, "Rotor", 1, 1, 2, 20, 300, 4000);
                var xMotorProfiles = new XElement("Profiles",
                    StepperMotorProfile(2, 1, 150),
                    StepperMotorProfile(3, 1, 1500));
                xe.Add(xMotorProfiles);
                var xMotorPositions = new XElement("Program",
                    new XAttribute("Type", "Position"),
                    new XAttribute("Profile", "2"),
                    new XAttribute("Velocity", "60"),
                    new XAttribute("Name", "Rotor 4 Positions"),
                    new XElement("Positions",
                        StepperMotorPositionNode(0, 100),
                        StepperMotorPositionNode(1, 900),
                        StepperMotorPositionNode(2, 1700),
                        StepperMotorPositionNode(3, 2500)
                    )) ;
                xe.Add(xMotorPositions);
                var xMotorVelocities = new XElement("Program",
                    new XAttribute("Type", "Velocity"),
                    new XAttribute("Profile", "3"),
                    new XAttribute("Velocity", "1"),
                    new XAttribute("Name", "Rotor Velocity 1"),
                    new XElement("Velocities",
                        StepperMotorVelocityNode(0, 500, 60.0, "Spin Up"),
                        StepperMotorVelocityNode(1, 700, 60.0, "Digitize"), // 900
                        StepperMotorVelocityNode(2, 800, 60.0, "Clear"),    // 1200
                        StepperMotorVelocityNode(3, 400, 90.0, "Spin Down")
                        ));
                xe.Add(xMotorVelocities);
                return xe;
            }
            internal XElement DefaultXStageNode()
            {
                StepperMotorNode(out XElement xe, "XStage", 2, 1, 1, 200, 400, 22000);
                var xMotorProfiles = new XElement("Profiles",
                    StepperMotorProfile(1, 1, 1200));
                xe.Add(xMotorProfiles);
                var xMotorPositions = new XElement("Program",
                    new XAttribute("Type", "Position"),
                    new XAttribute("Profile", "1"),
                    new XAttribute("Velocity", "200"),
                    new XAttribute("Name", "XStage 3 Positions"),
                    StepperMotorPositionNode(0, 400),
                        StepperMotorPositionNode(1, 2400),
                        StepperMotorPositionNode(2, 4400)
                        );
                xe.Add(xMotorPositions);
                return xe;
            }
            internal XElement DefaultYStageNode()
            {
                StepperMotorNode(out XElement xe, "YStage", 3, 0, 1, 200, 400, 22000);
                var xMotorProfiles = new XElement("Profiles",
                    StepperMotorProfile(1, 1, 1200));
                xe.Add(xMotorProfiles);
                var xMotorPositions = new XElement("Program",
                    new XAttribute("Type", "Position"),
                    new XAttribute("Profile", "1"),
                    new XAttribute("Velocity", "200"),
                    new XAttribute("Name", "YStage 2 Positions"),
                        StepperMotorPositionNode(0, 400),
                        StepperMotorPositionNode(1, 2400)
                        );
                xe.Add(xMotorPositions);
                xMotorPositions = new XElement("Program",
                    new XAttribute("Type", "Position"),
                    new XAttribute("Profile", "1"),
                    new XAttribute("Velocity", "200"),
                    new XAttribute("Name", "YStage 3 Positions"),
                        StepperMotorPositionNode(0, 400),
                        StepperMotorPositionNode(1, 1800),
                        StepperMotorPositionNode(2, 3000)
                    );
                
                xe.Add(xMotorPositions);
                return xe;
            }
            internal XElement DefaultFilterWheelNode()
            {
                StepperMotorNode(out XElement xe, "FilterWheel", 4, 1, 1, 20, 200, 4000);
                var xMotorProfiles = new XElement("Profiles",
                    StepperMotorProfile(0, 1, 20),
                    StepperMotorProfile(1, 1, 20));
                xe.Add(xMotorProfiles);
                var xMotorPositions = new XElement("Program",
                    new XAttribute("Type", "Position"),
                    new XAttribute("Profile", "0"),
                    new XAttribute("Velocity", "30"),
                    new XAttribute("Name", "FilterWheel 8 Positions"),
                        StepperMotorPositionNode(0, 100),
                        StepperMotorPositionNode(1, 500),
                        StepperMotorPositionNode(2, 900),
                        StepperMotorPositionNode(3, 1300),
                        StepperMotorPositionNode(4, 1700),
                        StepperMotorPositionNode(5, 2100),
                        StepperMotorPositionNode(6, 2500),
                        StepperMotorPositionNode(7, 2900)
                    );
                xe.Add(xMotorPositions);
                return xe;
            }
            //new XAttribute("MaximumVelocity", String.Format("{0}", MaximumVelocity)),
            //new XAttribute("MaximumProfile", String.Format("{0}", MaximumProfile)),

            internal XElement DefaultThermistorNode()
            {
                var xTherm = new XElement("Thermistors",
                    new XAttribute("ThermistorWeights", "100,100,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0"),
                    new XAttribute("ThermistorBiases", "0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00")
                    );
                return xTherm;
            }
            internal XElement DefaultPIDNode()
            {
                var xPID = new XElement("PID",
                    new XAttribute("IdleTemperature", "40.0"),
                    new XAttribute("LowTemperature", "60.0"),
                    new XAttribute("HighTemperature", "95.0"));
                var tProg = new XElement("Program",
                    new XAttribute("Type", "Temperature"),
                    new XAttribute("Name", "PCR 1"),
                    TemperatureProgramNode(0, 60.0, 60.0, "Setup"),
                    TemperatureProgramNode(1, 95.0, 300.0, "PreSoak")
                    );
                int numbSteps = 19;
                int nextStep = 2;
                for (int n = 0; n < numbSteps; n++)
                {
                    int step = (n + 1) * 2;
                    tProg.Add(TemperatureProgramNode(step, 60.0, 60.0, String.Format("{0}.1", n + 1)));
                    tProg.Add(TemperatureProgramNode(step + 1, 95.0, 60.0, String.Format("{0}.2", n + 1)));
                    nextStep += 2;
                }
                tProg.Add(TemperatureProgramNode(nextStep, 60.0, 60.0, String.Format("{0}.1", numbSteps + 1)));
                tProg.Add(TemperatureProgramNode(nextStep + 1, 75.0, 60.0, String.Format("{0}.2", numbSteps + 1)));
                tProg.Add(TemperatureProgramNode(nextStep + 2, 40.0, 245.0, "Cool Down"));
                xPID.Add(tProg);

                tProg = new XElement("Program",
                        new XAttribute("Type", "Temperature"),
                        new XAttribute("Name", "PCR 2"),
                        TemperatureProgramNode(0, 60.0, 60.0, "Setup"),
                        TemperatureProgramNode(1, 95.0, 300.0, "PreSoak")
                        );
                numbSteps = 3;
                nextStep = 2;
                for (int n = 0; n < numbSteps; n++)
                {
                    int step = (n + 1) * 2;
                    tProg.Add(TemperatureProgramNode(step, 60.0, 60.0, String.Format("{0}.1", n + 1)));
                    tProg.Add(TemperatureProgramNode(step + 1, 95.0, 60.0, String.Format("{0}.2", n + 1)));
                    nextStep += 2;
                }
                tProg.Add(TemperatureProgramNode(nextStep, 60.0, 60.0, String.Format("{0}.1", numbSteps + 1)));
                tProg.Add(TemperatureProgramNode(nextStep + 1, 75.0, 60.0, String.Format("{0}.2", numbSteps + 1)));
                tProg.Add(TemperatureProgramNode(nextStep + 2, 40.0, 245.0, "Cool Down"));
                xPID.Add(tProg);

                tProg = new XElement("Program",
                    new XAttribute("Type", "Temperature"),
                        new XAttribute("Name", "Isothermal 1"),
                        TemperatureProgramNode(0, 60.0, 30.0, "Setup"),
                        TemperatureProgramNode(1, 95.0, 90.0, "PreSoak"),
                        TemperatureProgramNode(2, 60.0, 600.0, "Isothermal"),
                        TemperatureProgramNode(3, 40.0, 180.0, "Cool Down"));
                xPID.Add(tProg);

                return xPID;
            }
            internal XElement DefaultLEDNode()
            {
                XElement xe = new XElement("LEDs",
                    LEDNode(0, "L350/30", 50, 50),
                    LEDNode(1, "L400/30", 50, 50),
                    LEDNode(2, "L500/30", 50, 50),
                    LEDNode(3, "L550/30", 50, 50),
                    LEDNode(4, "L600/30", 50, 50),
                    LEDNode(5, "L650/30", 50, 50)
                    );
                return xe;
            }
            internal XElement DefaultFilterNode()
            {
                XElement xe = new XElement("Filters",
                    FilterNode("VeryBlue", 0),
                    FilterNode("Blue", 1),
                    FilterNode("Green", 2),
                    FilterNode("Orange", 3),
                    FilterNode("Red", 4),
                    FilterNode("DeepRed", 5),
                    FilterNode("Glass", 6),
                    FilterNode("Air", 7)
                    );
                return xe;
            }
            internal XElement DefaultDyeNode()
            {
                XElement xe = new XElement("Dyes",
                    DyeNode("Dye-A", "L350/30", "VeryBlue", 383, 4000),
                    DyeNode("Dye-B", "L400/30", "Blue", 383, 4000),
                    DyeNode("Dye-C", "L500/30", "Green", 383, 4000),
                    DyeNode("Dye-D", "L550/30", "Orange", 383, 4000),
                    DyeNode("Dye-E", "L600/30", "Red", 383, 4000),
                    DyeNode("Dye-F", "L650/30", "DeepRed", 383, 4000),
                    DyeNode("BrightField", "L550/30", "Glass", 100, 100),
                    DyeNode("BigStokes", "L500/30", "DeepRed", 383, 4000)
                    );
                return xe;
            }
            void StepperMotorNode(out XElement XE, string MotorName, int MotorNumber,
                int Direction, int InitProfile, int InitVelocity, int Backout, int MaximumPosition)
            {
                XE = new XElement("StepperMotor",
                    new XAttribute("Velocity", String.Format("{0}", InitVelocity)),
                    new XAttribute("Profile", String.Format("{0}", InitProfile)),
                    new XAttribute("MaximumPosition", String.Format("{0}", MaximumPosition)),
                    new XAttribute("BackoutFromHome", String.Format("{0}", Backout)),                    
                    new XAttribute("DirectionCode", String.Format("{0}", Direction)),
                    new XAttribute("Number", String.Format("{0}", MotorNumber)),
                    new XAttribute("Name", MotorName)
                    );
            }
            XElement StepperMotorProfile(int Number, int MinVelocity, int MaxVelocity)
            {
                XElement xe = new XElement("Profile",
                    new XAttribute("Number", String.Format("{0}", Number)),
                    new XAttribute("MinimumVelocity", String.Format("{0}", MinVelocity)),
                    new XAttribute("MaximumVelocity", String.Format("{0}", MaxVelocity)));
                return xe;
            }
            XElement StepperMotorPositionNode(int Number, int Position)
            {
                XElement xe = new XElement("Step",
                    new XAttribute("Position", String.Format("{0}", Position)),
                    new XAttribute("Number", String.Format("{0}", Number))
                );
                return xe;
            }
            XElement StepperMotorVelocityNode(int Number, int Velocity, double Time, string Description)
            {
                XElement xe = new XElement("Step",
                    new XAttribute("Description", Description),
                    new XAttribute("Time", String.Format("{0:0.00}", Time)),
                    new XAttribute("Velocity", String.Format("{0}", Velocity)),
                    new XAttribute("Number", String.Format("{0}", Number))
                );
                return xe;
            }
            XElement TemperatureProgramNode(int StepNumb, double Temp, double Time, string Description)
            {
                XElement xe = new XElement("Step",
                new XAttribute("Description", Description),
                new XAttribute("Time", String.Format("{0:0.00}", Time)),
                new XAttribute("Temperature", String.Format("{0:0.00}", Temp)),
                new XAttribute("Number", String.Format("{0}", StepNumb))
                );
                return xe;
            }
            XElement LEDNode(int LEDNumber, string LEDName, int MaxInten, int InitInten)
            {
                XElement xe = new XElement("LED",
                    new XAttribute("MaxIntensity", String.Format("{0}", MaxInten)),
                    new XAttribute("Number", String.Format("{0}", LEDNumber)),
                    new XAttribute("Name", LEDName),
                    new XAttribute("Intensity", String.Format("{0}", InitInten))
                    );
                return xe;
            }
            XElement FilterNode(string FilterName, int FilterWheelPosition)
            {
                XElement xe = new XElement("Filter",
                    new XAttribute("FilterPosition", String.Format("{0}", FilterWheelPosition)),
                    new XAttribute("FilterName", FilterName)
                    );
                return xe;
            }
            XElement DyeNode(string DyeName, string LEDName, string FilterName, int Gain, int Exposure)
            {
                XElement xe = new XElement("Dye",
                    new XAttribute("ExposureTime", String.Format("{0}", Exposure)),
                    new XAttribute("Gain", String.Format("{0}", Gain)),
                    new XAttribute("FilterName", FilterName),
                    new XAttribute("LEDName", LEDName),
                    new XAttribute("Name", DyeName)
                    );
                return xe;
            }   
        }  
    }
}

//StepperMotorNode(out XElement xe, "Rotor", 1, 1, 1, 2000, 0, 20, 300, 4000);
//var xMotorPositions = new XElement("Program",
//    new XAttribute("Name", "Rotor 4 Positions"),
//    new XElement("Positions",
//        StepperMotorPositionNode(0, 100),
//        StepperMotorPositionNode(1, 900),
//        StepperMotorPositionNode(2, 1700),
//        StepperMotorPositionNode(3, 2500)
//    ));
//xe.Add(xMotorPositions);
//rootElement.Add(xe);

//StepperMotorNode(out xe, "XStage", 2, 1, 0, 1000, 0, 200, 400, 40000);
//xMotorPositions = new XElement("Program",
//    new XAttribute("Name", "XStage 3 Positions"),
//    StepperMotorPositionNode(0, 400),
//        StepperMotorPositionNode(1, 2400),
//        StepperMotorPositionNode(2, 4400)
//        );
//xe.Add(xMotorPositions);
//rootElement.Add(xe);

//StepperMotorNode(out xe, "YStage", 3, 0, 0, 1000, 0, 200, 400, 40000);
//xMotorPositions = new XElement("Program",
//    new XAttribute("Name", "YStage 2 Positions"),
//        StepperMotorPositionNode(0, 400),
//        StepperMotorPositionNode(1, 2400)
//        );
//xe.Add(xMotorPositions);

//xMotorPositions = new XElement("Program",
//    new XAttribute("Name", "YStage 3 Positions"),
//        StepperMotorPositionNode(0, 400),
//        StepperMotorPositionNode(1, 1800),
//        StepperMotorPositionNode(2, 3000)
//    );
//xe.Add(xMotorPositions);
//rootElement.Add(xe);

//StepperMotorNode(out xe, "FilterWheel", 4, 1, 0, 200, 0, 20, 200, 4000);
//xMotorPositions = new XElement("Program",
//    new XAttribute("Name", "FilterWheel 8 Positions"),
//        StepperMotorPositionNode(0, 100),
//        StepperMotorPositionNode(1, 500),
//        StepperMotorPositionNode(2, 900),
//        StepperMotorPositionNode(3, 1300),
//        StepperMotorPositionNode(4, 1700),
//        StepperMotorPositionNode(5, 2100),
//        StepperMotorPositionNode(6, 2500),
//        StepperMotorPositionNode(7, 2900)
//    );
//xe.Add(xMotorPositions);
//rootElement.Add(xe);
//var xTherm = new XElement("Thermistors",
//    new XElement("ThermistorWeights", "100,100,100,100,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0"),
//    new XElement("ThermistorBiases", "0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00")
//    );



//var xPID = new XElement("PID",
//    new XElement("IdleTemperature", "40.0"),
//    new XElement("LowTemperature", "60.0"),
//    new XElement("HighTemperature", "95.0"));
//var tProg = new XElement("Program",
//        new XAttribute("Name", "PID 1"),
//        TemperatureProgramNode(0, 60.0, 60.0, "Setup"),
//        TemperatureProgramNode(1, 95.0, 300.0, "PreSoak")
//        );
//int numbSteps = 19;
//int nextStep = 2;
//for (int n = 0; n < numbSteps; n++)
//{
//    int step = (n + 1) * 2;
//    tProg.Add(TemperatureProgramNode(step, 60.0, 60.0, String.Format("{0}.1", n + 1)));
//    tProg.Add(TemperatureProgramNode(step + 1, 95.0, 60.0, String.Format("{0}.2", n + 1)));
//    nextStep += 2;
//}
//tProg.Add(TemperatureProgramNode(nextStep, 60.0, 60.0, String.Format("{0}.1", numbSteps + 1)));
//tProg.Add(TemperatureProgramNode(nextStep + 1, 75.0, 60.0, String.Format("{0}.2", numbSteps + 1)));
//tProg.Add(TemperatureProgramNode(nextStep + 2, 40.0, 245.0, "Cool Down"));
//xPID.Add(tProg);
//rootElement.Add(xPID);

//XElement xLEDs = new XElement("LEDs",
//    LEDNode(0, "L350/30", 50, 50),
//    LEDNode(1, "L400/30", 50, 50),
//    LEDNode(2, "L500/30", 50, 50),
//    LEDNode(3, "L550/30", 50, 50),
//    LEDNode(4, "L600/30", 50, 50),
//    LEDNode(5, "L650/30", 50, 50)
//    );
//rootElement.Add(xLEDs);

//XElement xFilters = new XElement("Filters",
//    FilterNode("VeryBlue", 0),
//    FilterNode("Blue", 1),
//    FilterNode("Green", 2),
//    FilterNode("Orange", 3),
//    FilterNode("Red", 4),
//    FilterNode("DeepRed", 5),
//    FilterNode("Glass", 6),
//    FilterNode("Air", 7)
//    );
//rootElement.Add(xFilters);

//XElement xDyes = new XElement("Dyes",
//    DyeNode("Dye-A", "L350/30", "VeryBlue", 383, 4000),
//    DyeNode("Dye-B", "L400/30", "Blue", 383, 4000),
//    DyeNode("Dye-C", "L500/30", "Green", 383, 4000),
//    DyeNode("Dye-D", "L550/30", "Orange", 383, 4000),
//    DyeNode("Dye-E", "L600/30", "Red", 383, 4000),
//    DyeNode("Dye-F", "L650/30", "DeepRed", 383, 4000),
//    DyeNode("BrightField", "L550/30", "Glass", 100, 100),
//    DyeNode("BigStokes", "L500/30", "DeepRed", 383, 4000)
//    );
//rootElement.Add(xDyes);

