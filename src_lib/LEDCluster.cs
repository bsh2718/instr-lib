﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace sdpcrInstrLib
{
    using BoardComm;
    using Hardware;
    using OneResponse;

    namespace LEDCluster
    {

        static class LIdx
        {
            // Count must be set to the number of items below
            public const int Initialize = 0;
            public const int Setintensity = 1;
            public const int On = 2;
            public const int Off = 3;

            // This must The number ofitems above
            public const int Count = 4;

        }
        public class LEDCtl : HardwareBase
        {
            public void Response(ResponseItem Resp)
            {
                ResponsesFromBoard.Enqueue(Resp.Clone());
            }
            readonly int[] _maxCurrent;
            int[] _intensity;
            bool[] _ledOn;
            int _numbLEDs;
            protected System.Timers.Timer responseTimer;
            //int boardWaitTime;
            private static readonly object ledLock = new object();
            bool[] _initialized;
            //bool _state;
            SingleResponse startup;
            string[,] CmdList;

            public LEDCtl(ControlBoard Board, int NumbLEDs, int[] Max, int[] InitIntensity, out bool IsConnected) : base()
            {
                sdPCRBoard = Board;
                if (NumbLEDs < 1 || NumbLEDs != Max.Count() || NumbLEDs != InitIntensity.Count())
                {
                    IsConnected = false;
                    _connected = IsConnected;
                }
                else if (sdPCRBoard != null)
                {
                    IsConnected = true;
                    lock (ledLock)
                    {
                        CmdList = new string[NumbLEDs, LIdx.Count];
                        _maxCurrent = new int[NumbLEDs];
                        _intensity = new int[NumbLEDs];
                        _ledOn = new bool[NumbLEDs];
                        _initialized = new bool[NumbLEDs];
                        _numbLEDs = NumbLEDs;
                        for (int n = 0; n < NumbLEDs; n++)
                        {
                            _ledOn[n] = false;
                            _initialized[n] = false;
                            if (Max[n] > 0 && Max[n] < 101)
                            {
                                _maxCurrent[n] = Max[n];
                                CmdList[n, LIdx.Initialize] = String.Format("/2{0}LR", n);
                                CmdList[n, LIdx.Setintensity] = String.Format("/2{0}l", n);
                                CmdList[n, LIdx.On] = String.Format("/2{0}S1R", n);
                                CmdList[n, LIdx.Off] = String.Format("/2{0}S0R", n);
                                IsConnected = true;
                            }
                            else
                            {
                                IsConnected = false;
                                break;
                            }
                        }
                    }

                    if (IsConnected)
                    {
                        sdPCRBoard = Board;
                        BoardWaitTime = sdPCRBoard.BoardWaitTime;
                        startup = new SingleResponse(sdPCRBoard);
                        if (TestConnection())
                        {
                            responseTimer = new System.Timers.Timer(TimeStep);
                            // hook up the elapsed event for the timer. 
                            responseTimer.AutoReset = false;
                            responseTimer.Enabled = false;
                            responseTimer.Elapsed += CheckBoardResponseList;
                            lock (ledLock)
                            {
                                for (int n = 0; n < NumbLEDs; n++)
                                {
                                    if (InitIntensity[n] < 1)
                                        _intensity[n] = 1;
                                    else if (InitIntensity[n] > _maxCurrent[n])
                                        _intensity[n] = _maxCurrent[n];
                                    else
                                        _intensity[n] = InitIntensity[n];
                                }
                            }
                            IsConnected = true;
                        }
                        else
                            IsConnected = false;
                    }
                    else
                        IsConnected = false;
                }
                else
                    IsConnected = false;

                _connected = IsConnected;
            }
            bool TestConnection()
            {
                int wait = 2 * BoardWaitTime;
                bool allInitialized = true;
                for (int m = 0; m < _numbLEDs; m++)
                {
                    startup.AddCommand(CmdList[m, LIdx.Initialize]);
                    ResponseItem resp;
                    for (int n = 0; n < 8; n++)
                    {
                        lock (ledLock)
                        {
                            if (startup.GetResponse(wait, out resp))
                            {
                                //Console.WriteLine(resp.ToString());
                                if (resp.Response.IndexOf(DefaultResponses[0]) >= 0)
                                {
                                    _initialized[m] = true;
                                    break;
                                }
                                else
                                    _initialized[m] = false;
                            }
                            else
                                _initialized[m] = false;
                        }
                    }
                    lock (ledLock)
                    {
                        allInitialized = (allInitialized && _initialized[m]);
                    }
                }
                return allInitialized;
            }
            public void Dispose()
            {
                if (responseTimer.Enabled)
                    responseTimer.Dispose();
            }
            ~LEDCtl()
            {

            }
            public bool SetIntensity(int N, int Inten)
            {
                if (N >= 0 && N < _numbLEDs && Inten > 0 && Inten <= _maxCurrent[N])
                {
                    lock (ledLock)
                    {
                        _intensity[N] = Inten;
                        string c = String.Format("{0}{1}R", CmdList[N, LIdx.Setintensity], _intensity[N]);
                        sdPCRBoard.AddCommand(c, Notification.Always, Response);
                        CmdsSent.Enqueue(c);
                        //Console.WriteLine(String.Format("Set Intensity {0}", c));
                    }
                    return true;
                }
                else
                    return false;
            }

            public void On(int N)
            {
                if (N >= 0 && N < _numbLEDs)
                {
                    for (int m = 0; m < _numbLEDs; m++)
                    {
                        if (m != N && _ledOn[m])
                        {
                            sdPCRBoard.AddCommand(CmdList[m, LIdx.Off], Notification.OnError, Response);
                            CmdsSent.Enqueue(CmdList[m, LIdx.Off]);
                            _ledOn[m] = false;
                        }
                    }
                    _ledOn[N] = true;
                    sdPCRBoard.AddCommand(CmdList[N, LIdx.On], Notification.OnError, Response);
                    CmdsSent.Enqueue(CmdList[N, LIdx.On]);
                }
            }
            public void Off(int N)
            {
                if (N >= 0 && N < _numbLEDs)
                {
                    sdPCRBoard.AddCommand(CmdList[N, LIdx.Off], Notification.OnError, Response);
                    CmdsSent.Enqueue(CmdList[N, LIdx.Off]);
                    _ledOn[N] = false;
                }
            }
            public void Init(int N)
            {
                if (N >= 0 && N < _numbLEDs)
                {
                    sdPCRBoard.AddCommand(CmdList[N, 0], Notification.OnError, Response);
                    CmdsSent.Enqueue(CmdList[N, 0]);
                }
            }
            protected override void LogResponses(ResponseItem Resp)
            {
                ResponsesFromBoard.Enqueue(Resp.Clone());
                //responseTimerCount = 0;
                responseTimer.AutoReset = true;
                responseTimer.Start();
            }
            public override string ToString()
            {
                int on = -1;
                for (int m = 0; m < _numbLEDs; m++)
                {
                    if (_ledOn[m])
                    {
                        on = m;
                        break;
                    }
                }
                if (on < 0)
                    return base.ToString() + String.Format("Number of LEDs = {0}, All LEDS are off", _numbLEDs);
                else
                    return base.ToString() + String.Format("Number of LEDs = {0}, LED {1} is on", _numbLEDs, on);

            }
            public string ToString(int N)
            {
                if (N >= 0 && N < _numbLEDs)
                {
                    if (_ledOn[N])
                        return base.ToString() + String.Format("LED {0} is on\n", N);
                    else
                        return base.ToString() + String.Format("LED {0} is off\n", N);
                }
                else
                    return "Index out of range\n";

            }

            void CheckBoardResponseList(object sender, ElapsedEventArgs e)
            {
                ResponseItem resp;
                while (ResponsesFromBoard.TryDequeue(out resp))
                {
                    if (resp.InstrumentSignal)
                    {
                        ResponsesToCaller.Enqueue(resp.Clone());
                    }
                    else if (CmdsSent.Count < 1)
                    {
                        //resp.InstrumentSignal = true;
                        ResponsesToCaller.Enqueue(resp.Clone());
                    }
                    else
                    {
                        string cmd;
                        if (CmdsSent.TryDequeue(out cmd))
                        {
                            int iCmd = cmd.IndexOf(resp.Command);
                            if (iCmd < 0)
                                iCmd = resp.Command.IndexOf(cmd);
                            if (iCmd >= 0)
                            {
                                int cmdIdx = -1;
                                int ledIdx = -1;
                                for (int m = 0; m < _numbLEDs; m++)
                                {
                                    for (int n = 0; n < CmdList.Length; n++)
                                    {
                                        if (cmd.IndexOf(CmdList[m, n]) == 0)
                                        {
                                            cmdIdx = n;
                                            ledIdx = m;
                                            break;
                                        }
                                    }
                                    int resIdx = -1;
                                    for (int n = 0; n < DefaultResponses.Count; n++)
                                    {
                                        if (resp.Response.IndexOf(DefaultResponses[n]) >= 0)
                                        {
                                            //resIdx = n;
                                            break;
                                        }
                                        bool error;
                                        if (resIdx == 0)
                                            error = false;
                                        else
                                            error = true;
                                        switch (cmdIdx)
                                        {
                                            case 0:
                                                if (resIdx == 0)
                                                    _initialized[ledIdx] = true;
                                                else
                                                    _initialized[ledIdx] = false;
                                                break;
                                            case 1:
                                                if (resIdx == 0)
                                                {
                                                    int last = cmd.Length - 1;
                                                    string numbStr = cmd.Substring(4, cmd.Length - 5);
                                                    int numb;
                                                    if (Int32.TryParse(numbStr, out numb))
                                                    {
                                                        if (numb > 0 && numb <= _maxCurrent[ledIdx])
                                                            _intensity[ledIdx] = numb;
                                                        else
                                                            error = true;
                                                    }
                                                }
                                                break;
                                            case 2:
                                                if (resIdx == 0)
                                                    _ledOn[ledIdx] = true;
                                                break;
                                            case 3:
                                                if (resIdx == 0)
                                                    _ledOn[ledIdx] = false;
                                                break;
                                            default:
                                                break;
                                        }

                                        if (resp.Notify == Notification.Always)
                                            ResponsesToCaller.Enqueue(resp.Clone());
                                        else if (error && resp.Notify == Notification.OnError)
                                            ResponsesToCaller.Enqueue(resp.Clone());
                                    }
                                }
                            }
                        }
                    }
                    if (ResponsesFromBoard.Count <= 0)
                    {
                        responseTimer.AutoReset = false;
                        responseTimer.Enabled = false;

                    }
                }
            }

        }
    }
}
