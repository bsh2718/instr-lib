﻿//#define TRACE
#undef TRACE
using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.IO;
using System.IO.Ports;
using System.Diagnostics;
using System.Timers;
using sdpcrInstrLib.ErrorCodes;

namespace sdpcrInstrLib
{
    public enum Notification
    {
        Never,
        Always,
        OnError
    }
    public class ResponseItem
    {
        public bool InstrumentSignal;
        public string Message;
        public string Command;
        public string Response;
        public long Time;
        public long ElapsedTime;
        public Notification Notify;

        public ResponseItem()
        {
            Clear();
        }
        public void Clear()
        {
            InstrumentSignal = false;
            Message = "";
            Command = "";
            Response = "";
            Time = 0;
            ElapsedTime = 0;
            Notify = Notification.Never;
        }
        public ResponseItem Clone()
        {
            ResponseItem a = new ResponseItem();
            a.InstrumentSignal = InstrumentSignal;
            a.Message = Message;
            a.Command = Command;
            a.Response = Response;
            a.Time = Time;
            a.ElapsedTime = ElapsedTime;
            a.Notify = Notify;
            return a;
        }

        public override string ToString()
        {
            //string ntfy = "";
            //switch (Notify)
            //{
            //    case Notification.Never:
            //        ntfy = "Never";
            //        break;
            //    case Notification.Always:
            //        ntfy = "Always";
            //        break;
            //    case Notification.OnError:
            //        ntfy = "On Error";
            //        break;
            //}
            return String.Format("Message: {0}\n Comm: {1} / Resp: {2} / Time: {3} / Elapsed: {4} / Notify: {5} / Instr Signal: {6}",
                Message, Command, Response, Time, ElapsedTime, Notify.ToString(), InstrumentSignal.ToString());
        }

        public bool NotifyCaller()
        {
            if (Notify == Notification.Always || InstrumentSignal ||
                (Notify == Notification.OnError && (Response.IndexOf("/0B") >= 0 || Response.IndexOf("/0C") >= 0 
                || Response.IndexOf("/0S") >= 0 || Response.IndexOf("/0E") >= 0)))            
                return true;
            else
                return false;
        }
    }
    namespace BoardComm
    {
        public delegate void EnqueueResponse(ResponseItem Resp);
        public delegate void EnqueueInfo(string str);
        public delegate long GetBoardTime();
       
        public struct QueueItem
        {
            public string Command;
            public EnqueueResponse Sender;
            public long Time;
            public Notification Notify;
        }

        public class ControlBoard
        {
            private readonly char[] boardWhiteSpace = { ' ', '\n', '\r', '\t' };  // space, new line, carriage return, tab
            private readonly bool logResponses;
            private readonly bool logCommands;
            private readonly bool logConnection;
            private static readonly object waitingLock = new object();
            bool _waitingForResponse;
            public int BoardWaitTime
            {
                get;
                private set;
            }
            public bool WaitingForResponse
            {
                get
                {
                    bool res;
                    lock (waitingLock)
                    {
                        res = _waitingForResponse;
                    }
                    return res;
                }
            }
            internal void ClearWaiting()
            {
                lock (waitingLock)
                {
                    _waitingForResponse = false;
                }
            }
            private int _serialWaitTime = 5;
            private readonly char[] responseDelimiters = { '>' };
            private readonly char[] whiteSpace = { ' ', '\n', '\r', '\t', ',' }; // space, new line, carriage return, tab, comma
            private void BlankQueue(ResponseItem Resp) { }
            private static readonly object bufferLock = new object();
            StringBuilder inputBuffer = new StringBuilder();

            SerialPort _serialPort;
            public ConcurrentQueue<QueueItem> CommandQueue;
            bool _continue;
            
            static EnqueueResponse DefaultQueue;
            static EnqueueInfo LogInfo;
            static Stopwatch stopW = new Stopwatch();
            private Thread monitorThread;
            public ProcessErrorCode ParseError;
            public ControlBoard(EnqueueResponse DQ, EnqueueInfo LI, bool TrackConnect, bool TrackComm, bool TrackResp, out List<string> PortNames)
            {
                BoardWaitTime = 40;
                DefaultQueue = (EnqueueResponse)DQ;
                LogInfo = (EnqueueInfo)LI;
                CommandQueue = new ConcurrentQueue<QueueItem>(); 
                logConnection = TrackConnect;
                logCommands = TrackComm;
                logResponses = TrackResp;
                // Create a new SerialPort object with default settings.
                _serialPort = new SerialPort();

                // Allow the user to set the appropriate properties.
                _serialPort.Encoding = System.Text.Encoding.GetEncoding(1252);
                _serialPort.BaudRate = 57600; // SetPortBaudRate(_serialPort.BaudRate);
                _serialPort.Parity = (Parity)Enum.Parse(typeof(Parity), "None", true);
                _serialPort.DataBits = 8;
                _serialPort.StopBits = (StopBits)Enum.Parse(typeof(StopBits), "One", true);
                _serialPort.Handshake = (Handshake)Enum.Parse(typeof(Handshake), "None", true);
                _serialPort.NewLine = "\n\r";
                _serialPort.DataReceived += new SerialDataReceivedEventHandler(DataReceivedHandler);
                // Set the read/write timeouts
                _serialPort.ReadTimeout = 50;
                _serialPort.WriteTimeout = 50;

                GetPortNames(out PortNames);
            }
            public void SetupErrorParser(int NumbMotors)
            {
                ParseError = new ProcessErrorCode(4);
            }
            public void Disconnect()
            {
                _continue = false;
                if (_serialPort.IsOpen)
                {
                    _serialPort.Close();
                }
                if (monitorThread != null)
                {
                    if (monitorThread.IsAlive)
                    {
                        monitorThread.Join();
                    }
                }
            }
            public bool ConnectTo(string PortName, out string[] PortInfo)
            {
                Disconnect();
                if (logConnection)
                {
                     LogInfo(String.Format("Connecting to: {0}", PortName));
                }
                _serialPort.PortName = PortName;
                bool open = true;
                string openString = "";
                try
                {
                    _serialPort.Open();
                }
                catch (Exception ex)
                {
                    if (ex is UnauthorizedAccessException)
                    {
                        System.Threading.Thread.Sleep(1000);
                        try
                        {
                            _serialPort.Open();
                        }
                        catch (Exception ex2)
                        {
                            if (ex2 is UnauthorizedAccessException)
                                openString = String.Format("UnauthorizedAccessException for {0} (twice)", PortName);
                            else
                                openString = String.Format("UnauthorizedAccessException for {0}", PortName);
                            open = false;
                        }
                    }
                    else
                    {
                        if (ex is ArgumentOutOfRangeException)
                            openString = String.Format("ArgumentOutOfRangeException for {0}", PortName);
                        else if (ex is ArgumentException)
                            openString = String.Format("ArgumentException for {0}", PortName);
                        else if (ex is IOException)
                            openString = String.Format("IOException for {0}", PortName);
                        else if (ex is InvalidOperationException)
                            openString = String.Format("InvalidOperationException for {0}", PortName);
                        else
                            openString = String.Format("Exception for {0}", PortName);
                        open = false;

                    }
                }
                if (open && _serialPort.IsOpen)
                {
                    PortInfo = new string[7];
                    PortInfo[0] = "Port is open";
                    PortInfo[1] = String.Format("Port Name: {0}", _serialPort.PortName);
                    PortInfo[2] = String.Format("Baud Rate: {0}", _serialPort.BaudRate);
                    PortInfo[3] = String.Format("Parity: {0}", _serialPort.Parity);
                    PortInfo[4] = String.Format("DataBits: {0}", _serialPort.DataBits);
                    PortInfo[5] = String.Format("Stop Bits: {0}", _serialPort.StopBits);
                    PortInfo[6] = String.Format("Handshake: {0}", _serialPort.Handshake);
                    _continue = true;
                    monitorThread = new Thread(MonitorBoard);
                    monitorThread.Start();
                    System.Threading.Thread.Sleep(BoardWaitTime);
                    stopW.Start();
                    if (logConnection)
                    {
                        LogInfo("Serial Port is open");
                    }
                    return true;
                }
                else
                {
                    PortInfo = new string[2];
                    PortInfo[0] = "Port is not open!!";
                    PortInfo[1] = openString;
                    if (logConnection)
                    {
                        LogInfo("Serial Port is not open");
                    }
                    _continue = false;
                    return false;
                }
            }

            ~ControlBoard()
            {
                _continue = false;
                if (logConnection)
                {
                    LogInfo("Joining Monitor Thread");
                }
                if (monitorThread.IsAlive)
                    monitorThread.Join();

                if (logConnection)
                {
                    LogInfo("Monitor thread should be stopped");
                }
                if (stopW.IsRunning)
                    stopW.Stop();
            }
            internal long   BoardTime()
            {
                return stopW.ElapsedMilliseconds;
            }
            public void MonitorBoard()
            {
                object gen = new object();
                string blk = "";
                int responseTimeOut = 500;
                //int updateMonitor = BoardWaitTime;
                int updateMonitor = 10;
                QueueItem lastCommand;
                QueueItem nextCommand;
                QueueItem blkCommand;
                ResponseItem currResponse = new ResponseItem();
                ResponseItem noResponse = new ResponseItem();
                blkCommand.Notify = Notification.Never;
                blkCommand.Command = blk;
                blkCommand.Sender = BlankQueue;
                blkCommand.Time = 0;
                lastCommand = blkCommand;
                nextCommand = blkCommand;
                currResponse.Command = blk;
                currResponse.Response = blk;
                currResponse.Time = 0;
                currResponse.ElapsedTime = 0;
                noResponse.Command = blk;
                noResponse.Response = "No Response";
                noResponse.Time = 0;
                noResponse.ElapsedTime = 0;
                _waitingForResponse = false;
                if (logConnection)
                {
                    LogInfo("MonitorBoard thread started");
                }
                StringBuilder receivedBuffer = new StringBuilder();
                receivedBuffer.Clear();
                ResponseItem newResp = new ResponseItem();
#if (DEBUG || TRACE)
//                StringBuilder logString = new StringBuilder();
//                logString.Clear();
#endif 
                while (_continue)
                {
                    bool addWait = true;
                    lock (bufferLock)
                    {
                        if (inputBuffer.Length > 0)
                        {
                            receivedBuffer.Append(inputBuffer);
                            inputBuffer.Clear();
                        }
                    }
#if (DEBUG || TRACE)
                    int cnt = 0;
#endif
                    if (receivedBuffer.Length > 0)
                    {
                        string receivedString = receivedBuffer.ToString();
                        int lineT = receivedString.IndexOf("\n\r");
                        newResp.Clear();
#if (DEBUG || TRACE)
                        string msg = receivedBuffer.ToString();
                        msg = msg.Replace("\r", ">");
                        msg = msg.Replace("\n", "<");
                        LogInfo(String.Format("{0}: {1} - {2}", stopW.ElapsedMilliseconds, msg, lineT));
                        if (lineT >= 0)
                        {
                            msg = receivedBuffer.ToString(0, lineT);
                            LogInfo(String.Format("     {0}", msg));
                        }
                        LogInfo(String.Format("receivedString: {0} - lineT= {1}", receivedString, lineT));
#endif
                        while (receivedBuffer.Length > 0 && lineT >= 0)
                        {
                            if (lineT > 0)
                            {
                                string message = receivedBuffer.ToString(0, lineT);
                                message = message.Replace("\r\n", " > ");
                                message = message.Replace("\r", " > ");
                                message = message.Replace("\n", " > ");

                                if (lineT + 3 > receivedBuffer.Length)
                                    receivedBuffer.Clear();
                                else
                                    receivedBuffer.Remove(0, lineT + 2);

                                string[] splitMessage = message.Split(responseDelimiters);
                                int lastIdx = splitMessage.Count() - 1;
                                newResp.Time = stopW.ElapsedMilliseconds;
                                newResp.ElapsedTime = 0;
                                if (splitMessage.Count() > 0)
                                {
                                    newResp.Command = "";
                                    newResp.Notify = Notification.Always;
                                    newResp.Response = (splitMessage[lastIdx].TrimEnd(boardWhiteSpace)).TrimStart(boardWhiteSpace);
                                    newResp.Message = message;
                                    newResp.InstrumentSignal = false;
                                    if (newResp.Response.Length > 1)
                                    {
                                        if (newResp.Response.IndexOf("/0E") >= 0)
                                        {
                                            if (!ParseError.IsResponse(newResp.Response))
                                            {
                                                newResp.InstrumentSignal = true;
                                                newResp.Response
                                                    = String.Format("{0}/{1}", newResp.Response, ParseError.ToString(newResp.Response));
                                                DefaultQueue(newResp.Clone());
                                                if (logResponses)
                                                    LogInfo(String.Format("Received error msg: {0}", newResp.ToString()));
                                            }
                                        }
                                        if (!newResp.InstrumentSignal)
                                        {
                                            if (_waitingForResponse)
                                            {
                                                if (splitMessage.Count() > 1)
                                                {
                                                    string com = (splitMessage[lastIdx - 1].TrimEnd(boardWhiteSpace)).TrimStart(boardWhiteSpace);
                                                    if (com.IndexOf("/") == 0)
                                                    {
                                                        if (lastCommand.Command.IndexOf(com) < 0)
                                                        {
                                                            LogInfo(String.Format(">>> {0}: Received response with command {1}, but last command was {2}",
                                                                stopW.ElapsedMilliseconds, com, lastCommand.Command));
                                                            LogInfo(String.Format(">>>   {0}", newResp.ToString()));
                                                        }
                                                    }
                                                }
                                                newResp.Command = lastCommand.Command;
                                                newResp.ElapsedTime = newResp.Time - lastCommand.Time;
                                                newResp.Notify = lastCommand.Notify;
                                                if (logResponses)
                                                {
                                                    LogInfo(String.Format("{0} - MB1                    {1}", 
                                                        stopW.ElapsedMilliseconds, newResp.ToString()));
                                                }
                                                if (newResp.Response.IndexOf("/0E") >= 0)
                                                {
                                                    newResp.Response
                                                        = String.Format("{0}/{1}", newResp.Response, ParseError.ToString(newResp.Response));
                                                }
                                                lastCommand.Sender(newResp.Clone());
                                                lock (waitingLock)
                                                {
                                                    _waitingForResponse = false;
                                                }
                                            }
                                            else
                                            {
                                                DefaultQueue(newResp.Clone());
                                            }
                                        }
                                    }
#if (DEBUG || TRACE)
                                    if (newResp.Response.Length < 2)
                                    {
                                        LogInfo(String.Format("{0} Received Response with less than 2 characters |{1}|", 
                                            stopW.ElapsedMilliseconds, newResp.Response));
                                        LogInfo(String.Format("     {0}", newResp.ToString()));
                                    }
#endif
                                }
                                else if (lineT == 0)
                                {
                                    receivedBuffer.Remove(0, 2);
                                }
                                receivedString = receivedBuffer.ToString();
                                lineT = receivedString.IndexOf("\n\r");
#if (DEBUG || TRACE)
                                if (lineT >= 0)
                                {
                                    cnt++;
                                    LogInfo(String.Format("{0}:{1} - receivedString: {2} - lineT= {3}", 
                                        cnt, stopW.ElapsedMilliseconds,receivedString, lineT));
                                }
#endif
                                addWait = false;
                            }
                        }
                    }
                    else if (_waitingForResponse)
                    {
                        addWait = false;
                        //LogInfo(String.Format("Dequeue failed: lastCommand={0}/currResponse={1}",
                        //lastCommand.Command, currResponse.Command));
                        long currTime = stopW.ElapsedMilliseconds;
                        if (currTime - lastCommand.Time > responseTimeOut)
                        {
#if (DEBUG || TRACE)
                            LogInfo(String.Format("last.Command={0}/Time={1}", lastCommand.Command, lastCommand.Time));
                            LogInfo(String.Format("stopW.Elapsed={0}", currTime));
                            LogInfo(String.Format("timeout={0}", responseTimeOut));
#endif
                            noResponse.InstrumentSignal = false;
                            noResponse.Notify = Notification.Always;
                            noResponse.Command =
                                (lastCommand.Command.TrimEnd(boardWhiteSpace)).TrimStart(boardWhiteSpace);
                            noResponse.ElapsedTime = currTime - lastCommand.Time;
                            noResponse.Message = "Command Timed Out";
                            lastCommand.Sender(noResponse.Clone());
                            lock (waitingLock)
                            {
                                _waitingForResponse = false;
                            }

                            if (logResponses)
                            {
                                LogInfo(String.Format("{0} - MB3                    Command: {1} - Sent: {2}, Curr Time {3} - Timed Out",
                                    stopW.ElapsedMilliseconds, lastCommand.Command, lastCommand.Time, currTime));
                            }
                        }
                    }
                    else
                    {
                        if (CommandQueue.TryDequeue(out lastCommand))
                        {
                            //addWait = false;
                            lock (waitingLock)
                            {
                                _waitingForResponse = true;
                            }
                            long currTime = stopW.ElapsedMilliseconds;
                            _serialPort.Write(String.Format("{0}\n", lastCommand.Command));
                            lastCommand.Time = stopW.ElapsedMilliseconds;

                            if (logCommands)
                            {
                                LogInfo(String.Format("{0} - MB5 Command: {1} - Sent: {2}, Curr Time: {3}",
                                    stopW.ElapsedMilliseconds, lastCommand.Command, lastCommand.Time, currTime));
                            }
                        }
                        else
                            lastCommand = blkCommand;
                    }
                        
                    if (addWait)
                        System.Threading.Thread.Sleep(updateMonitor);
                }
            }

            public bool AddCommand(string NewCommand, Notification Notify, EnqueueResponse AddToQueue)
            {
                if (logCommands)
                {
                    LogInfo(String.Format("{0} - AC1 Command added {1} ", 
                        stopW.ElapsedMilliseconds, NewCommand));
                }
                QueueItem newItem;
                string tmpStr = NewCommand;
                NewCommand = NewCommand.TrimEnd(whiteSpace);
                NewCommand = NewCommand.TrimStart(whiteSpace);
                int lastIdx = NewCommand.Length - 1;
                if (NewCommand.IndexOf("/") == 0 && NewCommand.Substring(lastIdx, 1).IndexOf("R") >= 0
                    && NewCommand.IndexOfAny(whiteSpace) < 0)
                {
                    newItem.Command = NewCommand;
                    newItem.Notify = Notify;
                    newItem.Sender = AddToQueue;
                    newItem.Time = stopW.ElapsedMilliseconds;
                    CommandQueue.Enqueue(newItem);
                    return true;
                }
                else
                {
                    if (logCommands)
                    {
                        LogInfo(String.Format("{0} - AC2     Command {1} not sent, after trims command is {2}, last idx = {3}",
                            stopW.ElapsedMilliseconds, tmpStr, NewCommand, lastIdx));
                    }
                    return false;
                }
            }

            private void DataReceivedHandler(object sender,
                SerialDataReceivedEventArgs e)
            {
                SerialPort spL = (SerialPort)sender;
                System.Threading.Thread.Sleep(_serialWaitTime);
                lock (bufferLock)
                {
                    inputBuffer.Append(spL.ReadExisting());
                }
            }
            public void ClosePort()
            {
                _continue = false;
                if (logConnection)
                {
                    LogInfo("Closing Port");
                }
                _serialPort.Close();
                if (logConnection)
                {
                    LogInfo("Port should be closed");
                }
                if (monitorThread.IsAlive)
                    monitorThread.Join();
                if (stopW.IsRunning)
                    stopW.Stop();
            }

            public static void GetPortNames(out List<string> PossiblePorts)
            {
                PossiblePorts = new List<string>();
                foreach (string s in SerialPort.GetPortNames())
                {
                    PossiblePorts.Add(s);
                }
                return;
            }
        }
    } // namespace BoardComm
}
