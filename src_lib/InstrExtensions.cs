﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sdpcrInstrLib
{
    using BoardComm;
    using Hardware;
    using LEDCluster;
    using Stepper;
    using PIDController;
    using Therm;

    namespace InstrExtensions
    {
        public static class BoardExtensions
        {

            public static bool InitLEDCtl(this ControlBoard Board, int NumbLEDs, int[] LEDMax, int[] InitIntensities,
                out LEDCtl LEDs, out List<string> Message)
            {
                Message = new List<string>();
                bool connected;
                LEDs = new LEDCtl(Board, NumbLEDs, LEDMax, InitIntensities, out connected);
                if (!connected)
                    Message.Add(String.Format("LED Controller did not connect"));

                return connected;
            }
            //1, 1, 1, 1600, 1, 60, 300, 6400,
            public static bool InitMotorCtl(this ControlBoard Board,
                int MotorNumber, int DirectionCode, List<MotorProfile> MotorProfilesIn,
                int InitProfile, int InitVelocity, int Backout, int MaxPosition,
                out StepperMotor Motor,
                out List<string> Message)
            {
                Message = new List<string>();
                bool connected;
                //Console.WriteLine("Before Motor Cto");
                //Console.Out.Flush();
                System.Threading.Thread.Sleep(500);
                Motor = new StepperMotor(Board,
                    MotorNumber, DirectionCode, MotorProfilesIn, InitProfile, InitVelocity, Backout, MaxPosition,
                    out connected);
                if (!connected)
                {
                    Message.Add(String.Format("Motor {0} is NOT connected", MotorNumber));
                    Message.Add(String.Format("     Direction Code: {0}", DirectionCode));
                    if (MotorProfilesIn.Count > 0)
                    {
                        Message.Add("Motor Profiles");
                        for (int n = 0; n < MotorProfilesIn.Count; n++)
                        {
                            Message.Add(String.Format("     {0}: {1}", n, MotorProfilesIn[n].ToString()));
                        }
                    }
                    else
                    {
                        Message.Add("No Motor Profiles");
                    }
                    Message.Add(String.Format("     Init Profile: {0}", InitProfile));
                    Message.Add(String.Format("     Init Velocity: {0}", InitVelocity));
                    Message.Add(String.Format("     Max Position: {0}", MaxPosition));
                    Message.Add(String.Format("     Backout: {0}", Backout));
                    List<string> errMsg = Motor.ErrorMessage;
                    for (int s = 0; s < errMsg.Count; s++)
                        Message.Add(errMsg[s]);
                }
                else
                    Motor.EmptyResponseLists();

                return connected;
            }

            public static bool InitThermistors(this ControlBoard Board, out Thermistors Therm24,
                out List<string> Message)
            {
                Message = new List<string>();
                bool connected;
                Therm24 = new Thermistors(Board, out connected);
                if (!connected)
                    Message.Add(String.Format("Thermistors did not connected"));
                else
                    Therm24.EmptyResponseLists();

                return connected;
            }

            public static bool InitPIDCtl(this ControlBoard Board, out PIDCtl TempCtl,
                out List<string> Message, double IdleTemperature = 40.0,
                 double LowTemperature = 65.0, double HighTemperature = 95.0)
            {
                Message = new List<string>();
                bool connected;
                TempCtl = new PIDCtl(Board, out connected, IdleTemperature, LowTemperature, HighTemperature);
                if (!connected)
                    Message.Add(String.Format("PID controller did not connected"));
                else
                    TempCtl.EmptyResponseLists();

                return connected;
            }
        }
    }
}