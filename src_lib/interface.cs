﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Xml.Linq;
using System.Text;
//using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Timers;
using System.IO;
using System.IO.Ports;
using System.Net;
using System.Xml.Schema;
using System.Linq.Expressions;

using sdpcrInstrLib.BoardComm;
using sdpcrInstrLib.Hardware;
using sdpcrInstrLib.LEDCluster;
using sdpcrInstrLib.Stepper;
using sdpcrInstrLib.PIDController;
using sdpcrInstrLib.Therm;
using sdpcrInstrLib.InstrExtensions;
using sdpcrInstrLib.Sequencers;
using sdpcrInstrLib.CreateDefaultFile;

namespace sdpcrInstrLib
{
    namespace InstrInterface
    {
        public enum MotorNumber : int
        {
            Rotor = 1,
            XStage = 2,
            YStage = 3,
            FilterWheel = 4
        }
        
        public static class Instrument
        { 
            static bool _initializeAttempted = false;
            static bool XMLfileLoaded = false;
            static XDocument _configuration;
            // Predefined character sets
            readonly static char[] whiteSpace = { ' ', '\n', '\r', '\t' };  // space, new line, carriage return, tab || comma
            static List<string> _initializationResult;
            // Control Board
            static bool _initialized = false;
            static ControlBoard board;
            static string[] _portInfo;
            // Processor
            public static Processor SDPCRProcessor;
            // LEDs
            static bool ledConnected = false;
            static int numbLEDs = 6;
            static int[] LEDLevels = { 50, 50, 50, 50, 50, 50 }; // percent
            static int[] LEDMax = { 50, 50, 50, 50, 50, 50 }; // percent
            static string[] LEDName = { "L350/30", "L400/30", "L500/30", "L550/30", "L600/30", "L650/30" }; // LED names
            public static LEDCtl LED_Ctl;
            //Thermistors
            static bool thermConnected = false;
            public static Thermistors Thermistor_Ctl;
            static int[] thermWeights = { 100, 100, 0, 0,
            0, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 0};
            static double[] thermBiases = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
            0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, };

            // Temperature Program variables
            static bool pidConnected = false;
            public static PIDCtl Temp_Ctl;

            // Stepper Motors
            static bool xStageConnected;
            static bool yStageConnected;
            static bool filterWheelConnected;
            static bool rotorConnected;
            public static StepperMotor Rotor;
            public static StepperMotor XStage;
            public static StepperMotor YStage;
            public static StepperMotor FilterWheel;
            public static int NumbMotorProfiles = 4;

            // Sequencers
            static bool pidSeqConnected = false;
            static bool thermSeqConnected = false;
            static bool rotorSeqConnected = false;
            static bool xStageSeqConnected = false;
            static bool yStageSeqConnected = false;
            static bool filterWheelSeqConnected = false;
            static bool rotorVelSeqConnected = false;
            static public PIDSequencer TempProgramController;
            static public TempMonitoring ThermMonitoring;
            static public PositionSequencer RotorPositionSequencer;
            static public PositionSequencer XStageSequencer;
            static public PositionSequencer YStageSequencer;
            static public PositionSequencer FilterWheelSequencer;
            static public VelocitySequencer RotorVelocitySequencer;

            static internal List<XElement> PIDPrograms;
            static internal List<XElement> RotorPositionPrograms;
            static internal List<XElement> XStagePositionPrograms;
            static internal List<XElement> YStagePositionPrograms;
            static internal List<XElement> FilterWheelPositionPrograms;
            static internal List<XElement> RotorVelocityPrograms;

            //readonly static string outputDir = "C:\\instrument\\";
            public static string[] PortInfo
            {
                get
                {
                    if (_initialized)
                        return _portInfo;
                    else
                    {
                        string[] res = new string[0];
                        res.Append("Instrument Interface not inialized");
                        return res;
                    }
                }
            }
            public static string[] InstrumentLog
            {
                get
                {
                    if (_initializeAttempted)
                    {
                        SDPCRProcessor.ReadLogInfo(out string[] LogText);
                        return LogText;
                    }
                    else
                        return new string[0];
                }
            }
            public static long BoardTime()
            {
                return board.BoardTime();
            }
            public static string[] InitializationResult 
            { 
                get
                {
                    return _initializationResult.ToArray();
                }
             }
            internal static bool GetAttribute(XElement Ele, string AttributeString, out string Value)
            {
                XAttribute xat = Ele.Attribute(AttributeString);
                if (xat != null)
                {
                    Value = xat.Value;
                    return true;
                }
                else
                {
                    Value = "";
                    return false;
                }

            }
            public static void UseDefaultConfiguration(string BaseName = "d", string OutpuDir = "c:\\instrument")
            {
                CreateDefaultXML cdXML = new CreateDefaultXML();
                _configuration = cdXML.DefaultXML;
                XMLfileLoaded = true;
                if ( BaseName.Length > 2)
                    cdXML.WriteDefaultXML(OutpuDir, BaseName);
            }
            internal static bool StringToXDoc(string XDocString, out XDocument XDoc)
            {
                try { XDoc = XDocument.Parse(XDocString); }
                catch (System.Xml.XmlException )
                {
                    XDoc = new XDocument();
                    return false;
                }
                return true;
            }
            internal static bool GetXElementProgram(XDocument XDoc, out XElement XProgram)
            {
                IEnumerable<XElement> items =
                        from el in XDoc.Descendants("Program")
                        select el;
                if (items.Count() > 0)
                {
                    XProgram = items.First();
                    return true;
                }
                else
                {
                    XProgram = new XElement("None");
                    return false;
                }
            }
            public static bool LoadConfiguration(string ConfigString)
            {
                if (StringToXDoc(ConfigString, out _configuration))
                    return true;
                else
                {
                    UseDefaultConfiguration();
                    return false;
                }
            }
            public static bool ReadConfiguration(string Path, out string Err)
            {
                XDocument doc = XDocument.Load(@Path);
                if (CheckConfiguration(doc, out Err))
                {
                    _configuration = doc;
                    XMLfileLoaded = true;
                    return true;
                }
                else
                {
                    UseDefaultConfiguration();
                    return false;
                }
            }
            public static bool WriteConfiguration(string Path)
            {
                if ( XMLfileLoaded)
                {
                    _configuration.Save(@Path);
                }
                return XMLfileLoaded;
            }
            static bool CheckConfiguration(XDocument Doc, out string Err)
            {
                bool xmlOK = true;
                Err = "";
                StringBuilder eStr = new StringBuilder();
                eStr.Clear();
                IEnumerable<XElement> items =
                        from el in _configuration.Descendants("StepperMotor")
                        select el;
                if ( items.Count() != 4)
                {
                    xmlOK = false;
                    eStr.Append(String.Format("Number of <StepperMotor> nodes found = {0}\n", items.Count()));
                }
                items =
                        from el in _configuration.Descendants("PID")
                        select el;
                if (items.Count() != 1)
                {
                    xmlOK = false;
                    eStr.Append(String.Format("Number of <PID> nodes found = {0}\n", items.Count()));
                }
                items =
                        from el in _configuration.Descendants("LEDs")
                        select el;
                if (items.Count() != 1)
                {
                    xmlOK = false;
                    eStr.Append(String.Format("Number of <LEDs> nodes found = {0}\n", items.Count()));
                }
                items =
                        from el in _configuration.Descendants("Thermistors")
                        select el;
                if (items.Count() != 1)
                {
                    xmlOK = false;
                    eStr.Append(String.Format("Number of <Thermistors> nodes found = {0}\n", items.Count()));
                }
                items =
                        from el in _configuration.Descendants("Filters")
                        select el;
                if (items.Count() != 1)
                {
                    xmlOK = false;
                    eStr.Append(String.Format("Number of <Filters> nodes found = {0}\n", items.Count()));
                }
                items =
                        from el in _configuration.Descendants("Dyes")
                        select el;
                if (items.Count() != 1)
                {
                    xmlOK = false;
                    eStr.Append(String.Format("Number of <Dyes> nodes found = {0}\n", items.Count()));
                }
                Err = eStr.ToString();
                return xmlOK;
            }

            public static string InstrumentState
            {
                get
                {
                    if (Instrument.SDPCRProcessor.Connected)
                    {
                        bool r = Instrument.Rotor.GetMotorState(out string rState);
                        bool x = Instrument.XStage.GetMotorState(out string xState);
                        bool y = Instrument.YStage.GetMotorState(out string yState);
                        bool f = Instrument.FilterWheel.GetMotorState(out string fState);
                        if (!r)
                            rState = "1_Controller:Off";
                        else
                            rState = String.Format("1_Controller:On,{0}", rState);
                        if (!x)
                            xState = "2_Controller:Off";
                        else
                            xState = String.Format("2_Controller:On,{0}", xState);
                        if (!y)
                            yState = "3_Controller:Off";
                        else
                            yState = String.Format("3_Controller:On,{0}", yState);
                        if (!f)
                            fState = "4_Controller:Off";
                        else
                            fState = String.Format("4_Controller:On,{0}", fState);

                        string tState = String.Format("PID_Program_State:{0}",
                            Instrument.TempProgramController.CurrentState);
                        string vState = String.Format("Velocity_Program_State:{0}",
                            Instrument.RotorVelocitySequencer.CurrentState);

                        return String.Format("Processor:Connected,{0},{1},{2},{3},{4},{5}",
                            rState, xState, yState, fState, tState, vState);

                    }
                    else
                    {
                        return "Processor:NotConnected";
                    }
                }
            }
            public static bool Initialize(string PrevPort = "COM1", bool UseConsole = false, 
                bool TrackConnect = false, bool TrackComm = false, bool TrackResp = false)
            {
                _initializeAttempted = true;
                _initializationResult = new List<string>();
                if (!XMLfileLoaded)
                {
                    UseDefaultConfiguration();
                }
                List<string> errMessage;
                SDPCRProcessor = new Processor(PrevPort, UseConsole, TrackConnect, TrackComm, TrackResp);
                _initialized = SDPCRProcessor.Connected;
                if (_initialized)
                {
                    _portInfo = SDPCRProcessor.PortInformation;
                    _initialized = (SDPCRProcessor.GetBoard(out board) && board != null);
                    _initializationResult.Add("Processor connected");
                }
                else
                {
                    _initializationResult.Append("Processor not connected");
                }
                if (_initialized)
                {
                    if (GetLEDParams(out numbLEDs, out LEDMax, out LEDLevels, out LEDName))
                    {
                        ledConnected = board.InitLEDCtl(numbLEDs, LEDMax, LEDLevels, out LED_Ctl, out errMessage);
                        if (!ledConnected)
                        {  
                            _initializationResult.Add("LED controller not connected");
                            for (int n = 0; n < errMessage.Count; n++)
                            {
                                _initializationResult.Add(errMessage[n]);
                            }
                        }
                        else
                            _initializationResult.Add("LED controller connected");
                    }
                    else
                    {
                        ledConnected = false;
                        _initializationResult.Add("LEDs not connected");
                        _initializationResult.Add("     Problem with LED configuration");
                        Console.WriteLine(String.Format("Problem with LED configuration, IR.Length {0}", InitializationResult.Length));
                   }
                    _initializationResult.Add("=====");
                    if (GetMotorParams("Rotor", 
                        out int motorN, out int directionCode, out List<MotorProfile> motorProfiles,
                        out int backout, out int maxPosition, out int initProfile, out int initVelocity,
                        out RotorPositionPrograms, out RotorVelocityPrograms))
                    {
                        rotorConnected = board.InitMotorCtl(motorN, directionCode, motorProfiles,
                            initProfile, initVelocity, backout, maxPosition, out Rotor, out errMessage);
                        if (!rotorConnected)
                        {
                            _initializationResult.Add("Rotor Initialization failed");
                            for (int s = 0; s < errMessage.Count; s++)
                                _initializationResult.Add(errMessage[s]);
                        }
                        _initializationResult.Add("Rotor Parameters");
                        _initializationResult.Add(String.Format("N:{0}, direction:{1}, backout:{2}, maxPosition:{3}, initProfile:{4}, initVelocity:{5}",
                            motorN, directionCode, backout, maxPosition, initProfile, initVelocity));
                        for ( int n = 0; n < motorProfiles.Count; n++)
                        {
                            if ( motorProfiles[n].Used)
                            {
                                _initializationResult.Add(String.Format("     Profile:{0}, minVelocity:{1}, maxVelocity:{2}",
                                    n, motorProfiles[n].MinimumVelocity, motorProfiles[n].MaximumVelocity));
                            }
                        }
                    }
                    else
                    {
                        rotorConnected = false;
                        _initializationResult.Add("Problem with Rotor configuration");
                    }
                    _initializationResult.Add(String.Format(">>> Rotor Connected: {0}", rotorConnected.ToString()));
                    _initializationResult.Add("=====");
                    if (GetMotorParams("XStage", 
                        out motorN, out directionCode, out motorProfiles,
                        out backout, out maxPosition, out initProfile, out initVelocity,
                        out XStagePositionPrograms, out _))
                    {
                        xStageConnected = board.InitMotorCtl(motorN, directionCode, motorProfiles,
                            initProfile, initVelocity, backout, maxPosition, out XStage, out errMessage);
                        if (!xStageConnected)
                        {
                            _initializationResult.Add("XStage Initialization failed");
                            for (int s = 0; s < errMessage.Count; s++)
                                _initializationResult.Add(errMessage[s]);
                        }
                        _initializationResult.Add("XStage Parameters");
                        _initializationResult.Add(String.Format("N:{0}, direction:{1}, backout:{2}, maxPosition:{3}, initProfile:{4}, initVelocity:{5}",
                            motorN, directionCode, backout, maxPosition, initProfile, initVelocity));
                        for (int n = 0; n < motorProfiles.Count; n++)
                        {
                            if (motorProfiles[n].Used)
                            {
                                _initializationResult.Add(String.Format("     Profile:{0}, minVelocity:{1}, maxVelocity:{2}",
                                    n, motorProfiles[n].MinimumVelocity, motorProfiles[n].MaximumVelocity));
                            }
                        }
                    }
                    else
                    {
                        xStageConnected = false;
                        _initializationResult.Add("Problem with XStage configuration");
                    }
                    _initializationResult.Add(String.Format(">>> XStage Connected: {0}", xStageConnected.ToString()));
                    _initializationResult.Add("=====");
                    if (GetMotorParams("YStage", 
                        out motorN, out directionCode, out motorProfiles,
                        out backout, out maxPosition, out initProfile, out initVelocity,
                        out YStagePositionPrograms, out _))
                    {
                        yStageConnected = board.InitMotorCtl(motorN, directionCode, motorProfiles,
                            initProfile, initVelocity, backout, maxPosition, out YStage, out errMessage);
                        if (!yStageConnected)
                        {
                            _initializationResult.Add("YStage Initialization failed");
                            for (int s = 0; s < errMessage.Count; s++)
                                _initializationResult.Add(errMessage[s]);
                        }
                        _initializationResult.Add("YStage Parameters");
                        _initializationResult.Add(String.Format("N:{0}, direction:{1}, backout:{2}, maxPosition:{3}, initProfile:{4}, initVelocity:{5}",
                            motorN, directionCode, backout, maxPosition, initProfile, initVelocity));
                        for (int n = 0; n < motorProfiles.Count; n++)
                        {
                            if (motorProfiles[n].Used)
                            {
                                _initializationResult.Add(String.Format("     Profile:{0}, minVelocity:{1}, maxVelocity:{2}",
                                    n, motorProfiles[n].MinimumVelocity, motorProfiles[n].MaximumVelocity));
                            }
                        }
                    }
                    else
                    {
                        yStageConnected = false;
                        _initializationResult.Add("     Problem with YStage configuration");
                    }
                    _initializationResult.Add(String.Format(">>> YStage Connected: {0}", yStageConnected.ToString()));
                    _initializationResult.Add("=====");
                    if (GetMotorParams("FilterWheel", 
                        out motorN, out directionCode, out motorProfiles,
                        out backout, out maxPosition, out initProfile, out initVelocity,
                        out FilterWheelPositionPrograms, out _))
                    {
                        filterWheelConnected = board.InitMotorCtl(motorN, directionCode, motorProfiles,
                            initProfile, initVelocity, backout, maxPosition, out FilterWheel, out errMessage);
                        if (!filterWheelConnected)
                        {
                            _initializationResult.Add("Filter Wheel Initialization failed");
                            for (int s = 0; s < errMessage.Count; s++)
                                _initializationResult.Add(errMessage[s]);
                        }
                        _initializationResult.Add("FilterWheel Parameters");
                        _initializationResult.Add(String.Format("N:{0}, direction:{1}, backout:{2}, maxPosition:{3}, initProfile:{4}, initVelocity:{5}",
                            motorN, directionCode, backout, maxPosition, initProfile, initVelocity));
                        for (int n = 0; n < motorProfiles.Count; n++)
                        {
                            if (motorProfiles[n].Used)
                            {
                                _initializationResult.Add(String.Format("     Profile:{0}, minVelocity:{1}, maxVelocity:{2}",
                                    n, motorProfiles[n].MinimumVelocity, motorProfiles[n].MaximumVelocity));
                            }
                        }
                    }
                    else
                    {
                        filterWheelConnected = false;
                        _initializationResult.Add("     Problem with Filter Wheel configuration");
                    }
                    _initializationResult.Add(String.Format(">>> Filter Wheel Connected: {0}", filterWheelConnected.ToString()));
                    _initializationResult.Add("=====");                   
                    IEnumerable<XElement> items =
                        from el in _configuration.Descendants("Thermistors")
                        select el;
                    XElement xtherm = items.First();
                    //XAttribute xat = xtherm.Attribute("ThermistorWeights");
                    string weightsString;// = xat.Value;
                    if (GetAttribute(xtherm, "ThermistorWeights", out weightsString))
                    {
                        string[] weights = weightsString.Split(',');
                        //Console.WriteLine(String.Format("ThermistorWeightString: {0}, length: {1}", weightsString, weights.Length));
                        if (weights.Length == 24)
                        {
                            int[] tempW = new int[24];
                            bool wgtsOK = true;
                            for (int n = 0; n < 24; n++)
                            {
                                if (!Int32.TryParse(weights[n], out tempW[n]))
                                {
                                    wgtsOK = false;
                                    break;
                                }
                            }
                            if (wgtsOK)
                            {
                                thermWeights = tempW;
                                thermConnected = board.InitThermistors(out Thermistor_Ctl, out errMessage);
                                bool doneSettingWeights = true;
                                if (thermConnected)
                                {
                                    string wgts;
                                    Thermistor_Ctl.GetThermistorWeights(out wgts);
                                    if (!Thermistor_Ctl.Check24Weights(wgts))
                                    {
                                        int wait = 2 * SDPCRProcessor.BoardWaitTime;
                                        for (int n = 0; n < 24; n++)
                                        {
                                            Thermistor_Ctl.SetWeight(n, thermWeights[n]);
                                            System.Threading.Thread.Sleep(wait);
                                        }
                                        doneSettingWeights = false;
                                        for (int n = 0; n < 24; n++)
                                        {
                                            if (Thermistor_Ctl.NumbCommandsWaiting <= 0)
                                            {
                                                doneSettingWeights = true;
                                                break;
                                            }
                                            else
                                                System.Threading.Thread.Sleep(4 * wait);
                                        }
                                        if (!doneSettingWeights)
                                            thermConnected = false;
                                    }
                                }
                                if (!thermConnected)
                                {
                                    if (doneSettingWeights)
                                    {
                                        _initializationResult.Add("Thermistor controller not connected");
                                        for (int s = 0; s < errMessage.Count; s++)
                                            _initializationResult.Add(errMessage[s]);
                                    }
                                    else
                                    {
                                        _initializationResult.Add("Problem with weights in Thermistor controller");
                                    }
                                }
                            }
                            else
                            {
                                thermConnected = false;
                                _initializationResult.Add("Problem with weights in configuration");
                            }
                        }
                        else
                        {
                            thermConnected = false;
                            _initializationResult.Add("Problem with weights in configuration");
                        }
                    }
                    else
                    {
                        thermConnected = false;
                        _initializationResult.Add("Problem with weights in configuration");
                    }
                    _initializationResult.Add(String.Format(">>> Thermistor Controller Connected: {0}", thermConnected.ToString()));
                    _initializationResult.Add("=====");
                    
                    if (GetPIDParams( out double idleTemperature, out double lowTemperature,
                        out double highTemperature, out PIDPrograms))
                    {
                        pidConnected = board.InitPIDCtl(out Temp_Ctl, out errMessage, idleTemperature, lowTemperature, highTemperature);
                        if (!pidConnected)
                        {
                            _initializationResult.Add("PID controller not connected");
                            for (int s = 0; s < errMessage.Count; s++)
                                _initializationResult.Add(errMessage[s]);
                        }
                    }
                    else
                    {
                        _initializationResult.Add("Problem with PID configuration");
                    }
                    _initializationResult.Add(String.Format("PID controller connected: {0}", pidConnected.ToString()));
                    _initializationResult.Add("=====");
                    _initialized = ledConnected && rotorConnected && xStageConnected
                        && yStageConnected && filterWheelConnected && thermConnected && pidConnected;
                    _initializationResult.Add(String.Format("{0}: l:{1}, r:{2}, x:{3}, y:{4}, f:{5}, t:{6}, p:{7}",
                        _initialized.ToString(), ledConnected.ToString(), rotorConnected.ToString(), xStageConnected.ToString(),
                        yStageConnected.ToString(), filterWheelConnected.ToString(), thermConnected.ToString(), pidConnected.ToString()));
                    _initializationResult.Add("=====");
                }

                if (_initialized)
                {

                    for (int n = 0; n < numbLEDs; n++)
                        LED_Ctl.SetIntensity(n, LEDLevels[n]);
                    XStage.Home();
                    YStage.Home();
                    FilterWheel.Home();
                    Rotor.Home();
                    TempProgramController = new PIDSequencer(Temp_Ctl, out pidSeqConnected);
                    ThermMonitoring = new TempMonitoring(Thermistor_Ctl, out thermSeqConnected);
                    RotorPositionSequencer = new PositionSequencer(Rotor, out rotorSeqConnected);
                    XStageSequencer = new PositionSequencer(XStage, out xStageSeqConnected);
                    YStageSequencer = new PositionSequencer(YStage, out yStageSeqConnected);
                    FilterWheelSequencer = new PositionSequencer(FilterWheel, out filterWheelSeqConnected);
                    RotorVelocitySequencer = new VelocitySequencer(Rotor, out rotorVelSeqConnected, BoardTime);

                    if (!pidSeqConnected || !thermSeqConnected || !rotorSeqConnected
                        || !xStageSeqConnected || !yStageSeqConnected || !filterWheelSeqConnected || !rotorVelSeqConnected)
                    {
                        _initializationResult.Add("One of the sequencers did not initialize properly");
                        _initialized = false;
                    }
                }

                return _initialized;
            }
            internal static bool GetProgramHeaders(List<XElement> Programs, out List<string> ProgramHeaders)
            {
                ProgramHeaders = new List<string>();
                if (Programs.Count < 1)
                    return false;
                string header;
                for (int n = 0; n < Programs.Count; n++)
                {
                    XAttribute xa = Programs[n].Attribute("Name");
                    IEnumerable<XElement> items =
                        from el in Programs[n].Descendants("Step")
                        select el;
                    header = String.Format("{0} : {1} : {2}", n, xa.Value, items.Count());
                    ProgramHeaders.Add(header);
                }
                return true;
            }
            public static bool GetPositionProgramHeaders(int MotorNumber, out List<string> ProgramHeaders)
            {
                bool res = false;
                switch (MotorNumber)
                {
                    case 1:
                        res = GetProgramHeaders(RotorPositionPrograms, out ProgramHeaders);
                        break;
                    case 2:
                        res = GetProgramHeaders(XStagePositionPrograms, out ProgramHeaders);
                        break;
                    case 3:
                        res = GetProgramHeaders(YStagePositionPrograms, out ProgramHeaders);
                        break;
                    case 4:
                        res = GetProgramHeaders(FilterWheelPositionPrograms, out ProgramHeaders);
                        break;
                    default:
                        ProgramHeaders = new List<string>();
                        break;
                }
                return res;
            }
            
            public static bool GetPIDProgramHeaders(out List<string> ProgramHeaders)
            {
                return GetProgramHeaders(PIDPrograms, out ProgramHeaders);
            }

            public static bool GetPositionProgram(int MotorNumber, int Prog, out int ProgProfile, out int ProgVelocity, out List<int> Positions)
            {
                bool res = false;
                switch (MotorNumber)
                {
                    case 1:
                        res = FormatPositionProgram(Rotor, RotorPositionPrograms, Prog, out ProgProfile, out ProgVelocity, out Positions);
                        break;
                    case 2:
                        res = FormatPositionProgram(XStage, XStagePositionPrograms, Prog, out ProgProfile, out ProgVelocity, out Positions);
                        break;
                    case 3:
                        res = FormatPositionProgram(YStage, YStagePositionPrograms, Prog, out ProgProfile, out ProgVelocity, out Positions);
                        break;
                    case 4:
                        res = FormatPositionProgram(FilterWheel, FilterWheelPositionPrograms, Prog, out ProgProfile, out ProgVelocity, out Positions);
                        break;
                    default:
                        Positions = new List<int>();
                        ProgProfile = 0;
                        ProgVelocity = 0;
                        break;
                }
                return res;
            }
            public static bool GetPIDProgram(int Prog, out List<string> Program)
            {
                if (FormatPIDProgram(PIDPrograms, Prog, out Program))
                    return true;
                else
                    return false;
            }
            
            public static bool LoadPositionProgram(int MotorNumber, int Prog, out string Err, int ProfileIn = -1, int VelocityIn = -1)
            {
                Err = "Invalid Motor";
                switch (MotorNumber)
                {
                    case 1:
                        return LoadPositionProgram(Rotor, RotorPositionSequencer, RotorPositionPrograms,
                            Prog, out Err, ProfileIn, VelocityIn);
                    case 2:
                        return LoadPositionProgram(XStage, XStageSequencer, XStagePositionPrograms,
                            Prog, out Err, ProfileIn, VelocityIn);
                    case 3:
                        return LoadPositionProgram(YStage, YStageSequencer, YStagePositionPrograms,
                            Prog, out Err, ProfileIn, VelocityIn);
                    case 4:
                        return LoadPositionProgram(FilterWheel, FilterWheelSequencer, FilterWheelPositionPrograms,
                            Prog, out Err, ProfileIn, VelocityIn);
                    default:
                        return false;
                }
            }
            public static bool PrintPositionProgram(int MotorNumber, int Prog = 0)
            {
                bool res = true;
                int pProfile;
                int pVelocity;
                switch (MotorNumber)
                {
                    case 1:
                        if (FormatPositionProgram(Rotor, RotorPositionPrograms, Prog, out pProfile, out pVelocity, out List<int> positions))
                        {
                            Console.WriteLine(String.Format("Rotor Progarm {0}", Prog));
                            Console.WriteLine(String.Format("Profile: {0} - Velocity: {1}", pProfile, pVelocity));
                            for (int n = 0; n < positions.Count; n++)
                                Console.WriteLine(String.Format("{0} : {1}", n, positions[n]));
                        }
                        else
                            res = false;
                        break;
                    case 2:
                        if (FormatPositionProgram(XStage, XStagePositionPrograms, Prog, out pProfile, out pVelocity, out positions))
                        {
                            Console.WriteLine(String.Format("XStage Progarm {0}", Prog));
                            Console.WriteLine(String.Format("Profile: {0} - Velocity: {1}", pProfile, pVelocity));
                            for (int n = 0; n < positions.Count; n++)
                                Console.WriteLine(String.Format("{0} : {1}", n, positions[n]));
                        }
                        else
                            res = false;
                        break;
                    case 3:
                        if (FormatPositionProgram(YStage, YStagePositionPrograms, Prog, out pProfile, out pVelocity, out positions))
                        {
                            Console.WriteLine(String.Format("YStage Progarm {0}", Prog));
                            Console.WriteLine(String.Format("Profile: {0} - Velocity: {1}", pProfile, pVelocity));
                            for (int n = 0; n < positions.Count; n++)
                                Console.WriteLine(String.Format("{0} : {1}", n, positions[n]));
                        }
                        else
                            res = false;
                        break;
                    case 4:
                        if (FormatPositionProgram(FilterWheel, FilterWheelPositionPrograms, Prog, out pProfile, out pVelocity, out positions))
                        {
                            Console.WriteLine(String.Format("FilterWheel Progarm {0}", Prog));
                            Console.WriteLine(String.Format("Profile: {0} - Velocity: {1}", pProfile, pVelocity));
                            for (int n = 0; n < positions.Count; n++)
                                Console.WriteLine(String.Format("{0} : {1}", n, positions[n]));
                        }
                        else
                            res = false;
                        break;
                    default:
                        res = false;
                        break;
                }
                if (res)
                    Console.WriteLine("Program Printed");
                else
                    Console.WriteLine("Program not Printed");
                return res;
            }
            public static bool AddPositionProgram(int MotorNumber, string ProgramString)
            {
                if (MotorNumber < 1 || MotorNumber > 4 || ProgramString.Length < 40)
                    return false;
                bool res = true;
                if (StringToXDoc(ProgramString, out XDocument xDoc))
                {
                    if (GetXElementProgram(xDoc, out XElement xProg))
                    {
                        var elList = new List<XElement>{ xProg };

                        switch (MotorNumber)
                        {
                            case 1:
                                if (FormatPositionProgram(Rotor, elList, 0, out _, out _, out _))
                                    RotorPositionPrograms.Add(xProg);
                                else
                                    res = false;
                                break;
                            case 2:
                                if (FormatPositionProgram(XStage, elList, 0, out _, out _, out _ ))
                                    XStagePositionPrograms.Add(xProg);
                                else
                                    res = false;
                                break;
                            case 3:
                                if (FormatPositionProgram(YStage, elList, 0, out _, out _, out _))
                                    YStagePositionPrograms.Add(xProg);
                                else
                                    res = false;
                                break;
                            case 4:
                                if (FormatPositionProgram(FilterWheel, elList, 0, out _, out _, out _))
                                    FilterWheelPositionPrograms.Add(xProg);
                                else
                                    res = false;
                                break;
                            default:
                                res = false;
                                break;
                        }
                        if (!res)
                        {
                            Console.WriteLine("FormatMotorProgram failed");
                        }
                    }
                    else
                    {
                        res = false;
                        Console.WriteLine("GetXElementProgram failed");
                    }
                }
                else
                {
                    res = false;
                    Console.WriteLine("StringToXDoc failed");
                }

                return res;
            }
            internal static bool LoadPositionProgram(StepperMotor Motor, PositionSequencer MSequencer, List<XElement> Programs,
                int Prog, out string Err, int ProfileIn = -1, int VelocityIn = -1 )
            {
                int minV;
                int maxV;
                int pProfile;
                int pVelocity;
                if (Programs.Count < 1)
                {
                    Err = "No Programs in Configuration";
                    return false;
                }
                else if (Prog < 0 || Prog >= Programs.Count)
                {
                    Err = "Invalid Program number";
                    return false;
                }
                if (FormatPositionProgram(Motor, Programs, Prog,
                    out pProfile, out pVelocity, out List<int> positions))
                {
                    if (ProfileIn < 0)
                        ProfileIn = pProfile;
                    if (VelocityIn < 1)
                        VelocityIn = pVelocity;
                    //Console.WriteLine(String.Format("LoadPositionProgram - Profile: {0}, Velocity: {1}", ProfileIn, VelocityIn));
                    if (Motor.MinMaxVelocity(ProfileIn, out minV, out maxV))
                    {
                        if (VelocityIn < minV || VelocityIn > maxV)
                        {
                            Err = "Invalid Velocity";
                            return false;
                        }
                    }
                    else
                    {
                        Err = "Invalid Profile";
                        return false;
                    }
                    return MSequencer.LoadPositionProgram(positions, ProfileIn, VelocityIn, out Err);
                }
                else
                {
                    Err = String.Format("Problem with format of program {0}, Profile: {1}, Velocity: {2}", Prog, ProfileIn, VelocityIn);
                    return false;
                }
            }
            static bool FormatPositionProgram(StepperMotor Motor, List<XElement> Programs, int Prog,
                out int ProgProfile, out int ProgVelocity, out List<int> Positions)
            {
                Positions = new List<int>();
                ProgVelocity = 1;
                ProgProfile = 1;
                //Console.WriteLine(String.Format("FormatPositionProgram - Prog: {0}, Programs.Count: {1}", Prog, Programs.Count));
                if (Prog < 0 || Prog >= Programs.Count)
                    return false;
                XElement program = Programs[Prog];
                string v0;// = program.Attribute("Type").Value;
                if (GetAttribute(program, "Type", out v0) && v0 == "Position")
                {
                    //Console.WriteLine(String.Format("FormatPositionProgram - Type: {0}", v0));
                    string v1;// = program.Attribute("Profile").Value;
                    string v2;// = program.Attribute("Velocity").Value;
                    if (GetAttribute(program, "Profile", out v1) && GetAttribute(program, "Velocity", out v2)
                        && Int32.TryParse(v1, out ProgProfile) && Int32.TryParse(v2, out ProgVelocity))
                    {
                        //Console.WriteLine(String.Format("FormatPositionProgram - Profile: {0}, Velocity: {1}", ProgProfile, ProgVelocity));
                        IEnumerable<XElement> items =
                        from el in program.Descendants("Step")
                        select el;
                        List<XElement> pos = items.ToList();

                        //Console.WriteLine(String.Format("     pos.Count: {0}", pos.Count));
                        if (pos.Count < 2)
                            return false;
                        var posList = new List<KeyValuePair<int, int>>();
                        bool res = true;
                        for (int n = 0; n < pos.Count; n++)
                        {
                            int w1; int w2;
                            string v3;// = pos[n].Attribute("Number").Value;
                            string v4;// = pos[n].Attribute("Position").Value;
                            if (GetAttribute(pos[n], "Number", out v3) && GetAttribute(pos[n], "Position", out v4)
                                && Int32.TryParse(v3, out w1) && Int32.TryParse(v4, out w2))
                            {
                                if (w2 < 0 || w2 > Motor.MaxPosition)
                                {
                                    //Console.WriteLine(String.Format("     w1: {0}, w2: {1}, MaxPosition: {2}", w1, w2, Motor.MaxPosition));
                                    res = false;
                                    break;
                                }
                                else
                                    posList.Add(new KeyValuePair<int, int>(w1, w2));

                            }
                            else
                            {
                                //if (GetAttribute(pos[n], "Number", out v3))
                                //{
                                //    Console.WriteLine(String.Format("     Step {0}, Number String: {1}", n, v3));
                                //}
                                //else
                                //{
                                //    Console.WriteLine(String.Format("     Step {0}, could not get Number String", n));
                                //}

                                //if (GetAttribute(pos[n], "Position", out v4))
                                //{
                                //    Console.WriteLine(String.Format("     Step {0}, Position String: {1}", n, v4));
                                //}
                                //else
                                //{
                                //    Console.WriteLine(String.Format("     Step {0}, could not get Position String", n));
                                //}
                                res = false;
                                break;
                            }
                        }
                        if (res)
                        {
                            //posList = posList.OrderBy(x => x.Key).ToList();
                            for (int n = 0; n < posList.Count; n++)
                                Positions.Add(posList[n].Value);
                        }
                        return res;
                    }
                    else
                    {
                        //if (GetAttribute(program, "Profile", out v1))
                        //{
                        //    Console.WriteLine(String.Format("FormatPositionProgram - Profile String:{0}", v1));
                        //}
                        //else
                        //{
                        //    Console.WriteLine("FormatPositionProgram - Could not get Profile String");
                        //}
                        //if (GetAttribute(program, "Velocity", out v2))
                        //{
                        //    Console.WriteLine(String.Format("FormatPositionProgram - Velocity String:{0}", v2));
                        //}
                        //else
                        //{
                        //    Console.WriteLine("FormatPositionProgram - Could not get Velocity String");
                        //}
                        return false;
                    }
                }
                else
                {
                    //if (GetAttribute(program, "Type", out v0))
                    //{
                    //    Console.WriteLine(String.Format("FormatPositionProgram - Type String:{0}", v0));
                    //}
                    //else
                    //{
                    //    Console.WriteLine("FormatPositionProgram - Could not get Type String");
                    //}
                    return false;
                }
            }
            public static bool GetVelocityProgramHeaders(int MotorNumber, out List<string> ProgramHeaders)
            {
                bool res = false;
                switch (MotorNumber)
                {
                    case 1:
                        res = GetProgramHeaders(RotorVelocityPrograms, out ProgramHeaders);
                        break;
                    default:
                        ProgramHeaders = new List<string>();
                        break;
                }
                return res;
            }

            public static bool GetVelocityProgram(int MotorNumber, int Prog, out int ProgProfile, out List<string> Program)
            {
                bool res = false;
                //Console.WriteLine(String.Format("Inside GetVelocity Program: {0} / {1}", MotorNumber, Prog));
                switch (MotorNumber)
                {
                    case 1:
                        res = FormatVelocityProgram(Rotor, RotorVelocityPrograms, Prog, out ProgProfile, out Program);
                        break;
                    default:
                        Program = new List<string>();
                        ProgProfile = 0;
                        break;
                }
                return res;
            }

            public static bool LoadVelocityProgram(int MotorNumber, int Prog, out string Err)
            {
                Err = "Invalid Motor";
                switch (MotorNumber)
                {
                    case 1:
                        return LoadVelocityProgram(Rotor, RotorVelocitySequencer, RotorVelocityPrograms,
                            Prog, out Err);
                    default:
                        return false;
                }
            }
            public static bool PrintVelocityProgram(int MotorNumber, int Prog = 0)
            {
                bool res = true;
                int pProfile;
                switch (MotorNumber)
                {
                    case 1:
                        if (FormatVelocityProgram(Rotor, RotorVelocityPrograms, Prog, out pProfile, out List<string> program))
                        {
                            Console.WriteLine(String.Format("Rotor Velocity Progarm {0}", Prog));
                            Console.WriteLine(String.Format("Profile: {0}", pProfile));
                            for (int n = 0; n < program.Count; n++)
                                Console.WriteLine(String.Format("{0} : {1}", n, program[n]));
                        }
                        else
                            res = false;
                        break;
                    default:
                        res = false;
                        break;
                }
                if (res)
                    Console.WriteLine("Program Printed");
                else
                    Console.WriteLine("Program not Printed");
                return res;
            }
            public static bool AddVelocityProgram(int MotorNumber, string ProgramString)
            {
                if (MotorNumber < 1 || MotorNumber > 4 || ProgramString.Length < 40)
                    return false;
                bool res = true;
                if (StringToXDoc(ProgramString, out XDocument xDoc))
                {
                    if (GetXElementProgram(xDoc, out XElement xProg))
                    {
                        var elList = new List<XElement>{ xProg };

                        switch (MotorNumber)
                        {
                            case 1:
                                if (FormatVelocityProgram(Rotor, elList, 0, out _, out _))
                                    RotorVelocityPrograms.Add(xProg);
                                else
                                    res = false;
                                break;
                            default:
                                res = false;
                                break;
                        }
                        if (!res)
                        {
                            Console.WriteLine("FormatVelocityProgram failed");
                        }
                    }
                    else
                    {
                        res = false;
                        Console.WriteLine("GetXElementProgram failed");
                    }
                }
                else
                {
                    res = false;
                    Console.WriteLine("StringToXDoc failed");
                    Console.WriteLine(ProgramString);
                }

                return res;
            }
            internal static bool LoadVelocityProgram(StepperMotor Motor, VelocitySequencer MSequencer, List<XElement> Programs,
                int Prog, out string Err)
            {
                int pProfile;
                if (Programs.Count < 1)
                {
                    Err = "No Programs in Configuration";
                    return false;
                }
                else if (Prog < 0 || Prog >= Programs.Count)
                {
                    Err = "Invalid Program number";
                    return false;
                }

                if (FormatVelocityProgram(Motor, Programs, Prog,
                    out pProfile, out List<string> program))
                {
                    return MSequencer.LoadVelocityProgram(program, pProfile, out Err);
                }
                else
                {
                    Err = String.Format("Problem with format of velocity program {0}", Prog);
                    return false;
                }
            }
            static bool FormatVelocityProgram(StepperMotor Motor, List<XElement> Programs, int Prog,
                out int ProgProfile, out List<string> Program)
            {
                //Console.WriteLine(String.Format("FormatVelocityProgram: {0} / {1}", Programs.Count, Prog));
                Program = new List<string>();
                ProgProfile = 1;
                if (Prog < 0 || Prog >= Programs.Count)
                    return false;
                XElement program = Programs[Prog];
                //XAttribute xat = program.Attribute("Type");
                string v0;
                string v1;
                int vMin;
                int vMax;
                if (GetAttribute(program, "Type", out v0))
                {
                    //Console.WriteLine(String.Format("FormatVelocityProgram Type: {0}", v0));
                    if (GetAttribute(program, "Profile", out v1) && v0 == "Velocity")
                    {
                        if (Int32.TryParse(v1, out ProgProfile) && Motor.MinMaxVelocity(ProgProfile, out vMin, out vMax))
                        {
                            //Console.WriteLine(String.Format("FormatVelocityProgram Profile: {0}, vMin/Max: {1} / {2}", ProgProfile, vMin, vMax));
                            IEnumerable<XElement> items =
                                from el in program.Descendants("Step")
                                select el;
                            List<XElement> vel = items.ToList();
                            //Console.WriteLine(String.Format("FormatVelocityProgram vel.Count: {0}", vel.Count));
                            if (vel.Count < 1)
                                return false;
                            List<string> velList = new List<string>();
                            bool res = true;

                            for (int n = 0; n < vel.Count; n++)
                            {
                                int w1; int w2; double w3;
                                string v3; 
                                string v4;
                                string v5;
                                string v6;
                                if (GetAttribute(vel[n], "Number", out v3)
                                    && GetAttribute(vel[n], "Velocity", out v4)
                                    && GetAttribute(vel[n], "Time", out v5)
                                    && GetAttribute(vel[n], "Description", out v6)
                                    )
                                {
                                    //Console.WriteLine(String.Format("FormatVelocityProgram v: {0} / {1} / {2} / {3}", v3, v4, v5, v6));
                                    if (Int32.TryParse(v3, out w1) && Int32.TryParse(v4, out w2) && Double.TryParse(v5, out w3))
                                    {
                                        if (w1 < 0 || w2 < vMin || w2 > vMax || w3 < 0.5)
                                        {
                                            res = false;
                                            break;
                                        }
                                        else
                                            velList.Add(String.Format("{0},{1},{2}", w2, w3, v6));

                                    }
                                    else
                                    {
                                        res = false;
                                        break;
                                    }
                                }
                                else
                                {
                                    res = false;
                                    break;
                                }
                            }
                            if (res)
                            {
                                Program = velList;
                            }
                            return res;
                        }
                        return false;
                    }
                    else
                        return false;
                }
                else
                    return false;
            }
            public static bool AddPIDProgram(string ProgramString)
            {
                if (ProgramString.Length < 40)
                    return false;
                if (StringToXDoc(ProgramString, out XDocument xDoc))
                {
                    if (GetXElementProgram(xDoc, out XElement xProg))
                    {
                        var elList = new List<XElement>{ xProg };
                        if (FormatPIDProgram(elList, 0, out List<string> _))
                        {
                            PIDPrograms.Add(xProg);
                            return true;
                        }
                        else
                            return false;
                    }
                    else
                        return false;
                }
                return false;
            }
            public static bool LoadPIDProgram(int Prog, out string Err)
            {
                if (PIDPrograms.Count < 1)
                {
                    Err = "No Programs in Configuration";
                    return false;
                }
                else if (Prog < 0 || Prog >= PIDPrograms.Count)
                {
                    Err = "Invalid Program number";
                    return false;
                }
                if (FormatPIDProgram(PIDPrograms, Prog, out List<string> temperatureSteps))
                {
                    return TempProgramController.LoadProgram(temperatureSteps, out Err);
                }
                else
                {
                    Err = String.Format("Problem with format of program {0}", Prog);
                    return false;
                }
            }
            static bool FormatPIDProgram(List<XElement> Programs, int Prog, out List<string> TemperatureSteps)
            {
                TemperatureSteps = new List<string>();
                if (Prog < 0 || Prog >= Programs.Count)
                    return false;
                XElement program = Programs[Prog];
                string type;
                if (GetAttribute(program, "Type", out type) && type == "Temperature")
                {
                    IEnumerable<XElement> items =
                        from el in program.Descendants("Step")
                        select el;
                    List<XElement> steps = items.ToList();

                    if (steps.Count < 2)
                        return false;
                    var stepList = new List<KeyValuePair<int, string>>();
                    bool res = true;
                    for (int n = 0; n < steps.Count; n++)
                    {
                        string v1;// = steps[n].Attribute("Number").Value;
                        string v2;// = steps[n].Attribute("Temperature").Value;
                        string v3;// = steps[n].Attribute("Time").Value;
                        string v4;// = steps[n].Attribute("Description").Value;

                        int w1;
                        double x2;
                        double x3;
                        if (GetAttribute(steps[n], "Number", out v1) && GetAttribute(steps[n], "Temperature", out v2) &&
                            GetAttribute(steps[n], "Time", out v3) && GetAttribute(steps[n], "Description", out v4) && 
                            Int32.TryParse(v1, out w1) && Double.TryParse(v2, out x2) && Double.TryParse(v3, out x3))
                        {
                            if (w1 >= 0 && w1 < steps.Count && x2 > 20.0 && x2 < 130.0 && x3 > 0.0)
                            {
                                stepList.Add(new KeyValuePair<int, string>(w1, String.Format("{0},{1},{2}", v2, v3, v4)));
                            }
                            else
                            {
                                res = false;
                                break;
                            }
                        }
                        else
                        {
                            res = false;
                            break;
                        }
                    }
                    //stepList = stepList.OrderBy(x => x.Key).ToList();
                    for (int n = 0; n < stepList.Count; n++)
                    {
                        TemperatureSteps.Add(stepList[n].Value);
                    }
                    return res;
                }
                else
                    return false;
            }

            static public bool GetMotorParams(string MotorName,
                out int MotorN, out int DirectionCode, out List<MotorProfile> MotorProfilesOut,
                out int Backout, out int MaxPosition, out int InitProfile, out int InitVelocity,
                out List<XElement> PositionPrograms, out List<XElement> VelocityPrograms)
            {
                MotorN = 0;
                DirectionCode = 0;
                MotorProfilesOut = new List<MotorProfile>(NumbMotorProfiles);
                MotorProfile newMotorProfile = new MotorProfile();
                newMotorProfile.Used = false;
                for (int n = 0; n < NumbMotorProfiles; n++)
                    MotorProfilesOut.Add(new MotorProfile());
                for (int n = 0; n < MotorProfilesOut.Count; n++)
                    MotorProfilesOut[n].Used = false;

                Backout = 100;
                MaxPosition = 1000;
                InitProfile = 0;
                InitVelocity = 10;
                XElement motorXE = new XElement("Blank");
                PositionPrograms = new List<XElement>();
                VelocityPrograms = new List<XElement>();
                List<XElement> programs = new List<XElement>();
                IEnumerable<XElement> items =
                        from el in _configuration.Descendants("StepperMotor")
                        select el;
                if (items.Count() < 1)
                    return false;
                List<XElement> xst = items.ToList();
                bool foundMotor = false;
                for (int n = 0; n < xst.Count; n++)
                {
                    XAttribute xat = xst[n].Attribute("Name");
                    if (xat.Value == MotorName.Trim(whiteSpace))
                    {
                        motorXE = xst[n];
                        foundMotor = true;
                        break;
                    }
                }
                bool parse = foundMotor;
                if (parse)
                {
                    //XAttribute xat = motorXE.Attribute("Number");
                    //parse = Int32.TryParse(xat.Value, out MotorN);
                    //xat = motorXE.Attribute("DirectionCode");
                    //parse = parse && Int32.TryParse(xat.Value, out DirectionCode);
                    //xat = motorXE.Attribute("BackoutFromHome");
                    //parse = parse && Int32.TryParse(xat.Value, out Backout);
                    //xat = motorXE.Attribute("MaximumPosition");
                    //parse = parse && Int32.TryParse(xat.Value, out MaxPosition);
                    //xat = motorXE.Attribute("Profile");
                    //parse = parse && Int32.TryParse(xat.Value, out InitProfile);
                    //xat = motorXE.Attribute("Velocity");
                    //parse = parse && Int32.TryParse(xat.Value, out InitVelocity);
                    string v0;
                    string v1;
                    string v2;
                    string v3;
                    string v4;
                    string v5;
                    if (GetAttribute(motorXE, "Number", out v0) && GetAttribute(motorXE, "DirectionCode", out v1)
                        && GetAttribute(motorXE, "BackoutFromHome", out v2) && GetAttribute(motorXE, "MaximumPosition", out v3)
                        && GetAttribute(motorXE, "Profile", out v4) && GetAttribute(motorXE, "Velocity", out v5)
                        && Int32.TryParse(v0, out MotorN) && Int32.TryParse(v1, out DirectionCode)
                        && Int32.TryParse(v2, out Backout) && Int32.TryParse(v3, out MaxPosition)
                        && Int32.TryParse(v4, out InitProfile) && Int32.TryParse(v5, out InitVelocity))
                    {
                        parse = true;
                    }
                    else
                    {
                        parse = false;
                    }
                }
                if (parse)
                {
                    items =
                        from el in motorXE.Descendants("Profiles")
                        select el;
                    if (items.Count() > 0)
                    {
                        List<XElement> xpr = items.ToList();
                        for (int n = 0; n < xpr.Count; n++)
                        {
                            items = from el in xpr.Descendants("Profile")
                                    select el;
                            if (items.Count() > 0)
                            {
                                List<XElement> xp = items.ToList();
                                for (int m = 0; m < xp.Count; m++)
                                {
                                    string v0;
                                    string v1;
                                    string v2;
                                    int w0 = -1;
                                    int w1 = -1;
                                    int w2 = -1;
                                    //XAttribute xat = xp[m].Attribute("Number");
                                    //parse = parse && Int32.TryParse(xat.Value, out w0);
                                    if (GetAttribute(xp[m], "Number", out v0) && Int32.TryParse(v0, out w0) && w0 >= 0 && w0 < NumbMotorProfiles)
                                    {
                                        //xat = xp[m].Attribute("MinimumVelocity");
                                        //parse = parse && Int32.TryParse(xat.Value, out w1);
                                        //xat = xp[m].Attribute("MaximumVelocity");
                                        //parse = parse && Int32.TryParse(xat.Value, out w2);
                                        if ( GetAttribute(xp[m], "MinimumVelocity", out v1) && GetAttribute(xp[m], "MaximumVelocity", out v2)
                                            && Int32.TryParse(v1, out w1) && Int32.TryParse(v2, out w2)
                                            && parse && w1 > 0 && w2 > w1 && !MotorProfilesOut[w0].Used)
                                        {
                                            MotorProfilesOut[w0].Used = true;
                                            MotorProfilesOut[w0].MinimumVelocity = w1;
                                            MotorProfilesOut[w0].MaximumVelocity = w2;
                                        }
                                        else
                                            parse = false;
                                    }
                                    else
                                        parse = false;

                                    if (!parse)
                                        break;
                                }
                            }
                            if (!parse)
                                break;
                        }
                    }
                    else
                        parse = false;
                }
                if (parse)
                {
                    items =
                        from el in motorXE.Descendants("Program")
                        select el;
                    if (items.Count() > 0)
                    {
                        programs = items.ToList();
                        for (int n = 0; n < programs.Count; n++)
                        {
                            string pType;// = programs[n].Attribute("Type").Value;
                            if (GetAttribute(programs[n], "Type", out pType))
                            {
                                if (pType.IndexOf("Position") >= 0)
                                    PositionPrograms.Add(programs[n]);

                                else if (pType.IndexOf("Velocity") >= 0)
                                    VelocityPrograms.Add(programs[n]);
                            }
                        }
                    }
                }
                return parse;
            }

            static public bool GetPIDParams(out double IdleTemperature, out double LowTemperature,
                out double HighTemperature, out List<XElement> PIDPrograms)
            {
                bool xmlOK = true;
                PIDPrograms = new List<XElement>();
                IEnumerable<XElement> items =
                        from el in _configuration.Descendants("PID")
                        select el;
                //Console.WriteLine(String.Format("items.Count = {0}", items.Count()));
                //Console.Out.Flush();
                XElement pidXE = items.First();
                string v0;
                string v1;
                string v2;
                IdleTemperature = 40.0;
                LowTemperature = 60.0;
                HighTemperature = 95.0;
                if ( GetAttribute(pidXE, "IdleTemperature", out v0) && GetAttribute(pidXE, "LowTemperature", out v1)
                    && GetAttribute(pidXE, "HighTemperature", out v2)
                    && Double.TryParse(v0, out IdleTemperature) && Double.TryParse(v1, out LowTemperature)
                    && Double.TryParse(v2, out HighTemperature)
                    )
                {
                    xmlOK = true;
                }
                else
                {
                    xmlOK = false;
                }
                //xmlOK = Double.TryParse(pidXE.Attribute("IdleTemperature").Value, out IdleTemperature);
                //xmlOK = Double.TryParse(pidXE.Attribute("LowTemperature").Value, out LowTemperature) && xmlOK;
                //xmlOK = Double.TryParse(pidXE.Attribute("HighTemperature").Value, out HighTemperature) && xmlOK;
                //Console.WriteLine(String.Format("I:{0}, L:{1}, H:{2}", IdleTemperature, LowTemperature, HighTemperature));
                //Console.Out.Flush();
                if (xmlOK)
                {
                    items = from el in pidXE.Descendants("Program")
                            select el;
                    if (items.Count() > 0)
                        PIDPrograms = items.ToList();
                }
                return xmlOK;
            }

            static public bool GetThermistorParameters(out string WgtString, out int[] Weights, 
                out string BiasString, out double[] Biases, out string Err)
            {
                bool wgtsOK = true;
                bool biasesOK = true;
                Err = "";
                IEnumerable<XElement> items =
                    from el in _configuration.Descendants("Thermistors")
                    select el;
                XElement xtherm = items.First();
                if (GetAttribute(xtherm, "ThermistorWeights", out WgtString))
                {
                    string[] weights = WgtString.Split(',');
                    if (weights.Length == 24)
                    {
                        Weights = new int[24];
                        for (int n = 0; n < 24; n++)
                        {
                            if (!Int32.TryParse(weights[n], out Weights[n]) || Weights[n] < 0)
                            {
                                wgtsOK = false;
                                break;
                            }
                        }
                    }
                    else
                    {
                        Weights = new int[0];
                        wgtsOK = false;
                    }
                }
                else
                {
                    Weights = new int[0];
                    WgtString = "";
                    wgtsOK = false;
                }

                //BiasString = xtherm.Attribute("ThermistorBiases").Value;
                if (GetAttribute(xtherm, "ThermistorBiases", out BiasString))
                {
                    string[] biases = BiasString.Split(',');
                    if (biases.Length == 24)
                    {
                        Biases = new double[24];
                        for (int n = 0; n < 24; n++)
                        {
                            if (!Double.TryParse(biases[n], out Biases[n]))
                            {
                                biasesOK = false;
                                break;
                            }
                        }
                    }
                    else
                    {
                        Biases = new double[0];
                        biasesOK = false;
                    }
                }
                else
                {
                    Biases = new double[0];
                    BiasString = "";
                    biasesOK = false;
                }

                if (!wgtsOK)
                    Err = "Problem with thermistor weights in configuration";

                if (!biasesOK)
                {
                    if (Err.Length > 1)
                    {
                        Err = String.Format("{0}\n{1}", Err, "Problem with thermistor biases in configuration");
                    }
                    else
                    {
                        Err = "Problem with thermistor biases in configuration";
                    }
                }
                return (wgtsOK && biasesOK);
            }
            public static bool GetLEDParams(out int NumbLEDs, out int[] LEDMax,
                out int[] LEDLevels, out string[] LEDName)
            {
                bool xmlOK = true;
                IEnumerable<XElement> items =
                        from el in _configuration.Descendants("LEDs")
                        select el;
                XElement xled = items.First();
                items =
                    from el in xled.Descendants("LED")
                    select el;
                List<XElement> xl = items.ToList();
                NumbLEDs = xl.Count;
                LEDMax = new int[numbLEDs];
                LEDLevels = new int[numbLEDs];
                LEDName = new string[numbLEDs];
                for (int n = 0; n < NumbLEDs; n++)
                    LEDMax[n] = -1;
                for (int n = 0; n < NumbLEDs; n++)
                {
                    int index = -1;
                    int max = -1;
                    int inten = -1;
                    string name = "";
                    string v0;
                    string v1;
                    string v2;
                    if ( GetAttribute(xl[n], "Number", out v0) && GetAttribute(xl[n], "MaxIntensity", out v1) && GetAttribute(xl[n], "Intensity", out v2)
                        && Int32.TryParse(v0, out index) && Int32.TryParse(v1, out max) && Int32.TryParse(v2, out inten) 
                        && GetAttribute(xl[n], "Name", out name))
                    {
                        xmlOK = true;
                    }
                    else
                    {
                        xmlOK = false;
                    }
                    //xmlOK = Int32.TryParse(xl[n].Attribute("Number").Value, out index);
                    //xmlOK = Int32.TryParse(xl[n].Attribute("MaxIntensity").Value, out max) && xmlOK;
                    //xmlOK = Int32.TryParse(xl[n].Attribute("Intensity").Value, out inten) && xmlOK;
                    //name = xl[n].Attribute("Name").Value;
                    if (xmlOK && index >= 0 && index < NumbLEDs && max > 0 && max < 101 && inten >= 0 && inten <= max)
                    {
                        LEDMax[index] = max;
                        LEDLevels[index] = inten;
                        LEDName[index] = name;
                    }
                    else
                    {
                        xmlOK = false;
                        break;
                    }
                }
                for (int n = 0; n < NumbLEDs; n++)
                {
                    if ( LEDMax[n] <= 0 )
                    {
                        xmlOK = false;
                        break;
                    }
                }
                return xmlOK;
            }
            public static bool GetFilterParams(out int NumbFilters, out string[] FilterName)
            {
                bool xmlOK = true;
                IEnumerable<XElement> items =
                        from el in _configuration.Descendants("Filters")
                        select el;
                XElement xled = items.First();
                items =
                    from el in xled.Descendants("Filter")
                    select el;
                List<XElement> xl = items.ToList();
                NumbFilters = xl.Count;
                FilterName = new string[NumbFilters];
                for (int n = 0; n < NumbFilters; n++)
                    FilterName[n] = "-";
                for (int n = 0; n < NumbFilters; n++)
                {
                    int index;
                    //string name;
                    string v0;
                    string v1;
                    if ( GetAttribute(xl[n], "FilterPosition", out v0) && GetAttribute(xl[n], "FilterName", out v1)
                        && Int32.TryParse(v0, out index) && index < NumbFilters && v1.Length > 2 )
                    //xmlOK = Int32.TryParse(xl[n].Attribute("FilterPosition").Value, out index);
                    //name = xl[n].Attribute("FilterName").Value;
                    //if (xmlOK && index >= 0 && index < NumbFilters && name.Length > 2)
                    {
                        FilterName[index] = v1.Trim(whiteSpace);
                        xmlOK = true;
                    }
                    else
                    {
                        xmlOK = false;
                        break;
                    }
                }
                for (int n = 0; n < NumbFilters; n++)
                {
                    if (FilterName[n].Length < 3)
                    {
                        xmlOK = false;
                        break;
                    }
                }
                return xmlOK;
            }

            public static int GetDyeParams(string[] LEDName, string[] FilterName, out int NumbDyes, out DyeInformation[] DyeInfo)
            {
                int numbDyesConfigured = 0;
                IEnumerable<XElement> items =
                        from el in _configuration.Descendants("Dyes")
                        select el;
                XElement xled = items.First();
                items =
                    from el in xled.Descendants("Dye")
                    select el;
                List<XElement> xl = items.ToList();
                NumbDyes = xl.Count;
                DyeInfo = new DyeInformation[NumbDyes];

                for (int n = 0; n < NumbDyes; n++)
                {
                    bool xmlOK = true;
                    int gain = 100;
                    int exposure = 100;
                    string v0;
                    string v1;
                    string v2;
                    string v3;
                    string v4;
                    string dName = "";
                    string fName = "";
                    string lName = "";
                    if ( GetAttribute(xl[n], "Gain", out v0) && GetAttribute(xl[n], "ExposureTime", out v1)
                        && Int32.TryParse(v0, out gain) && Int32.TryParse(v1, out exposure)
                        && GetAttribute(xl[n], "Name", out v2) && GetAttribute(xl[n], "FilterName", out v3) 
                        && GetAttribute(xl[n], "LEDName", out v4) )
                    {
                        xmlOK = true;
                        dName = v2.Trim(whiteSpace);
                        fName = v3.Trim(whiteSpace);
                        lName = v4.Trim(whiteSpace);
                    }
                    else
                    {
                        xmlOK = false;
                    }
                    //xmlOK = Int32.TryParse(xl[n].Attribute("Gain").Value, out gain);
                    //xmlOK = Int32.TryParse(xl[n].Attribute("ExposureTime").Value, out exposure) && xmlOK;
                    //string dName = xl[n].Attribute("Name").Value.Trim(whiteSpace);
                    //string fName = xl[n].Attribute("FilterName").Value;
                    //string lName = xl[n].Attribute("LEDName").Value;
                    if (xmlOK && dName.Length > 2 && FilterName.Length > 2 && LEDName.Length > 2)
                    {
                        int fNumb = -1;
                        int lNumb = -1;
                        for (int m = 0; m < LEDName.Length; m++)
                        {
                            if (lName.IndexOf(LEDName[m]) == 0)
                                lNumb = m;
                        }
                        for (int m = 0; m < FilterName.Length; m++)
                        {
                            if (fName.IndexOf(FilterName[m]) == 0)
                                fNumb = m;
                        }
                        DyeInfo[n].Name = dName;
                        DyeInfo[n].FilterPosition = fNumb;
                        DyeInfo[n].LEDPosition = lNumb;
                        DyeInfo[n].FilterName = fName;
                        DyeInfo[n].LEDName = lName;
                        DyeInfo[n].Gain = gain;
                        DyeInfo[n].Exposure = exposure;

                        if (fNumb >= 0 && lNumb >= 0)
                            numbDyesConfigured++;
                    }
                }
                return numbDyesConfigured;
            }
        }
    }
}
