using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
//using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Timers;
using System.IO;
using System.IO.Ports;

namespace sdpcrInstrLib
{
    using BoardComm;
    using Hardware;
    using OneResponse;
    // public delegate void EnqueueResponse(ResponseItem Resp);
    // public delegate void EnqueueInfo(string str);
    namespace PIDController
    {
        //public struct TempProgUnit
        //{
        //    public double Time;
        //    public double Temperature;
        //    public string Description;
        //}
        public enum PTemp
        {
            Idle,
            Low,
            High
        }

        //public enum WhenDone
        //{
        //    CntrlOff_MnlOff,
        //    CntrlOn_MnlOff,
        //    CntrlOn_MnlOn
        //}
        static class PIdx
        {
            // Count must be set to the number of items below
            public const int PID_On = 0;
            public const int PID_Off = 1;
            public const int PIDStatus = 2;
            public const int ReadTemp = 3;
            public const int SetIdleTemp = 4;
            public const int SetLowTemp = 5;
            public const int SetHighTemp = 6;
            public const int GetIdleTemp = 7;
            public const int GetLowTemp = 8;
            public const int GetHighTemp = 9;
            public const int Idle = 10;
            public const int Low = 11;
            public const int High = 12;

            public const int Manual_On = 13;
            public const int Manual_Off = 14;
            public const int SetManualTemp = 15;
            public const int GetTargetTemp = 16;


            // This must The number ofitems above
            public const int Count = 17;

        }
        public class PIDCtl : HardwareBase
        {
            char[] whiteSpace = { ' ', '\n', '\r', '\t', ',' };  // space, new line, carriage return, tab, comma
            protected System.Timers.Timer responseTimer;

            private static readonly object lockIdleLowHighManual = new object();
            double _idleTemp;
            double _lowTemp;
            double _highTemp;
            double _manualTemp;
            private static readonly object lockTemps = new object();
            double _targetTemp;
            double _processTemp;
            long _currentTime;
            bool _targetTempUpdated;
            bool _processTempUpdated;
            private static readonly object lockStatus = new object();
            bool _PIDStatus;
            bool _manualStatus;
            bool _statusUpdated;

            List<string> _errorMsg;
            //public PTemp lastLevelUsed;

            //static System.Timers.Timer tempTimer;
            //SynchronizedCollection<string> listTemp = new SynchronizedCollection<string>();
            //string _logFilePath;
            //FileStream FS;
            //bool tempTimerOn;
            //int _timeStep = 2000; // milliseconds, for temperature monitoring

            //bool logCmds;
            //bool logTemp;

            //int responseTimerCount;
            readonly string[] CmdList;
            SingleResponse startup;
            //string defaultString;
            //int boardWaitTime;
            //int updateTime;
            //int _calibrationTime;
            //bool _busyCalibrating;

            //char[] commaDelimiters = { ',' };

            public double ProcessTemperature
            {
                get
                {
                    FetchProcessTemperature();
                    double res = 0.0;
                    for (int n = 0; n < 10; n++)
                    {
                        System.Threading.Thread.Sleep(BoardWaitTime);
                        lock (lockTemps)
                        {
                            if (_processTempUpdated)
                            {
                                res = _processTemp;
                                break;
                            }
                        }
                    }
                    return res;
                }
            }
            public double TargetTemperature
            {
                get
                {
                    FetchTargetTemperature();
                    double res = 0.0;
                    for (int n = 0; n < 10; n++)
                    {
                        System.Threading.Thread.Sleep(BoardWaitTime);
                        lock (lockTemps)
                        {
                            if (_targetTempUpdated)
                            {
                                res = _targetTemp;
                                break;
                            }
                        }
                    }
                    return res;
                }
            }
            public bool PIDStatus
            {
                get
                {
                    FetchPIDStatus();
                    bool res = false;
                    for (int n = 0; n < 10; n++)
                    {
                        System.Threading.Thread.Sleep(BoardWaitTime);
                        lock (lockStatus)
                        {
                            if (_statusUpdated)
                            {
                                res = _PIDStatus;
                                break;
                            }
                        }
                    }
                    return res;
                }
            }
            public bool ManualControlStatus
            {
                get
                {
                    lock (lockStatus) { return _manualStatus; }
                }
            }
            public long CurrentTime
            {
                get
                {
                    lock (lockTemps)
                    {
                        return _currentTime;
                    }
                }
            }
            //public int BoardWaitTime
            //{
            //    get
            //    {
            //        return boardWaitTime;
            //    }
            //}
            public PIDCtl(ControlBoard Board, out bool Connected, double IdleTemperature = 40.0,
                double LowTemperature = 65.0, double HighTemperature = 95.0) : base()
            {
                sdPCRBoard = Board;
                _errorMsg = new List<string>();
                CmdList = new string[PIdx.Count];
                CmdList[PIdx.PID_On] = "/70C1R";            //  0 - Turn PID Controller On
                CmdList[PIdx.PID_Off] = "/70C0R";           //  1 - Turn PID Controller Off
                CmdList[PIdx.PIDStatus] = "/70S0R";         //  2 - Get PID Controller Status
                CmdList[PIdx.ReadTemp] = "/70R0R";          //  3 - Reads current temperature (process temperature)
                                                            //    The following three commands are stubs and must be followed
                                                            //      by the temperature (double) and an R
                CmdList[PIdx.SetIdleTemp] = "/70F0T";       //  4 - Set Idle Temperature
                CmdList[PIdx.SetLowTemp] = "/70F1T";        //  5 - Set Low Temperature
                CmdList[PIdx.SetHighTemp] = "/70F2T";       //  6 - Set High Temperature
                                                            //
                CmdList[PIdx.GetIdleTemp] = "/70F0R";       //  7 - Get Idle Temperature
                CmdList[PIdx.GetLowTemp] = "/70F1R";        //  8 - Get Low Temperature
                CmdList[PIdx.GetHighTemp] = "/70F2R";       //  9 - Get High Temperature
                                                            //    If Manual control is on, the next three commands are ignored
                CmdList[PIdx.Idle] = "/70T0R";              // 10 - Set PID to Idle temperature
                CmdList[PIdx.Low] = "/70T1R";               // 11 - Set PID to Low temperature
                CmdList[PIdx.High] = "/70T2R";              // 12 - Set PID to High Temperature
                CmdList[PIdx.Manual_On] = "/70M1R";         // 13 - Set Manual mode on, PID follows manual reference temperature
                CmdList[PIdx.Manual_Off] = "/70M0R";        // 14 - Set Manual mode off, PID follows Idle, Low or High (which ever was last set)
                CmdList[PIdx.SetManualTemp] = "/70A";       // 15 - Set Manual temperature (stub), 
                                                            //      must be followed by reference temperture (double) and R
                CmdList[PIdx.GetTargetTemp] = "/70QR";      // 16 - Get Manual temperature


                //Console.WriteLine("Finished Loading CmdList");
                //Console.Out.Flush();

                lock (lockIdleLowHighManual)
                {
                    _idleTemp = IdleTemperature;
                    _lowTemp = LowTemperature;
                    _highTemp = HighTemperature;
                }
                lock (lockTemps)
                {
                    _processTemp = 0.0;
                    _manualTemp = _idleTemp;
                    _targetTemp = _idleTemp;
                    _targetTempUpdated = true;
                    _processTempUpdated = true;
                }
                lock ( lockStatus)
                {
                    _PIDStatus = false;
                    _manualStatus = false;
                    _statusUpdated = true;
                }

                //logCmds = false;
                //logTemp = false;
                //lastLevelUsed = PTemp.Idle;
                startup = new SingleResponse(sdPCRBoard);
                BoardWaitTime = sdPCRBoard.BoardWaitTime;
                //updateTime = 2 * boardWaitTime;
                //tempTimerOn = false;

                if (TestConnection(ref _errorMsg))
                {
                    responseTimer = new System.Timers.Timer(TimeStep);
                    // hook up the elapsed event for the timer. 
                    responseTimer.AutoReset = false;
                    responseTimer.Enabled = false;
                    responseTimer.Elapsed += CheckBoardResponseList;

                    //Console.WriteLine(String.Format("Board Wait Time: {0}", boardWaitTime));
                    Connected = true;
                }
                else
                    Connected = false;

                _connected = Connected;
            }


            public void PIDControllerOn()
            {
                sdPCRBoard.AddCommand(CmdList[PIdx.PID_On], Notification.OnError, LogResponses);
                CmdsSent.Enqueue(CmdList[PIdx.PID_On]);
            }
            public void PIDControllerOff()
            {
                sdPCRBoard.AddCommand(CmdList[PIdx.PID_Off], Notification.OnError, LogResponses);
                CmdsSent.Enqueue(CmdList[PIdx.PID_Off]);
            }
            void FetchPIDStatus()
            {
                lock( lockStatus)
                {
                    _statusUpdated = false;
                }
                sdPCRBoard.AddCommand(CmdList[PIdx.PIDStatus], Notification.OnError, LogResponses);
                CmdsSent.Enqueue(CmdList[PIdx.PIDStatus]);
            }
            public bool SetPIDTemp(PTemp Level, double Temperature)
            {
                string c;
                if (Temperature < 20.0 || Temperature > 140.0)
                    return false;
                switch (Level)
                {
                    case PTemp.Idle:
                        c = String.Format("{0}{1:0.00}R", CmdList[PIdx.SetIdleTemp], Temperature);
                        sdPCRBoard.AddCommand(c, Notification.OnError, LogResponses);
                        CmdsSent.Enqueue(c);
                        break;
                    case PTemp.Low:
                        c = String.Format("{0}{1:0.00}R", CmdList[PIdx.SetLowTemp], Temperature);
                        sdPCRBoard.AddCommand(c, Notification.OnError, LogResponses);
                        CmdsSent.Enqueue(c);
                        break;
                    case PTemp.High:
                        c = String.Format("{0}{1:0.00}R", CmdList[PIdx.SetHighTemp], Temperature);
                        sdPCRBoard.AddCommand(c, Notification.OnError, LogResponses);
                        CmdsSent.Enqueue(c);
                        break;
                }
                return true;
            }
            public double GetPIDTemp(PTemp Level)
            {
                double t = 0.0;
                switch (Level)
                {
                    case PTemp.Idle:
                        sdPCRBoard.AddCommand(CmdList[PIdx.GetIdleTemp], Notification.OnError, LogResponses);
                        CmdsSent.Enqueue(CmdList[PIdx.GetIdleTemp]);
                        System.Threading.Thread.Sleep(BoardWaitTime);
                        lock (lockIdleLowHighManual) { t = _idleTemp; }
                        break;
                    case PTemp.Low:
                        sdPCRBoard.AddCommand(CmdList[PIdx.SetLowTemp], Notification.OnError, LogResponses);
                        CmdsSent.Enqueue(CmdList[PIdx.SetLowTemp]);
                        System.Threading.Thread.Sleep(BoardWaitTime);
                        lock (lockIdleLowHighManual) { t = _lowTemp; }
                        break;
                    case PTemp.High:
                        sdPCRBoard.AddCommand(CmdList[PIdx.SetHighTemp], Notification.OnError, LogResponses);
                        CmdsSent.Enqueue(CmdList[PIdx.SetHighTemp]);
                        System.Threading.Thread.Sleep(BoardWaitTime);
                        lock (lockIdleLowHighManual) { t = _highTemp; }
                        break;
                }
                return t;
            }
            public void UseIdle()
            {
                sdPCRBoard.AddCommand(CmdList[PIdx.Idle], Notification.OnError, LogResponses);
                CmdsSent.Enqueue(CmdList[PIdx.Idle]);
                lock (lockIdleLowHighManual) 
                { 
                    lock (lockTemps) { _targetTemp = _idleTemp; } 
                }
                //lastLevelUsed = PTemp.Idle;
            }
            public void UseLow()
            {
                sdPCRBoard.AddCommand(CmdList[PIdx.Low], Notification.OnError, LogResponses);
                CmdsSent.Enqueue(CmdList[PIdx.Low]);
                lock (lockIdleLowHighManual)
                {
                    lock (lockTemps) { _targetTemp = _lowTemp; }
                }
                //lastLevelUsed = PTemp.Low;
            }
            public void UseHigh()
            {
                sdPCRBoard.AddCommand(CmdList[PIdx.High], Notification.OnError, LogResponses);
                CmdsSent.Enqueue(CmdList[PIdx.High]);
                lock (lockIdleLowHighManual)
                {
                    lock (lockTemps) { _targetTemp = _highTemp; }
                }
                //lastLevelUsed = PTemp.High;
            }
            public bool ManualControlOn()
            {
                sdPCRBoard.AddCommand(CmdList[PIdx.Manual_On], Notification.OnError, LogResponses);
                CmdsSent.Enqueue(CmdList[PIdx.Manual_On]);
                lock (lockStatus)
                {
                    _manualStatus = true;
                }
                lock (lockIdleLowHighManual)
                {
                    lock (lockTemps)
                    {
                        _targetTemp = _manualTemp;
                    }
                }
                return _PIDStatus;
            }
            public void ManualControlOff()
            {
                sdPCRBoard.AddCommand(CmdList[PIdx.Manual_Off], Notification.OnError, LogResponses);
                CmdsSent.Enqueue(CmdList[PIdx.Manual_Off]);
                lock (lockStatus) { _manualStatus = false; }
            }
            public bool SetManualTemperature(double Temperature)
            {
                if (Temperature < 20.0 || Temperature > 140.0)
                    return false;
                string c = String.Format("{0}{1:0.00}R", CmdList[PIdx.SetManualTemp], Temperature);
                sdPCRBoard.AddCommand(c, Notification.OnError, LogResponses);
                CmdsSent.Enqueue(c);
                return true;
            }

            void FetchProcessTemperature()
            {
                lock (lockTemps)
                {
                    _processTempUpdated = false;
                    _currentTime = sdPCRBoard.BoardTime();
                }
                sdPCRBoard.AddCommand(CmdList[PIdx.ReadTemp], Notification.OnError, LogResponses);
                CmdsSent.Enqueue(CmdList[PIdx.ReadTemp]);
            }
            void FetchTargetTemperature()
            {
                lock (lockTemps)
                {
                    _targetTempUpdated = false;
                }
                sdPCRBoard.AddCommand(CmdList[PIdx.GetTargetTemp], Notification.OnError, LogResponses);
                CmdsSent.Enqueue(CmdList[PIdx.GetTargetTemp]);
            }

            bool useStartup(int K, int Wait, string Cmd,
                ref List<string> ErrMsg, out string StartTime, out string ResponseValue)
            {
                ResponseItem resp;
                startup.AddCommand(Cmd);
                ErrMsg.Add(String.Format("Command: {0}", Cmd));
                //Console.WriteLine(String.Format("PID Startup - Command: {0}", Cmd));
                StartTime = "";
                ResponseValue = "";
                bool cont = false;
                for (int n = 0; n < 5; n++)
                {
                    if (startup.GetResponse(Wait, out resp))
                    {
                        //Console.WriteLine(resp.ToString());
                        if (resp.Response.IndexOf(DefaultResponses[0]) >= 0)
                        {
                            cont = true;
                            StartTime = String.Format("{0}:{1}", K, n);
                            ResponseValue = resp.Response.Substring(2);
                            break;
                        }
                        else
                        {
                            bool err = false;
                            for (int m = 1; m < DefaultResponses.Count; m++)
                            {
                                if (resp.Response.IndexOf(DefaultResponses[m]) >= 0)
                                {
                                    err = true;
                                    break;
                                }
                            }
                            if (err)
                            {
                                ErrMsg.Add(String.Format("{0}:{1}, Cmd: {2}, Resp: {3}", K, n, resp.Command, resp.Response));
                                //Console.WriteLine(String.Format("{0}:{1}, Cmd: {2}, Resp: {3}", K, n, resp.Command, resp.Response));
                                ResponseValue = resp.Response.Substring(2);
                            }
                            else if (resp.Response.IndexOf("/0") >= 0)
                            {
                                if (Cmd.IndexOf(CmdList[PIdx.PIDStatus]) >= 0)
                                {
                                    if (resp.Response.IndexOf("/00") >= 0 || resp.Response.IndexOf("/01") >= 0)
                                    {
                                        ResponseValue = resp.Response.Substring(2);
                                        cont = true;
                                        break;
                                    }
                                }
                                else if (Cmd.IndexOf(CmdList[PIdx.ReadTemp]) >= 0
                                    || Cmd.IndexOf(CmdList[PIdx.GetIdleTemp]) >= 0
                                    || Cmd.IndexOf(CmdList[PIdx.GetLowTemp]) >= 0
                                    || Cmd.IndexOf(CmdList[PIdx.GetIdleTemp]) >= 0
                                    || Cmd.IndexOf(CmdList[PIdx.GetTargetTemp]) >= 0)
                                {
                                    int respStart = resp.Response.IndexOf("/0") + 2;
                                    int len = resp.Response.Length - respStart;
                                    double d1;
                                    if (Double.TryParse(resp.Response.Substring(respStart, len), out d1))
                                    {
                                        ResponseValue = resp.Response.Substring(respStart, len);
                                        cont = true;
                                    }
                                    break;
                                }
                                else
                                {
                                    ErrMsg.Add(String.Format("{0}:{1}, Cmd: {2}, Resp: {3}", K, n, resp.Command, resp.Response));
                                    //Console.WriteLine(String.Format("{0} - {1}, Cmd: {2}, Resp: {3}", K, n, resp.Command, resp.Response));
                                    ResponseValue = resp.Response.Substring(2);
                                }
                            }
                        }
                    }
                    else
                    {
                        ErrMsg.Add(String.Format("{0}:{1} - No response", K, n));
                        //Console.WriteLine(String.Format("{0}:{1} - No response, wait = {2}, time = {3}", K, n, Wait, sdPCRBoard.BoardTime()));
                        //Console.Out.Flush();
                    }
                }
                return cont;
            }

            bool TestConnection(ref List<string> ErrMsg)
            {
                // ResponseItem resp;

                List<string> startupTimes = new List<string>();
                int wait = 2 * BoardWaitTime;
                ErrMsg = new List<string>();
                string startTime;
                string respValue;
                double doubleOut;
                bool cont = false;
                string cmd;
                if (useStartup(0, wait, CmdList[PIdx.PID_Off], ref ErrMsg, out startTime, out respValue))
                {
                    startupTimes.Add(startTime);
                    if (useStartup(1, wait, CmdList[PIdx.Manual_Off], ref ErrMsg, out startTime, out respValue))
                    {            
                        startupTimes.Add(startTime);     
                        cmd = String.Format("{0}{1}R", CmdList[PIdx.SetIdleTemp], _idleTemp);
                        if (useStartup(2, wait, cmd, ref ErrMsg, out startTime, out respValue))
                        {
                            startupTimes.Add(startTime);
                            cmd = String.Format("{0}{1}R", CmdList[PIdx.SetLowTemp], _lowTemp);
                            if (useStartup(3, wait, cmd, ref ErrMsg, out startTime, out respValue))
                            {
                                startupTimes.Add(startTime);
                                cmd = String.Format("{0}{1}R", CmdList[PIdx.SetHighTemp], _highTemp);
                                if (useStartup(4, wait, cmd, ref ErrMsg, out startTime, out respValue))
                                {
                                    startupTimes.Add(startTime);
                                    cmd = String.Format("{0}{1}R", CmdList[PIdx.SetManualTemp], _manualTemp);
                                    if (useStartup(5, wait, cmd, ref ErrMsg, out startTime, out respValue))
                                    {
                                        startupTimes.Add(startTime);
                                        if (useStartup(6, wait, CmdList[PIdx.ReadTemp], ref ErrMsg, out startTime, out respValue))
                                        {
                                            if (Double.TryParse(respValue, out doubleOut))
                                            {
                                                if (doubleOut > 0.0 && doubleOut < 140.0)
                                                {
                                                    lock (lockTemps) 
                                                    { 
                                                        _processTemp = doubleOut;
                                                        _processTempUpdated = true;
                                                    }
                                                    startupTimes.Add(startTime);
                                                    if (useStartup(7, wait, CmdList[PIdx.Idle], ref ErrMsg, out startTime, out respValue))
                                                    {
                                                        startupTimes.Add(startTime);
                                                        cont = true;
                                                    }
                                                }
                                                else
                                                {
                                                    ErrMsg.Add(String.Format("Response {0} is out of range", respValue));
#if DEBUG
                                                    InternalLog.Enqueue(String.Format("Time {0}: Response {1} is out of range", 
                                                        sdPCRBoard.BoardTime(), respValue));
#endif
                                                }
                                            }
                                            else
                                            {
                                                ErrMsg.Add(String.Format("Response {0} is not a double", respValue));
#if DEBUG
                                                InternalLog.Enqueue(String.Format("Time {0}: Response {1} is not a double", 
                                                    sdPCRBoard.BoardTime(), respValue));
#endif
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                if (cont)
                {
                    ErrMsg = new List<string>();
                }
                else
                {
                    ErrMsg.Add("     Start up Times");
                    for (int n = 0; n < startupTimes.Count; n++)
                    {
                        ErrMsg.Add(startupTimes[n]);
                    }
                }
                return cont;
            }

            protected override void LogResponses(ResponseItem Resp)
            {
                ResponsesFromBoard.Enqueue(Resp.Clone());
                responseTimer.AutoReset = true;
                responseTimer.Enabled = true;
            }
            void CheckBoardResponseList(object sender, ElapsedEventArgs e)
            {
                ResponseItem resp;
                while (ResponsesFromBoard.TryDequeue(out resp))
                {
                    if (resp.InstrumentSignal)
                    {
                        ResponsesToCaller.Enqueue(resp.Clone());
                        //Console.WriteLine(String.Format("1=====\n{0}\nCmdsSent={1}\n=====", resp.ToString(), CmdsSent));
                    }
                    else if (CmdsSent.Count < 1)
                    {
                        //resp.InstrumentSignal = true;
                        //Console.WriteLine(String.Format("2=====\n{0}\nCmdsSent={1}\n=====", resp.ToString(), CmdsSent));
                        ResponsesToCaller.Enqueue(resp.Clone());
                    }
                    else
                    {
                        string cmd;
                        if (CmdsSent.TryDequeue(out cmd))
                        {
                            int iCmd = cmd.IndexOf(resp.Command);
                            if (iCmd < 0)
                                iCmd = resp.Command.IndexOf(cmd);
                            if (iCmd >= 0)
                            {
                                int cmdIdx = -1;
                                for (int n = 0; n < CmdList.Length; n++)
                                {
                                    if (cmd.IndexOf(CmdList[n]) == 0)
                                    {
                                        cmdIdx = n;
                                        break;
                                    }
                                }
                                int resIdx = -1;
                                int respStart = 2;
                                for (int n = 0; n < DefaultResponses.Count; n++)
                                {
                                    int idx = resp.Response.IndexOf(DefaultResponses[n]);

                                    if (idx >= 0)
                                    {
                                        resIdx = n;
                                        respStart = idx + 2;
                                        break;
                                    }
                                }
                                bool error;
                                if (resIdx == 0)
                                    error = false;
                                else
                                    error = true;
                                //string outString;
                                //double outTime;
                                double outDouble;
                                switch (cmdIdx)
                                {
                                    case PIdx.PID_On:       // 0 - Turns PID controller on
                                        break;
                                    case PIdx.PID_Off:      // 1 - Turns PID controller off
                                        break;
                                    case PIdx.PIDStatus:    // 2 - Gets PID controller status
                                        if (resIdx == -1)
                                        {
                                            lock (lockStatus)
                                            {
                                                if (resp.Response.IndexOf("/00") >= 0)
                                                {
                                                    _PIDStatus = false;
                                                    _statusUpdated = true;
                                                    error = false;
                                                }
                                                else if (resp.Response.IndexOf("/01") >= 0)
                                                {
                                                    _PIDStatus = true;
                                                    _statusUpdated = true;
                                                    error = false;
                                                }
                                            }
                                        }
                                        else
                                            error = true;
                                        break;
                                    case PIdx.ReadTemp:     // 3 - Read current temperature
                                                            //Console.WriteLine(String.Format("resIdx = {0} / respStart = {1} / resp.Response = {2} / substring = {3}",
                                                            //    resIdx, respStart, resp.Response, resp.Response.Substring(respStart)));
                                        if (resIdx == -1)
                                        {
                                            if (Double.TryParse(resp.Response.Substring(respStart), out outDouble))
                                            {
                                                lock (lockTemps)
                                                {
                                                    _processTempUpdated = true;
                                                    _processTemp = outDouble;
                                                }
                                                error = false;
                                            }
                                            else
                                                error = true;
                                        }
                                        else
                                            error = true;
                                        break;
                                    case PIdx.SetIdleTemp:  // 4 - Set Idle setpoint temperature
                                        break;
                                    case PIdx.SetLowTemp:   // 5 - Set Low setpoint temperature
                                        break;
                                    case PIdx.SetHighTemp:  // 6 - Set High setpoint temperature
                                        break;
                                    case PIdx.GetIdleTemp:  // 7 - Get Idle setpoint temperature
                                        if (resIdx == -1)
                                        {
                                            if (Double.TryParse(resp.Response.Substring(respStart), out outDouble))
                                            {
                                                lock (lockIdleLowHighManual)
                                                {
                                                    _idleTemp = outDouble;
                                                }
                                                error = false;
                                            }
                                            else
                                                error = true;
                                        }
                                        else
                                            error = true;
                                        break;
                                    case PIdx.GetLowTemp:   //  8 - Get Low setpoint temperature
                                        if (resIdx == -1)
                                        {
                                            if (Double.TryParse(resp.Response.Substring(respStart), out outDouble))
                                            {
                                                lock (lockIdleLowHighManual)
                                                {
                                                    _lowTemp = outDouble;
                                                }
                                                error = false;
                                            }
                                            else
                                                error = true;
                                        }
                                        else
                                            error = true;
                                        break;
                                    case PIdx.GetHighTemp:  // 9 - Get High setpoint temperature
                                        if (resIdx == -1)
                                        {
                                            if (Double.TryParse(resp.Response.Substring(respStart), out outDouble))
                                            {
                                                lock (lockIdleLowHighManual)
                                                {
                                                    _highTemp = outDouble;
                                                }
                                                error = false;
                                            }
                                            else
                                                error = true;
                                        }
                                        else
                                            error = true;
                                        break;
                                    case PIdx.Idle:     // 10 - Use Idle setpoint temperature
                                        break;
                                    case PIdx.Low:      // 11 - Use Idle setpoint temperature
                                        break;
                                    case PIdx.High:     // 12 - Use High setpoint temperature
                                        break;
                                    case PIdx.Manual_On:     // 13 - Turn Manual Controller on
                                        break;
                                    case PIdx.Manual_Off:     // 14 - Turn Manual Controller off
                                        break;
                                    case PIdx.SetManualTemp:
                                        break;
                                    case PIdx.GetTargetTemp: //  16 - Get Manual setpoint temperature
                                        if (resIdx == -1)
                                        {
                                            if (Double.TryParse(resp.Response.Substring(respStart), out outDouble))
                                            {
                                                lock (lockTemps)
                                                {
                                                    _targetTemp = outDouble;
                                                    _targetTempUpdated = true;
                                                    if (ManualControlStatus)
                                                    {
                                                        lock (lockIdleLowHighManual)
                                                        { _manualTemp = outDouble; }
                                                    }
                                                }
                                                error = false;
                                            }
                                            else
                                                error = true;
                                        }
                                        else
                                            error = true;
                                        break;
                                    default:
                                        break;
                                }

                                if (resp.Notify == Notification.Always)
                                {
                                    //Console.WriteLine(String.Format("3=====\n{0}\nerror={1}\n=====", resp.ToString(), error.ToString()));
                                    ResponsesToCaller.Enqueue(resp.Clone());
                                }
                                else if (error && resp.Notify == Notification.OnError)
                                {
                                    //Console.WriteLine(String.Format("4=====\n{0}\nerror={1}\n=====", resp.ToString(), error.ToString()));
                                    ResponsesToCaller.Enqueue(resp.Clone());
                                }
                            }
                        }
                    }
                }
                if (ResponsesFromBoard.Count <= 0)
                {
                    responseTimer.AutoReset = false;
                    responseTimer.Enabled = false;
                }
            }
            public void Dispose()
            {
                //if (tempTimer.Enabled)
                //{
                //    string err;
                //    StopTempTimer(out err);
                //}
                if (responseTimer.Enabled)
                    responseTimer.Dispose();
            }
        }



    } // namespace PIDController
}
