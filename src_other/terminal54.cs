﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
//using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Timers;
using System.IO;
using System.IO.Ports;
using System.Net;
using System.Xml.Schema;
using System.Linq.Expressions;

using sdpcrInstrLib;
using sdpcrInstrLib.BoardComm;
using sdpcrInstrLib.Hardware;
using sdpcrInstrLib.LEDCluster;
using sdpcrInstrLib.Stepper;
using sdpcrInstrLib.PIDController;
using sdpcrInstrLib.Therm;
using sdpcrInstrLib.InstrExtensions;
using sdpcrInstrLib.Sequencers;
using sdpcrInstrLib.ErrorCodes;

namespace terminal54
{
    public struct tempProgUnit
    {
        public double Temperature;
        public double Time;
        public string Iteration;
    }
    class terminal54
    {
        readonly static char[] whiteSpace = { ' ', '\n', '\r', '\t' };  // space, new line, carriage return, tab || comma
        readonly static char[] whiteSpaceQuote = { ' ', '\n', '\r', '\t', '"', '\'' };  // space, new line, carriage return, tab, ", '
        readonly static char[] whiteSpaceQuoteBack = { ' ', '\n', '\r', '\t', '"', '\'', '\\' };  // space, new line, carriage return, tab, ", '
        //readonly static char[] quotes = { '"', '\'', ' ' };

        public static Stopwatch progTimer = new Stopwatch();
        private static readonly object motorDirectionLock = new object();

        // logging variables
        static SynchronizedCollection<string> logList0 = new SynchronizedCollection<string>();
        static StringBuilder logList0_Name = new StringBuilder();
        static bool logIO = false;
        //static StringBuilder infoString = new StringBuilder();

        // Temperature control 
        private static readonly object currentSetTLock = new object();
        static double currentSetT;
        //static double currentTemp;
        //static double currentTime;
        //static double current24Time;
        //static string current24Temp = "00.99,01.1,02,2,03.3,04.4,05.5,06.6,07.7,08.8,09.9,10.10,"
        //    + "11.11,12.12,13.13,14.14,15.15,16.16,17.17,18.18,19.19,20.20,21.21,22.22,23.23";
        //static string currentIteration = "";

        static int numbLEDs = 6;
        static int[] LEDLevels = { 50, 50, 50, 50, 50, 50 }; // percent
        static int[] LEDMax = { 50, 50, 50, 50, 50, 50 }; // percent
        static LEDCtl ledController;

        //static int[] backOutDistance = { 200, 1500, 1500, 200 }; // steps
        static int[] weights = { 100, 100, 0, 0,
            0, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 0};
        static char[] delimiterChars = { ' ', ',', '\t' }; // space, comma, and tab
        static char[] comma = { ',' };
        //static char[] responseDelimiters = { '>' };
        static string outputDir = "C:\\instrument\\";
        static StringBuilder inputBuffer = new StringBuilder();
        //static int boardTime;
        // logging variables

        // Temperature monitoring variables
        static Thermistors therm24;
        //static System.Timers.Timer timer1;
        //static System.Timers.Timer timer2;
        static SynchronizedCollection<string> list1 = new SynchronizedCollection<string>();
        //static SynchronizedCollection<string> list2 = new SynchronizedCollection<string>();
        static StringBuilder list1_Name = new StringBuilder();
        //static StringBuilder list2_Name = new StringBuilder();
        //static bool timer1On;
        //static bool timer2On;
        static int timeStep = 2000; // milliseconds, for temperature monitoring

        // Temperature Program variables
        static PIDCtl tempCtl;
        static List<tempProgUnit> Cycle = new List<tempProgUnit>();
        static tempProgUnit newTempProgUnit = new tempProgUnit();

        //static List<string> tempProgList = new List<string>();
        //static StringBuilder tempList_Name = new StringBuilder();
        //static System.Timers.Timer tempProgTimer;
        //static int nextStep = -1;
        //static bool offWhenDone;
        //static bool firstStep = false;

        static double[] preloadedTemp0 = { 45.0, 60.0, 75.0, 85.0, 95.0 }; // C

        // Motor test variables
        static StepperMotor rotor;
        static StepperMotor xStage;
        static StepperMotor yStage;
        static StepperMotor filterWheel;

        //static System.Timers.Timer motorTimer;
        //static SynchronizedCollection<string> motorList = new SynchronizedCollection<string>();
        //static StringBuilder motorList_Name = new StringBuilder();
        //static bool motorTimerOn;
        static int motorNumber;
        //static int motorTimerLimit;
        //static int motorTimerModulous;
        static List<int> motorTimerPos = new List<int>();
        //static int motorTimerCount;
        //static int motorTimeStep;
        static int motorProfile;
        static int motorVelocity;

        //static int consoleWaitTime = 300;
        static public ConcurrentQueue<ResponseItem> ResponseQueue;
        static public ConcurrentQueue<string> InfoQueue;

        static public PIDSequencer pidProgram;
        static public string[] pidProgramLog;
        static public bool pidProgramLoaded = false;
        static bool tempProgOn = false;

        static public TempMonitoring thermProgram;
        static public string[] thermProgramLog;
        static public bool thermProgramLoaded = false;
        static public bool thermTimerOn = false;

        static public MotorSequencer motorProgram;
        static public bool motorProgramLoaded = false;
        static public bool motorTimerOn = false;
        static public void LogInfo(string str)
        {
            InfoQueue.Enqueue(str);
        }
        static void Main(string[] args)
        {
            Console.Write("Enter Previous Port Name: ");
            string defPortName = Console.ReadLine().ToUpperInvariant();
            bool useConsole = true;
            bool trackConnect = false;
            bool trackComm = false;
            bool trackResp = false;
            Processor sdPCRProcessor = new Processor(defPortName, useConsole, trackConnect, trackComm, trackResp);
            ResponseItem resp = new ResponseItem();
            string[] portInfo = sdPCRProcessor.PortInformation;
            for (int n = 0; n < portInfo.Length; n++)
                Console.WriteLine(portInfo[n]);
            Console.WriteLine("=====");
            Console.WriteLine(String.Format("\nFirmware: {0}", sdPCRProcessor.FirmwareVersion));
            Console.WriteLine("Press Enter to continue");
            string any;
            Console.ReadLine();

            ControlBoard board;
            bool ledConnected;
            bool pidConnected;
            bool pidSeqConnected = false;
            //bool tempMonConnected = false;
            bool motorSeqConnected = false;
            bool thermConnected;
            bool xStageConnected;
            bool yStageConnected;
            bool filterWheelConnected;
            bool rotorConnected;

            ProcessErrorCode parseCode = new ProcessErrorCode(4);
            string testerr = parseCode.ToString("/0E52M2");
            Console.WriteLine(String.Format("/0E52M2 is interpreted as {0}", testerr));
            Console.WriteLine("");
            lock (currentSetTLock)
            {
                currentSetT = -1.0;
                //currentTemp = -1.0;
                //currentTime = -1.0;
                //current24Time = -1.0;
                //current24Temp = "00.99,01.1,02,2,03.3,04.4,05.5,06.6,07.7,08.8,09.9,10.10,"
                //    + "11.11,12.12,13.13,14.14,15.15,16.16,17.17,18.18,19.19,20.20,21.21,22.22,23.23";
            }

            if (sdPCRProcessor.GetBoard(out board) && board != null)
            {
                List<string> errMessage;
                Console.WriteLine("\nInitializing LEDs");
                ledConnected = board.InitLEDCtl(numbLEDs, LEDMax, LEDLevels, out ledController, out errMessage);
                if (ledConnected)
                {
                    Console.WriteLine("Connection made to LEDs");
                }
                else
                {
                    for (int n = 0; n < errMessage.Count; n++)
                        Console.WriteLine(errMessage[n]);
                }

                Console.Out.Flush();
                Console.WriteLine("\nInitializing X Stage");
                xStageConnected = board.InitMotorCtl(2, 1, 0, 1200, 0, 40, 1500, 40000, out xStage, out errMessage);
                if (xStageConnected)
                {
                    Console.WriteLine("Connection made to X Stage");
                }
                else
                {
                    for (int s = 0; s < errMessage.Count; s++)
                        Console.WriteLine(errMessage[s]);
                }

                Console.Out.Flush();
                Console.WriteLine("\nInitializing Y Stage");
                yStageConnected = board.InitMotorCtl(3, 0, 0, 1200, 0, 40, 1500, 40000, out yStage, out errMessage);
                if (yStageConnected)
                {
                    Console.WriteLine("Connection made to Y Stage");
                }
                else
                {
                    for (int s = 0; s < errMessage.Count; s++)
                        Console.WriteLine(errMessage[s]);
                }

                Console.Out.Flush();
                Console.WriteLine("\nInitializing Filter Wheel");
                filterWheelConnected = board.InitMotorCtl(4, 1, 0, 120, 0, 40, 800, 6400, out filterWheel, out errMessage);
                if (filterWheelConnected)
                {
                    Console.WriteLine("Connection made to Filter Wheel");
                }
                else
                {
                    for (int s = 0; s < errMessage.Count; s++)
                        Console.WriteLine(errMessage[s]);
                }

                Console.Out.Flush();
                Console.WriteLine("\nInitializing Rotor");
                rotorConnected = board.InitMotorCtl(1, 1, 1, 1600, 0, 20, 400, 6400, out rotor, out errMessage);
                if (rotorConnected)
                {
                    Console.WriteLine("Connection made to Rotor");
                }
                else
                {
                    for (int s = 0; s < errMessage.Count; s++)
                        Console.WriteLine(errMessage[s]);
                }

                //motorTimerOn = false;

                Console.Out.Flush();
                Console.WriteLine("\nInitializing Thermistors");
                thermConnected = board.InitThermistors(out therm24, out errMessage);
                if (thermConnected)
                {
                    Console.WriteLine("Connection made to Thermistors");
                    string wgts;
                    therm24.GetThermistorWeights(out wgts);
                    //Console.WriteLine(String.Format("     Wgts = {0}", wgts));
                    if (!therm24.Check24Weights(wgts))
                    {
                        int wait = 2 * sdPCRProcessor.BoardWaitTime;
                        Console.WriteLine("Resetting weights to default");
                        for (int n = 0; n < 24; n++)
                        {
                            therm24.SetWeight(n, weights[n]);
                            System.Threading.Thread.Sleep(wait);
                        }
                        bool doneSettingWeights = false;
                        for (int n = 0; n < 24; n++)
                        {
                            if (therm24.NumbCommandsWaiting <= 0)
                            {
                                doneSettingWeights = true;
                                break;
                            }
                            else
                                System.Threading.Thread.Sleep(4 * wait);
                        }
                        if (!doneSettingWeights)
                            Console.WriteLine("Board is responding slowly to SetWeight commands");
                    }
                }
                else
                {
                    for (int s = 0; s < errMessage.Count; s++)
                        Console.WriteLine(errMessage[s]);
                }


                Console.Out.Flush();
                Console.WriteLine("\nInitializing PID controller");
                pidConnected = board.InitPIDCtl(out tempCtl, out errMessage);
                if (pidConnected)
                {
                    Console.WriteLine("Connection made to PID Controller");
                }
                else
                {
                    for (int s = 0; s < errMessage.Count; s++)
                        Console.WriteLine(errMessage[s]);
                }

                tempProgOn = false;
                newTempProgUnit.Time = 60.0;
                newTempProgUnit.Temperature = 50.0;
                newTempProgUnit.Iteration = "1";
                Cycle.Add(newTempProgUnit);
                newTempProgUnit.Temperature = 70.0;
                newTempProgUnit.Iteration = "2";
                Cycle.Add(newTempProgUnit);
                newTempProgUnit.Temperature = 50.0;
                newTempProgUnit.Iteration = "3";
                Cycle.Add(newTempProgUnit);
                newTempProgUnit.Temperature = 70.0;
                newTempProgUnit.Iteration = "4";
                Cycle.Add(newTempProgUnit);
                newTempProgUnit.Temperature = 50.0;
                newTempProgUnit.Iteration = "5";
                Cycle.Add(newTempProgUnit);

                ResponseItem rItem;
                string rString;
                ResponseQueue = new ConcurrentQueue<ResponseItem>();
                while (!ResponseQueue.IsEmpty)
                    ResponseQueue.TryDequeue(out rItem);

                InfoQueue = new ConcurrentQueue<string>();
                while (!InfoQueue.IsEmpty)
                    InfoQueue.TryDequeue(out rString);

                if (pidConnected && thermConnected && rotorConnected && filterWheelConnected && yStageConnected && xStageConnected
                    && ledConnected)
                {
                    for (int n = 0; n < numbLEDs; n++)
                        ledController.SetIntensity(n, LEDLevels[n]);

                    xStage.Home();
                    yStage.Home();
                    filterWheel.Home();
                    rotor.Home();
                    Console.WriteLine("Time and Calibrated Temperatures");
                    Console.WriteLine(therm24.TimeAndCalibratedTemperatures);
                    Console.WriteLine("");

                    progTimer.Start();
                    string message;
                    int[] motorDirection = new int[5];
                    int cmdSize = 100;
                    List<string> cmds = new List<string>(cmdSize);
                    LoadCommandList(cmdSize, ref cmds);

                    StringComparer stringComparer = StringComparer.OrdinalIgnoreCase;
                    bool _continue = true;
                    Console.WriteLine("\nReady");
                    while (_continue)
                    {
                        message = Console.ReadLine();
                        if (stringComparer.Equals("quit", message))
                        {
                            _continue = false;
                        }
                        else
                        {
                            string[] words = message.Split(delimiterChars);
                            bool isInt = false;
                            int sw;
                            Console.WriteLine(String.Format(" {0}, {1}", words.Length, words[0]));
                            while (words.Length > 0 && !isInt)
                            {
                                isInt = int.TryParse(words[0], out sw);
                                if (!isInt)
                                {
                                    //Console.WriteLine(String.Format(" {0}, {1}", words.Length, words[0]));
                                    List<string> newWords = new List<string>();
                                    if (words.Length > 1)
                                    {
                                        for (int m = 1; m < words.Length; m++)
                                            newWords.Add(words[m]);
                                    }
                                    words = new string[newWords.Count];
                                    for (int m = 0; m < newWords.Count; m++)
                                        words[m] = newWords[m];
                                }
                            }
                            if (words.Length > 0)
                            {
                                switch (Convert.ToInt32(words[0]))
                                {
                                    case 0:
                                        Console.WriteLine("==========");
                                        for (int n = 0; n < cmdSize; n++)
                                        {
                                            if (cmds[n].Length > 0)
                                                Console.Write(String.Format("{0} - {1}\r\n", n.ToString(" 0"), cmds[n]));
                                        }
                                        Console.WriteLine();
                                        break;
                                    case 1:
                                        if (!logIO && words.Length > 1)
                                        {
                                            logList0_Name.Clear();
                                            logList0_Name.Append(String.Format("{0}{1}_log.txt", outputDir, words[1]));
                                            if (IsValidFilename(logList0_Name.ToString()) && !File.Exists(logList0_Name.ToString()))
                                            {
                                                logList0.Clear();
                                                Console.WriteLine(String.Format("IO logging started, output to {0}",
                                                    logList0_Name.ToString()));
                                                DateTime date = DateTime.Now;
                                                logList0.Add(String.Format("Day: {0:d} Time: {1:g}", date.Date, date.TimeOfDay));
                                                logIO = true;
                                            }
                                            else if (File.Exists(logList0_Name.ToString()))
                                                Console.WriteLine(String.Format("{0} already exists, not overwritten",
                                                    logList0_Name.ToString()));
                                            else
                                                Console.WriteLine(String.Format("{0} is not a valid file name",
                                                    logList0_Name.ToString()));
                                        }
                                        else if (logIO)
                                        {
                                            Console.WriteLine("Program is already logging IO, no change");
                                        }
                                        else
                                        {
                                            Console.WriteLine("No log file named");
                                        }
                                        break;
                                    case 2:

                                        if (logIO)
                                        {
                                            StopLogging();
                                        }
                                        else
                                        {
                                            Console.WriteLine("Program was not logging IO, no change");
                                        }
                                        break;
                                    case 3:
                                        string f = String.Format("Firmware: {0}", sdPCRProcessor.FirmwareVersion);
                                        Console.WriteLine(String.Format("\r\n{0}", f));
                                        if (logIO)
                                            LogInfo(String.Format("{0:0.00}: {1}", CurrentTime(), f));
                                        break;
                                    case 5:
                                        if (words.Length > 1)
                                        {
                                            list1.Clear();
                                            list1_Name.Clear();
                                            list1_Name.Append(String.Format("{0}{1}", outputDir, words[1]));
                                            if (IsValidFilename(list1_Name.ToString()) && !File.Exists(list1_Name.ToString()))
                                            {
                                                if (sdPCRProcessor.ReadLogInfo(out string[] lines))
                                                    System.IO.File.WriteAllLines(@list1_Name.ToString(), lines);
                                                else
                                                    Console.WriteLine("Log list is empty");
                                            }
                                            else
                                            {
                                                Console.WriteLine(String.Format("Problems with file name {0}", list1_Name.ToString()));
                                            }
                                        } 
                                        break;
                                    case 7:
                                        if (words.Length > 1)
                                        {
                                            int w1;
                                            if (Int32.TryParse(words[1], out w1))
                                            {
                                                if (w1 > 199)
                                                {
                                                    timeStep = w1;
                                                    Console.WriteLine(String.Format(
                                                        "Monitoring time step reset to {0} ms", timeStep));
                                                }
                                            }
                                        }
                                        break;
                                    case 9:
                                        lock (currentSetTLock)
                                        {
                                            if (pidProgram != null)
                                                Console.WriteLine(String.Format("Step/Time/SetTemp/Temp/Descr. {0}", pidProgram.CurrentState));
                                            else
                                                Console.WriteLine(String.Format("Time/SetTemp/Temp: {0:0.00}\t{1:0.00}\t{2:0.00}",
                                                    CurrentTime(), currentSetT, tempCtl.ProcessTemperature));
                                        }
                                        break;
                                    case 11:
                                        rotor.Terminate();
                                        rotor.HighImpedance();
                                        rotor.SetProfile(0);
                                        rotor.SetVelocity(10);
                                        //rotor.Home();
                                        if (logIO)
                                            LogInfo(String.Format("{0:0.00}: Rotor - /1TR, /1hR, /1V0R, /1v10R, /1ZR", CurrentTime()));
                                        break;
                                    case 12:
                                        xStage.Terminate();
                                        xStage.HighImpedance();
                                        xStage.SetProfile(0);
                                        xStage.SetVelocity(10);
                                        //xStage.Home();
                                        if (logIO)
                                            LogInfo(String.Format("{0:0.00}: X-Stage - /2TR, /2hR, /2V0R, /2v10R, /2ZR", CurrentTime()));
                                        break;
                                    case 13:
                                        yStage.Terminate();
                                        yStage.HighImpedance();
                                        yStage.SetProfile(0);
                                        yStage.SetVelocity(10);
                                        // yStage.Home();
                                        if (logIO)
                                            LogInfo(String.Format("{0:0.00}: Y-Stage - /3TR, /3hR, /3V0R, /3v10R, /3ZR", CurrentTime()));
                                        break;
                                    case 14:
                                        filterWheel.Terminate();
                                        filterWheel.HighImpedance();
                                        filterWheel.SetProfile(0);
                                        filterWheel.SetVelocity(10);
                                        //filterWheel.Home();
                                        if (logIO)
                                            LogInfo(String.Format("{0:0.00}: Filter Wheel - /4TR, /4hR, /4V0R, /4v10R, /4ZR", CurrentTime()));
                                        break;
                                    case 15:
                                        rotor.Home();
                                        if (logIO)
                                            LogInfo(String.Format("{0:0.00}: Rotor - /1ZR", CurrentTime()));
                                        break;
                                    case 16:
                                        xStage.Home();
                                        if (logIO)
                                            LogInfo(String.Format("{0:0.00}: X-Stage - /2ZR", CurrentTime()));
                                        break;
                                    case 17:
                                        yStage.Home();
                                        if (logIO)
                                            LogInfo(String.Format("{0:0.00}: Y-Stage - /3ZR", CurrentTime()));
                                        break;
                                    case 18:
                                        filterWheel.Home();
                                        if (logIO)
                                            LogInfo(String.Format("{0:0.00}: Filter Wheel - /4ZR", CurrentTime()));
                                        break;
                                    case 20:
                                        rotor.Terminate();
                                        rotor.HighImpedance();
                                        rotor.SetProfile(1);
                                        rotor.SetVelocity(60);
                                        rotor.VelocityModePositive();
                                        lock (motorDirectionLock)
                                        {
                                            motorDirection[1] = 1;
                                        }
                                        if (logIO)
                                            LogInfo(String.Format("{0:0.00}: Rotor - /1TR, /1hR, /1V1R, /1v60R, /1P0R", CurrentTime()));
                                        break;
                                    case 21:
                                        rotor.Terminate();
                                        lock (motorDirectionLock)
                                        {
                                            motorDirection[1] = 0;
                                        }
                                        if (logIO)
                                            LogInfo(String.Format("{0:0.00}: Rotor - /1TR", CurrentTime()));
                                        break;
                                    case 22:
                                        xStage.Terminate();
                                        lock (motorDirectionLock)
                                        {
                                            motorDirection[2] = 0;
                                        }
                                        if (logIO)
                                            LogInfo(String.Format("{0:0.00}: X-Stage- /2TR", CurrentTime()));
                                        break;
                                    case 23:
                                        yStage.Terminate();
                                        lock (motorDirectionLock)
                                        {
                                            motorDirection[3] = 0;
                                        }
                                        if (logIO)
                                            LogInfo(String.Format("{0:0.00}: Y-Stage- /3TR", CurrentTime()));
                                        break;
                                    case 24:
                                        filterWheel.Terminate();
                                        lock (motorDirectionLock)
                                        {
                                            motorDirection[4] = 0;
                                        }
                                        if (logIO)
                                            LogInfo(String.Format("{0:0.00}: Filtr Wheel- /4TR", CurrentTime()));
                                        break;

                                    case 25:
                                        if (words.Length > 1)
                                        {
                                            int w1;
                                            if (Int32.TryParse(words[1], out w1))
                                            {
                                                if (w1 > 0 && w1 < 1500)
                                                {
                                                    rotor.Terminate();
                                                    rotor.HighImpedance();
                                                    rotor.SetProfile(1);
                                                    rotor.SetVelocity(w1);
                                                    rotor.VelocityModePositive();
                                                    lock (motorDirectionLock)
                                                    {
                                                        motorDirection[1] = 0;
                                                    }
                                                }
                                                if (logIO)
                                                    LogInfo(String.Format("{0:0.00}: Rotor - /1TR, /1hR, /1V1R, /1v{1}R, /1P0R", CurrentTime(), w1));
                                            }
                                        }
                                        break;
                                    case 26:
                                        if (words.Length > 2)
                                        {
                                            int w1;
                                            if (Int32.TryParse(words[1], out w1))
                                            {
                                                int w2;
                                                if (Int32.TryParse(words[2], out w2))
                                                {
                                                    if (w2 > 0)
                                                    {
                                                        switch (w1)
                                                        {
                                                            case 1:
                                                                rotor.AbsolutePosition(w2);
                                                                if (logIO)
                                                                    LogInfo(String.Format("{0:0.00}: Rotor - /1A{1}R", CurrentTime(), w2));
                                                                break;
                                                            case 2:
                                                                xStage.AbsolutePosition(w2);
                                                                if (logIO)
                                                                    LogInfo(String.Format("{0:0.00}: X-Stage - /2A{1}R", CurrentTime(), w2));
                                                                break;
                                                            case 3:
                                                                yStage.AbsolutePosition(w2);
                                                                if (logIO)
                                                                    LogInfo(String.Format("{0:0.00}: Y-Stage - /3A{1}R", CurrentTime(), w2));
                                                                break;
                                                            case 4:
                                                                filterWheel.AbsolutePosition(w2);
                                                                if (logIO)
                                                                    LogInfo(String.Format("{0:0.00}: Filter Wheel - /4A{1}R", CurrentTime(), w2));
                                                                break;
                                                            default:
                                                                break;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        break;
                                    case 27:
                                        rotor.VelocityModePositive();
                                        if (logIO)
                                            LogInfo(String.Format("{0:0.00}: Rotor - /1P0}R", CurrentTime()));
                                        break;
                                    case 28:
                                        if (words.Length > 1)
                                        {
                                            int w1;
                                            if (Int32.TryParse(words[1], out w1))
                                            {
                                                switch (w1)
                                                {
                                                    case 1:
                                                        Console.WriteLine(String.Format("{0}", rotor.Register));
                                                        if (logIO)
                                                            LogInfo(String.Format("{0:0.00}: Rotor - /1rR", CurrentTime()));
                                                        break;
                                                    case 2:
                                                        Console.WriteLine(String.Format("{0}", xStage.Register));
                                                        if (logIO)
                                                            LogInfo(String.Format("{0:0.00}: X-Stage - /2rR", CurrentTime()));
                                                        break;
                                                    case 3:
                                                        Console.WriteLine(String.Format("{0}", yStage.Register));
                                                        if (logIO)
                                                            LogInfo(String.Format("{0:0.00}: Y-Stage - /3rR", CurrentTime()));
                                                        break;
                                                    case 4:
                                                        Console.WriteLine(String.Format("{0}", filterWheel.Register));
                                                        if (logIO)
                                                            LogInfo(String.Format("{0:0.00}: Filter Wheel - /4rR", CurrentTime()));
                                                        break;
                                                    default:
                                                        break;
                                                }
                                            }
                                        }
                                        break;
                                    case 29:
                                        if (words.Length > 1)
                                        {
                                            int w1;
                                            if (Int32.TryParse(words[1], out w1))
                                            {
                                                //Console.WriteLine(String.Format("{0}", w1));
                                                //if (w1 > 0 && w1 < 4)
                                                //    Console.WriteLine(String.Format("{0}", preloadedTemp[w1]));
                                                switch (w1)
                                                {
                                                    case 1:
                                                        if (rotor.Busy)
                                                        {
                                                            Console.WriteLine("Rotor is busy");
                                                            if (logIO)
                                                                LogInfo(String.Format("{0:0.00}: Rotor is busy", CurrentTime()));
                                                        }
                                                        else
                                                        {
                                                            Console.WriteLine("Rotor is not busy");
                                                            if (logIO)
                                                                LogInfo(String.Format("{0:0.00}: Rotor not is busy", CurrentTime()));
                                                        }
                                                        break;
                                                    case 2:
                                                        if (xStage.Busy)
                                                        {
                                                            Console.WriteLine("X-Stage is busy");
                                                            if (logIO)
                                                                LogInfo(String.Format("{0:0.00}: X-Stage is busy", CurrentTime()));
                                                        }
                                                        else
                                                        {
                                                            Console.WriteLine("X-Stage is not busy");
                                                            if (logIO)
                                                                LogInfo(String.Format("{0:0.00}: X-Stage is not busy", CurrentTime()));
                                                        }
                                                        break;
                                                    case 3:
                                                        if (yStage.Busy)
                                                        {
                                                            Console.WriteLine("Y-Stage is busy");
                                                            if (logIO)
                                                                LogInfo(String.Format("{0:0.00}: Y-Stage is busy", CurrentTime()));
                                                        }
                                                        else
                                                        {
                                                            Console.WriteLine("Y-Stage is not busy");
                                                            if (logIO)
                                                                LogInfo(String.Format("{0:0.00}: Y-Stage is not busy", CurrentTime()));
                                                        }
                                                        break;
                                                    case 4:
                                                        if (filterWheel.Busy)
                                                        {
                                                            Console.WriteLine("Filter Wheel is busy");
                                                            if (logIO)
                                                                LogInfo(String.Format("{0:0.00}: Filter Wheel is busy", CurrentTime()));
                                                        }
                                                        else
                                                        {
                                                            Console.WriteLine("Filter Wheel is not busy");
                                                            if (logIO)
                                                                LogInfo(String.Format("{0:0.00}: Filter Wheel is not busy", CurrentTime()));
                                                        }
                                                        break;
                                                    default:
                                                        break;
                                                }
                                            }
                                        }
                                        break;
                                    case 30:
                                        if (words.Length > 2)
                                        {
                                            int w1;
                                            if (Int32.TryParse(words[1], out w1))
                                            {
                                                int w2;
                                                if (Int32.TryParse(words[2], out w2))
                                                {
                                                    if (w1 >= 0 && w1 < 2 && w2 < 76)
                                                    {
                                                        rotor.Terminate();
                                                        rotor.HighImpedance();
                                                        rotor.SetProfile(w1);
                                                        rotor.SetVelocity(w2);
                                                        if (logIO)
                                                            LogInfo(String.Format("{0:0.00}: Rotor - /1TR, /1hR, /1V{1}R, /1v{2}R",
                                                                CurrentTime(), w1, w2));
                                                    }
                                                    else
                                                        Console.WriteLine(
                                                            "Velocity mode must be 0 or 1 and velocity must be <= 75");
                                                }
                                            }
                                        }
                                        break;
                                    case 31:
                                        if (words.Length > 2)
                                        {
                                            int w1;
                                            if (Int32.TryParse(words[1], out w1))
                                            {
                                                int w2;
                                                if (Int32.TryParse(words[2], out w2))
                                                {
                                                    if (w1 >= 0 && w2 >= 0 && w1 != w2)
                                                    {
                                                        rotor.Oscillate(w1, w2);
                                                        if (logIO)
                                                            LogInfo(String.Format("{0:0.00}: Rotor - /1A{1}A{2}G0R - Oscillate",
                                                                CurrentTime(), w1, w2));
                                                    }
                                                    else
                                                        Console.WriteLine("Position values must be >= 0 and unequal to each other");
                                                }
                                            }
                                        }
                                        break;
                                    case 39:
                                        if (words.Length > 1)
                                        {
                                            int w1;
                                            if (Int32.TryParse(words[1], out w1))
                                            {
                                                switch (w1)
                                                {
                                                    case 1:
                                                        if (rotor.Homed)
                                                            Console.WriteLine("Rotor is homed");
                                                        else
                                                            Console.WriteLine("Rotor is not homed");
                                                        break;
                                                    case 2:
                                                        if (xStage.Homed)
                                                            Console.WriteLine("X stage is homed");
                                                        else
                                                            Console.WriteLine("X stage is not homed");
                                                        break;
                                                    case 3:
                                                        if (yStage.Homed)
                                                            Console.WriteLine("Y stage is homed");
                                                        else
                                                            Console.WriteLine("Y stage is not homed");
                                                        break;
                                                    case 4:
                                                        if (filterWheel.Homed)
                                                            Console.WriteLine("Filter Wheel is homed");
                                                        else
                                                            Console.WriteLine("Filter Wheel is not homed");
                                                        break;
                                                    default:
                                                        break;
                                                }
                                            }
                                        }
                                        break;
                                    case 41:
                                        if (words.Length > 2)
                                        {
                                            int w1;
                                            int inten;
                                            if (Int32.TryParse(words[1], out w1) && Int32.TryParse(words[2], out inten))
                                            {
                                                if (w1 >= 0 && w1 < numbLEDs && inten > 0 && inten <= 100)
                                                {
                                                    ledController.SetIntensity(w1, inten);
                                                    if (logIO)
                                                    {
                                                        int l = 20 + w1;
                                                        LogInfo(String.Format("{0:0.00}: LED - /{1}l{2}R",
                                                            CurrentTime(), l, inten));
                                                    }
                                                }
                                            }
                                        }
                                        break;
                                    case 42:
                                        if (words.Length > 1)
                                        {
                                            int w1;
                                            if (Int32.TryParse(words[1], out w1))
                                            {
                                                if (w1 >= 0 && w1 < numbLEDs)
                                                {
                                                    ledController.On(w1);
                                                    if (logIO)
                                                    {
                                                        int l = 20 + w1;
                                                        LogInfo(String.Format("{0:0.00}: LED - /{1}S1R",
                                                            CurrentTime(), l)); ;
                                                    }
                                                }
                                            }
                                        }
                                        break;
                                    case 43:
                                        if (words.Length > 1)
                                        {
                                            int w1;
                                            if (Int32.TryParse(words[1], out w1))
                                            {
                                                if (w1 >= 0 && w1 < 6)
                                                {
                                                    ledController.Off(w1);
                                                    if (logIO)
                                                    {
                                                        int l = 20 + w1;
                                                        LogInfo(String.Format("{0:0.00}: LED - /{1}S0R",
                                                            CurrentTime(), l));
                                                    }
                                                }
                                            }
                                        }
                                        break;
                                    case 50:
                                        for (int n = 0; n < weights.Length; n++)
                                        {
                                            therm24.SetWeight(n, weights[n]);
                                            int t = 100 + n;
                                            if (logIO)
                                                LogInfo(String.Format("{0:0.00}: Thermistors - /{1}W{2}R",
                                                    CurrentTime(), t, weights[n]));
                                        }
                                        while (therm24.CheckResponseList(out resp))
                                        {
                                            Console.WriteLine(String.Format("Thermistors: {0}", resp.ToString()));
                                            if (logIO)
                                                LogInfo(resp.ToString());
                                        }
                                        break;
                                    case 51:
                                        if (words.Length > 2)
                                        {
                                            int w1;
                                            int w2;
                                            if (Int32.TryParse(words[1], out w1))
                                            {
                                                if (w1 >= 0 && w1 < 24)
                                                {
                                                    if (Int32.TryParse(words[2], out w2))
                                                    {
                                                        if (w2 >= 0)
                                                        {
                                                            therm24.SetWeight(w1, w2);
                                                            if (logIO)
                                                                LogInfo(String.Format("{0:0.00}: Thermistors - /{1}W{2}R",
                                                                    CurrentTime(), w1, w2));
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        break;
                                    case 52:
                                        string wgt;
                                        therm24.GetThermistorWeights(out wgt);
                                        Console.WriteLine(String.Format("Wgts: {0}", wgt));
                                        words = wgt.Split(comma);
                                        double dwgt;
                                        for (int n = 0; n < words.Length; n++)
                                        {
                                            if (Double.TryParse(words[n], out dwgt))
                                            {
                                                weights[n] = (int)Math.Round(dwgt);
                                            }
                                        }
                                        break;
                                    case 53:
                                        if (words.Length > 1)
                                        {
                                            double w1;
                                            if (Double.TryParse(words[1], out w1))
                                            {
                                                if (w1 > 5.0 && w1 < 95.0)
                                                {
                                                    therm24.SetReferenceTemperature(w1);
                                                    Console.WriteLine(String.Format("Set reference temperature ({0}) [calculate bias]", w1));
                                                    if (logIO)
                                                        LogInfo(String.Format("{0:0.00}: Thermistors - /100{1:0.00}R - Calcuate bias",
                                                            CurrentTime(), w1));
                                                }
                                            }
                                        }
                                        break;
                                    case 54:
                                        therm24.ClearAllTemperatureBiases();
                                        Console.WriteLine(String.Format("Clear all temperature biases"));
                                        if (logIO)
                                            LogInfo(String.Format("{0:0.00}: Thermistors - /100ZR",
                                                CurrentTime()));
                                        break;
                                    case 55:
                                        string bias = therm24.ThermistorBiases;
                                        Console.WriteLine(String.Format("Bias: {0}", bias));
                                        if (logIO)
                                            LogInfo(String.Format("{0:0.00}: Thermistors - /100DR - Thermistor biases = {1} ",
                                                CurrentTime(), bias));
                                        break;
                                    case 56:
                                        string calTemp = therm24.CalibratedTemperatures;
                                        Console.WriteLine(String.Format("Calibrated Temp: {0}", calTemp));
                                        if (logIO)
                                            LogInfo(String.Format("{0:0.00}: Thermistors - /100AR - Calibrated Temperatures = {1} ",
                                                CurrentTime(), calTemp));
                                        break;

                                    case 60:
                                        double refTemp = tempCtl.ProcessTemperature;
                                        Console.WriteLine(String.Format("Temperature: {0}", refTemp));
                                        if (logIO)
                                            LogInfo(String.Format("{0:0.00}: PID Controller - /70R0R - Current Process temperature = {1}",
                                                  CurrentTime(), refTemp));
                                        break;
                                    case 61:
                                        double targetTemp = tempCtl.TargetTemperature;
                                        Console.WriteLine(String.Format("Target Temperature: {0}", targetTemp));
                                        if (logIO)
                                            LogInfo(String.Format("{0:0.00}: PID Controller - /70QR - Target Temperature = {1}",
                                                  CurrentTime(), targetTemp));
                                        break;
                                    case 62:
                                        tempCtl.PIDControllerOn();
                                        Console.WriteLine("PID controller turned on");
                                        if (logIO)
                                            LogInfo(String.Format("{0:0.00}: PID Controller - /70C1R - turn on PID controller",
                                                  CurrentTime()));
                                        break;
                                    case 63:
                                        tempCtl.PIDControllerOff();
                                        Console.WriteLine("PID controller turned off");
                                        if (logIO)
                                            LogInfo(String.Format("{0:0.00}: PID Controller - /70C0R - turn off PID controller",
                                                CurrentTime()));
                                        break;
                                    case 64:
                                        if (tempCtl.PIDStatus)
                                        {
                                            Console.WriteLine("PID controller is on");
                                            if (logIO)
                                                LogInfo(String.Format("{0:0.00}: PID Controller - /70S0R - PID Status checked: PID controller off",
                                                              CurrentTime()));
                                        }
                                        else
                                        {
                                            Console.WriteLine("PID controller is on");
                                            if (logIO)
                                                LogInfo(String.Format("{0:0.00}: PID Controller - /70S0R - PID Status checked: PID controller off",
                                                              CurrentTime()));
                                        }
                                        break;
                                    case 65:
                                        if (words.Length > 2)
                                        {
                                            int w1;
                                            if (Int32.TryParse(words[1], out w1))
                                            {
                                                if (w1 >= 0 && w1 < preloadedTemp0.Length)
                                                {
                                                    double w2;
                                                    if (Double.TryParse(words[2], out w2))
                                                    {
                                                        if (w2 >= 30.0 && w2 <= 125.0)
                                                        {
                                                            preloadedTemp0[w1] = w2;
                                                        }
                                                        else
                                                        {
                                                            Console.WriteLine("Requested preload temperature out of range!");
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        break;
                                    case 66:
                                        if (tempProgOn)
                                        {
                                            Console.WriteLine("Cannot change Manual Temperature while temperature program is running!!");
                                        }
                                        else
                                        {
                                            if (words.Length > 1)
                                            {
                                                int w1;
                                                if (Int32.TryParse(words[1], out w1))
                                                {
                                                    //Console.WriteLine(String.Format("{0}", w1));
                                                    //if (w1 > 0 && w1 < 4)
                                                    //    Console.WriteLine(String.Format("{0}", preloadedTemp[w1]));
                                                    if (w1 > 0 && w1 < 4)
                                                    {
                                                        tempCtl.SetManualTemperature(preloadedTemp0[w1]);
                                                        lock (currentSetTLock)
                                                        {
                                                            currentSetT = preloadedTemp0[w1];
                                                        }
                                                        if (logIO)
                                                            LogInfo(String.Format(
                                                                "{0:0.00}: PID Controller - /70A{1:0.00}R - Change Manual Temperature",
                                                                          CurrentTime(), preloadedTemp0[w1]));
                                                    }
                                                }
                                            }
                                        }
                                        break;
                                    case 67:
                                        if (tempProgOn)
                                        {
                                            Console.WriteLine(
                                                "Cannot change Manual Temperature while temperature program is running!!");
                                        }
                                        else
                                        {
                                            if (words.Length > 1)
                                            {
                                                double w1;
                                                if (Double.TryParse(words[1], out w1))
                                                {
                                                    if (w1 > 10.0 && w1 < 115.0)
                                                    {
                                                        tempCtl.SetManualTemperature(w1);
                                                        lock (currentSetTLock)
                                                        {
                                                            currentSetT = w1;
                                                        }
                                                        if (logIO)
                                                            LogInfo(String.Format(
                                                                "{0:0.00}: PID Controller - /70A{1:0.00}R - Change Manual Temperature",
                                                                          CurrentTime(), w1));
                                                    }
                                                    else
                                                    {
                                                        Console.WriteLine("Requested temperature out of range!");
                                                    }
                                                }
                                            }
                                        }
                                        break;
                                    case 68:
                                        tempCtl.ManualControlOn();
                                        Console.WriteLine("Manual Temper controller turned on");
                                        if (logIO)
                                            LogInfo(String.Format("{0:0.00}: PID Controller - Manual Temper controller turned on",
                                                  CurrentTime()));
                                        break;
                                    case 69:
                                        tempCtl.ManualControlOff();
                                        Console.WriteLine("Manual Temper controller turned off");
                                        if (logIO)
                                            LogInfo(String.Format("{0:0.00}: PID Controller - Manual Temper controller turned off",
                                                  CurrentTime()));
                                        break;

                                    case 80:
                                        if (words.Length > 1 && !tempProgOn)
                                        {
                                            words[1] = words[1].TrimEnd(whiteSpaceQuoteBack).TrimStart(whiteSpaceQuote);
                                            if (words[1].Length > 4 && words[1].IndexOf(".csv") == words[1].Length - 4)
                                            {
                                                Console.WriteLine(words[1]);
                                                if (IsValidFilename(words[1]) && File.Exists(words[1]))
                                                {
                                                    string[] lines = System.IO.File.ReadAllLines(@String.Format("{0}", words[1]));
                                                    List<string> prog = new List<string>(0);
                                                    for (int n = 0; n < lines.Length; n++)
                                                        prog.Add(lines[n]);

                                                    Console.WriteLine(String.Format("prog.Count: {0}, tempCtl.Connected: {1}",
                                                        prog.Count, tempCtl.Connected.ToString()));
                                                    if (pidProgram == null)
                                                        pidProgram = new PIDSequencer(tempCtl, out pidSeqConnected);
                                                    pidProgramLoaded = false;
                                                    if (pidSeqConnected)
                                                    {
                                                        string err;
                                                        pidProgramLoaded = pidProgram.LoadProgram(prog, out err);
                                                        if (!pidProgramLoaded)
                                                        {
                                                            Console.WriteLine(String.Format("PID program not accepted: {0}", err));
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        break;
                                    case 81:
                                        if (pidProgram.ProgramLoaded)
                                        {
                                            tempProgOn = true;
                                            pidProgram.StartProgram(0, timeStep, true);
                                        }
                                        break;
                                    case 82:
                                        if (pidProgram.ProgramLoaded)
                                        {
                                            pidProgram.StopProgram();
                                            tempProgOn = false;
                                        }
                                        break;
                                    case 83:
                                        if (pidProgram.ProgramLoaded && words.Length > 1 && pidProgram.GetOutput(out pidProgramLog))
                                        {
                                            list1.Clear();
                                            list1_Name.Clear();
                                            list1_Name.Append(String.Format("{0}{1}_tempProg_{2}.csv",
                                                outputDir, words[1], progTimer.ElapsedMilliseconds
                                                ));
                                            if (IsValidFilename(list1_Name.ToString()) && !File.Exists(list1_Name.ToString()))
                                                System.IO.File.WriteAllLines(@list1_Name.ToString(), pidProgramLog);

                                            list1.Clear();
                                            list1_Name.Clear();
                                            list1_Name.Append(String.Format("{0}{1}_thermLog_{2}.txt",
                                                outputDir, words[1], progTimer.ElapsedMilliseconds
                                                ));
                                            if (IsValidFilename(list1_Name.ToString()) && !File.Exists(list1_Name.ToString()))
                                            {
                                                if (therm24.GetInternalLog(out string[] tLog))
                                                    System.IO.File.WriteAllLines(@list1_Name.ToString(), tLog);
                                                else
                                                    Console.WriteLine("therm24 internal log is empty");
                                            }
                                        }
                                        break;
                                    case 84:
                                        if (pidProgramLoaded)
                                        {
                                            Console.WriteLine(pidProgram.CurrentState);
                                        }
                                        break;
                                    case 85:
                                        if (pidProgramLoaded)
                                        {
                                            Console.WriteLine(pidProgram.CurrentStep);
                                        }
                                        break;
                                    case 86:
                                        if (pidProgramLoaded)
                                        {
                                            List<TempProgUnit> prog = pidProgram.TempProgram;
                                            Console.WriteLine("Temp - Time - Description");
                                            for (int n = 0; n < prog.Count; n++)
                                            {
                                                Console.WriteLine(String.Format("{0} - {1} - {2}",
                                                    prog[n].Temperature, prog[n].Time, prog[n].Description));
                                            }
                                            Console.WriteLine("----------");
                                        }
                                        break;
                                    case 90:
                                        if (thermProgram == null)
                                        {
                                            thermProgram = new TempMonitoring(therm24, out thermProgramLoaded);
                                            thermTimerOn = false;
                                        }
                                        break;
                                    case 91:
                                        if (thermProgram != null && thermProgramLoaded && timeStep > 199)
                                        {
                                            thermProgram.StartMonitoring(timeStep);
                                            thermTimerOn = true;
                                        }
                                        break;
                                    case 92:
                                        if (thermProgram != null && thermProgram.ProgramRunning)
                                        {
                                            thermProgram.StopMonitoring();
                                            thermTimerOn = false;
                                        }
                                        break;
                                    case 93:
                                        if (thermProgram.ProgramLoaded && words.Length > 1 && thermProgram.GetOutput(out thermProgramLog))
                                        {
                                            list1.Clear();
                                            list1_Name.Clear();
                                            list1_Name.Append(String.Format("{0}{1}_thermistorMonitoring_{2}.csv",
                                                outputDir, words[1], progTimer.ElapsedMilliseconds
                                                ));
                                            if (IsValidFilename(list1_Name.ToString()) && !File.Exists(list1_Name.ToString()))
                                                System.IO.File.WriteAllLines(@list1_Name.ToString(), thermProgramLog);
                                        }
                                        break;

                                    case 95:
                                        if (words.Length > 1 && motorProgram == null)
                                        {
                                            words[1] = words[1].TrimEnd(whiteSpaceQuoteBack).TrimStart(whiteSpaceQuote);
                                            //Console.WriteLine(words[1]);
                                            if (words[1].Length > 4 && words[1].IndexOf(".csv") == words[1].Length - 4)
                                            {
                                                //Console.WriteLine(words[1]);
                                                if (IsValidFilename(words[1]) && File.Exists(words[1]))
                                                {
                                                    string[] lines = System.IO.File.ReadAllLines(@String.Format("{0}", words[1]));
                                                    int motorNumberIn = 0;
                                                    int motorProfileIn = 0;
                                                    int motorVelocityIn = 0;
                                                    List<int> motorTimerPosIn = new List<int>();
                                                    motorTimerPosIn.Clear();
                                                    //int posIn;
                                                    bool valid;
                                                    if (lines.Length > 6)
                                                    {
                                                        valid = Int32.TryParse(lines[0], out motorNumberIn);
                                                        valid = valid && motorNumberIn > 0 && motorNumberIn < 5;
                                                        valid = valid && Int32.TryParse(lines[1], out motorProfileIn);
                                                        valid = valid && Int32.TryParse(lines[2], out motorVelocityIn);
                                                        valid = valid && !(motorProfileIn < 0 || motorVelocityIn < 1);
                                                    }
                                                    else
                                                        valid = false;
                                                    int p = 3;
                                                    int max = 0;
                                                    if (motorNumberIn == 1)
                                                        max = rotor.MaxPosition;
                                                    else if (motorNumberIn == 2)
                                                        max = xStage.MaxPosition;
                                                    else if (motorNumberIn == 3)
                                                        max = yStage.MaxPosition;
                                                    else if (motorNumberIn == 4)
                                                        max = filterWheel.MaxPosition;
                                                    else
                                                        valid = false;

                                                    while (valid && p < lines.Length)
                                                    {
                                                        int w1;
                                                        if (Int32.TryParse(lines[p], out w1))
                                                        {
                                                            if (w1 >= 0 && w1 <= max)
                                                                motorTimerPosIn.Add(w1);
                                                            else
                                                                break;
                                                        }
                                                        else
                                                            break;
                                                        p++;
                                                    }
                                                    if (motorTimerPosIn.Count < 3)
                                                        valid = false;

                                                    if (valid)
                                                    {
                                                        motorNumber = motorNumberIn;
                                                        motorProfile = motorProfileIn;
                                                        motorVelocity = motorVelocityIn;
                                                        motorTimerPos.Clear();
                                                        Console.WriteLine(String.Format("Motor: {0} / Profile: {1} / Velocity: {2} / Count: {3}",
                                                            motorNumber, motorProfile, motorVelocity, motorTimerPosIn.Count));
                                                        for (int n = 0; n < motorTimerPosIn.Count; n++)
                                                        {
                                                            motorTimerPos.Add(motorTimerPosIn[n]);
                                                            Console.WriteLine(String.Format("{0} - {1}", n, motorTimerPos[n]));
                                                        }
                                                        if (motorNumber == 1 && rotorConnected)
                                                        {
                                                            motorProgram = new MotorSequencer(rotor, out motorSeqConnected);
                                                        }
                                                        else if (motorNumber == 2 && xStageConnected)
                                                        {
                                                            motorProgram = new MotorSequencer(xStage, out motorSeqConnected);
                                                        }
                                                        else if (motorNumber == 3 && yStageConnected)
                                                        {
                                                            motorProgram = new MotorSequencer(yStage, out motorSeqConnected);
                                                        }
                                                        else if (motorNumber == 4 && filterWheelConnected)
                                                        {
                                                            motorProgram = new MotorSequencer(filterWheel, out motorSeqConnected);
                                                        }
                                                        if (motorSeqConnected)
                                                        {
                                                            string err;
                                                            motorProgramLoaded = motorProgram.LoadProgram(motorTimerPos, motorProfile, motorVelocity, out err);
                                                            if (!motorProgramLoaded)
                                                            {
                                                                Console.WriteLine(String.Format("Motor program not accepted: {0}", err));
                                                            }
                                                        }
                                                        else
                                                        {
                                                            Console.WriteLine(String.Format("Problem with program in {0}", words[1]));
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        break;
                                    case 96:
                                        if (words.Length > 1 && motorProgram != null)
                                        {
                                            int w1;
                                            if (Int32.TryParse(words[1], out w1))
                                            {
                                                if (w1 >= 0)
                                                {
                                                    motorProgram.GoToPosition(w1);
                                                }
                                            }
                                        }
                                        break;
                                    case 97:
                                        if (!motorTimerOn)
                                        {
                                            if (words.Length > 2)
                                            {
                                                int w1;
                                                if (Int32.TryParse(words[1], out w1))
                                                {
                                                    if (w1 > 1999)
                                                    {
                                                        int w2;
                                                        if (Int32.TryParse(words[2], out w2))
                                                        {
                                                            if (w2 > 1)
                                                            {
                                                                motorProgram.StartMotorProg(w1, w2);
                                                                motorTimerOn = true;
                                                            }
                                                            else
                                                            {
                                                                Console.WriteLine("Motor Count Limit must be > 1");
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        Console.WriteLine("Time Interval must be greater than 1999");
                                                    }
                                                }
                                            }
                                        }
                                        break;
                                    case 98:
                                        if (motorProgram.ProgramRunning)
                                            motorProgram.StopMotorProg();

                                        motorTimerOn = false;
                                        break;
                                    case 99:
                                        List<string> progOut;
                                        if (motorProgram != null)
                                        {
                                            if (motorProgram.GetProgram(out progOut))
                                            {
                                                for (int n = 0; n < progOut.Count; n++)
                                                    Console.WriteLine(progOut[n]);
                                            }
                                        }
                                        break;
                                    default:
                                        Console.WriteLine("Command not implemented");
                                        break;
                                }
                            }
                        }
                        while (sdPCRProcessor.ReadStatusMessages(out resp))
                        {
                            if (resp.Response.IndexOf("/0E") >= 0)
                            {
                                Console.WriteLine("Error Message Received");
                                Console.WriteLine(resp.ToString());
                            }
                            else if (resp.Response.IndexOf("/0S") >= 0)
                            {
                                Console.WriteLine("Status Message Received");
                                Console.WriteLine(resp.ToString());
                            }
                            if (logIO)
                                LogInfo(resp.ToString());

                        }

                        while (sdPCRProcessor.CheckResponseList(out resp))
                        {
                            Console.WriteLine(String.Format("{0}: Processor: {1}", board.BoardTime(), resp.ToString()));
                            if (logIO)
                                LogInfo(resp.ToString());
                        }

                        while (ledController.CheckResponseList(out resp))
                        {
                            Console.WriteLine(String.Format("{0}: LED Controller: {1}", board.BoardTime(), resp.ToString()));
                            if (logIO)
                                LogInfo(resp.ToString());
                        }

                        while (rotor.CheckResponseList(out resp))
                        {
                            Console.WriteLine(String.Format("{0}: Rotor: {1}", board.BoardTime(), resp.ToString()));
                            if (logIO)
                                LogInfo(resp.ToString());
                        }
                        while (xStage.CheckResponseList(out resp))
                        {
                            Console.WriteLine(String.Format("{0}: XStage: {1}", board.BoardTime(), resp.ToString()));
                            if (logIO)
                                LogInfo(resp.ToString());
                        }
                        while (yStage.CheckResponseList(out resp))
                        {
                            Console.WriteLine(String.Format("{0}: YStage: {1}", board.BoardTime(), resp.ToString()));
                            if (logIO)
                                LogInfo(resp.ToString());
                        }
                        while (filterWheel.CheckResponseList(out resp))
                        {
                            Console.WriteLine(String.Format("{0}: Filter Wheel: {1}", board.BoardTime(), resp.ToString()));
                            if (logIO)
                                LogInfo(resp.ToString());
                        }
                        while (therm24.CheckResponseList(out resp))
                        {
                            Console.WriteLine(String.Format("{0}: Thermistors: {1}", board.BoardTime(), resp.ToString()));
                            if (logIO)
                                LogInfo(resp.ToString());
                        }
                        
                        while (tempCtl.CheckResponseList(out resp))
                        {
                            Console.WriteLine(String.Format("{0}: PID Controller: {1}", board.BoardTime(), resp.ToString()));
                            if (logIO)
                                LogInfo(resp.ToString());
                        }

                        if (pidProgram != null)
                        {
                            while (pidProgram.CheckErrorList(out resp))
                            {
                                Console.WriteLine(String.Format("{0}: pidProgram Error: {1}", board.BoardTime(), resp.ToString()));
                                if (logIO)
                                    LogInfo(String.Format("{0}: pidProgram Error: {1}", board.BoardTime(), resp.ToString()));
                            }
                            if (tempProgOn)
                            {
                                tempProgOn = pidProgram.ProgramRunning;
                                if (!tempProgOn)
                                    Console.WriteLine("PID Program has finished");
                            }
                        }

                        if (thermProgram != null )
                        {
                            while (thermProgram.CheckErrorList(out resp))
                            {
                                Console.WriteLine(String.Format("{0}: thermProgram Error: {1}", board.BoardTime(), resp.ToString()));
                                if (logIO)
                                    LogInfo(String.Format("{0}: thermProgram Error: {1}", board.BoardTime(), resp.ToString()));
                            }
                            if (thermTimerOn)
                            {
                                thermTimerOn = thermProgram.ProgramRunning;
                                if (!thermTimerOn)
                                    Console.WriteLine("Temperature Monitoring has finished");
                            }
                        }

                        if( motorProgram != null)
                        {
                            if (motorTimerOn)
                            {
                                motorTimerOn = motorProgram.ProgramRunning;
                                if (!motorTimerOn)
                                    Console.WriteLine("Motor program has finished");
                            }
                        }
                    }
                }
                else
                {
                    Console.WriteLine("One of the components of the board did not connect properly");
                }
                Console.WriteLine("\nShutting down program");

                if (logIO)
                    StopLogging();

                Console.WriteLine("\nPress any key to exit");
                any = Console.ReadLine();
                sdPCRProcessor.Dispose();
            }
            else
            {
                Console.WriteLine("could not get board");
                sdPCRProcessor.Dispose();
            }
        }
       
        private static void StopLogging()
        {
            if (logIO)
            {
                logIO = false;
                DateTime date = DateTime.Now;
                logList0.Add(String.Format("Day: {0:d} Time: {1:g}", date.Date, date.TimeOfDay));

                if (InfoQueue.Count > 0)
                {
                    string[] Ilines = new string[InfoQueue.Count];
                    InfoQueue.CopyTo(Ilines, 0);
                    logList0.Add("");
                    logList0.Add("======================================");
                    for (int n = 0; n < Ilines.Length; n++)
                        logList0.Add(Ilines[n]);
                }
                string[] lines = new string[logList0.Count];
                logList0.CopyTo(lines, 0);
                for (int n = 0; n < lines.Length; n++)
                {
                    lines[n] = lines[n].TrimEnd(whiteSpace);
                    lines[n] = lines[n].TrimStart(whiteSpace);
                }
                System.IO.File.WriteAllLines(@logList0_Name.ToString(), lines);
                Console.WriteLine("Program has stopped logging IO");
            }
        }

        public static double CurrentTime()
        {
            long elapsed = progTimer.ElapsedMilliseconds;
            return elapsed * 0.001;
        }
        public static bool IsValidFilename(string testName)
        {
            System.Text.RegularExpressions.Regex containsABadCharacter =
                new System.Text.RegularExpressions.Regex("["
                  + System.Text.RegularExpressions.Regex.Escape(new string(System.IO.Path.GetInvalidPathChars())) + "]");
            if (containsABadCharacter.IsMatch(testName)) { return false; };

            // other checks for UNC, drive-path format, etc

            return true;
        }
        static void LoadCommandList(int CmdSize, ref List<string> cmds)
        {
            for (int n = 0; n < CmdSize; n++)
                cmds.Add("");
            cmds[0] = "Print Preloaded Commands";
            cmds[1] = "Log IO {enter as 1 BaseName}";
            cmds[2] = "Stop logging IO";
            cmds[3] = "Get Firmware version [/0FR]";
            //cmds[4] = "Initialize motors [/1IR /2IR /3IR /4IR /1f1R /2f1R /3f0R /4f1R]";
            //cmds[5] = "Initialize LEDs [/20LR /21LR /22LR /23LR /24LR /25LR]";
            cmds[5] = "Save Board Log {Enter as 5 board log name}";
            cmds[7] = "Change time step for timers 1 & 2 (these record temperatures) \r\n"
                + "     {enter as 7 X, where X > 199, units ms)";
            //cmds[8] =;
            cmds[9] = "Print current Elapsed Time, Temperature, Set Temperature";
            cmds[11] = "Motor 1 (Profile 0, 10 rpm, Home) [/1TR /1hR /1V0R /1v10R]";
            cmds[12] = "Motor 2 (Profile 0, 10 rpm, Home) [/2TR /2hR /2V0R /2v10R]";
            cmds[13] = "Motor 3 (Profile 0, 10 rpm, Home) [/3TR /3hR /3V0R /3v10R]";
            cmds[14] = "Motor 4 (Profile 0, 10 rpm, Home) [/4TR /4hR /4V0R /4v10R]";
            cmds[15] = "Motor 1 (Home) [/1ZR]";
            cmds[16] = "Motor 2 (Home) [/2ZR]";
            cmds[17] = "Motor 3 (Home) [/3ZR]";
            cmds[18] = "Motor 4 (Home) [/4ZR]";
            cmds[20] = "Start Rotor at 60 rpm, Profile 1 [/1TR /1hR /1V1R /1v60R /P0R]";
            cmds[21] = "Stop Rotor [/1TR]";
            cmds[22] = "Stop X stage [/2TR]";
            cmds[23] = "Stop Y stage [/3TR]";
            cmds[24] = "Stop Filter Wheel [/4TR]";
            cmds[25] = "Motor 1 (Profile 1), velocity X {enter as 25 X, where X is velocity} [/1TR /1hR /1V1R /1vXR]";
            cmds[26] = "Move motor N Absolute {enter as 26 X Y, where X = 1, ... 4 and Y is position [/XAYR]";
            cmds[27] = "Set motor 1 in positive velocity mode [/1P0R]";
            cmds[28] = "Read motor register {Enter as 28 X, where X is the motor number} [/XrR]";
            cmds[29] = "Check if motor busy {Enter as 29 X, where X is the motor number} [/XQR]";

            cmds[30] = "Set up for Absolute Position Loop for motor 1\r\n"
                + "     {Enter as 30 X Y, where X and Y are the velocity mode and velocity [/1TR /1hR /1VXR /1vYR]";
            cmds[31] = "Start Absolute Position Loop for motor 1\r\n"
                + "     {Enter as 31 X Y, where X and Y are the two absolute positions, use /1TR to terminate}\r\n"
                + "     [/1AXAYGR]";
            cmds[39] = "Check home flag {Enter as 39 X, where X is the motor number} [/6XER]";
            //cmds[40] = "Set initial LED levels [/20lxxR ...]";
            cmds[41] = "Set LED levels\r\n     {enter as 41 X Y, where X = 0, ... 5 and 0 < Y <= 100} [/2XlYR ...]";
            cmds[42] = "Turn LED X on {enter as 42 X, where X = 0, ... 5} [/2XS1R]";
            cmds[43] = "Turn LED X off {enter as 43 X, where X = 0, ... 5} [/2XS0R]";
            cmds[50] = "Set All Thremistor wgts [/1xxWyyR]"
                + "     (xx=0... 23) with preloaded weights (yy)";
            cmds[51] = "Set Thermistor wgt [/1xxWyyR].  (enter as 51 xx yy, qhwew xx is the thermistor and yy >= 0 is the weight)";
            cmds[52] = "Read Thermistor Weights [/1XXWR, for XX = 00 .. 23]";
            cmds[53] = "Set reference temp & flashes biases\r\n" +
                "     {enter as 53 X.XX, where X.XX is the reference temperature}\r\n     [/100TXX.XR]";
            cmds[54] = "Clear All Thermistor Bias [/100ZR]";
            cmds[55] = "Read All Thermistor Bias [/100DR]";
            cmds[56] = "Read All calibrated temperatures [/100AR]";

            cmds[60] = "Read Current Temperature (weighted average) [/70R0R]";
            cmds[61] = "Read Target Temperature [/70QR]";
            cmds[62] = "Turn PID controller on [/70C1R]";
            cmds[63] = "Turn PID controller off [/70C0R]";
            cmds[64] = "Get status of PID controller [/70S0R]";
            cmds[65] = "Change preloaded temperature\r\n"
                + "     {enter as 65 X Y, where X = 0, 1, 2, or 3 and Y is the temperature in C}";
            cmds[66] = "Set Manual T to preloaded val \r\n"
                + "     {enter as 66 X, X = 1, 2, 3} [/70App.pR, pp.p is preloaded temp for X]";
            cmds[67] = "Set Manual Temperature \r\n     {enter as 67 YY.Y, where YY.Y = temperature [/70AYY.YR]";
            cmds[68] = "Turn Manual Temp control on [/70M1R]";
            cmds[69] = "Turn Manual Temp Control off [/70M0R]";
            cmds[80] = "Load temperature program {enter as 80 full path to program.csv}"
                + " [must be a csv file with each line containing: temperature,time,descriptive string]"
                + " (timestep set by command 7)";
            cmds[81] = "Start Temp Control Program {enter as 81 BaseName}\r\n"
                + "     {output is to Basename_temp_rrrr.csv}, [rrrr is a time which ensures that all such output has a unique name]";
            cmds[82] = "Stop Temp Control Program";
            cmds[83] = "Save output from Temp Control Program (entered as 83 Basename)";
            cmds[84] = "Print current state of Temp Control Program";
            cmds[85] = "Print current step of Temp Control Program";
            cmds[86] = "Print current Temp Control Program";

            cmds[90] = "Set up Temp Monitoring Program";
            cmds[91] = "Start monitoring temperature (timestep set by command 7";
            cmds[92] = "Stop monitoring temperature";
            cmds[93] = "Save output from Temp Monitoring (enter as 93 BaseName)";

            cmds[95] = "Load temperature program {enter as 95 full path to program.csv M}"
                + " [must be a csv file with each line containing: ]";
            cmds[96] = "Go to Position N {enter as 96 N, which goes to the Nth position in program}";
            cmds[97] = "Start Motor Timer Program {enter as 97 X Y, where X is the time interval (ms)\r\n"
                +   "     and Y is the number of steps";
            cmds[98] = "Stop Motor Timer Program";
            cmds[99] = "Print Motor Program";
        }
    }
}
