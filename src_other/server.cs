﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Text.Json;
using System.Text.Json.Serialization;

using sdpcrInstrLib.BoardComm;
using sdpcrInstrLib.Hardware;
using sdpcrInstrLib.LEDCluster;
using sdpcrInstrLib.Stepper;
using sdpcrInstrLib.PIDController;
using sdpcrInstrLib.Therm;
using sdpcrInstrLib.InstrExtensions;
using sdpcrInstrLib.Sequencers;

namespace server
{
    // State object for reading client data asynchronously  
    public class StateObject
    {
        // Client  socket.  
        public Socket workSocket = null;
        // Size of receive buffer.  
        public const int BufferSize = 1024;
        // Receive buffer.  
        public byte[] buffer = new byte[BufferSize];
        // Received data string.  
        public StringBuilder sb = new StringBuilder();
    }

    class server
    {
        // Thread signal.  
        public static ManualResetEvent allDone = new ManualResetEvent(false);

        public static void StartListening()
        {
            // Establish the local endpoint for the socket.  
            // The DNS name of the computer  
            // running the listener is "host.contoso.com".  
            IPHostEntry ipHostInfo = Dns.GetHostEntry(Dns.GetHostName());
            IPAddress ipAddress = ipHostInfo.AddressList[0];
            IPEndPoint localEndPoint = new IPEndPoint(ipAddress, 54321);

            Console.WriteLine("IP Address: " + localEndPoint.Address);
            Console.WriteLine("Port: " + localEndPoint.Port);

            // Create a TCP/IP socket.  
            Socket listener = new Socket(ipAddress.AddressFamily,
                SocketType.Stream, ProtocolType.Tcp);

            // Bind the socket to the local endpoint and listen for incoming connections.  
            try
            {
                listener.Bind(localEndPoint);
                listener.Listen(100);

                while (true)
                {
                    // Set the event to nonsignaled state.  
                    allDone.Reset();

                    // Start an asynchronous socket to listen for connections.  
                    Console.WriteLine("Waiting for a connection...");
                    listener.BeginAccept(new AsyncCallback(AcceptCallback), listener);

                    // Wait until a connection is made before continuing.  
                    allDone.WaitOne();
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }

            Console.WriteLine("\nPress ENTER to continue...");
            Console.Read();

        }

        public static void AcceptCallback(IAsyncResult ar)
        {
            // Signal the main thread to continue.  
            allDone.Set();

            // Get the socket that handles the client request.  
            Socket listener = (Socket)ar.AsyncState;
            Socket handler = listener.EndAccept(ar);

            // Create the state object.  
            StateObject state = new StateObject();
            state.workSocket = handler;
            handler.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
                new AsyncCallback(ReadCallback), state);
        }

        public static void ReadCallback(IAsyncResult ar)
        {
            String content = String.Empty;

            // Retrieve the state object and the handler socket  
            // from the asynchronous state object.  
            StateObject state = (StateObject)ar.AsyncState;
            Socket handler = state.workSocket;

            // Read data from the client socket.
            int bytesRead = handler.EndReceive(ar);

            if (bytesRead > 0)
            {
                // There  might be more data, so store the data received so far.  
                state.sb.Append(Encoding.ASCII.GetString(
                    state.buffer, 0, bytesRead));

                // Check for end-of-file tag. If it is not there, read
                // more data.  
                content = state.sb.ToString();

                if (content.IndexOf("<EOF>") > -1)
                {
                    // All the data has been read from the
                    // client. Display it on the console.  
                    Console.WriteLine("Read {0} bytes from socket. \n Data : {1}", content.Length, content);


                    // HERE IS THE ENTRY POINT FOR RUNNING INSTRUMENT COMMANDS
                    Message deserial = JsonSerializer.Deserialize<Message>(content.Substring(0,content.IndexOf('<')));

                    Console.WriteLine("Message ID: {0}", deserial.id);
                    Console.WriteLine("Message Command: {0}", deserial.command);
                    Console.WriteLine("Message Command Arguments: {0}", deserial.command_args);

                    if (deserial.command.ToLower().Equals("bingo"))
                    {
                        Console.WriteLine("Running Bingo");
                        Bingo();
                    }

                    // Echo the data back to the client.  
                    Send(handler, content);
                }
                else
                {
                    // Not all data received. Get more.  
                    handler.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
                    new AsyncCallback(ReadCallback), state);
                }
            }
        }

        private class Message
        {
            public string id { get; set; }
            public string command { get; set; }
            public Object command_args { get; set; }
        }

        private static void Bingo() 
        {
            Console.WriteLine("Hello Bingo, using port COM4");
            string defPortName = "COM4";
            bool useConsole = true;
            bool trackConnect = false;
            bool trackComm = false;
            bool trackResp = false;
            Processor sdPCRProcessor = new Processor(defPortName, useConsole, trackConnect, trackComm, trackResp);
            string[] portInfo = sdPCRProcessor.PortInformation;
            for (int n = 0; n < portInfo.Length; n++)
                Console.WriteLine(portInfo[n]);
            Console.WriteLine("=====");
            Console.WriteLine(String.Format("\nFirmware: {0}", sdPCRProcessor.FirmwareVersion));
            ControlBoard board;
            if (sdPCRProcessor.GetBoard(out board) && board != null)
            {
                Console.WriteLine("\nInitializing LEDs");
                int numbLEDs = 6;
                int[] LEDLevels = { 50, 50, 50, 50, 50, 50 }; // percent
                int[] LEDMax = { 50, 50, 50, 50, 50, 50 }; // percent
                LEDCtl ledController;
                List<string> errMessage;
                bool ledConnected = board.InitLEDCtl(numbLEDs, LEDMax, LEDLevels, out ledController, out errMessage);
                if (ledConnected)
                {
                    Console.WriteLine("Connection made to LEDs");
                }
                Console.WriteLine("Disconnecting board");
                board.Disconnect();
            }
        }

        private static void Send(Socket handler, String data)
        {
            // Convert the string data to byte data using ASCII encoding.  
            byte[] byteData = Encoding.ASCII.GetBytes(data);

            // Begin sending the data to the remote device.  
            handler.BeginSend(byteData, 0, byteData.Length, 0,
                new AsyncCallback(SendCallback), handler);
        }

        private static void SendCallback(IAsyncResult ar)
        {
            try
            {
                // Retrieve the socket from the state object.  
                Socket handler = (Socket)ar.AsyncState;

                // Complete sending the data to the remote device.  
                int bytesSent = handler.EndSend(ar);
                Console.WriteLine("Sent {0} bytes to client.", bytesSent);

                handler.Shutdown(SocketShutdown.Both);
                handler.Close();

            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        public static int Main(String[] args)
        {
            StartListening();
            return 0;
        }
    }
}
