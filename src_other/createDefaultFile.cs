﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics;
//using System.Timers;
using System.IO;
using System.IO.Ports;

namespace CreateDefaultFile
{
    public class CreateDefaultXML
    {

        public static void Main(string[] Args)
        {
            string fileName = "c:\\instrument\\default.xml";
            if ( Args.Length > 1)
            {
                fileName = Args[1];
            }
            //var configXML = new XDocument();
            //var rootElement = new XElement("Configuration");
            //configXML.Add(rootElement);

            //var configElement = new XElement("PID");
            //var presetTempElem = new XElement("IdleTemp", "40.0");
            //configElement.Add(presetTempElem);
            //presetTempElem = new XElement("LowTemp", "65.0");
            //configElement.Add(presetTempElem);
            //presetTempElem = new XElement("HighTemp", "95.0");
            //configElement.Add(presetTempElem);
            //var programElement = new XElement("Progarm", "1");
            //var step0 = new XElement("Step", "60.0,2.0,Setup");
            //programElement.Add(step0);
            //var step1 = new XElement("Step", "95.0,5.0,Presoak");
            //programElement.Add(step1);
            //for (int n = 0; n < 20; n++)
            //{
            //    var stepA = new XElement("Step", String.Format("60.0,1.0,{0}.1", n + 1));
            //    var stepB = new XElement("Step", String.Format("95.0,1.0,{0}.2", n + 1));
            //    programElement.Add(stepA);
            //    programElement.Add(stepB);
            //}
            //var stepC = new XElement("Step", "40.0,4.0,Cool Down");
            //programElement.Add(stepC);
            //configElement.Add(programElement);
            //rootElement.Add(configElement);



            //Console.WriteLine("");
            //Console.WriteLine(configXML.ToString());
            //Console.Read();

            
            XmlTextWriter xw = new XmlTextWriter(fileName, System.Text.Encoding.UTF8);
            xw.WriteStartDocument(true);
            xw.Formatting = Formatting.Indented;
            xw.Indentation = 2;
            xw.WriteStartElement("Configuration");
            xw.WriteStartElement("PID");
            createPresetTempNode(xw, "IdleTemp", 40.0);
            createPresetTempNode(xw, "LowTemp", 60.0);
            createPresetTempNode(xw, "HighTemp", 95.0);
            xw.WriteStartElement("Program");
            xw.WriteStartElement("ProgramName"); xw.WriteString("1"); xw.WriteEndElement();
            createProgramStepNode(xw, 0, 60.0, 1.0, "Setup");
            createProgramStepNode(xw, 1, 95.0, 5.0, "Presoak");
            int numbSteps = 20;
            for ( int n = 0; n < numbSteps; n++)
            {
                int step = (n + 1) * 2;
                createProgramStepNode(xw, step, 60.0, 1.0, String.Format("{0}.1", n + 1));
                createProgramStepNode(xw, step+1, 95.0, 1.0, String.Format("{0}.2", n + 1));
            }
            createProgramStepNode(xw, (numbSteps + 1) * 2, 40.0, 3.0, "Cool Down");
            xw.WriteEndElement();
            xw.WriteEndElement();



            xw.WriteEndElement();
            xw.WriteEndDocument();
            xw.Close();
        }

        public static void createPresetTempNode(XmlTextWriter XW, string Name, double Temp)
        {
            XW.WriteStartElement(Name);
            XW.WriteString(String.Format("{0,0:2}", Temp));
            XW.WriteEndElement();
        }

        public static void createProgramStepNode(XmlTextWriter XW, int StepNumb, double Temp, double Time, string Description)
        {
            XW.WriteStartElement("Step");
            XW.WriteStartElement("StepNumber"); XW.WriteString(String.Format("{0}", StepNumb)); XW.WriteEndElement();
            XW.WriteStartElement("Temperature"); XW.WriteString(String.Format("{0,0:2}", Temp)); XW.WriteEndElement();
            XW.WriteStartElement("Time"); XW.WriteString(String.Format("{0,0:2}", Time)); XW.WriteEndElement();
            XW.WriteStartElement("Description"); XW.WriteString(Description); XW.WriteEndElement();
            XW.WriteEndElement();
        }
    }
}

