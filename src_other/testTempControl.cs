﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Timers;

using sdpcrInstrLib;
using sdpcrInstrLib.InstrInterface;
using System.Runtime.Remoting.Metadata.W3cXsd2001;
using System.IO;

namespace testTempControl
{
    class TestTempControl
    {
        static System.Timers.Timer timer1 = new System.Timers.Timer();
        static bool cont;
        static StringBuilder fileName = new StringBuilder();
        static void Main(string[] args)
        {
            Console.Write("Enter Previous Port Name: ");
            string defPortName = Console.ReadLine().ToUpperInvariant();
            if (!Instrument.Initialize(defPortName)) //, true, true, true, true))
            {
                Console.WriteLine("Instrument did not initialize");
                Console.WriteLine("One of the components of the board did not connect properly");
                string[] iResults = Instrument.InitializationResult;
                for (int n = 0; n < iResults.Length; n++)
                    Console.WriteLine(iResults[n]);
                iResults = Instrument.InstrumentLog;
                Console.WriteLine("==========");
                for (int n = 0; n < iResults.Length; n++)
                    Console.WriteLine(iResults[n]);
                Console.WriteLine("Press enter to exit");
                Console.ReadLine();
                System.Environment.Exit(0);
            }
            else
            {
                string err;
                //List<string> progOut;
                string[] portInfo = Instrument.PortInfo;
                for (int n = 0; n < portInfo.Length; n++)
                    Console.WriteLine(portInfo[n]);

                Console.WriteLine(String.Format("\nFirmware: {0}", Instrument.SDPCRProcessor.FirmwareVersion));
                ResponseItem resp0;
                System.Threading.Thread.Sleep(500);
                Instrument.XStage.HighImpedance();
                Instrument.YStage.HighImpedance();
                Instrument.FilterWheel.HighImpedance();
                int wait = 0;
                while (Instrument.SDPCRProcessor.BoardWaiting)
                {
                    wait++;
                    if (wait > 10)
                        break;

                    System.Threading.Thread.Sleep(500);
                }
                //Console.WriteLine(String.Format("Wait = {0}", wait));
                if (wait > 0)
                {
                    while (Instrument.XStage.CheckResponseList(out resp0))
                    {
                        if (resp0.Command.IndexOf("hR") > 0)
                        {
                            Instrument.XStage.HighImpedance();
                            System.Threading.Thread.Sleep(100);
                        }
                    }
                    while (Instrument.YStage.CheckResponseList(out resp0))
                    {
                        if (resp0.Command.IndexOf("hR") > 0)
                        {
                            Instrument.YStage.HighImpedance();
                            System.Threading.Thread.Sleep(100);
                        }
                    }
                    while (Instrument.FilterWheel.CheckResponseList(out resp0))
                    {
                        if (resp0.Command.IndexOf("hR") > 0)
                        {
                            Instrument.FilterWheel.HighImpedance();
                            System.Threading.Thread.Sleep(100);
                        }
                    }
                }
                Console.WriteLine("Press Enter to continue");
                Console.ReadLine();
                List<string> program = new List<string>(0);

                program.Add("40.0,30.0,Setup");
                program.Add("90.0,120.0,Preasoak");

                program.Add("60.0,60.0,1.1");
                program.Add("90.0,60.0,1.2");

                program.Add("60.0,60.0,2.1");
                program.Add("90.0,60.0,2.2");

                program.Add("60.0,60.0,3.1");
                program.Add("90.0,60.0,3.2");

                program.Add("60.0,60.0,4.1");
                program.Add("90.0,60.0,4.2");

                program.Add("40.0,180.0,Cool Down");



                if (Instrument.TempProgramController.LoadProgram(program, out err))
                {
                    List<TempProgUnit> prog = Instrument.TempProgramController.TempProgram;
                    Console.WriteLine("Temperature Program");
                    Console.WriteLine("Temp, Time, Description");
                    for (int n = 0; n < prog.Count; n++)
                    {
                        Console.WriteLine(String.Format("{0}, {1}, {2}", prog[n].Temperature, prog[n].Time, prog[n].Description));
                    }
                    Instrument.Rotor.SetProfile(2);
                    Instrument.Rotor.SetVelocity(60);
                    Instrument.Rotor.VelocityModePositive();
                    Instrument.ThermMonitoring.StartMonitoring(2000);
                    Instrument.TempProgramController.StartProgram(0, 2000, true);

                    //bool timer1On = false;

                    int timeStep = 10000;

                    if (timer1 != null)
                    {
                        timer1.Stop();
                        timer1.Dispose();
                    }
                    cont = true;
                    // Create a timer with a two second interval.  // commeted out timer to see if this fixes hanging at end of program
                    timer1 = new System.Timers.Timer(timeStep);
                    // Hook up the Elapsed event for the timer. 
                    timer1.Elapsed += OnTimer1Event;
                    timer1.AutoReset = true;
                    timer1.Enabled = true;

                    Console.WriteLine("Enter q to quit, or blank line for current state");
                    Console.WriteLine("When program finishes user must enter q or blank line to exit the loop");

                    while (true)
                    {
                        string any = Console.ReadLine();
                        //Console.WriteLine(String.Format("{0}: any = {1}", Instrument.BoardTime, any));
                        if (any.IndexOf('q') >= 0 || any.IndexOf('Q') >= 0 || !cont)
                        {
                            //Console.WriteLine("while loop 1");
                            Instrument.TempProgramController.StopProgram();
                            Instrument.ThermMonitoring.StopMonitoring();
                            Instrument.Rotor.Terminate();
                            StopTimer1();
                            cont = false;
                            break;
                        }
                        else if (!Instrument.TempProgramController.ProgramRunning)
                        {
                            //Console.WriteLine("while loop 2");
                            Instrument.ThermMonitoring.StopMonitoring();
                            Instrument.Rotor.Terminate();
                            StopTimer1();
                            cont = false;
                            break;
                        }
                        else
                        {
                            //Console.WriteLine("while loop 3");
                            string curr = Instrument.TempProgramController.CurrentState;
                            Console.WriteLine(curr);
                        }
                    }

                    Console.WriteLine("Enter basename for output or blank line for no output");
                    Console.WriteLine("Output files will be created in c:\\instrument, which must exist");
                    string nam = Console.ReadLine();
                    if (nam.Length > 0)
                    {
                        fileName.Clear();
                        fileName.Append("c:\\instrument\\");
                        fileName.Append(nam);
                        fileName.Append("_PIDCtl.csv");
                        string[] output = new string[0];
                        Instrument.TempProgramController.GetOutput(out output);
                        if (IsValidFilename(fileName.ToString()))
                            System.IO.File.WriteAllLines(@fileName.ToString(), output);

                        fileName.Clear();
                        fileName.Append("c:\\instrument\\");
                        fileName.Append(nam);
                        fileName.Append("_thermistors.csv");
                        output = new string[0];
                        Instrument.ThermMonitoring.GetOutput(out output);
                        if (IsValidFilename(fileName.ToString()))
                            System.IO.File.WriteAllLines(@fileName.ToString(), output);

                        List<string> log = new List<string>(0);
                        fileName.Clear();
                        fileName.Append("c:\\instrument\\");
                        fileName.Append(nam);
                        fileName.Append("_processor_log.txt");
                        if (IsValidFilename(fileName.ToString()) && !File.Exists(fileName.ToString()))
                        {
                            if (Instrument.SDPCRProcessor.ReadLogInfo(out string[] lines))
                                System.IO.File.WriteAllLines(@fileName.ToString(), lines);
                            else
                                Console.WriteLine("Log list is empty");
                        }
                        fileName.Clear();
                        fileName.Append("c:\\instrument\\");
                        fileName.Append(nam);
                        fileName.Append("_thermistor_log.txt");
                        if (Instrument.Thermistor_Ctl.GetInternalLog(out string[] tLog))
                            System.IO.File.WriteAllLines(@fileName.ToString(), tLog);
                        else
                            Console.WriteLine("therm24 internal log is empty");
                        fileName.Clear();
                        fileName.Append("c:\\instrument\\");
                        fileName.Append(nam);
                        fileName.Append("_log.txt");
                        output = log.ToArray();
                        if (IsValidFilename(fileName.ToString()))
                            System.IO.File.WriteAllLines(@fileName.ToString(), output);
                    }
                }
                else
                {
                    Console.WriteLine("Problems loading temperature program");
                    Console.WriteLine(err);
                    Console.WriteLine("----------");
                }
            }

            if (Instrument.SDPCRProcessor != null)
            {  
                Instrument.SDPCRProcessor.Dispose();
            }
            Console.WriteLine("\nPress any key to exit");
            Console.ReadLine();
        }

        private static void OnTimer1Event(Object source, ElapsedEventArgs e)
        {
            ResponseItem resp;
            //Console.WriteLine("Checking timer1 events, program running: {0}", Instrument.TempProgramController.ProgramRunning.ToString());
            if (Instrument.TempProgramController.ProgramRunning)
            {
                //System.Threading.Thread.Sleep(10000);
                Console.WriteLine(Instrument.TempProgramController.CurrentState);
                if (Instrument.SDPCRProcessor.CheckResponseList(out resp))
                {
                    if (resp.NotifyCaller())
                    {
                        Console.WriteLine(String.Format("sdpcrProcessor Response\n{0}", resp.ToString()));
                        System.Threading.Thread.Sleep(25);
                    }
                }
                if (Instrument.Rotor.CheckResponseList(out resp))
                {
                    if (resp.NotifyCaller())
                    {
                        Console.WriteLine(String.Format("Rotor Response\n{0}", resp.ToString()));
                        System.Threading.Thread.Sleep(25);
                    }
                }
                if (Instrument.Thermistor_Ctl.CheckResponseList(out resp))
                {
                    if (resp.NotifyCaller())
                    {
                        Console.WriteLine(String.Format("Thermistor Controller Response\n{0}", resp.ToString()));
                        System.Threading.Thread.Sleep(25);
                    }
                }
                if (Instrument.Temp_Ctl.CheckResponseList(out resp))
                {
                    if (resp.NotifyCaller())
                    {
                        Console.WriteLine(String.Format("PID Controller Response\n{0}", resp.ToString()));
                        System.Threading.Thread.Sleep(25);
                    }
                }
            }
            else
            {
                timer1.Stop();
                timer1.Dispose();
                cont = false;
                Console.WriteLine("PID program has finished");
                Console.WriteLine("Press enter to continue");
            }
            //else
            //    cont = false;

        }
        public static void StopTimer1()
        {
            if (timer1 != null)
            {
                timer1.Stop();
                timer1.Dispose();
            }
        }
        public static bool IsValidFilename(string testName)
        {
            System.Text.RegularExpressions.Regex containsABadCharacter =
                new System.Text.RegularExpressions.Regex("["
                  + System.Text.RegularExpressions.Regex.Escape(new string(System.IO.Path.GetInvalidPathChars())) + "]");
            if (containsABadCharacter.IsMatch(testName)) { return false; };

            // other checks for UNC, drive-path format, etc

            return true;
        }
    }
   
}
