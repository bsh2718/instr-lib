﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using sdpcrInstrLib.InstrInterface;
using sdpcrInstrLib.CreateDefaultFile;
using System.Management.Instrumentation;

namespace testProgramLoading
{
    class Program
    {
        static void Main(string[] args)
        {
            if ( Instrument.Initialize("com3") )
            {
                if (Instrument.GetPositionProgramHeaders((int)MotorNumber.Rotor, out List<string> headers) )
                {
                    Console.WriteLine("Rotor Program Headers");
                    for (int n = 0; n < headers.Count; n++)
                        Console.WriteLine("{0} : {1}", n, headers[n]);

                    Instrument.PrintPositionProgram((int)MotorNumber.Rotor);
                }

                if (Instrument.GetPositionProgramHeaders((int)MotorNumber.XStage, out headers))
                {
                    Console.WriteLine("XStage Program Headers");
                    for (int n = 0; n < headers.Count; n++)
                        Console.WriteLine("{0} : {1}", n, headers[n]);

                    Instrument.PrintPositionProgram((int)MotorNumber.XStage);
                }

                if (Instrument.GetPositionProgramHeaders((int)MotorNumber.YStage, out headers))
                {
                    Console.WriteLine("YStage Program Headers");
                    for (int n = 0; n < headers.Count; n++)
                        Console.WriteLine("{0} : {1}", n, headers[n]);

                    Instrument.PrintPositionProgram((int)MotorNumber.YStage);
                }

                if (Instrument.GetPositionProgramHeaders((int)MotorNumber.FilterWheel, out headers))
                {
                    Console.WriteLine("FilterWheel Program Headers");
                    for (int n = 0; n < headers.Count; n++)
                        Console.WriteLine("{0} : {1}", n, headers[n]);

                    Instrument.PrintPositionProgram((int)MotorNumber.FilterWheel);
                }
            }
            else
            {
                Console.WriteLine("Port not connected");
            }

        }
    }
}
