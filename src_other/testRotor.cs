﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

using sdpcrInstrLib;
using sdpcrInstrLib.InstrInterface;
using sdpcrInstrLib.Stepper;
using sdpcrInstrLib.Sequencers;

namespace testRotor
{
    class TestRotor
    {
        public static void Main(string[] Args)
        {
            int iV = -1;
            if ( Args.Length > 0 )
            {
                if ( Int32.TryParse(Args[0], out iV))
                {
                    if (iV < 1 || iV > 150)
                        iV = -1;
                }
            }
            Console.Write("Enter Previous Port Name: ");
            string defPortName = Console.ReadLine().ToUpperInvariant();
            if (!Instrument.Initialize(defPortName))
            {
                Console.WriteLine("Instrument did not initialize");
                Console.WriteLine("One of the components of the board did not connect properly");
                string[] iResults = Instrument.InitializationResult;
                for (int n = 0; n < iResults.Length; n++)
                    Console.WriteLine(iResults[n]);
                iResults = Instrument.InstrumentLog;
                Console.WriteLine("==========");
                for (int n = 0; n < iResults.Length; n++)
                    Console.WriteLine(iResults[n]);
                Console.WriteLine("Press enter to exit");
                Console.ReadLine();
                System.Environment.Exit(0);
            }
            else
            {
                string[] logText;
                string err;
                List<string> progOut;
                string[] portInfo = Instrument.PortInfo;
                for (int n = 0; n < portInfo.Length; n++)
                    Console.WriteLine(portInfo[n]);

                Console.WriteLine(String.Format("\nFirmware: {0}", Instrument.SDPCRProcessor.FirmwareVersion));
                //string any;
                //Instrument.SDPCRProcessor.ReadLogInfo(out logText);
                //Console.WriteLine(String.Format("     logText.Length: {0}", logText.Length));
                //for (int n = 0; n < logText.Length; n++)
                //    Console.WriteLine(logText[n]);

                Console.WriteLine("Press Enter to continue");
                Console.ReadLine();
                int initVelocity = 60;
                if (iV >= 1)
                    initVelocity = iV;
                int initProfile = 2;

                Instrument.Rotor.AbsolutePosition(400);
                Console.WriteLine("Rotor should be at position 400");
                Console.WriteLine("Press enter to load rotor program");
                Console.ReadLine();
                List<int> positions = new List<int>(0)
                {
                    1200,
                    2000,
                    2800,
                    1200,
                    2000,
                    2800,
                    400
                };
                ResponseItem resp;
                if (Instrument.RotorPositionSequencer.LoadPositionProgram(positions, initProfile, initVelocity, out err))
                {
                    Console.WriteLine("program should be loaded: {0}", err);
                    Console.Out.Flush();
                    Instrument.RotorPositionSequencer.GetPositionProgram(out progOut);
                    Console.WriteLine("Program Loaded");
                    for (int n = 0; n < progOut.Count; n++)
                        Console.WriteLine(progOut[n]);

                    Console.WriteLine("Press enter to start program, 3 cycles of 7 steps, 4 seconds between steps");
                    Console.ReadLine();
                    if (Instrument.RotorPositionSequencer.StartPositionProgram(4000, 21))
                        Console.WriteLine("Motor program started");
                    else
                        Console.WriteLine("Motor program not started");

                    for (int n = 0; n < 30; n++)
                    {
                        System.Threading.Thread.Sleep(5000);
                        if (Instrument.RotorPositionSequencer.ProgramRunning)
                            Console.WriteLine(String.Format("{0} : Program is running", n));
                        else
                        {
                            Console.WriteLine(String.Format("{0} : Program is not running", n));
                            break;
                        }
                        while (Instrument.RotorPositionSequencer.CheckErrorList(out resp))
                        {
                            Console.WriteLine(resp.ToString());
                        }
                    }
                    Console.WriteLine("Rotor program should have finished with rotor at position 400");
                }
                else
                {
                    Console.WriteLine("Problems loading first program");
                    Console.WriteLine(err);
                    Console.WriteLine("----------");
                }
                Console.WriteLine("Press enter to continue");
                Console.ReadLine();
                Instrument.Rotor.SetVelocity(20);
                Instrument.Rotor.Home();
                System.Threading.Thread.Sleep(4000);
                Instrument.Rotor.AbsolutePosition(400);
                Instrument.Rotor.SetVelocity(initVelocity);
                Console.WriteLine("Rotor should be at position 400");
                Console.WriteLine("Press enter to load next program");
                Console.ReadLine();
                string progString = "<Program Type=\"Position\" Profile=\"2\" Velocity=\"100\" Name=\"Rotor-2 8 Positions\">" +
                    "<Step Number=\"0\" Position=\"400\"/>" +
                    "<Step Number=\"1\" Position=\"1200\"/>" +
                    "<Step Number=\"2\" Position=\"2000\"/>" +
                    "<Step Number=\"3\" Position=\"2800\"/>" +
                    "<Step Number=\"4\" Position=\"800\"/>" +
                    "<Step Number=\"5\" Position=\"1600\"/>" +
                    "<Step Number=\"6\" Position=\"2400\"/>" +
                    "<Step Number=\"7\" Position=\"3200\"/>" +
                    "</Program>";
                Console.WriteLine(progString);
                int motorNumb = (int)MotorNumber.Rotor;
                if (Instrument.AddPositionProgram(1, progString))
                {
                    Instrument.GetPositionProgramHeaders(motorNumb, out List<string> programHeaders);
                    Console.WriteLine("Rotor Program Headers");
                    for (int n = 0; n < programHeaders.Count; n++)
                        Console.WriteLine(programHeaders[n]);
                    int prog = programHeaders.Count - 1;
                    int pProfile;
                    int pVelocity;
                    Instrument.GetPositionProgram(motorNumb, prog, out pProfile, out pVelocity, out List<int> program);
                    Console.WriteLine(String.Format("Position program {0}", prog));
                    Console.WriteLine(String.Format("Profile: {0} - Velocity: {1}", pProfile, pVelocity));
                    for (int n = 0; n < program.Count; n++)
                        Console.WriteLine(program[n]);

                    Console.WriteLine("Press enter to start program");
                    Console.ReadLine();
                    
                    if ( !Instrument.LoadPositionProgram(motorNumb, prog, out err) )
                    {
                        Console.WriteLine(String.Format("Error loading program {0}", prog));
                        Console.WriteLine(String.Format("     {0}", err));
                    }    
                    Instrument.RotorPositionSequencer.StartPositionProgram(4000, 25);
                    while (Instrument.RotorPositionSequencer.ProgramRunning)
                    {
                        Console.WriteLine(String.Format("Program is running")); 
                        while (Instrument.RotorPositionSequencer.CheckErrorList(out resp))
                        {
                            Console.WriteLine(resp.ToString());
                        }
                        System.Threading.Thread.Sleep(4000);
                    }
                    Console.WriteLine("Rotor program should have finished with rotor at position 400");
                    Console.WriteLine("Press enter to proceed");

                    int mod = Instrument.RotorPositionSequencer.Modulous;
                    Console.WriteLine(String.Format("Modulous = {0}", mod));
                    for (int n = 0; n < mod; n++)
                    {
                        Console.WriteLine(String.Format("Press enter to advance to position {0} at {1}", n, program[n]));
                        Console.ReadLine();
                        Instrument.RotorPositionSequencer.GoToPosition(n);
                    }

                    Console.WriteLine(">>>>>> Press enter to conclude rotor tests");
                    Console.ReadLine();
                    //string[] logText;
                    Instrument.SDPCRProcessor.ReadLogInfo(out logText);
                    if ( logText.Length > 0 )
                    {
                        for (int l = 0; l < logText.Length; l++)
                            Console.WriteLine(logText[l]);
                    }
                }
                else
                {
                    Console.WriteLine("Problems adding second program");
                    Console.WriteLine(err);
                    Console.WriteLine("----------");
                }
            }
            if (Instrument.SDPCRProcessor != null)
            {
                Console.WriteLine("Calling sdPCRProcessor.Dispose()");
                Instrument.SDPCRProcessor.Dispose();
                Console.WriteLine("Press any key ");
                Console.ReadLine();
            }
        }
    }
}
