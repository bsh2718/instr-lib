﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

using sdpcrInstrLib;
using sdpcrInstrLib.InstrInterface;
using sdpcrInstrLib.Stepper;
using sdpcrInstrLib.Sequencers;

namespace testRotorVelocity
{
    class TestRotor
    {
        public static ConcurrentQueue<string>VResponses;
        public static void LogVResponses(string Resp)
        {
            VResponses.Enqueue(Resp);
        }
        public static void Main(string[] Args)
        {
            int iP = -1;
            if ( Args.Length > 0 )
            {
                if ( Int32.TryParse(Args[0], out iP))
                {
                    if (iP < 2 || iP > 3)
                        iP = 3;
                }
            }
            Console.Write("Enter Previous Port Name: ");
            string defPortName = Console.ReadLine().ToUpperInvariant();
            if (!Instrument.Initialize(defPortName))
            {
                Console.WriteLine("Instrument did not initialize");
                Console.WriteLine("One of the components of the board did not connect properly");
                string[] iResults = Instrument.InitializationResult;
                for (int n = 0; n < iResults.Length; n++)
                    Console.WriteLine(iResults[n]);
                iResults = Instrument.InstrumentLog;
                Console.WriteLine("==========");
                for (int n = 0; n < iResults.Length; n++)
                    Console.WriteLine(iResults[n]);
                Console.WriteLine("Press enter to exit");
                Console.ReadLine();
                System.Environment.Exit(0);
            }
            else
            {
                ResponseItem resp;
                int w1;
                double w2;
                string reg;
                VResponses = new ConcurrentQueue<string>();
                string err;
                List<string> progOut;
                string[] portInfo = Instrument.PortInfo;
                for (int n = 0; n < portInfo.Length; n++)
                    Console.WriteLine(portInfo[n]);

                Console.WriteLine(String.Format("\nFirmware: {0}", Instrument.SDPCRProcessor.FirmwareVersion));
                //string any;
                Console.WriteLine("Press Enter to continue");
                Console.ReadLine();
                int initProfile;
                if (iP < 2)
                    initProfile = 3;
                else
                    initProfile = iP;

                //Instrument.Rotor.AbsolutePosition(400);
                //Console.WriteLine("Rotor should be at position 400");
                Console.WriteLine("Press enter to load velocity program");
                Console.ReadLine();
                List<string> vProgram = new List<string>(0);
                vProgram.Add("600,60.0,Step 1");
                vProgram.Add("900,60.0,Step 2");
                vProgram.Add("1200,60.0,Step 3");
                vProgram.Add("400,60.0,Step 4");
                if (Instrument.RotorVelocitySequencer.LoadVelocityProgram(vProgram, initProfile,  out err))
                {
                    Console.WriteLine("program should be loaded: {0}", err);
                    Console.Out.Flush();
                    Instrument.RotorVelocitySequencer.GetVelocityProgram(out progOut);
                    Console.WriteLine("Program Loaded");
                    for (int n = 0; n < progOut.Count; n++)
                        Console.WriteLine(progOut[n]);

                    Console.WriteLine("Press enter to start program, 4 different velocities, 60 seconds each");
                    Console.ReadLine();
                    if (Instrument.RotorVelocitySequencer.StartVelocityProgram())
                        Console.WriteLine("Motor program started");
                    else
                        Console.WriteLine("Motor program not started");

                    for (int n = 0; n < 30; n++)
                    {
                        System.Threading.Thread.Sleep(10000);
                        if (Instrument.RotorVelocitySequencer.ProgramRunning)
                        {
                            Console.WriteLine(String.Format("{0} : Program is running", n));
                            if ( (n+1) % 4 == 0)
                            {
                                Console.WriteLine(Instrument.RotorVelocitySequencer.CurrentVelocityState());
                                Console.WriteLine("");
                                reg = Instrument.Rotor.Register;
                                Console.WriteLine("===== register");
                                Console.WriteLine(reg);
                                Console.WriteLine("=====");
                            }
                        }
                        else
                        {
                            Console.WriteLine(String.Format("{0} : Program is not running", n));
                            break;
                        }
                    }
                    Console.WriteLine("Rotor program should have finished with rotor stopped");
                }
                else
                {
                    Console.WriteLine("Problems loading first program");
                    Console.WriteLine(err);
                    Console.WriteLine("----------");
                }
                Console.WriteLine("Press enter to continue");
                Console.ReadLine();
                Instrument.Rotor.Terminate();
                Console.WriteLine("Press enter to load next program");
                Console.ReadLine();
                string progString = "<Program Type=\"Velocity\" Profile=\"3\" Velocity=\"1\" Name=\"Rotor-2 5 Velocities\">" +
                    "<Step Number=\"0\" Velocity=\"600\" Time=\"60.0\" Description=\"Spin Up\"/>" +
                    "<Step Number=\"0\" Velocity=\"1000\" Time=\"60.0\" Description=\"Step 1\"/>" +
                    "<Step Number=\"0\" Velocity=\"1500\" Time=\"20.0\" Description=\"Step 2\"/>" +
                    "<Step Number=\"0\" Velocity=\"1000\" Time=\"60.0\" Description=\"Step 3\"/>" +
                    "<Step Number=\"0\" Velocity=\"400\" Time=\"60.0\" Description=\"Spin Down\"/>" +
                    "</Program>";
                Console.WriteLine(progString);
                int motorNumb = (int)MotorNumber.Rotor;
                List<string> programHeaders;
                
                if (Instrument.AddVelocityProgram(1, progString))
                {
                    Instrument.GetVelocityProgramHeaders(motorNumb, out programHeaders);
                    Console.WriteLine("Rotor Program Headers");
                    for (int n = 0; n < programHeaders.Count; n++)
                        Console.WriteLine(programHeaders[n]);
                    int prog = programHeaders.Count - 1;
                    int pProfile;
                    Instrument.GetVelocityProgram(motorNumb, prog, out pProfile, out List<string> program);
                    Console.WriteLine(String.Format("Velocity program {0}", prog));
                    Console.WriteLine(String.Format("Profile: {0} ", pProfile));
                    double totalTime = 0;
                    for (int n = 0; n < program.Count; n++)
                    {
                        Console.WriteLine(program[n]);
                        string[] progStep = program[n].Split(',');
                        if (progStep.Length > 2)
                        {
                            if (Double.TryParse(progStep[1], out w2))
                                totalTime += w2;
                        }
                    }
                    Console.WriteLine(String.Format(" Program Total Time: {0} sec", totalTime));

                    Console.WriteLine("Press enter to start program");
                    Console.ReadLine();
                    int numbIter = (int)Math.Ceiling(totalTime / 10.0) + 2;
                   
                    if (!Instrument.LoadVelocityProgram(motorNumb, prog, out err))
                    {
                        Console.WriteLine(String.Format("Error loading program {0}", prog));
                        Console.WriteLine(String.Format("     {0}", err));
                    }
                    else
                    {
                        Instrument.RotorVelocitySequencer.StartVelocityProgram();
                        for (int n = 0; n < numbIter; n++)
                        {
                            System.Threading.Thread.Sleep(10000);
                            if (Instrument.RotorVelocitySequencer.ProgramRunning)
                            {
                                Console.WriteLine(String.Format("{0} : Program is running", n));
                                int m = n % 5;
                                switch (m)
                                {
                                    case 0:
                                        Console.WriteLine(Instrument.InstrumentState);
                                        break;
                                    case 1:
                                        Console.WriteLine(Instrument.RotorVelocitySequencer.CurrentVelocityState());
                                        if (Instrument.Rotor.GetMotorSpeed(out w2))
                                            Console.WriteLine(String.Format("     Speed: {0}", w2));
                                        else
                                            Console.WriteLine("     Could not get Speed");
                                        Console.WriteLine("");
                                        break;
                                    case 2:
                                        Console.WriteLine(Instrument.RotorVelocitySequencer.CurrentVelocityState());
                                        if (Instrument.Rotor.GetMotorDirection(out w1) )
                                            Console.WriteLine(String.Format("     Direction: {0}", w1));
                                        else
                                            Console.WriteLine("     Could not get Direction");
                                        Console.WriteLine("");
                                        break;
                                    case 3:
                                        Console.WriteLine(Instrument.RotorVelocitySequencer.CurrentVelocityState());
                                        if (Instrument.Rotor.GetMotorMotion(out w1))
                                            Console.WriteLine(String.Format("     Motion: {0}", w1));
                                        else
                                            Console.WriteLine("     Could not get Motion");
                                        Console.WriteLine("");
                                        break;
                                    case 4:
                                        Console.WriteLine(Instrument.RotorVelocitySequencer.CurrentVelocityState());
                                        reg = Instrument.Rotor.Register;
                                        Console.WriteLine("===== register");
                                        Console.WriteLine(reg);
                                        Console.WriteLine("=====");
                                        break;
                                    default:
                                        break;

                                }
                                while (Instrument.RotorVelocitySequencer.CheckErrorList(out resp))
                                {
                                    Console.WriteLine(resp.ToString());
                                }
                            }
                            else
                            {
                                Console.WriteLine(String.Format("{0} : Program is not running", n));
                                break;
                            }
                        }
                    }
                    Console.WriteLine("Rotor velocity program should have finished with rotor stopped");
                    Console.WriteLine("Press enter to proceed");

                    Console.WriteLine("More motor register checks");
                    reg = Instrument.Rotor.Register;
                    Console.WriteLine("===== register");
                    Console.WriteLine(reg);
                    Console.WriteLine("=====");

                    if (Instrument.Rotor.GetMotorSpeed(out w2))
                        Console.WriteLine(String.Format("     Speed: {0}", w2));
                    else
                        Console.WriteLine("     Could not get Speed");

                    if (Instrument.Rotor.GetMotorDirection(out w1))
                        Console.WriteLine(String.Format("     Direction: {0}", w1));
                    else
                        Console.WriteLine("     Could not get Direction");

                    if (Instrument.Rotor.GetMotorMotion(out w1))
                        Console.WriteLine(String.Format("     Motion: {0}", w1));
                    else
                        Console.WriteLine("     Could not get Motion");

                    Console.WriteLine("");
                    Console.WriteLine(">>>>>> Press enter to continue rotor tests");
                    Console.ReadLine();
                }
                else
                {
                    Console.WriteLine("Problems adding second program");
                    Console.WriteLine(err);
                    Console.WriteLine("----------");
                }

                Instrument.GetVelocityProgramHeaders(motorNumb, out programHeaders);
                Console.WriteLine("Rotor Program Headers");
                for (int n = 0; n < programHeaders.Count; n++)
                    Console.WriteLine(programHeaders[n]);
                Console.WriteLine("Enter number of program to run");
                string v0 = Console.ReadLine();
                int w0;
                if (Int32.TryParse(v0, out w0) && w0 >= 0 && w0 < programHeaders.Count)
                {
                    int prog;
                    if (w0 < programHeaders.Count && w0 >= 0)
                        prog = w0;
                    else
                        prog = programHeaders.Count - 1;
                    int pProfile;
                    Instrument.GetVelocityProgram(motorNumb, prog, out pProfile, out List<string> program);
                    Console.WriteLine(String.Format("Velocity program {0}", prog));
                    Console.WriteLine(String.Format("Profile: {0} ", pProfile));
                    double totalTime = 0;
                    for (int n = 0; n < program.Count; n++)
                    {
                        Console.WriteLine(program[n]);
                        string[] progStep = program[n].Split(',');
                        if (progStep.Length > 2)
                        {
                            if (Double.TryParse(progStep[1], out w2))
                                totalTime += w2;
                        }
                    }
                    Console.WriteLine(String.Format(" Program Total Time: {0} sec", totalTime));

                    Console.WriteLine("Press enter to start program");
                    Console.ReadLine();

                    if (!Instrument.LoadVelocityProgram(motorNumb, prog, out err))
                    {
                        Console.WriteLine(String.Format("Error loading program {0}", prog));
                        Console.WriteLine(String.Format("     {0}", err));
                    }
                    else
                    {
                        int numbIter = (int)Math.Ceiling(totalTime / 10.0) + 2;
                        Instrument.RotorVelocitySequencer.StartVelocityProgram(LogVResponses);
                        for (int n = 0; n < numbIter; n++)
                        {
                            System.Threading.Thread.Sleep(10000);
                            if (Instrument.RotorVelocitySequencer.ProgramRunning)
                            {
                                Console.WriteLine(String.Format("{0} : Program is running", n));
                                if ((n + 1) % 4 == 0)
                                {
                                    Console.WriteLine(Instrument.RotorVelocitySequencer.CurrentVelocityState());
                                    Console.WriteLine("");
                                    reg = Instrument.Rotor.Register;
                                    Console.WriteLine("===== register");
                                    Console.WriteLine(reg);
                                    Console.WriteLine("=====");
                                }
                            }
                            else
                            {
                                Console.WriteLine(String.Format("{0} : Program is not running", n));
                                break;
                            }
                            while (Instrument.RotorVelocitySequencer.CheckErrorList(out resp))
                            {
                                Console.WriteLine(resp.ToString());
                            }
                        }
                    }
                    Console.WriteLine("Rotor velocity program should have finished with rotor stopped");
                    Console.WriteLine("Press enter to proceed");

                    string vResp;
                    Console.WriteLine("Velocity Log");
                    while (VResponses.TryDequeue(out vResp))
                        Console.WriteLine(vResp);

                    Console.WriteLine(">>>>>> Press enter to conclude rotor tests");
                    Console.ReadLine();
                }
            }
            if (Instrument.SDPCRProcessor != null)
            {
                Console.WriteLine("Calling sdPCRProcessor.Dispose()");
                Instrument.SDPCRProcessor.Dispose();
                Console.WriteLine("Press any key ");
                Console.ReadLine();
            }
        }
    }
}
