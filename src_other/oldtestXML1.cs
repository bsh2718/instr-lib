﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Xml.Linq;
using System.Text;
//using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Timers;
using System.IO;
using System.IO.Ports;
using System.Net;
using System.Xml.Schema;
using System.Linq.Expressions;

using sdpcrInstrLib.BoardComm;
using sdpcrInstrLib.Hardware;
using sdpcrInstrLib.LEDCluster;
using sdpcrInstrLib.Stepper;
using sdpcrInstrLib.PIDController;
using sdpcrInstrLib.Therm;
using sdpcrInstrLib.InstrExtensions;
using sdpcrInstrLib.Sequencers;
using sdpcrInstrLib.CreateDefaultFile;
using System.Security.Policy;

namespace TestInterface
{
    public static class Instrument
    {
        //static bool XMLfileLoaded = false;
        static XDocument _configuration;
        // Predefined character sets
        readonly static char[] whiteSpace = { ' ', '\n', '\r', '\t' };  // space, new line, carriage return, tab || comma
        readonly static char[] whiteSpaceQuote = { ' ', '\n', '\r', '\t', '"', '\'' };  // space, new line, carriage return, tab, ", '
        readonly static char[] whiteSpaceQuoteBack = { ' ', '\n', '\r', '\t', '"', '\'', '\\' };  // space, new line, carriage return, tab, ", '
        readonly static char[] delimiterChars = { ' ', ',', '\t' }; // space, comma, and tab
        readonly static char[] comma = { ',' };


        static public int numbLEDs = 6;
        static public int[] ledLevels = { 50, 50, 50, 50, 50, 50 }; // percent
        static public int[] ledMax = { 50, 50, 50, 50, 50, 50 }; // percent
        static public string[] ledName = { "L350/30", "L400/30", "L500/30", "L550/30", "L600/30", "L650/30" }; // LED names

        static public int numbFilters;
        static public string[] filterName;

        static public int numbDyes;
        static public DyeInformation[] dyeInfo;

        static public List<XElement> pidPrograms;
        static public List<XElement> rotorPrograms;
        static public List<XElement> xStagePrograms;
        static public List<XElement> yStagePrograms;
        static public List<XElement> filterWheelPrograms;

        //readonly static string outputDir = "C:\\instrument\\";


        public static void UseDefaultConfiguration()
        {
            CreateDefaultXML cdXML = new CreateDefaultXML();
            _configuration = cdXML.DefaultXML;
            //XMLfileLoaded = true;
        }
        public static bool StringToXDoc(string XDocString, out XDocument XDoc)
        {
            try { XDoc = XDocument.Parse(XDocString); }
            catch (System.Xml.XmlException)
            {
                XDoc = new XDocument();
                return false;
            }
            return true;
        }
        public static bool GetXElementProgram(XDocument XDoc, out XElement XProgram)
        {
            IEnumerable<XElement> items =
                    from el in XDoc.Descendants("Program")
                    select el;
            if (items.Count() > 0)
            {
                XProgram = items.First();
                return true;
            }
            else
            {
                XProgram = new XElement("None");
                return false;
            }
        }
        public static bool LoadConfiguration(string ConfigString)
        {
            if (StringToXDoc(ConfigString, out _configuration))
                return true;
            else
            {
                UseDefaultConfiguration();
                return false;
            }
        }
        public static void Main()
        {
            UseDefaultConfiguration();
            if (GetLEDParams(_configuration, out numbLEDs, out ledMax, out ledLevels, out ledName))
            {
                Console.WriteLine("LED Params");
                for (int n = 0; n < numbLEDs; n++)
                    Console.WriteLine(String.Format("{0}: {1} / {2} / {3}", n, ledMax[n], ledLevels[n], ledName[n]));
            }
            else
            {
                Console.WriteLine("     Problem with LED configuration");
            }

            Console.WriteLine("");
            if (GetMotorParams("Rotor", _configuration,
                out int motorN, out int directionCode, out int maxProfile, out int maxVelocity,
                out int backout, out int maxPosition, out int initProfile, out int initVelocity, out rotorPrograms))
            {
                Console.WriteLine("Rotor Configuration: {0} / {1}", motorN, directionCode);
                Console.WriteLine("     {0} / {1} / {2} / {3} / {4} /{5}",
                    maxProfile, maxVelocity, backout, maxPosition, initProfile, initVelocity);
            }
            else
            {
                Console.WriteLine("     Problem with Rotor configuration");
            }
            Console.WriteLine("");
            if (GetMotorParams("XStage", _configuration,
                out motorN, out directionCode, out maxProfile, out maxVelocity,
                out backout, out maxPosition, out initProfile, out initVelocity, out xStagePrograms))
            {
                Console.WriteLine("XStage Configuration: {0} / {1}", motorN, directionCode);
                Console.WriteLine("     {0} / {1} / {2} / {3} / {4} /{5}",
                    maxProfile, maxVelocity, backout, maxPosition, initProfile, initVelocity);
            }
            else
            {
                Console.WriteLine("     Problem with XStage configuration");
            }
            Console.WriteLine("");
            if (GetMotorParams("YStage", _configuration,
                out motorN, out directionCode, out maxProfile, out maxVelocity,
                out backout, out maxPosition, out initProfile, out initVelocity, out yStagePrograms))
            {
                Console.WriteLine("YStage Configuration: {0} / {1}", motorN, directionCode);
                Console.WriteLine("     {0} / {1} / {2} / {3} / {4} /{5}",
                    maxProfile, maxVelocity, backout, maxPosition, initProfile, initVelocity);
            }
            else
            {
                Console.WriteLine("     Problem with YStage configuration");
            }
            Console.WriteLine("");
            if (GetMotorParams("FilterWheel", _configuration,
                out motorN, out directionCode, out maxProfile, out maxVelocity,
                out backout, out maxPosition, out initProfile, out initVelocity, out filterWheelPrograms))
            {
                Console.WriteLine("FilterWheel Configuration: {0} / {1}", motorN, directionCode);
                Console.WriteLine("     {0} / {1} / {2} / {3} / {4} /{5}",
                    maxProfile, maxVelocity, backout, maxPosition, initProfile, initVelocity);
            }
            else
            {
                Console.WriteLine("     Problem with Filter Wheel configuration");
            }

            IEnumerable<XElement> items =
                from el in _configuration.Descendants("Thermistors")
                select el;
            XElement xtherm = items.First();
            XAttribute xat = xtherm.Attribute("ThermistorWeights");
            string weightsString = xat.Value;
            string[] weights = weightsString.Split(',');
            if (weights.Length == 24)
            {
                int[] tempW = new int[24];
                bool wgtsOK = true;
                for (int n = 0; n < 24; n++)
                {
                    if (!Int32.TryParse(weights[n], out tempW[n]))
                    {
                        wgtsOK = false;
                        break;
                    }
                }
                if (wgtsOK)
                {
                    Console.WriteLine("");
                    Console.WriteLine("Thermistor Weights");
                    Console.Write(String.Format("     {0}", weights[0]));
                    for (int n = 1; n < 24; n++)
                        Console.Write(String.Format(", {0}", weights[n]));
                    Console.WriteLine("");
                }
                else
                {
                    Console.WriteLine("Problem with weights in configuration");
                }
            }
            else
            {
                Console.WriteLine("Problem with weights in configuration");
            }

            Console.WriteLine("");
            
            if (GetPIDParams(_configuration, out double idleTemperature, out double lowTemperature,
                out double highTemperature, out pidPrograms))
            {
                Console.WriteLine("PID parameters");
                Console.WriteLine(String.Format("PID presets, Idle: {0}, Low: {1}, High: {2}",
                    idleTemperature, lowTemperature, highTemperature));
            }
            else
            {
                Console.WriteLine("     Problem with PID configuration");
            }

            Console.WriteLine("");
            if ( GetFilterParams(_configuration, out numbFilters, out filterName))
            {
                Console.WriteLine("Filter Parameters");
                for (int n = 0; n < numbFilters; n++)
                    Console.WriteLine(String.Format("{0} : {1}", n, filterName[n]));
            }
            else
            {
                Console.WriteLine("     Problem with PID configuration");
            }

            Console.WriteLine("");
            if (GetDyeParams(_configuration, ledName, filterName, out numbDyes, out dyeInfo) > 0 )
            {
                Console.WriteLine("Dye Information");
                for (int n = 0; n < dyeInfo.Length; n++)
                    Console.WriteLine(String.Format("{0}: {1},{2} / {3},{4} / {5},{6}",
                        dyeInfo[n].Name, dyeInfo[n].LEDName, dyeInfo[n].LEDPosition,
                        dyeInfo[n].FilterName, dyeInfo[n].FilterPosition, dyeInfo[n].Gain, dyeInfo[n].Exposure));
            }

            Console.WriteLine("");
            List<string> programHeaders;
            if (GetProgramHeaders(rotorPrograms, out programHeaders))
            {
                Console.WriteLine("Rotor Programs");
                for (int n = 0; n < programHeaders.Count; n++)
                    Console.WriteLine(String.Format("{0} : {1}", n, programHeaders[n]));
            }
            else
                Console.WriteLine("No Rotor programs found");
            Console.WriteLine("");
            if (GetProgramHeaders(xStagePrograms, out programHeaders))
            {
                Console.WriteLine("XStage Programs");
                for (int n = 0; n < programHeaders.Count; n++)
                    Console.WriteLine(String.Format("{0} : {1}", n, programHeaders[n]));
            }
            else
                Console.WriteLine("No XStage programs found");
            Console.WriteLine("");
            if (GetProgramHeaders(yStagePrograms, out programHeaders))
            {
                Console.WriteLine("YStage Programs");
                for (int n = 0; n < programHeaders.Count; n++)
                    Console.WriteLine(String.Format("{0} : {1}", n, programHeaders[n]));
            }
            else
                Console.WriteLine("No YStage programs found");
            Console.WriteLine("");
            if (GetProgramHeaders(filterWheelPrograms, out programHeaders))
            {
                Console.WriteLine("Filter Wheel Programs");
                for (int n = 0; n < programHeaders.Count; n++)
                    Console.WriteLine(String.Format("{0} : {1}", n, programHeaders[n]));
            }
            else
                Console.WriteLine("No Filter Wheel programs found");
            Console.WriteLine("");
            if (GetProgramHeaders(pidPrograms, out programHeaders))
            {
                Console.WriteLine("PID Programs");
                for (int n = 0; n < programHeaders.Count; n++)
                    Console.WriteLine(String.Format("{0} : {1}", n, programHeaders[n]));
            }
            else
                Console.WriteLine("No PID programs found");

            List<int> motorProgram;
            if (GetRotorProgram(0, out motorProgram))
            {
                Console.WriteLine("Rotor Program 0");
                for (int n = 0; n < motorProgram.Count; n++)
                    Console.WriteLine(String.Format("{0} : {1}", n, motorProgram[n]));
            }
            else
                Console.WriteLine("Rotor Program 0 not found!");

            if (GetXStageProgram(0, out motorProgram))
            {
                Console.WriteLine("XStage Program 0");
                for (int n = 0; n < motorProgram.Count; n++)
                    Console.WriteLine(String.Format("{0} : {1}", n, motorProgram[n]));
            }
            else
                Console.WriteLine("XStage Program 0 not found!");

            if (GetXStageProgram(1, out motorProgram))
            {
                Console.WriteLine("XStage Program 1");
                for (int n = 0; n < motorProgram.Count; n++)
                    Console.WriteLine(String.Format("{0} : {1}", n, motorProgram[n]));
            }
            else
                Console.WriteLine("XStage Program 1 not found!");

            if (GetFilterWheelProgram(0, out motorProgram))
            {
                Console.WriteLine("Filter Wheel Program 0");
                for (int n = 0; n < motorProgram.Count; n++)
                    Console.WriteLine(String.Format("{0} : {1}", n, motorProgram[n]));
            }
            else
                Console.WriteLine("Filter Wheel Program 0 not found!");

            List<string> pidProgram;
            if (GetPIDProgram(0, out pidProgram))
            {
                Console.WriteLine("PID Program 0");
                for (int n = 0; n < pidProgram.Count; n++)
                    Console.WriteLine(String.Format("{0} : {1}", n, pidProgram[n]));
            }
            else
                Console.WriteLine("PID Program 0 not found!");
            Console.WriteLine("Press Enter to Exit");
            Console.ReadLine();
        }

        

        internal static bool GetProgramHeaders(List<XElement> Programs, out List<string> ProgramHeaders)
        {
            ProgramHeaders = new List<string>(0);
            if (Programs.Count < 1)
                return false;
            string header;
            for (int n = 0; n < Programs.Count; n++)
            {
                XAttribute xa = Programs[n].Attribute("Name");
                IEnumerable<XElement> items =
                    from el in Programs[n].Descendants("Step")
                    select el;
                header = String.Format("{0} : {1} : {2}", n, xa.Value, items.Count());
                ProgramHeaders.Add(header);
            }
            return true;
        }
        public static bool GetRotorPrograms(out List<string> ProgramHeaders)
        {
            return GetProgramHeaders(rotorPrograms, out ProgramHeaders);
        }
        public static bool GetXStagePrograms(out List<string> ProgramHeaders)
        {
            return GetProgramHeaders(xStagePrograms, out ProgramHeaders);
        }
        public static bool GetYStagePrograms(out List<string> ProgramHeaders)
        {
            return GetProgramHeaders(yStagePrograms, out ProgramHeaders);
        }
        public static bool GetFilterWheelPrograms(out List<string> ProgramHeaders)
        {
            return GetProgramHeaders(filterWheelPrograms, out ProgramHeaders);
        }
        public static bool GetPIDPrograms(out List<string> ProgramHeaders)
        {
            return GetProgramHeaders(pidPrograms, out ProgramHeaders);
        }

        public static bool GetRotorProgram(int Prog, out List<int> Program)
        {
            Program = new List<int>(0);
            if (FormatMotorProgram(rotorPrograms, Prog, out Program))
                return true;
            else
                return false;
        }
        public static bool GetXStageProgram(int Prog, out List<int> Program)
        {
            Program = new List<int>(0);
            if (FormatMotorProgram(xStagePrograms, Prog, out Program))
                return true;
            else
                return false;
        }
        public static bool GetYStageProgram(int Prog, out List<int> Program)
        {
            Program = new List<int>(0);
            if (FormatMotorProgram(yStagePrograms, Prog, out Program))
                return true;
            else
                return false;
        }
        public static bool GetFilterWheelProgram(int Prog, out List<int> Program)
        {
            Program = new List<int>(0);
            if (FormatMotorProgram(filterWheelPrograms, Prog, out Program))
                return true;
            else
                return false;
        }
        public static bool GetPIDProgram(int Prog, out List<string> Program)
        {
            Program = new List<string>(0);
            if (FormatPIDProgram(pidPrograms, Prog, out Program))
                return true;
            else
                return false;
        }
        //public static bool LoadRotorProgram(int Prog, int ProfileIn, int VelocityIn, out string Err)
        //{
        //    return LoadMotorProgram(Rotor, RotorSequencer, RotorPrograms,
        //        Prog, ProfileIn, VelocityIn, out Err);
        //}
        //public static bool LoadXStageProgram(int Prog, int ProfileIn, int VelocityIn, out string Err)
        //{
        //    return LoadMotorProgram(XStage, XStageSequencer, XStagePrograms,
        //        Prog, ProfileIn, VelocityIn, out Err);
        //}
        //public static bool LoadYStageProgram(int Prog, int ProfileIn, int VelocityIn, out string Err)
        //{
        //    return LoadMotorProgram(YStage, YStageSequencer, YStagePrograms,
        //        Prog, ProfileIn, VelocityIn, out Err);
        //}
        //public static bool LoadFilterWheelProgram(int Prog, int ProfileIn, int VelocityIn, out string Err)
        //{
        //    return LoadMotorProgram(FilterWheel, FilterWheelSequencer, FilterWheelPrograms,
        //        Prog, ProfileIn, VelocityIn, out Err);
        //}

        public static void PrintMotorProgram(StepperMotor Motor)
        {
            int Prog = 0;
            bool res = true;
            switch (Motor.MotorNumber)
            {
                case 1:
                    if (FormatMotorProgram(rotorPrograms, Prog, out List<int> positions))
                    {
                        Console.WriteLine("Rotor Progarm 0");
                        for (int n = 0; n < positions.Count; n++)
                            Console.WriteLine(String.Format("{0} : {1}", n, positions[n]));
                    }
                    else
                        res = false;
                    break;
                case 2:
                    if (FormatMotorProgram(xStagePrograms, Prog, out positions))
                    {
                        Console.WriteLine("XStage Progarm 0");
                        for (int n = 0; n < positions.Count; n++)
                            Console.WriteLine(String.Format("{0} : {1}", n, positions[n]));
                    }
                    else
                        res = false;
                    break;
                case 3:
                    if (FormatMotorProgram(yStagePrograms, Prog, out positions))
                    {
                        Console.WriteLine("YStage Progarm 0");
                        for (int n = 0; n < positions.Count; n++)
                            Console.WriteLine(String.Format("{0} : {1}", n, positions[n]));
                    }
                    else
                        res = false;
                    break;
                case 4:
                    if (FormatMotorProgram(filterWheelPrograms, Prog, out positions))
                    {
                        Console.WriteLine("FilterWheel Progarm 0");
                        for (int n = 0; n < positions.Count; n++)
                            Console.WriteLine(String.Format("{0} : {1}", n, positions[n]));
                    }
                    else
                        res = false;
                    break;
                default:
                    res = false;
                    break;
            }
            if (res)
                Console.WriteLine("Program Printed");
            else
                Console.WriteLine("Program not Printed");
        }
        public static bool AddMotorProgram(StepperMotor Motor, string ProgramString)
        {
            if (Motor == null || ProgramString.Length < 40)
                return false;
            bool res = true;
            if (StringToXDoc(ProgramString, out XDocument xDoc))
            {
                if (GetXElementProgram(xDoc, out XElement xProg))
                {
                    var elList = new List<XElement>(1) { xProg };
                    if (FormatMotorProgram(elList, 0, out List<int> _))
                    {
                        switch (Motor.MotorNumber)
                        {
                            case 1:
                                rotorPrograms.Add(xProg);
                                break;
                            case 2:
                                xStagePrograms.Add(xProg);
                                break;
                            case 3:
                                yStagePrograms.Add(xProg);
                                break;
                            case 4:
                                filterWheelPrograms.Add(xProg);
                                break;
                            default:
                                res = false;
                                break;
                        }
                    }
                    else
                        res = false;
                }
                else
                    res = false;
            }
            else
                res = false;

            return res;
        }

        static bool FormatMotorProgram(List<XElement> Programs, int Prog, out List<int> Positions)
        {
            Positions = new List<int>(0);
            if (Prog < 0 || Prog >= Programs.Count)
                return false;
            XElement program = Programs[Prog];
            IEnumerable<XElement> items =
                    from el in program.Descendants("Step")
                    select el;
            List<XElement> pos = items.ToList();
            Console.WriteLine(String.Format("pos.Count = {0}", pos.Count));
            if (pos.Count < 2)
                return false;
            var posList = new List<KeyValuePair<int, int>>(0);
            bool res = true;
            for (int n = 0; n < pos.Count; n++)
            {
                int w1; int w2;
                string v1 = pos[n].Attribute("Number").Value;
                string v2 = pos[n].Attribute("Position").Value;
                Console.WriteLine(String.Format("{0} : {1} / {2}", n, v1, v2));
                if (Int32.TryParse(v1, out w1) && Int32.TryParse(v2, out w2))
                {
                    if (w2 < 0)
                    {
                        res = false;
                        break;
                    }
                    else
                        posList.Add(new KeyValuePair<int, int>(w1, w2));

                }
                else
                {
                    res = false;
                    break;
                }
            }
            if (res)
            {
                posList = posList.OrderBy(x => x.Key).ToList();
                for (int n = 0; n < posList.Count; n++)
                    Positions.Add(posList[n].Value);
            }
            return res;
        }
        public static bool AddPIDProgram(string ProgramString)
        {
            if (ProgramString.Length < 40)
                return false;
            if (StringToXDoc(ProgramString, out XDocument xDoc))
            {
                if (GetXElementProgram(xDoc, out XElement xProg))
                {
                    var elList = new List<XElement>(1) { xProg };
                    if (FormatPIDProgram(elList, 0, out List<string> _))
                    {
                        pidPrograms.Add(xProg);
                        return true;
                    }
                    else
                        return false;
                }
                else
                    return false;
            }
            return false;
        }

        static bool FormatPIDProgram(List<XElement> Programs, int Prog, out List<string> TemperatureSteps)
        {
            TemperatureSteps = new List<string>(0);
            if (Prog < 0 || Prog >= Programs.Count)
                return false;
            XElement program = Programs[Prog];
            IEnumerable<XElement> items =
                    from el in program.Descendants("Step")
                    select el;
            List<XElement> steps = items.ToList();

            if (steps.Count < 2)
                return false;
            var stepList = new List<KeyValuePair<int, string>>(0);
            bool res = true;
            for (int n = 0; n < steps.Count; n++)
            {
                string v1 = steps[n].Attribute("Number").Value;
                string v2 = steps[n].Attribute("Temperature").Value;
                string v3 = steps[n].Attribute("Time").Value;
                string v4 = steps[n].Attribute("Description").Value;

                int w1;
                double x2;
                double x3;
                if (Int32.TryParse(v1, out w1) && Double.TryParse(v2, out x2) && Double.TryParse(v3, out x3))
                {
                    if (w1 >= 0 && w1 < steps.Count && x2 > 20.0 && x2 < 130.0 && x3 > 0.0)
                    {
                        stepList.Add(new KeyValuePair<int, string>(w1, String.Format("{0},{1},{2}", v2, v3, v4)));
                    }
                    else
                    {
                        res = false;
                        break;
                    }
                }
                else
                {
                    res = false;
                    break;
                }
            }
            stepList = stepList.OrderBy(x => x.Key).ToList();
            for (int n = 0; n < stepList.Count; n++)
            {
                TemperatureSteps.Add(stepList[n].Value);
            }
            return res;
        }

        static bool GetMotorParams(string MotorName, XDocument Config,
            out int MotorN, out int DirectionCode, out int MaxProfile, out int MaxVelocity,
            out int Backout, out int MaxPosition, out int InitProfile, out int InitVelocity,
            out List<XElement> Programs)
        {
            MotorN = 0;
            DirectionCode = 0;
            MaxProfile = 0;
            MaxVelocity = 100;
            Backout = 100;
            MaxPosition = 1000;
            InitProfile = 0;
            InitVelocity = 10;
            XElement motorXE = new XElement("Blank");
            Programs = new List<XElement>();
            IEnumerable<XElement> items =
                    from el in Config.Descendants("StepperMotor")
                    select el;
            List<XElement> xst = items.ToList();
            bool foundMotor = false;
            for (int n = 0; n < xst.Count; n++)
            {
                XAttribute xat = xst[n].Attribute("Name");
                if (xat.Value.IndexOf(MotorName) >= 0 )
                {
                    motorXE = xst[n];
                    foundMotor = true;
                    break;
                }
            }
            bool parse = foundMotor;
            if (parse)
            {
                XAttribute xat = motorXE.Attribute("Number");
                parse = Int32.TryParse(xat.Value, out MotorN);
                xat = motorXE.Attribute("DirectionCode"); ;
                parse = parse && Int32.TryParse(xat.Value, out DirectionCode);
                xat = motorXE.Attribute("MaximumProfile");
                parse = parse && Int32.TryParse(xat.Value, out MaxProfile);
                xat = motorXE.Attribute("MaximumVelocity");
                parse = parse && Int32.TryParse(xat.Value, out MaxVelocity);
                xat = motorXE.Attribute("BackoutFromHome");
                parse = parse && Int32.TryParse(xat.Value, out Backout);
                xat = motorXE.Attribute("MaximumPosition");
                parse = parse && Int32.TryParse(xat.Value, out MaxPosition);
                xat = motorXE.Attribute("Profile");
                parse = parse && Int32.TryParse(xat.Value, out InitProfile);
                xat = motorXE.Attribute("Velocity");
                parse = parse && Int32.TryParse(xat.Value, out InitVelocity);
            }
            if (parse)
            {
                items =
                    from el in motorXE.Descendants("Program")
                    select el;
                if (items.Count() > 0)
                    Programs = items.ToList();
            }
            return parse;
        }

        static bool GetPIDParams(XDocument Config, out double IdleTemperature, out double LowTemperature,
            out double HighTemperature, out List<XElement> PIDPrograms)
        {
            bool xmlOK = true;
            PIDPrograms = new List<XElement>();
            IEnumerable<XElement> items =
                    from el in Config.Descendants("PID")
                    select el;
            XElement pidXE = items.First();
            xmlOK = Double.TryParse(pidXE.Attribute("IdleTemperature").Value, out IdleTemperature);
            xmlOK = Double.TryParse(pidXE.Attribute("LowTemperature").Value, out LowTemperature) && xmlOK;
            xmlOK = Double.TryParse(pidXE.Attribute("HighTemperature").Value, out HighTemperature) && xmlOK;

            if (xmlOK)
            {
                items = from el in pidXE.Descendants("Program")
                        select el;
                if (items.Count() > 0)
                    PIDPrograms = items.ToList();
            }
            return xmlOK;
        }
        static bool GetLEDParams(XDocument Config, out int NumbLEDs, out int[] LEDMax,
                out int[] LEDLevels, out string[] LEDName)
        {
            bool xmlOK = true;
            IEnumerable<XElement> items =
                    from el in Config.Descendants("LEDs")
                    select el;
            XElement xled = items.First();
            items =
                from el in xled.Descendants("LED")
                select el;
            List<XElement> xl = items.ToList();
            NumbLEDs = xl.Count;
            LEDMax = new int[NumbLEDs];
            LEDLevels = new int[NumbLEDs];
            LEDName = new string[NumbLEDs];
            for (int n = 0; n < NumbLEDs; n++)
                LEDMax[n] = -1;
            for (int n = 0; n < NumbLEDs; n++)
            {
                int index;
                int max;
                int inten;
                string name;
                xmlOK = Int32.TryParse(xl[n].Attribute("Number").Value, out index);
                xmlOK = Int32.TryParse(xl[n].Attribute("MaxIntensity").Value, out max) && xmlOK;
                xmlOK = Int32.TryParse(xl[n].Attribute("Intensity").Value, out inten) && xmlOK;
                name = xl[n].Attribute("Name").Value;
                if (xmlOK && index >= 0 && index < NumbLEDs && max > 0 && max < 101 && inten >= 0 && inten <= max)
                {
                    LEDMax[index] = max;
                    LEDLevels[index] = inten;
                    LEDName[index] = name.Trim(whiteSpace);
                }
                else
                {
                    xmlOK = false;
                    break;
                }
            }
            for (int n = 0; n < NumbLEDs; n++)
            {
                if (LEDMax[n] <= 0)
                {
                    xmlOK = false;
                    break;
                }
            }
            return xmlOK;
        }

        static bool GetFilterParams(XDocument Config, out int NumbFilters, out string[] FilterName)
        {
            bool xmlOK = true;
            IEnumerable<XElement> items =
                    from el in Config.Descendants("Filters")
                    select el;
            XElement xled = items.First();
            items =
                from el in xled.Descendants("Filter")
                select el;
            List<XElement> xl = items.ToList();
            NumbFilters = xl.Count;
            FilterName = new string[NumbFilters];
            for (int n = 0; n < NumbFilters; n++)
                FilterName[n] = "-";
            for (int n = 0; n < NumbFilters; n++)
            {
                int index;
                string name;
                xmlOK = Int32.TryParse(xl[n].Attribute("FilterPosition").Value, out index);
                name = xl[n].Attribute("FilterName").Value;
                if (xmlOK && index >= 0 && index < NumbFilters && name.Length > 2)
                {
                    FilterName[index] = name.Trim(whiteSpace);
                }
                else
                {
                    xmlOK = false;
                    break;
                }
            }
            for (int n = 0; n < NumbFilters; n++)
            {
                if (FilterName[n].Length < 3)
                {
                    xmlOK = false;
                    break;
                }
            }
            return xmlOK;
        }

        static int GetDyeParams(XDocument Config, string[] LEDName, string[] FilterName, out int NumbDyes, out DyeInformation[] DyeInfo)
        {
            int numbDyesConfigured = 0;
            IEnumerable<XElement> items =
                    from el in Config.Descendants("Dyes")
                    select el;
            XElement xled = items.First();
            items =
                from el in xled.Descendants("Dye")
                select el;
            List<XElement> xl = items.ToList();
            NumbDyes = xl.Count;
            DyeInfo = new DyeInformation[NumbDyes];
           
            for (int n = 0; n < NumbDyes; n++)
            {
               bool xmlOK = true;
                int gain;
                int exposure;
                xmlOK = Int32.TryParse(xl[n].Attribute("Gain").Value, out gain);
                xmlOK = Int32.TryParse(xl[n].Attribute("ExposureTime").Value, out exposure) && xmlOK;
                string dName = xl[n].Attribute("Name").Value.Trim(whiteSpace);
                string fName = xl[n].Attribute("FilterName").Value;
                string lName = xl[n].Attribute("LEDName").Value;
                if (xmlOK && dName.Length > 2 && filterName.Length > 2 && ledName.Length > 2)
                {
                    int fNumb = -1;
                    int lNumb = -1;
                    for ( int m = 0; m < LEDName.Length; m++)
                    {
                        if (lName.IndexOf(LEDName[m]) == 0)
                            lNumb = m;
                    }
                    for (int m = 0; m < FilterName.Length; m++)
                    {
                        if (fName.IndexOf(FilterName[m]) == 0)
                            fNumb = m;
                    }
                    DyeInfo[n].Name = dName;
                    DyeInfo[n].FilterPosition = fNumb;
                    DyeInfo[n].LEDPosition = lNumb;
                    DyeInfo[n].FilterName = fName;
                    DyeInfo[n].LEDName = lName;
                    DyeInfo[n].Gain = gain;
                    DyeInfo[n].Exposure = exposure;

                    if (fNumb >= 0 && lNumb >= 0)
                         numbDyesConfigured++;
                }
            }
            return numbDyesConfigured;
        }
    }
}

