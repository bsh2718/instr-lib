﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Timers;
using System.IO;
using System.IO.Ports;
using sdpcrInstrLib;
using sdpcrInstrLib.InstrInterface;

namespace testLEDs
{
    class TestLEDs
    {
        public static Stopwatch stopW = new Stopwatch();

        //static int numbLEDs = 6;
        static int[] LEDLevels = { 50, 50, 50, 50, 50, 50 }; // percent

        //readonly static char[] delimiterChars = { ' ', ',', '\t' }; // space, comma, and tab
        //static int boardWaitTime;
        static void Main(string[] args)
        {
            Console.Write("Enter Previous Port Name: ");
            string defPortName = Console.ReadLine().ToUpperInvariant();
            if (!Instrument.Initialize(defPortName)) //, true)) //, true, true, true))
            {
                Console.WriteLine("Instrument did not initialize");
                Console.WriteLine("One of the components of the board did not connect properly");
                string[] iResults = Instrument.InitializationResult;
                for (int n = 0; n < iResults.Length; n++)
                    Console.WriteLine(iResults[n]);
                iResults = Instrument.InstrumentLog;
                Console.WriteLine("==========");
                for (int n = 0; n < iResults.Length; n++)
                    Console.WriteLine(iResults[n]);
                Console.WriteLine("Press enter to exit");
                Console.ReadLine();
                System.Environment.Exit(0);
            }
            else
            {
                string[] portInfo = Instrument.PortInfo;
                for (int n = 0; n < portInfo.Length; n++)
                    Console.WriteLine(portInfo[n]);

                Console.WriteLine(String.Format("\nFirmware: {0}", Instrument.SDPCRProcessor.FirmwareVersion));
                Console.Out.Flush();
                Console.WriteLine(">>>>>> Press enter to start test with Inten = 50");
                Console.ReadLine();
                for (int n = 0; n < 6; n++)
                {
                    Console.WriteLine(String.Format("Press enter to turn LED {0} on with Intensity {1}", n, LEDLevels[n]));
                    Console.ReadLine();
                    Instrument.LED_Ctl.On(n);
                    Console.WriteLine(String.Format("LED {0} should be on, all other LEDs should be off", n));
                    Console.WriteLine("Press enter to change LED intensity to 1");
                    Console.ReadLine();
                    //ledController.Off(n);
                    Instrument.LED_Ctl.SetIntensity(n, 1);
                    Instrument.LED_Ctl.On(n);
                    Console.WriteLine(String.Format("LED {0} should be on at low intensity, all other LEDs should be off\n", n));
                }
                Console.WriteLine("Press enter to proceed to next tests");
                Console.ReadLine();
                Instrument.LED_Ctl.Off(5);
                Console.WriteLine("Press enter to turn LED 0 on with previous set intensity (1)");
                Console.ReadLine();
                Instrument.LED_Ctl.On(0);
                Instrument.LED_Ctl.SetIntensity(1, 50);
                Instrument.LED_Ctl.On(0);
                Console.WriteLine("LED 0 should be on. Set LED 1 intensity to 50 (LED 1 should still be off");
                Console.WriteLine("Press enter to turn LED 0 off and LED 1 on");
                Console.ReadLine();
                Instrument.LED_Ctl.On(1);
                Console.WriteLine("LED 0 should be off. Set LED 1 should be on at 50");
                Console.WriteLine("Press enter to turn LED 1 off and LED 2 on");
                Console.ReadLine();
                Instrument.LED_Ctl.On(2);
                Console.WriteLine("LED 1 should be off. LED 2 should be on at 1");
                Console.WriteLine("Press enter to change LED 2 intensity to 50");
                Console.ReadLine();
                Instrument.LED_Ctl.SetIntensity(2, 50);
                Instrument.LED_Ctl.On(2);
                Console.WriteLine("LED 2 should be on at 50. Press enter to change LED 2 intensity to 5");
                Console.ReadLine();
                //ledController.Off(2);
                Instrument.LED_Ctl.SetIntensity(2, 5);
                Instrument.LED_Ctl.On(2);
                Console.WriteLine("LED 2 should be on at 5. Press enter to change LED 2 intensity to 1");
                Console.ReadLine();
                //ledController.Off(2);
                Instrument.LED_Ctl.SetIntensity(2, 1);
                Instrument.LED_Ctl.On(2);
                Console.WriteLine("LED 2 should be on at 1. Press enter to change LED 2 intensity to 50");
                Console.ReadLine();
                //ledController.Off(2);
                Instrument.LED_Ctl.SetIntensity(2, 50);
                Instrument.LED_Ctl.On(2);
                Console.WriteLine("LED 2 should be on at 50. Press enter to change LED 2 intensity to 1");
                Console.ReadLine();
                //ledController.Off(2);
                Instrument.LED_Ctl.SetIntensity(2, 1);
                Instrument.LED_Ctl.On(2);
                Console.WriteLine("LED 2 should be on at 1. Press enter to turn LED 3 on");
                Console.ReadLine();
                Instrument.LED_Ctl.On(3);
                Console.WriteLine("LED 3 should be on at 1. Press enter to turn LED 3 off");
                Console.ReadLine();
                Instrument.LED_Ctl.Off(3);
                Console.WriteLine("All LEDs should be off. Press enter to turn LED 4 on");
                Console.ReadLine();
                Instrument.LED_Ctl.On(4);
                Console.WriteLine("LED 4 should be on at 1. Press enter to turn to blink LED 4 (1 to 50, 3 second intervals 4 times)");
                Console.ReadLine();
                for (int n = 0; n < 4; n++)
                {
                    //ledController.Off(4);
                    Instrument.LED_Ctl.SetIntensity(4, 50);
                    Instrument.LED_Ctl.On(4);
                    System.Threading.Thread.Sleep(3000);
                    //ledController.Off(4);
                    Instrument.LED_Ctl.SetIntensity(4, 1);
                    Instrument.LED_Ctl.On(4);
                    System.Threading.Thread.Sleep(3000);
                }
                Console.WriteLine("LED 4 should have stopped blinking and be on at 1. Press enter to turn to LED 5 on");
                Console.ReadLine();
                Instrument.LED_Ctl.On(5);
                Console.WriteLine("LED 5 should be on at 1. Press enter to change LED 5 intensity to 50");
                Console.ReadLine();
                Instrument.LED_Ctl.SetIntensity(5, 50);
                Instrument.LED_Ctl.On(5);
                Console.WriteLine("LED 5 should be on at 50. Press enter to change LED 5 intensity to 1");
                Console.ReadLine();
                Instrument.LED_Ctl.SetIntensity(5, 1);
                Instrument.LED_Ctl.On(5);
                Console.WriteLine("LED 5 should be on at 1. Press enter to turn LED 5 off");
                Console.ReadLine();
                Instrument.LED_Ctl.Off(5);
                Console.WriteLine("All LEDs should be off");
                Console.WriteLine(">>>>>> Press enter to conclude LED tests");
                Console.ReadLine();

            }
            if (Instrument.SDPCRProcessor != null)
            {
                Console.WriteLine("Calling sdPCRProcessor.Dispose()");
                Instrument.SDPCRProcessor.Dispose();
                Console.WriteLine("Press any key ");
                Console.ReadLine();
            }
        }
    }
}
