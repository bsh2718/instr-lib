﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

using sdpcrInstrLib;
using sdpcrInstrLib.InstrInterface;
using sdpcrInstrLib.Stepper;
using sdpcrInstrLib.Sequencers;

namespace testRotor
{
    class TestRotor
    {
        public static void Main()
        {
            Console.Write("Enter Previous Port Name: ");
            string defPortName = Console.ReadLine().ToUpperInvariant();
            if (!Instrument.Initialize(defPortName))
            {
                Console.WriteLine("Instrument did not initialize");
                Console.WriteLine("One of the components of the board did not connect properly");
                string[] iResults = Instrument.InitializationResult;
                for (int n = 0; n < iResults.Length; n++)
                    Console.WriteLine(iResults[n]);
                iResults = Instrument.InstrumentLog;
                Console.WriteLine("==========");
                for (int n = 0; n < iResults.Length; n++)
                    Console.WriteLine(iResults[n]);
                Console.WriteLine("Press enter to exit");
                Console.ReadLine();
                System.Environment.Exit(0);
            }
            else
            {
                //string err;
                //List<string> progOut;
                string[] portInfo = Instrument.PortInfo;
                for (int n = 0; n < portInfo.Length; n++)
                    Console.WriteLine(portInfo[n]);

                Console.WriteLine(String.Format("\nFirmware: {0}", Instrument.SDPCRProcessor.FirmwareVersion));
                //string any;

                int initRVelocity = 60;
                int initTVelocity = 45;
                int initFVelocity = 30;
                int initProfile = 0;

                Instrument.Rotor.SetProfile(initProfile);
                Instrument.XStage.SetProfile(initProfile);
                Instrument.YStage.SetProfile(initProfile);
                Instrument.FilterWheel.SetProfile(initProfile);
                Instrument.Rotor.SetVelocity(initRVelocity);
                Instrument.XStage.SetVelocity(initTVelocity);
                Instrument.YStage.SetVelocity(initTVelocity);
                Instrument.FilterWheel.SetVelocity(initFVelocity);

                Console.WriteLine("Press Enter to start running all four motors");
                Console.ReadLine();

                Instrument.Rotor.VelocityModePositive();
                Instrument.FilterWheel.VelocityModePositive();

                for (int n = 0; n < 10; n++)
                {
                    Console.WriteLine(String.Format("Pass {0}", n));
                    Instrument.XStage.AbsolutePosition(21000);
                    Instrument.YStage.AbsolutePosition(21000);
                    System.Threading.Thread.Sleep(10000);
                    Instrument.XStage.AbsolutePosition(1000);
                    Instrument.YStage.AbsolutePosition(1000);
                    if (n < 9)
                        System.Threading.Thread.Sleep(10000);
                }
                Console.WriteLine("Test finished, press enter to exit");
            }

            if (Instrument.SDPCRProcessor != null)
            {
                Console.WriteLine("Calling sdPCRProcessor.Dispose()");
                Instrument.SDPCRProcessor.Dispose();
                Console.WriteLine("Press any key ");
                Console.ReadLine();
            }
        }
    }
}
