﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

using sdpcrInstrLib;
using sdpcrInstrLib.InstrInterface;

namespace testyStage
{
    class TestYStage
    {
        public static void Main(string[] Args)
        {
            int iV = -1;
            if (Args.Length > 0)
            {
                if (Int32.TryParse(Args[0], out iV))
                {
                    if (iV < 1 || iV > 1200)
                        iV = -1;
                }
            }
            Console.Write("Enter Previous Port Name: ");
            string defPortName = Console.ReadLine().ToUpperInvariant();
            if (!Instrument.Initialize(defPortName))
            {
                Console.WriteLine("Instrument did not initialize");
                Console.WriteLine("One of the components of the board did not connect properly");
                string[] iResults = Instrument.InitializationResult;
                for (int n = 0; n < iResults.Length; n++)
                    Console.WriteLine(iResults[n]);
                iResults = Instrument.InstrumentLog;
                Console.WriteLine("==========");
                for (int n = 0; n < iResults.Length; n++)
                    Console.WriteLine(iResults[n]);
                Console.WriteLine("Press enter to exit");
                Console.ReadLine();
                System.Environment.Exit(0);
            }
            else
            {
                string err;
                List<string> progOut;
                string[] portInfo = Instrument.PortInfo;
                for (int n = 0; n < portInfo.Length; n++)
                    Console.WriteLine(portInfo[n]);

                Console.WriteLine(String.Format("\nFirmware: {0}", Instrument.SDPCRProcessor.FirmwareVersion));
                //string any;
                Console.WriteLine("Press Enter to continue");
                Console.ReadLine();
                int initVelocity = 500;
                if (iV > 0 && iV <= 1200)
                    initVelocity = iV;
                int initProfile = 1;
                int timeBetween = 2000;
                if (initVelocity < 50)
                    timeBetween = 10000;
                else if (initVelocity < 100)
                    timeBetween = 9000;
                else if (initVelocity < 200)
                    timeBetween = 8000;
                else if (initVelocity < 300)
                    timeBetween = 7000;
                else if (initVelocity < 400)
                    timeBetween = 6000;
                else if (initVelocity < 500)
                    timeBetween = 5000;
                else if (initVelocity < 600)
                    timeBetween = 4000;
                else if (initVelocity < 700)
                    timeBetween = 3000;
                int timeBetweenSec = timeBetween / 1000;
                Instrument.YStage.AbsolutePosition(2000);
                Console.WriteLine("YStage should be at position 2000");
                Console.WriteLine("Press enter to load YStage program");
                Console.ReadLine();
                List<int> positions = new List<int>(0)
                {
                    2000,
                    11600,
                    21200
                };
                ResponseItem resp;
                if (Instrument.YStageSequencer.LoadPositionProgram(positions, initProfile, initVelocity, out err))
                {
                    Instrument.YStageSequencer.GetPositionProgram(out progOut);
                    Console.WriteLine("Program Loaded");
                    for (int n = 0; n < progOut.Count; n++)
                        Console.WriteLine(progOut[n]);

                    Console.WriteLine("Y Stage should be at position 2000");
                    Console.WriteLine(String.Format("Press enter to start program, 7 cycles of 3 steps, {0} seconds between steps", timeBetweenSec));
                    Console.ReadLine();
                    Instrument.YStageSequencer.StartPositionProgram(timeBetween, 22);

                    int waitTime = 0;
                    while (Instrument.YStageSequencer.ProgramRunning)
                    {
                        waitTime++;
                        if (waitTime % 5 == 0)
                            Console.WriteLine(String.Format("{0} : Program is running", waitTime));
                        while (Instrument.YStageSequencer.CheckErrorList(out resp))
                        {
                            Console.WriteLine(resp.ToString());
                        }
                        System.Threading.Thread.Sleep(1000);
                    }
                    Console.WriteLine("Y Stage program should have finished with YStage at position 2000");
                }
                else
                {
                    Console.WriteLine("Problems loading first program");
                    Console.WriteLine(err);
                    Console.WriteLine("----------");
                }
                Console.WriteLine("Press enter to continue");
                Console.ReadLine();
                Instrument.YStage.SetVelocity(200);
                Instrument.YStage.Home();
                System.Threading.Thread.Sleep(5000);
                Instrument.YStage.AbsolutePosition(2000);
                Console.WriteLine("Rotor should be at position 2000");
                Console.WriteLine("Press enter to load next program");
                Console.ReadLine();
                string progString = "<Program Type=\"Position\" Profile=\"1\" Velocity=\"600\" Name=\"YStage-2 5 Positions\" >" +
                    "<Step Number=\"0\" Position=\"2000\"/>" +
                    "<Step Number=\"1\" Position=\"6800\"/>" +
                    "<Step Number=\"2\" Position=\"11600\"/>" +
                    "<Step Number=\"3\" Position=\"16400\"/>" +
                    "<Step Number=\"4\" Position=\"21200\"/>" +
                    "</Program>";
                Console.WriteLine(progString);
                int motorNumb = (int)MotorNumber.YStage;
                if (Instrument.AddPositionProgram(motorNumb, progString))
                {
                    Instrument.GetPositionProgramHeaders(motorNumb, out List<string> programHeaders);
                    Console.WriteLine("YStage Program Headers");
                    for (int n = 0; n < programHeaders.Count; n++)
                        Console.WriteLine(programHeaders[n]);
                    int prog = programHeaders.Count - 1;
                    int pProfile;
                    int pVelocity;
                    Instrument.GetPositionProgram(motorNumb, prog, out pProfile, out pVelocity, out List<int> program);
                    Console.WriteLine(String.Format("Position program {0}", prog));
                    Console.WriteLine(String.Format("Profile: {0} - Velocity: {1}", pProfile, pVelocity));
                    for (int n = 0; n < program.Count; n++)
                        Console.WriteLine(program[n]);

                    Console.WriteLine("Press enter to start program");
                    Console.ReadLine();
                    Instrument.YStage.Home();
                    if (!Instrument.LoadPositionProgram(motorNumb, prog, out err, initProfile, initVelocity))
                    {
                        Console.WriteLine(String.Format("Error loading program {0}", prog));
                        Console.WriteLine(String.Format("     {0}", err));
                    }
                    Instrument.YStageSequencer.StartPositionProgram(timeBetween, 21);
                    int waitTime = 0;
                    while (Instrument.YStageSequencer.ProgramRunning)
                    {
                        waitTime++;
                        if (waitTime % 5 == 0)
                            Console.WriteLine(String.Format("{0} : Program is running", waitTime));
                        while (Instrument.YStageSequencer.CheckErrorList(out resp))
                        {
                            Console.WriteLine(resp.ToString());
                        }
                        System.Threading.Thread.Sleep(1000);
                    }
                    Console.WriteLine("YStage program should have finished with yStage at position 2000");
                    Console.WriteLine("Press enter to proceed");
                    Console.ReadLine();

                    int mod = Instrument.YStageSequencer.Modulous;
                    for (int n = 1; n < mod+1; n++)
                    {
                        int m = n % mod;
                        Console.WriteLine(String.Format("Press enter to advance to position {0} at {1}", m, program[m]));
                        Console.ReadLine();
                        Instrument.YStageSequencer.GoToPosition(m);
                    }

                    Console.WriteLine(">>>>>> Press enter to conclude Y stage tests");
                    Console.ReadLine();
                }
                else
                {
                    Console.WriteLine("Problems loading second program");
                    Console.WriteLine(err);
                    Console.WriteLine("----------");
                }

            }
            if (Instrument.SDPCRProcessor != null)
            {
                Console.WriteLine("Calling sdPCRProcessor.Dispose()");
                Instrument.SDPCRProcessor.Dispose();
                Console.WriteLine("Press any key ");
                Console.ReadLine();
            }
        }
    }
}
