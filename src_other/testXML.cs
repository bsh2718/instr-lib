﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Xml.Linq;
using System.Text;
//using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Timers;
using System.IO;
using System.IO.Ports;
using System.Net;
using System.Xml.Schema;
using System.Linq.Expressions;

using sdpcrInstrLib;
using sdpcrInstrLib.InstrInterface;
using sdpcrInstrLib.CreateDefaultFile;
using System.Security.Policy;

namespace TestInterface
{
    public static class TestXML
    {
        //static bool XMLfileLoaded = false;
        static XDocument _configuration;
        // Predefined character sets
        readonly static char[] whiteSpace = { ' ', '\n', '\r', '\t' };  // space, new line, carriage return, tab || comma
        readonly static char[] whiteSpaceQuote = { ' ', '\n', '\r', '\t', '"', '\'' };  // space, new line, carriage return, tab, ", '
        readonly static char[] whiteSpaceQuoteBack = { ' ', '\n', '\r', '\t', '"', '\'', '\\' };  // space, new line, carriage return, tab, ", '
        readonly static char[] delimiterChars = { ' ', ',', '\t' }; // space, comma, and tab
        readonly static char[] comma = { ',' };


        static public int numbLEDs = 6;
        static public int[] ledLevels = { 50, 50, 50, 50, 50, 50 }; // percent
        static public int[] ledMax = { 50, 50, 50, 50, 50, 50 }; // percent
        static public string[] ledName = { "L350/30", "L400/30", "L500/30", "L550/30", "L600/30", "L650/30" }; // LED names

        static public int numbFilters;
        static public string[] filterName;

        static public int numbDyes;
        static public DyeInformation[] dyeInfo;

        static public List<XElement> pidPrograms;
        static public List<XElement> rotorPositionPrograms;
        static public List<XElement> xStagePositionPrograms;
        static public List<XElement> yStagePositionPrograms;
        static public List<XElement> filterWheelPositionPrograms;
        static public List<XElement> rotorVelocityPrograms;
        static public List<XElement> miscVelocityPrograms;
        static public List<string> programHeaders;


        public static bool StringToXDoc(string XDocString, out XDocument XDoc)
        {
            try { XDoc = XDocument.Parse(XDocString); }
            catch (System.Xml.XmlException)
            {
                XDoc = new XDocument();
                return false;
            }
            return true;
        }
        public static bool GetXElementProgram(XDocument XDoc, out XElement XProgram)
        {
            IEnumerable<XElement> items =
                    from el in XDoc.Descendants("Program")
                    select el;
            if (items.Count() > 0)
            {
                XProgram = items.First();
                return true;
            }
            else
            {
                XProgram = new XElement("None");
                return false;
            }
        }
        public static bool LoadConfiguration(string ConfigString)
        {
            if (StringToXDoc(ConfigString, out _configuration))
                return true;
            else
            {
                Instrument.UseDefaultConfiguration();
                return false;
            }
        }
        public static void Main(string[] args)
        {
            if (args.Length > 0)
            {
                Instrument.UseDefaultConfiguration("c:\\instrument", args[0]);
            }
            else
            {
                Instrument.UseDefaultConfiguration();
            }
            Console.Write("Enter Previous Port Name: ");
            string defPortName = Console.ReadLine().ToUpperInvariant();
            if (!Instrument.Initialize(defPortName))
            {
                Console.WriteLine("Instrument did not initialize");
                Console.WriteLine("One of the components of the board did not connect properly");
                string[] iResults = Instrument.InitializationResult;
                for (int n = 0; n < iResults.Length; n++)
                    Console.WriteLine(iResults[n]);
                iResults = Instrument.InstrumentLog;
                Console.WriteLine("==========");
                for (int n = 0; n < iResults.Length; n++)
                    Console.WriteLine(iResults[n]);
                Console.WriteLine("Press enter to exit");
                Console.ReadLine();
                System.Environment.Exit(0);
            }
            if (Instrument.GetLEDParams(out numbLEDs, out ledMax, out ledLevels, out ledName))
            {
                Console.WriteLine("LED Params");
                for (int n = 0; n < numbLEDs; n++)
                    Console.WriteLine(String.Format("{0}: {1} / {2} / {3}", n, ledMax[n], ledLevels[n], ledName[n]));
            }
            else
            {
                Console.WriteLine("     Problem with LED configuration");
            }

            Console.WriteLine("");
            if (Instrument.GetMotorParams("Rotor",
                        out int motorN, out int directionCode, out List<sdpcrInstrLib.Stepper.MotorProfile> motorProfiles,
                        out int backout, out int maxPosition, out int initProfile, out int initVelocity,
                        out rotorPositionPrograms, out rotorVelocityPrograms))
            {
                Console.WriteLine("Rotor Configuration - Number:{0}, Direction:{1}, Backout: {2}, MaxPosition: {3}, InitProfile: {4}, InitVelocity: {5}",
                    motorN, directionCode, backout, maxPosition, initProfile, initVelocity);
                Console.WriteLine("     Motor Profiles");
                for (int n = 0; n < motorProfiles.Count; n++)
                    if (motorProfiles[n].Used)
                        Console.WriteLine(String.Format("{0}: {1}", n, motorProfiles[n].ToString()));
                Instrument.GetPositionProgramHeaders(1, out programHeaders);
                Console.WriteLine("     Position Program Headers");
                for (int n = 0; n < programHeaders.Count; n++)
                    Console.WriteLine(String.Format("{0}: {1}", n, programHeaders[n].ToString()));
                Instrument.GetVelocityProgramHeaders(1, out programHeaders);
                Console.WriteLine("     Velocity Program Headers");
                for (int n = 0; n < programHeaders.Count; n++)
                    Console.WriteLine(String.Format("{0}: {1}", n, programHeaders[n].ToString()));
            }
            else
            {
                Console.WriteLine("     Problem with Rotor configuration");
            }
            Console.WriteLine("");
            if (Instrument.GetMotorParams("XStage",
                out motorN, out directionCode, out motorProfiles,
                out backout, out maxPosition, out initProfile, out initVelocity,
                out xStagePositionPrograms, out miscVelocityPrograms))
            {
                Console.WriteLine("X Stage Configuration - Number:{0}, Direction:{1}, Backout: {2}, MaxPosition: {3}, InitProfile: {4}, InitVelocity: {5}",
                    motorN, directionCode, backout, maxPosition, initProfile, initVelocity);
                Console.WriteLine("     Motor Profiles");
                for (int n = 0; n < motorProfiles.Count; n++)
                    if (motorProfiles[n].Used)
                        Console.WriteLine(String.Format("{0}: {1}", n, motorProfiles[n].ToString()));
                Instrument.GetPositionProgramHeaders(2, out programHeaders);
                Console.WriteLine("     Position Program Headers");
                for (int n = 0; n < programHeaders.Count; n++)
                    Console.WriteLine(String.Format("{0}: {1}", n, programHeaders[n].ToString()));
                Instrument.GetVelocityProgramHeaders(2, out programHeaders);
                if (programHeaders.Count > 0)
                {
                    Console.WriteLine("     Velocity Program Headers");
                    for (int n = 0; n < programHeaders.Count; n++)
                        Console.WriteLine(String.Format("{0}: {1}", n, programHeaders[n].ToString()));
                }
            }
            else
            {
                Console.WriteLine("     Problem with XStage configuration");
            }
            Console.WriteLine("");
            if (Instrument.GetMotorParams("YStage",
                out motorN, out directionCode, out motorProfiles,
                out backout, out maxPosition, out initProfile, out initVelocity,
                out yStagePositionPrograms, out miscVelocityPrograms))
            {
                Console.WriteLine("Y Stage Configuration - Number:{0}, Direction:{1}, Backout: {2}, MaxPosition: {3}, InitProfile: {4}, InitVelocity: {5}",
                    motorN, directionCode, backout, maxPosition, initProfile, initVelocity);
                Console.WriteLine("     Motor Profiles");
                for (int n = 0; n < motorProfiles.Count; n++)
                    if (motorProfiles[n].Used)
                        Console.WriteLine(String.Format("{0}: {1}", n, motorProfiles[n].ToString()));
                Instrument.GetPositionProgramHeaders(3, out programHeaders);
                Console.WriteLine("     Position Program Headers");
                for (int n = 0; n < programHeaders.Count; n++)
                    Console.WriteLine(String.Format("{0}: {1}", n, programHeaders[n].ToString()));
                Instrument.GetVelocityProgramHeaders(3, out programHeaders);
                if (programHeaders.Count > 0)
                {
                    Console.WriteLine("     Velocity Program Headers");
                    for (int n = 0; n < programHeaders.Count; n++)
                        Console.WriteLine(String.Format("{0}: {1}", n, programHeaders[n].ToString()));
                }
            }
            else
            {
                Console.WriteLine("     Problem with YStage configuration");
            }
            Console.WriteLine("");
            if (Instrument.GetMotorParams("FilterWheel",
                out motorN, out directionCode, out motorProfiles,
                out backout, out maxPosition, out initProfile, out initVelocity,
                out filterWheelPositionPrograms, out miscVelocityPrograms))
            {
                Console.WriteLine("Filter Wheel Configuration - Number:{0}, Direction:{1}, Backout: {2}, MaxPosition: {3}, InitProfile: {4}, InitVelocity: {5}",
                    motorN, directionCode, backout, maxPosition, initProfile, initVelocity);
                Console.WriteLine("     Motor Profiles");
                for (int n = 0; n < motorProfiles.Count; n++)
                    if (motorProfiles[n].Used)
                        Console.WriteLine(String.Format("{0}: {1}", n, motorProfiles[n].ToString()));
                Instrument.GetPositionProgramHeaders(4, out programHeaders);
                Console.WriteLine("     Position Program Headers");
                for (int n = 0; n < programHeaders.Count; n++)
                    Console.WriteLine(String.Format("{0}: {1}", n, programHeaders[n].ToString()));
                Instrument.GetVelocityProgramHeaders(4, out programHeaders);
                if (programHeaders.Count > 0)
                {
                    Console.WriteLine("     Velocity Program Headers");
                    for (int n = 0; n < programHeaders.Count; n++)
                        Console.WriteLine(String.Format("{0}: {1}", n, programHeaders[n].ToString()));
                }
            }
            else
            {
                Console.WriteLine("     Problem with Filter Wheel configuration");
            }

            Console.WriteLine("");
            string wgtString;
            int[] weights;
            string biasString;
            double[] biases;
            string err;
            if (Instrument.GetThermistorParameters(out wgtString, out weights,
                out biasString, out biases, out err))
            {
                Console.WriteLine("Thermistor Weights");
                Console.Write(String.Format("     {0}", wgtString));
                Console.WriteLine("Thermistor Biases");
                Console.Write(String.Format("     {0}", biasString));
                Console.WriteLine("");
            }
            else
            {
                Console.WriteLine("Problem with weights in configuration");
                Console.WriteLine(err);
            }

            Console.WriteLine("");

            if (Instrument.GetPIDParams(out double idleTemperature, out double lowTemperature,
                out double highTemperature, out pidPrograms))
            {
                Console.WriteLine("PID parameters");
                Console.WriteLine(String.Format("PID presets, Idle: {0}, Low: {1}, High: {2}",
                    idleTemperature, lowTemperature, highTemperature));
            }
            else
            {
                Console.WriteLine("     Problem with PID configuration");
            }

            Console.WriteLine("");
            if (Instrument.GetFilterParams(out numbFilters, out filterName))
            {
                Console.WriteLine("Filter Parameters");
                for (int n = 0; n < numbFilters; n++)
                    Console.WriteLine(String.Format("{0} : {1}", n, filterName[n]));
            }
            else
            {
                Console.WriteLine("     Problem with PID configuration");
            }

            Console.WriteLine("");
            if (Instrument.GetDyeParams(ledName, filterName, out numbDyes, out dyeInfo) > 0)
            {
                Console.WriteLine("Dye Information");
                for (int n = 0; n < dyeInfo.Length; n++)
                    Console.WriteLine(String.Format("{0}: {1},{2} / {3},{4} / {5},{6}",
                        dyeInfo[n].Name, dyeInfo[n].LEDName, dyeInfo[n].LEDPosition,
                        dyeInfo[n].FilterName, dyeInfo[n].FilterPosition, dyeInfo[n].Gain, dyeInfo[n].Exposure));
            }

            Console.WriteLine("");
            List<int> motorProgram;
            List<string> velocityProgram;
            int progProfile;
            int progVelocity;
            if (Instrument.GetPositionProgram(1, 0, out progProfile, out progVelocity, out motorProgram))
            {
                Console.WriteLine("Rotor Program 0");
                Console.WriteLine(String.Format("     Profile: {0} - Velocity: {1}", progProfile, progVelocity));
                for (int n = 0; n < motorProgram.Count; n++)
                    Console.WriteLine(String.Format("{0} : {1}", n, motorProgram[n]));
            }
            else
                Console.WriteLine("Rotor Program 0 not found!");
            Console.WriteLine("");
            if (Instrument.GetVelocityProgram(1, 0, out progProfile, out velocityProgram))
            {
                Console.WriteLine("Rotor Velocity Program 0");
                Console.WriteLine(String.Format("     Profile: {0}", progProfile));
                for (int n = 0; n < velocityProgram.Count; n++)
                    Console.WriteLine(String.Format("{0} : {1}", n, velocityProgram[n]));
            }
            else
                Console.WriteLine("Rotor Velocity Program 0 not found!");
            Console.WriteLine("");

            if (Instrument.GetVelocityProgram(1, 1, out progProfile, out velocityProgram))
            {
                Console.WriteLine("Rotor Velocity Program 1");
                Console.WriteLine(String.Format("     Profile: {0}", progProfile));
                for (int n = 0; n < velocityProgram.Count; n++)
                    Console.WriteLine(String.Format("{0} : {1}", n, velocityProgram[n]));
            }
            else
                Console.WriteLine("Rotor Velocity Program 1 not found!");
            Console.WriteLine("");

            if (Instrument.GetPositionProgram(2, 0, out progProfile, out progVelocity, out motorProgram))
            {
                Console.WriteLine("XStage Program 0");
                Console.WriteLine(String.Format("     Profile: {0} - Velocity: {1}", progProfile, progVelocity));
                for (int n = 0; n < motorProgram.Count; n++)
                    Console.WriteLine(String.Format("{0} : {1}", n, motorProgram[n]));
            }
            else
                Console.WriteLine("XStage Program 0 not found!");
            Console.WriteLine("");
            if (Instrument.GetPositionProgram(2, 1, out progProfile, out progVelocity, out motorProgram))
            {
                Console.WriteLine("XStage Program 1");
                Console.WriteLine(String.Format("     Profile: {0} - Velocity: {1}", progProfile, progVelocity));
                for (int n = 0; n < motorProgram.Count; n++)
                    Console.WriteLine(String.Format("{0} : {1}", n, motorProgram[n]));
            }
            else
                Console.WriteLine("XStage Program 1 not found!");

            Console.WriteLine("");
            if (Instrument.GetPositionProgram(3, 0, out progProfile, out progVelocity, out motorProgram))
            {
                Console.WriteLine("YStage Program 0");
                Console.WriteLine(String.Format("     Profile: {0} - Velocity: {1}", progProfile, progVelocity));
                for (int n = 0; n < motorProgram.Count; n++)
                    Console.WriteLine(String.Format("{0} : {1}", n, motorProgram[n]));
            }
            else
                Console.WriteLine("YStage Program 0 not found!");
            Console.WriteLine("");
            if (Instrument.GetPositionProgram(3, 1, out progProfile, out progVelocity, out motorProgram))
            {
                Console.WriteLine("YStage Program 1");
                Console.WriteLine(String.Format("     Profile: {0} - Velocity: {1}", progProfile, progVelocity));
                for (int n = 0; n < motorProgram.Count; n++)
                    Console.WriteLine(String.Format("{0} : {1}", n, motorProgram[n]));
            }
            else
                Console.WriteLine("YStage Program 1 not found!");

            Console.WriteLine("");
            if (Instrument.GetPositionProgram(4, 0, out progProfile, out progVelocity, out motorProgram))
            {
                Console.WriteLine("Filter Wheel Program 0");
                Console.WriteLine(String.Format("     Profile: {0} - Velocity: {1}", progProfile, progVelocity));
                for (int n = 0; n < motorProgram.Count; n++)
                    Console.WriteLine(String.Format("{0} : {1}", n, motorProgram[n]));
            }
            else
                Console.WriteLine("Filter Wheel Program 0 not found!");
            Console.WriteLine("");


            List<string> pidProgram;
            if (Instrument.GetPIDProgram(0, out pidProgram))
            {
                Console.WriteLine("PID Program 0");
                for (int n = 0; n < pidProgram.Count; n++)
                    Console.WriteLine(String.Format("{0} : {1}", n, pidProgram[n]));
            }
            else
                Console.WriteLine("PID Program 0 not found!");
            Console.WriteLine("");
            if (Instrument.GetPIDProgram(1, out pidProgram))
            {
                Console.WriteLine("PID Program 1");
                for (int n = 0; n < pidProgram.Count; n++)
                    Console.WriteLine(String.Format("{0} : {1}", n, pidProgram[n]));
            }
            else
                Console.WriteLine("PID Program 1 not found!");
            Console.WriteLine("");
            if (Instrument.GetPIDProgram(2, out pidProgram))
            {
                Console.WriteLine("PID Program 2");
                for (int n = 0; n < pidProgram.Count; n++)
                    Console.WriteLine(String.Format("{0} : {1}", n, pidProgram[n]));
            }
            else
                Console.WriteLine("PID Program 2 not found!");

            string progString = "<Program Name=\"YStage 3 Positions - B\" Profile=\"1\" Velocity=\"500\" Type=\"Position\"> <Step Position=\"400\" Number=\"0\" /> <Step Position=\"1100\" Number=\"1\" />"
                + "<Step Position=\"1800\" Number=\"2\" /> <Step Position=\"2500\" Number=\"3\" /> <Step Position=\"3200\" Number=\"4\" />"
                + "<Step Position=\"3900\" Number=\"5\" /> </Program>";

            if (Instrument.AddPositionProgram(3, progString))
            {
                if (Instrument.GetPositionProgram(3, 2, out progProfile, out progVelocity, out motorProgram))
                {
                    Console.WriteLine("YStage Program 2");
                    Console.WriteLine(String.Format("     Profile: {0} - Velocity: {1}", progProfile, progVelocity));
                    for (int n = 0; n < motorProgram.Count; n++)
                        Console.WriteLine(String.Format("{0} : {1}", n, motorProgram[n]));
                }
                else
                    Console.WriteLine("YStage Program 2 not found!");
            }
            else
                Console.WriteLine("AddMotorProgram (YStage) failed");
            Console.WriteLine("");

            progString = "<Program Name=\"Rotor 6 Velocities\" Profile=\"3\" Velocity=\"500\" Type=\"Velocity\">"
                + "<Step Number=\"0\" Velocity=\"600\" Time=\"60.0\" Description=\"Init Spin\"/>"
                + "<Step Number=\"1\" Velocity=\"800\" Time=\"60.0\" Description=\"Med Spin\"/>"
                + "<Step Number=\"2\" Velocity=\"900\" Time=\"60.0\" Description=\"Cautious tSpin\"/>"
                + "<Step Number=\"3\" Velocity=\"1000\" Time=\"60.0\" Description=\"High(?) Spin\"/>"
                + "<Step Number=\"4\" Velocity=\"800\" Time=\"60.0\" Description=\"Slowing Down\"/>"
                + "<Step Number=\"5\" Velocity=\"400\" Time=\"60.0\" Description=\"More Slowing\"/>"
                + "</Program>";

            if (Instrument.AddVelocityProgram(1, progString))
            {
                if (Instrument.GetVelocityProgram(1, 1, out progProfile, out pidProgram))
                {
                    Console.WriteLine("Rotor Velocity Program 1");
                    Console.WriteLine(String.Format("     Profile: {0}", progProfile));
                    for (int n = 0; n < motorProgram.Count; n++)
                        Console.WriteLine(String.Format("{0} : {1}", n, pidProgram[n]));
                }
                else
                    Console.WriteLine("Rotor Velocity Program 1 not found!");
            }
            else
                Console.WriteLine("AddVelocityProgram (Rotor) failed");
            Console.WriteLine("");

            progString = "<Program Name=\"PID 3\" Type=\"Temperature\">"
                + "<Step Description=\"Setup\" Time=\"60.00\" Temperature=\"60.00\" Number=\"0\" />"
                + "<Step Description=\"PreSoak\" Time=\"300.00\" Temperature=\"95.00\" Number=\"1\" />"
                + "<Step Description=\"1.1\" Time=\"25.00\" Temperature=\"50.00\" Number=\"2\" />"
                + "<Step Description=\"1.2\" Time=\"35.00\" Temperature=\"60.00\" Number=\"3\" />"
                + "<Step Description=\"1.3\" Time=\"25.00\" Temperature=\"105.00\" Number=\"4\" />"
                + "<Step Description=\"1.4\" Time=\"35.00\" Temperature=\"95.00\" Number=\"5\" />"
                + "<Step Description=\"2.1\" Time=\"25.00\" Temperature=\"50.00\" Number=\"6\" />"
                + "<Step Description=\"2.2\" Time=\"35.00\" Temperature=\"60.00\" Number=\"7\" />"
                + "<Step Description=\"2.3\" Time=\"25.00\" Temperature=\"105.00\" Number=\"8\" />"
                + "<Step Description=\"2.4\" Time=\"35.00\" Temperature=\"95.00\" Number=\"9\" />"
                + "<Step Description=\"3.1\" Time=\"25.00\" Temperature=\"50.00\" Number=\"10\" />"
                + "<Step Description=\"3.2\" Time=\"35.00\" Temperature=\"60.00\" Number=\"11\" />"
                + "<Step Description=\"3.3\" Time=\"25.00\" Temperature=\"105.00\" Number=\"12\" />"
                + "<Step Description=\"3.4\" Time=\"35.00\" Temperature=\"95.00\" Number=\"13\" />"
                + "<Step Description=\"4.1\" Time=\"25.00\" Temperature=\"50.00\" Number=\"14\" />"
                + "<Step Description=\"4.2\" Time=\"35.00\" Temperature=\"60.00\" Number=\"15\" />"
                + "<Step Description=\"4.3\" Time=\"25.00\" Temperature=\"105.00\" Number=\"16\" />"
                + "<Step Description=\"4.4\" Time=\"35.00\" Temperature=\"95.00\" Number=\"17\" />"
                + "<Step Description=\"5.1\" Time=\"25.00\" Temperature=\"50.00\" Number=\"18\" />"
                + "<Step Description=\"5.2\" Time=\"35.00\" Temperature=\"60.00\" Number=\"19\" />"
                + "<Step Description=\"5.3\" Time=\"60.00\" Temperature=\"75.00\" Number=\"20\" />"
                + "<Step Description=\"Cool Down\" Time=\"200.00\" Temperature=\"40.00\" Number=\"21\" />"
                + "</Program>";

            if (Instrument.AddPIDProgram(progString))
            {
                if (Instrument.GetPIDProgram(2, out pidProgram))
                {
                    Console.WriteLine("PID Program 2");
                    for (int n = 0; n < pidProgram.Count; n++)
                        Console.WriteLine(String.Format("{0} : {1}", n, pidProgram[n]));
                }
                else
                    Console.WriteLine("PID Program 2 not found!");
            }
            else
                Console.WriteLine("AddPIDProgram failed");
            Console.WriteLine("");

            if (Instrument.SDPCRProcessor != null)
            {
                Console.WriteLine("Calling sdPCRProcessor.Dispose()");
                Instrument.SDPCRProcessor.Dispose();
            }
            Console.WriteLine("Press Enter to Exit");
            Console.ReadLine();
        }
    }
}

