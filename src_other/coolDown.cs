﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Timers;

using sdpcrInstrLib.BoardComm;
using sdpcrInstrLib.Hardware;
using sdpcrInstrLib.InstrExtensions;
using sdpcrInstrLib.Stepper;
using sdpcrInstrLib.Sequencers;
using sdpcrInstrLib.Therm;
using sdpcrInstrLib.PIDController;
using System.Runtime.Remoting.Metadata.W3cXsd2001;
using System.IO;

namespace CoolDown
{
    class CoolDown
    {
        static StepperMotor rotor;
        static Thermistors therm24;
        static PIDCtl pidController;
        //static PIDSequencer pidProgram;
        //static TempMonitoring tempProgram;
        static Processor sdPCRProcessor;
        static System.Timers.Timer timer1 = new System.Timers.Timer();
        //static bool cont;
        static StringBuilder fileName = new StringBuilder();
        static void Main(string[] args)
        {
            Console.Write("Enter Previous Port Name: ");
            string defPortName = Console.ReadLine().ToUpperInvariant();
            bool useConsole = true;
            bool trackConnect = false;
            bool trackComm = true;
            bool trackResp = true;
            sdPCRProcessor = new Processor(defPortName, useConsole, trackConnect, trackComm, trackResp);
            

            string[] portInfo = sdPCRProcessor.PortInformation;
            for (int n = 0; n < portInfo.Length; n++)
                Console.WriteLine(portInfo[n]);

            Console.WriteLine(String.Format("\nFirmware: {0}", sdPCRProcessor.FirmwareVersion));
            Console.WriteLine("Press Enter to continue");
            Console.ReadLine();
            //string any;
            ControlBoard board;
            if (sdPCRProcessor.GetBoard(out board) && board != null)
            {
                List<string> errMsg;
                bool rotorConnected = false;
                bool thermConnected = false;
                bool pidConnected = false;
                Console.Out.Flush();
                Console.WriteLine(">>>>>> Press enter to initialize Rotor/Thermistors/PIDController");
                Console.ReadLine();
                List<MotorProfile> motorProfiles = new List<MotorProfile>(4);
                for (int n = 0; n < motorProfiles.Count; n++)
                    motorProfiles[n].Used = false;

                motorProfiles[2].Used = true;
                motorProfiles[2].MinimumVelocity = 1;
                motorProfiles[2].MaximumVelocity = 150;
                motorProfiles[3].Used = true;
                motorProfiles[3].MinimumVelocity = 1;
                motorProfiles[3].MaximumVelocity = 1500;

                rotorConnected = board.InitMotorCtl(1, 1, motorProfiles, 1, 60, 300, 6400, out rotor, out errMsg);
                if (rotorConnected)
                {
                    Console.WriteLine("Rotor is connected");
                    //rotor.Home();
                }
                else
                {
                    Console.WriteLine("rotor did not connect");
                    for (int s = 0; s < errMsg.Count; s++)
                        Console.WriteLine(errMsg[s]);
                }
                if (rotorConnected)
                {
                    thermConnected = board.InitThermistors(out therm24, out errMsg);

                    if (thermConnected)
                    {
                        Console.WriteLine("Thermistors are connected");
                        pidConnected = board.InitPIDCtl(out pidController, out errMsg);
                        if (!pidConnected)
                        {
                            Console.WriteLine("PID controller did not connect");
                            for (int s = 0; s < errMsg.Count; s++)
                                Console.WriteLine(errMsg[s]);
                        }
                        else
                        {
                            Console.WriteLine("PID controller is connected");
                        }
                    }
                    else
                    {
                        Console.WriteLine("thermistors did not connect");
                        for (int s = 0; s < errMsg.Count; s++)
                            Console.WriteLine(errMsg[s]);
                    }
                }
                if (rotorConnected && thermConnected && pidConnected)
                {
                    rotor.VelocityModePositive();
                    pidController.SetManualTemperature(30.0);
                    pidController.ManualControlOn();
                    pidController.PIDControllerOn();
                    Console.WriteLine("Cool down started for 4 minutes");
                    for (int n = 0; n < 24; n++)
                    {
                        System.Threading.Thread.Sleep(10000);
                        double pTemp = pidController.ProcessTemperature;
                        Console.WriteLine(String.Format(" -- {0:0.00},{1:0.00},{2:0.00}",
                            pidController.CurrentTime * 0.001, pidController.TargetTemperature, pTemp));
                    }
                    rotor.Terminate();
                }
                Console.WriteLine("\nPress any key to exit");
                Console.ReadLine();
                sdPCRProcessor.Dispose();
            }
        }
    }
}
