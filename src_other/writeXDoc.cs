﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

using sdpcrInstrLib.CreateDefaultFile;


namespace WriteXDoc
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length < 1)
                Console.WriteLine("Usage WriteXDoc <basename>");
            else
                WriteDefaultXML.Config("c:\\instrument", args[0]);
        }
    }
}
