﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

using sdpcrInstrLib;
using sdpcrInstrLib.InstrInterface;

namespace testFilterWheel
{
    class TestFilterWheel
    {
        public static void Main()
        {
            Console.Write("Enter Previous Port Name: ");
            string defPortName = Console.ReadLine().ToUpperInvariant();
            if (!Instrument.Initialize(defPortName))
            {
                Console.WriteLine("Instrument did not initialize");
                Console.WriteLine("One of the components of the board did not connect properly");
                string[] iResults = Instrument.InitializationResult;
                for (int n = 0; n < iResults.Length; n++)
                    Console.WriteLine(iResults[n]);
                iResults = Instrument.InstrumentLog;
                Console.WriteLine("==========");
                for (int n = 0; n < iResults.Length; n++)
                    Console.WriteLine(iResults[n]);
                Console.WriteLine("Press enter to exit");
                Console.ReadLine();
                System.Environment.Exit(0);
            }
            else
            {
                string[] portInfo = Instrument.PortInfo;
                string[] logText;
                for (int n = 0; n < portInfo.Length; n++)
                    Console.WriteLine(portInfo[n]);

                Console.WriteLine(String.Format("\nFirmware: {0}", Instrument.SDPCRProcessor.FirmwareVersion));
                //string any;
                Console.WriteLine("Press Enter to continue");
                Console.ReadLine();

                int initVelocity = 20;
                int initProfile = 1;
                //int maxWait = (int)Math.Ceiling(((60.0 / (double)initVelocity) * 1000.0) / Instrument.BoardTime());
                int timeBetween;
                if (initVelocity < 5)
                    timeBetween = 30000;
                else if (initVelocity < 10)
                    timeBetween = 24000;
                else if (initVelocity < 15)
                    timeBetween = 12000;
                else 
                    timeBetween = 8000;
                 
                double timeBetweenSec = (double)timeBetween / 1000.0;
                ResponseItem resp;
                Console.WriteLine(String.Format("Time between steps: {0}", timeBetween));
                Instrument.FilterWheel.Home();
                Console.WriteLine("Filter Wheel should be homed");
                Console.WriteLine("Press enter to move Filter Wheel to postion 100");
                Console.ReadLine();
               // System.Threading.Thread.Sleep(timeBetween);
                Instrument.FilterWheel.AbsolutePosition(100);
                Console.WriteLine("Filter Wheel should be at position 100");
                Console.WriteLine("Press enter to load Filter Wheel program");
                Console.ReadLine();
                List<int> positions = new List<int>(0);
                positions.Add(900);
                positions.Add(1700);
                positions.Add(2500);
                positions.Add(500);
                positions.Add(1300);
                positions.Add(2100);
                positions.Add(2900);
                positions.Add(100);

                string err;
                List<string> progOut;
                if (Instrument.FilterWheelSequencer.LoadPositionProgram(positions, initProfile, initVelocity, out err))
                {
                    Instrument.FilterWheelSequencer.GetPositionProgram(out progOut);
                    Console.WriteLine("Program Loaded");
                    for (int n = 0; n < progOut.Count; n++)
                        Console.WriteLine(progOut[n]);
                    Console.WriteLine("Press enter to move the filter wheel to position 1700");
                    Console.ReadLine();
                    Instrument.FilterWheelSequencer.GoToPosition(1);
                    System.Threading.Thread.Sleep(2000);
                    Console.WriteLine("Press enter to move the filter wheel to position 100");
                    Console.ReadLine();
                    Instrument.FilterWheelSequencer.GoToPosition(7);
                    Console.WriteLine("Filter Wheel should be at position 100");
                    Console.WriteLine("Press enter to start program, 3 cycles of 8 steps, {0} seconds between steps", timeBetweenSec);
                    Console.ReadLine();
                    Instrument.FilterWheelSequencer.StartPositionProgram(timeBetween, 24);
                    
                    int waitTime = 0;
                    while (Instrument.FilterWheelSequencer.ProgramRunning)
                    {
                        waitTime++;
                        if (waitTime % 5 == 0)
                            Console.WriteLine(String.Format("{0} : Program is running", waitTime));
                        //bool errors = false;
                        while (Instrument.FilterWheelSequencer.CheckErrorList(out resp))
                        {
                            Console.WriteLine(resp.ToString());
                            //errors = true;
                        }
                        //if (!errors)
                        //    Console.WriteLine(String.Format("     No Errors: {0}", waitTime));

                        System.Threading.Thread.Sleep(1000);
                    }
                    Console.WriteLine("Filter Wheel program should have finished with filter wheel at position 100");
                }
                else
                {
                    Console.WriteLine("Problems loading first program");
                    Console.WriteLine(err);
                    Console.WriteLine("----------");
                }
                Console.WriteLine("Press enter to continue");
                Console.ReadLine();
                Instrument.FilterWheel.Home();
                //System.Threading.Thread.Sleep(timeBetween);
                Instrument.FilterWheel.AbsolutePosition(100);
                Console.WriteLine("Filter Wheel should be at position 100");
                Console.WriteLine("Press enter to load next program");
                Console.ReadLine();
                string progString = "<Program Type=\"Position\" Profile=\"1\" Velocity=\"20\" Name=\"FilterWheel-3 8 Positions\">" +
                    "<Step Number=\"0\" Position=\"100\"/>" +
                    "<Step Number=\"1\" Position=\"900\"/>" +
                    "<Step Number=\"2\" Position=\"1700\"/>" +
                    "<Step Number=\"3\" Position=\"2500\"/>" +
                    "<Step Number=\"4\" Position=\"500\"/>" +
                    "<Step Number=\"5\" Position=\"1300\"/>" +
                    "<Step Number=\"6\" Position=\"2900\"/>" +
                    "<Step Number=\"7\" Position=\"2100\"/>" +
                    "</Program>";
                Console.WriteLine(progString);
                int motorNumb = (int)MotorNumber.FilterWheel;
                if (Instrument.AddPositionProgram(motorNumb, progString))
                {
                    Instrument.GetPositionProgramHeaders(motorNumb, out List<string> programHeaders);
                    Console.WriteLine("Filter Wheel Program Headers");
                    for (int n = 0; n < programHeaders.Count; n++)
                        Console.WriteLine(programHeaders[n]);
                    int prog = programHeaders.Count - 1;
                    int pProfile;
                    int pVelocity;
                    Instrument.GetPositionProgram(motorNumb, prog, out pProfile, out pVelocity, out List<int> program);
                    Console.WriteLine(String.Format("Position program {0}", prog));
                    Console.WriteLine(String.Format("Profile: {0} - Velocity: {1}", pProfile, pVelocity));
                    for (int n = 0; n < program.Count; n++)
                        Console.WriteLine(program[n]);

                    if (pVelocity < 5)
                        timeBetween = 30000;
                    else if (pVelocity < 10)
                        timeBetween = 24000;
                    else if (pVelocity < 15)
                        timeBetween = 12000;
                    else
                        timeBetween = 8000;
                    timeBetweenSec = (double)timeBetween / 1000.0;
                    Console.WriteLine("Press enter to start program, 25 steps (3 cycles of 8 steps plus one), {0} seconds between steps", timeBetweenSec);
                    Console.ReadLine();
                    Instrument.FilterWheel.Home();
                    if(!Instrument.LoadPositionProgram(motorNumb, prog, out err))
                    {
                        Console.WriteLine(String.Format("Error loading program {0}", prog));
                        Console.WriteLine(String.Format("     {0}", err));
                    }
                    Instrument.FilterWheelSequencer.StartPositionProgram(timeBetween, 25);
                    int waitTime = 0;
                    while (Instrument.FilterWheelSequencer.ProgramRunning)
                    {
                        waitTime++;
                        if (waitTime % 5 == 0)
                            Console.WriteLine(String.Format("{0} : Program is running", waitTime));
                        //bool errors = false;
                        while (Instrument.FilterWheelSequencer.CheckErrorList(out resp))
                        {
                            Console.WriteLine(resp.ToString());
                            //errors = true;
                        }
                        //if (!errors)
                        //    Console.WriteLine(String.Format("     No Errors: {0}", waitTime));

                        System.Threading.Thread.Sleep(1000);
                    }
                    Console.WriteLine("Filter Wheel program should have finished with filter wheel at position 100");
                    Console.WriteLine("Press enter to proceed");
                    Console.ReadLine();

                    int mod = Instrument.FilterWheelSequencer.Modulous;
                    for (int n = 1; n <= mod; n++)
                    {
                        int m = n % mod;
                        Console.WriteLine(String.Format("Press enter to advance to position {0} at {1}", m, program[m]));
                        Console.ReadLine();
                        Instrument.FilterWheelSequencer.GoToPosition(m);
                    }

                    Console.WriteLine(">>>>>> Press enter to conclude filter wheel tests");
                    Console.ReadLine();
                }
                else
                {
                    Console.WriteLine("Problems loading second program");
                    Console.WriteLine(err);
                    Console.WriteLine("----------");
                }

                Instrument.SDPCRProcessor.ReadLogInfo(out logText);
                if (logText.Length > 0)
                {
                    Console.WriteLine("Log Info");
                    for (int l = 0; l < logText.Length; l++)
                        Console.WriteLine(logText[l]);

                    Console.Write("=====");
                }
            }
            if (Instrument.SDPCRProcessor != null)
            {
                Console.WriteLine("Calling sdPCRProcessor.Dispose()");
                Instrument.SDPCRProcessor.Dispose();
                Console.WriteLine("Press any key ");
                Console.ReadLine();
            }
        }
    }
}
