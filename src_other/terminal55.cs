﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
//using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Timers;
using System.IO;
using System.IO.Ports;
using System.Net;
using System.Xml.Schema;
using System.Linq.Expressions;

using sdpcrInstrLib;
using sdpcrInstrLib.InstrInterface;

namespace terminal55
{
    class Terminal55
    {
        static System.Timers.Timer timer0;
        readonly static char[] whiteSpace = { ' ', '\n', '\r', '\t' };  // space, new line, carriage return, tab || comma
        //readonly static char[] whiteSpaceQuote = { ' ', '\n', '\r', '\t', '"', '\'' };  // space, new line, carriage return, tab, ", '
        //readonly static char[] whiteSpaceQuoteBack = { ' ', '\n', '\r', '\t', '"', '\'', '\\' };  // space, new line, carriage return, tab, ", '
        //readonly static char[] quotes = { '"', '\'', ' ' };

        // logging variables
        static SynchronizedCollection<string> logList0 = new SynchronizedCollection<string>();
        static StringBuilder logList0_Name = new StringBuilder();
        static bool logIO = false;
        //static StringBuilder infoString = new StringBuilder();

        // Temperature control 
        private static readonly object currentSetTLock = new object();
        static double currentSetT;

        readonly static int numbLEDs = 6;
        readonly static int[] weights = { 100, 100, 0, 0,
            0, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 0};
        static char[] delimiterChars = { ' ', ',', '\t' }; // space, comma, and tab
        static char[] comma = { ',' };
        //static char[] responseDelimiters = { '>' };
        static string outputDir = "C:\\instrument\\";
        //static StringBuilder inputBuffer = new StringBuilder();
    
        //static System.Timers.Timer timer2;
        readonly static SynchronizedCollection<string> list1 = new SynchronizedCollection<string>();
        //static SynchronizedCollection<string> list2 = new SynchronizedCollection<string>();
        static readonly StringBuilder list1_Name = new StringBuilder();
        readonly static int timeStep = 2000; // milliseconds, for temperature monitoring

        // Temperature Program variables
        //static List<TempProgUnit> Cycle = new List<TempProgUnit>();
        //static TempProgUnit newTempProgUnit = new TempProgUnit();
        //static int consoleWaitTime = 300;
        //static public ConcurrentQueue<ResponseItem> ResponseQueue;
        static public ConcurrentQueue<string> InfoQueue;

        static public string[] pidProgramLog;
        static public bool pidProgramLoaded = false;
        static bool tempProgOn = false;

        static public string[] thermProgramLog;
        static public bool thermProgramLoaded = false;
        static public bool thermTimerOn = false;

        static public bool motorProgramLoaded = false;
        static public bool motorTimerOn = false;
        static public void LogInfo(string str)
        {
            InfoQueue.Enqueue(str);
        }
        static void Main(string[] args)
        {
            string defPortName;
            if (args.Length > 0)
                defPortName = args[0].ToUpperInvariant();
            else
            {
                Console.Write("Enter Previous Port Name: ");
                defPortName = Console.ReadLine().ToUpperInvariant();
            }
            //bool Instrument.Initialize(string PrevPort = "COM1", bool UseConsole = false, 
            //     bool TrackConnect = false, bool TrackComm = false, bool TrackResp = false)
            if (!Instrument.Initialize(defPortName)) 
            {
                Console.WriteLine("Instrument did not initialize");
                Console.WriteLine("One of the components of the board did not connect properly");
                string[] iResults = Instrument.InitializationResult;
                for (int n = 0; n < iResults.Length; n++)
                    Console.WriteLine(iResults[n]);
                iResults = Instrument.InstrumentLog;
                Console.WriteLine("==========");
                for (int n = 0; n < iResults.Length; n++)
                    Console.WriteLine(iResults[n]);
                Console.WriteLine("Press enter to exit");
                Console.ReadLine();
                System.Environment.Exit(0);
            }
            else
            {
                Console.WriteLine("Initialization Results");
                string[] iResults = Instrument.InitializationResult;
                for (int n = 0; n < iResults.Length; n++)
                    Console.WriteLine(iResults[n]);
            }
            ResponseItem resp = new ResponseItem();
            List<string> programHeaders;
            string errorMessage;
            string[] portInfo = Instrument.PortInfo;
            for (int n = 0; n < portInfo.Length; n++)
                Console.WriteLine(portInfo[n]);
            Console.WriteLine("=====");
            Console.WriteLine("Insturment should be initialized");
            Console.WriteLine(String.Format("\nFirmware: {0}", Instrument.SDPCRProcessor.FirmwareVersion));
            Console.WriteLine("Time and Calibrated Temperatures");
            Console.WriteLine(Instrument.Thermistor_Ctl.TimeAndCalibratedTemperatures);
            Console.WriteLine("Press Enter to continue");
            //string any;
            Console.ReadLine();

            string message;
            int cmdSize = 100;
            List<string> cmds = new List<string>(cmdSize);
            LoadCommandList(cmdSize, ref cmds);
            StringComparer stringComparer = StringComparer.OrdinalIgnoreCase;
            bool _continue = true;
            timer0 = new System.Timers.Timer(timeStep);
            // Hook up the Elapsed event for the timer. 
            timer0.Elapsed += OnTimer0Event;
            timer0.AutoReset = true;
            timer0.Enabled = true;
            InfoQueue = new ConcurrentQueue<string>();
            while (!InfoQueue.IsEmpty)
                InfoQueue.TryDequeue(out _);

            while (_continue)
            {
                message = Console.ReadLine();
                if (stringComparer.Equals("quit", message))
                {
                    _continue = false;
                }
                else
                {
                    string[] words = message.Split(delimiterChars);
                    bool isInt = false;
                    int sw;
                    Console.WriteLine(String.Format(" {0}, {1}", words.Length, words[0]));
                    while (words.Length > 0 && !isInt)
                    {
                        isInt = int.TryParse(words[0], out sw);
                        if (!isInt)
                        {
                            //Console.WriteLine(String.Format(" {0}, {1}", words.Length, words[0]));
                            List<string> newWords = new List<string>();
                            if (words.Length > 1)
                            {
                                for (int m = 1; m < words.Length; m++)
                                    newWords.Add(words[m]);
                            }
                            words = new string[newWords.Count];
                            for (int m = 0; m < newWords.Count; m++)
                                words[m] = newWords[m];
                        }
                    }
                    if (words.Length > 0)
                    {
                        switch (Convert.ToInt32(words[0]))
                        {
                            case 0:
                                Console.WriteLine("==========");
                                for (int n = 0; n < cmdSize; n++)
                                {
                                    if (cmds[n].Length > 0)
                                        Console.Write(String.Format("{0} - {1}\r\n", n.ToString(" 0"), cmds[n]));
                                }
                                Console.WriteLine();
                                break;
                            case 1:
                                if (!logIO && words.Length > 1)
                                {
                                    logList0_Name.Clear();
                                    logList0_Name.Append(String.Format("{0}{1}_log.txt", outputDir, words[1]));
                                    if (IsValidFilename(logList0_Name.ToString()) && !File.Exists(logList0_Name.ToString()))
                                    {
                                        logList0.Clear();
                                        Console.WriteLine(String.Format("IO logging started, output to {0}",
                                            logList0_Name.ToString()));
                                        DateTime date = DateTime.Now;
                                        logList0.Add(String.Format("Day: {0:d} Time: {1:g}", date.Date, date.TimeOfDay));
                                        logIO = true;
                                    }
                                    else if (File.Exists(logList0_Name.ToString()))
                                        Console.WriteLine(String.Format("{0} already exists, not overwritten",
                                            logList0_Name.ToString()));
                                    else
                                        Console.WriteLine(String.Format("{0} is not a valid file name",
                                            logList0_Name.ToString()));
                                }
                                else if (logIO)
                                {
                                    Console.WriteLine("Program is already logging IO, no change");
                                }
                                else
                                {
                                    Console.WriteLine("No log file named");
                                }
                                break;
                            case 2:
                                if (logIO)
                                {
                                    StopLogging();
                                }
                                else
                                {
                                    Console.WriteLine("Program was not logging IO, no change");
                                }
                                break;
                            case 3:
                                string f = String.Format("Firmware: {0}", Instrument.SDPCRProcessor.FirmwareVersion);
                                Console.WriteLine(String.Format("\r\n{0}", f));
                                if (logIO)
                                    LogInfo(String.Format("{0:0.00}: {1}", CurrentTimeSec(), f));
                                break;
                            case 5:
                                if (words.Length > 1)
                                {
                                    list1.Clear();
                                    list1_Name.Clear();
                                    list1_Name.Append(String.Format("{0}{1}", outputDir, words[1]));
                                    if (IsValidFilename(list1_Name.ToString()) && !File.Exists(list1_Name.ToString()))
                                    {
                                        if (Instrument.SDPCRProcessor.ReadLogInfo(out string[] lines))
                                            System.IO.File.WriteAllLines(@list1_Name.ToString(), lines);
                                        else
                                            Console.WriteLine("Log list is empty");
                                    }
                                    else
                                    {
                                        Console.WriteLine(String.Format("Problems with file name {0}", list1_Name.ToString()));
                                    }
                                }
                                break;
                            
                            case 9:
                                Console.WriteLine(String.Format("Step/Time/SetTemp/Temp/Descr. {0}",
                                    Instrument.TempProgramController.CurrentState));
                                break;
                            case 11:
                                Instrument.Rotor.Home();
                                if (logIO)
                                    LogInfo(String.Format("{0:0.00}: Rotor - /1ZR", CurrentTimeSec()));
                                break;
                            case 12:
                                Instrument.XStage.Home();
                                if (logIO)
                                    LogInfo(String.Format("{0:0.00}: X-Stage - /2ZR", CurrentTimeSec()));
                                break;
                            case 13:
                                Instrument.YStage.Home();
                                if (logIO)
                                    LogInfo(String.Format("{0:0.00}: Y-Stage - /3ZR", CurrentTimeSec()));
                                break;
                            case 14:
                                Instrument.FilterWheel.Home();
                                if (logIO)
                                    LogInfo(String.Format("{0:0.00}: Filter Wheel - /4ZR", CurrentTimeSec()));
                                break;
                            case 15:
                                if (words.Length > 1)
                                {
                                    if (Int32.TryParse(words[1], out int w2))
                                    {
                                        if (w2 >= 0 && w2 <= Instrument.Rotor.MaxPosition)
                                            Instrument.Rotor.AbsolutePosition(w2);
                                        if (logIO)
                                            LogInfo(String.Format("{0:0.00}: Rotor - /1A{1}R", CurrentTimeSec(), w2));
                                    }
                                }
                                break;
                            case 16:
                                if (words.Length > 1)
                                {
                                    if (Int32.TryParse(words[1], out int w2))
                                    {
                                        if (w2 >= 0 && w2 <= Instrument.XStage.MaxPosition)
                                            Instrument.XStage.AbsolutePosition(w2);
                                        if (logIO)
                                            LogInfo(String.Format("{0:0.00}: XStage - /2A{1}R", CurrentTimeSec(), w2));
                                    }
                                }
                                break;
                            case 17:
                                if (words.Length > 1)
                                {
                                    if (Int32.TryParse(words[1], out int w2))
                                    {
                                        if (w2 >= 0 && w2 <= Instrument.YStage.MaxPosition)
                                            Instrument.YStage.AbsolutePosition(w2);
                                        if (logIO)
                                            LogInfo(String.Format("{0:0.00}: YStage - /3A{1}R", CurrentTimeSec(), w2));
                                    }
                                }
                                break;
                            case 18:
                                if (words.Length > 1)
                                {
                                    if (Int32.TryParse(words[1], out int w2))
                                    {
                                        if (w2 >= 0 && w2 <= Instrument.FilterWheel.MaxPosition)
                                            Instrument.FilterWheel.AbsolutePosition(w2);
                                        if (logIO)
                                            LogInfo(String.Format("{0:0.00}: Filter Wheel - /4A{1}R", CurrentTimeSec(), w2));
                                    }
                                }
                                break;
                            case 20:
                                Instrument.Rotor.Terminate();
                                Instrument.Rotor.HighImpedance();
                                Instrument.Rotor.SetProfile(2);
                                Instrument.Rotor.SetVelocity(60);
                                Instrument.Rotor.VelocityModePositive();
                                if (logIO)
                                    LogInfo(String.Format("{0:0.00}: Rotor - /1TR, /1hR, /1V1R, /1v60R, /1P0R", CurrentTimeSec()));
                                break;
                            case 21:
                                Instrument.Rotor.Terminate();
                                if (logIO)
                                    LogInfo(String.Format("{0:0.00}: Rotor - /1TR", CurrentTimeSec()));
                                break;
                            case 22:
                                Instrument.XStage.Terminate();
                                if (logIO)
                                    LogInfo(String.Format("{0:0.00}: X-Stage- /2TR", CurrentTimeSec()));
                                break;
                            case 23:
                                Instrument.YStage.Terminate();
                                if (logIO)
                                    LogInfo(String.Format("{0:0.00}: Y-Stage- /3TR", CurrentTimeSec()));
                                break;
                            case 24:
                                Instrument.FilterWheel.Terminate();
                                if (logIO)
                                    LogInfo(String.Format("{0:0.00}: Filtr Wheel- /4TR", CurrentTimeSec()));
                                break;

                            case 25:
                                if (words.Length > 1)
                                {
                                    int w1;
                                    if (Int32.TryParse(words[1], out w1))
                                    {
                                        if (w1 > 0 && w1 < 1500)
                                        {
                                            Instrument.Rotor.Terminate();
                                            Instrument.Rotor.HighImpedance();
                                            Instrument.Rotor.SetProfile(3);
                                            Instrument.Rotor.SetVelocity(w1);
                                            Instrument.Rotor.VelocityModePositive();
                                        }
                                        if (logIO)
                                            LogInfo(String.Format("{0:0.00}: Rotor - /1TR, /1hR, /1V1R, /1v{1}R, /1P0R", CurrentTimeSec(), w1));
                                    }
                                }
                                break;
                            
                            case 27:
                                Instrument.Rotor.VelocityModePositive();
                                if (logIO)
                                    LogInfo(String.Format("{0:0.00}: Rotor - /1P0}R", CurrentTimeSec()));
                                break;
                            case 28:
                                if (words.Length > 1)
                                {
                                    int w1;
                                    if (Int32.TryParse(words[1], out w1))
                                    {
                                        switch (w1)
                                        {
                                            case 1:
                                                Console.WriteLine(String.Format("{0}", Instrument.Rotor.Register));
                                                if (logIO)
                                                    LogInfo(String.Format("{0:0.00}: Rotor - /1rR", CurrentTimeSec()));
                                                break;
                                            case 2:
                                                Console.WriteLine(String.Format("{0}", Instrument.XStage.Register));
                                                if (logIO)
                                                    LogInfo(String.Format("{0:0.00}: X-Stage - /2rR", CurrentTimeSec()));
                                                break;
                                            case 3:
                                                Console.WriteLine(String.Format("{0}", Instrument.YStage.Register));
                                                if (logIO)
                                                    LogInfo(String.Format("{0:0.00}: Y-Stage - /3rR", CurrentTimeSec()));
                                                break;
                                            case 4:
                                                Console.WriteLine(String.Format("{0}", Instrument.FilterWheel.Register));
                                                if (logIO)
                                                    LogInfo(String.Format("{0:0.00}: Filter Wheel - /4rR", CurrentTimeSec()));
                                                break;
                                            default:
                                                break;
                                        }
                                    }
                                }
                                break;
                            case 29:
                                if (words.Length > 1)
                                {
                                    int w1;
                                    if (Int32.TryParse(words[1], out w1))
                                    {
                                        //Console.WriteLine(String.Format("{0}", w1));
                                        //if (w1 > 0 && w1 < 4)
                                        //    Console.WriteLine(String.Format("{0}", preloadedTemp[w1]));
                                        switch (w1)
                                        {
                                            case 1:
                                                if (Instrument.Rotor.Busy)
                                                {
                                                    Console.WriteLine("Rotor is busy");
                                                    if (logIO)
                                                        LogInfo(String.Format("{0:0.00}: Rotor is busy", CurrentTimeSec()));
                                                }
                                                else
                                                {
                                                    Console.WriteLine("Rotor is not busy");
                                                    if (logIO)
                                                        LogInfo(String.Format("{0:0.00}: Rotor not is busy", CurrentTimeSec()));
                                                }
                                                break;
                                            case 2:
                                                if (Instrument.XStage.Busy)
                                                {
                                                    Console.WriteLine("X-Stage is busy");
                                                    if (logIO)
                                                        LogInfo(String.Format("{0:0.00}: X-Stage is busy", CurrentTimeSec()));
                                                }
                                                else
                                                {
                                                    Console.WriteLine("X-Stage is not busy");
                                                    if (logIO)
                                                        LogInfo(String.Format("{0:0.00}: X-Stage is not busy", CurrentTimeSec()));
                                                }
                                                break;
                                            case 3:
                                                if (Instrument.YStage.Busy)
                                                {
                                                    Console.WriteLine("Y-Stage is busy");
                                                    if (logIO)
                                                        LogInfo(String.Format("{0:0.00}: Y-Stage is busy", CurrentTimeSec()));
                                                }
                                                else
                                                {
                                                    Console.WriteLine("Y-Stage is not busy");
                                                    if (logIO)
                                                        LogInfo(String.Format("{0:0.00}: Y-Stage is not busy", CurrentTimeSec()));
                                                }
                                                break;
                                            case 4:
                                                if (Instrument.FilterWheel.Busy)
                                                {
                                                    Console.WriteLine("Filter Wheel is busy");
                                                    if (logIO)
                                                        LogInfo(String.Format("{0:0.00}: Filter Wheel is busy", CurrentTimeSec()));
                                                }
                                                else
                                                {
                                                    Console.WriteLine("Filter Wheel is not busy");
                                                    if (logIO)
                                                        LogInfo(String.Format("{0:0.00}: Filter Wheel is not busy", CurrentTimeSec()));
                                                }
                                                break;
                                            default:
                                                break;
                                        }
                                    }
                                }
                                break;
                            case 30:
                                if (words.Length > 2)
                                {
                                    int w1;
                                    if (Int32.TryParse(words[1], out w1))
                                    {
                                        int w2;
                                        if (Int32.TryParse(words[2], out w2))
                                        {
                                            if (( w1 == 2 ||  w1 == 3 ) && w2 < 76)
                                            {
                                                Instrument.Rotor.Terminate();
                                                Instrument.Rotor.HighImpedance();
                                                Instrument.Rotor.SetProfile(w1);
                                                Instrument.Rotor.SetVelocity(w2);
                                                if (logIO)
                                                    LogInfo(String.Format("{0:0.00}: Rotor - /1TR, /1hR, /1V{1}R, /1v{2}R",
                                                        CurrentTimeSec(), w1, w2));
                                            }
                                            else
                                                Console.WriteLine(
                                                    "Velocity mode must be 2 or 3 and velocity must be <= 75");
                                        }
                                    }
                                }
                                break;
                            case 31:
                                if (words.Length > 2)
                                {
                                    int w1;
                                    if (Int32.TryParse(words[1], out w1))
                                    {
                                        int w2;
                                        if (Int32.TryParse(words[2], out w2))
                                        {
                                            if (w1 >= 0 && w2 >= 0 && w1 != w2)
                                            {
                                                Instrument.Rotor.Oscillate(w1, w2);
                                                if (logIO)
                                                    LogInfo(String.Format("{0:0.00}: Rotor - /1A{1}A{2}G0R - Oscillate",
                                                        CurrentTimeSec(), w1, w2));
                                            }
                                            else
                                                Console.WriteLine("Position values must be >= 0 and unequal to each other");
                                        }
                                    }
                                }
                                break;
                            case 39:
                                if (words.Length > 1)
                                {
                                    int w1;
                                    if (Int32.TryParse(words[1], out w1))
                                    {
                                        switch (w1)
                                        {
                                            case 1:
                                                if (Instrument.Rotor.Homed)
                                                    Console.WriteLine("Rotor is homed");
                                                else
                                                    Console.WriteLine("Rotor is not homed");
                                                break;
                                            case 2:
                                                if (Instrument.XStage.Homed)
                                                    Console.WriteLine("X stage is homed");
                                                else
                                                    Console.WriteLine("X stage is not homed");
                                                break;
                                            case 3:
                                                if (Instrument.YStage.Homed)
                                                    Console.WriteLine("Y stage is homed");
                                                else
                                                    Console.WriteLine("Y stage is not homed");
                                                break;
                                            case 4:
                                                if (Instrument.FilterWheel.Homed)
                                                    Console.WriteLine("Filter Wheel is homed");
                                                else
                                                    Console.WriteLine("Filter Wheel is not homed");
                                                break;
                                            default:
                                                break;
                                        }
                                    }
                                }
                                break;
                            case 41:
                                if (words.Length > 2)
                                {
                                    int w1;
                                    int inten;
                                    if (Int32.TryParse(words[1], out w1) && Int32.TryParse(words[2], out inten))
                                    {
                                        if (w1 >= 0 && w1 < numbLEDs && inten > 0 && inten <= 100)
                                        {
                                            Instrument.LED_Ctl.SetIntensity(w1, inten);
                                            if (logIO)
                                            {
                                                int l = 20 + w1;
                                                LogInfo(String.Format("{0:0.00}: LED - /{1}l{2}R",
                                                    CurrentTimeSec(), l, inten));
                                            }
                                        }
                                    }
                                }
                                break;
                            case 42:
                                if (words.Length > 1)
                                {
                                    int w1;
                                    if (Int32.TryParse(words[1], out w1))
                                    {
                                        if (w1 >= 0 && w1 < numbLEDs)
                                        {
                                            Instrument.LED_Ctl.On(w1);
                                            if (logIO)
                                            {
                                                int l = 20 + w1;
                                                LogInfo(String.Format("{0:0.00}: LED - /{1}S1R",
                                                    CurrentTimeSec(), l)); ;
                                            }
                                        }
                                    }
                                }
                                break;
                            case 43:
                                if (words.Length > 1)
                                {
                                    int w1;
                                    if (Int32.TryParse(words[1], out w1))
                                    {
                                        if (w1 >= 0 && w1 < 6)
                                        {
                                            Instrument.LED_Ctl.Off(w1);
                                            if (logIO)
                                            {
                                                int l = 20 + w1;
                                                LogInfo(String.Format("{0:0.00}: LED - /{1}S0R",
                                                    CurrentTimeSec(), l));
                                            }
                                        }
                                    }
                                }
                                break;
                            case 50:
                                for (int n = 0; n < weights.Length; n++)
                                {
                                    Instrument.Thermistor_Ctl.SetWeight(n, weights[n]);
                                    int t = 100 + n;
                                    if (logIO)
                                        LogInfo(String.Format("{0:0.00}: Thermistors - /{1}W{2}R",
                                            CurrentTimeSec(), t, weights[n]));
                                }
                                while (Instrument.Thermistor_Ctl.CheckResponseList(out resp))
                                {
                                    Console.WriteLine(String.Format("Thermistors: {0}", resp.ToString()));
                                    if (logIO)
                                        LogInfo(resp.ToString());
                                }
                                break;
                            case 51:
                                if (words.Length > 2)
                                {
                                    int w1;
                                    int w2;
                                    if (Int32.TryParse(words[1], out w1))
                                    {
                                        if (w1 >= 0 && w1 < 24)
                                        {
                                            if (Int32.TryParse(words[2], out w2))
                                            {
                                                if (w2 >= 0)
                                                {
                                                    Instrument.Thermistor_Ctl.SetWeight(w1, w2);
                                                    if (logIO)
                                                        LogInfo(String.Format("{0:0.00}: Thermistors - /{1}W{2}R",
                                                            CurrentTimeSec(), w1, w2));
                                                }
                                            }
                                        }
                                    }
                                }
                                break;
                            case 52:
                                string wgt;
                                Instrument.Thermistor_Ctl.GetThermistorWeights(out wgt);
                                Console.WriteLine(String.Format("Wgts: {0}", wgt));
                                words = wgt.Split(comma);
                                double dwgt;
                                for (int n = 0; n < words.Length; n++)
                                {
                                    if (Double.TryParse(words[n], out dwgt))
                                    {
                                        weights[n] = (int)Math.Round(dwgt);
                                    }
                                }
                                break;
                            case 53:
                                if (words.Length > 1)
                                {
                                    double w1;
                                    if (Double.TryParse(words[1], out w1))
                                    {
                                        if (w1 > 5.0 && w1 < 95.0)
                                        {
                                            Instrument.Thermistor_Ctl.SetReferenceTemperature(w1);
                                            Console.WriteLine(String.Format("Set reference temperature ({0}) [calculate bias]", w1));
                                            if (logIO)
                                                LogInfo(String.Format("{0:0.00}: Thermistors - /100{1:0.00}R - Calcuate bias",
                                                    CurrentTimeSec(), w1));
                                        }
                                    }
                                }
                                break;
                            case 54:
                                Instrument.Thermistor_Ctl.ClearAllTemperatureBiases();
                                Console.WriteLine(String.Format("Clear all temperature biases"));
                                if (logIO)
                                    LogInfo(String.Format("{0:0.00}: Thermistors - /100ZR",
                                        CurrentTimeSec()));
                                break;
                            case 55:
                                string bias = Instrument.Thermistor_Ctl.ThermistorBiases;
                                Console.WriteLine(String.Format("Bias: {0}", bias));
                                if (logIO)
                                    LogInfo(String.Format("{0:0.00}: Thermistors - /100DR - Thermistor biases = {1} ",
                                        CurrentTimeSec(), bias));
                                break;
                            case 56:
                                string calTemp = Instrument.Thermistor_Ctl.CalibratedTemperatures;
                                Console.WriteLine(String.Format("Calibrated Temp: {0}", calTemp));
                                if (logIO)
                                    LogInfo(String.Format("{0:0.00}: Thermistors - /100AR - Calibrated Temperatures = {1} ",
                                        CurrentTimeSec(), calTemp));
                                break;

                            case 60:
                                double refTemp = Instrument.Temp_Ctl.ProcessTemperature;
                                Console.WriteLine(String.Format("Temperature: {0}", refTemp));
                                if (logIO)
                                    LogInfo(String.Format("{0:0.00}: PID Controller - /70R0R - Current Process temperature = {1}",
                                          CurrentTimeSec(), refTemp));
                                break;
                            case 61:
                                double targetTemp = Instrument.Temp_Ctl.TargetTemperature;
                                Console.WriteLine(String.Format("Target Temperature: {0}", targetTemp));
                                if (logIO)
                                    LogInfo(String.Format("{0:0.00}: PID Controller - /70QR - Target Temperature = {1}",
                                          CurrentTimeSec(), targetTemp));
                                break;
                            case 62:
                                Instrument.Temp_Ctl.PIDControllerOn();
                                Console.WriteLine("PID controller turned on");
                                if (logIO)
                                    LogInfo(String.Format("{0:0.00}: PID Controller - /70C1R - turn on PID controller",
                                          CurrentTimeSec()));
                                break;
                            case 63:
                                Instrument.Temp_Ctl.PIDControllerOff();
                                Console.WriteLine("PID controller turned off");
                                if (logIO)
                                    LogInfo(String.Format("{0:0.00}: PID Controller - /70C0R - turn off PID controller",
                                        CurrentTimeSec()));
                                break;
                            case 64:
                                if (Instrument.Temp_Ctl.PIDStatus)
                                {
                                    Console.WriteLine("PID controller is on");
                                    if (logIO)
                                        LogInfo(String.Format("{0:0.00}: PID Controller - /70S0R - PID Status checked: PID controller off",
                                                      CurrentTimeSec()));
                                }
                                else
                                {
                                    Console.WriteLine("PID controller is off");
                                    if (logIO)
                                        LogInfo(String.Format("{0:0.00}: PID Controller - /70S0R - PID Status checked: PID controller off",
                                                      CurrentTimeSec()));
                                }
                                break;
                            case 67:
                                if (Instrument.TempProgramController.ProgramRunning)
                                {
                                    Console.WriteLine(
                                        "Cannot change Manual Temperature while temperature program is running!!");
                                }
                                else
                                {
                                    if (words.Length > 1)
                                    {
                                        double w1;
                                        if (Double.TryParse(words[1], out w1))
                                        {
                                            if (w1 > 10.0 && w1 < 115.0)
                                            {
                                                Instrument.Temp_Ctl.SetManualTemperature(w1);
                                                lock (currentSetTLock)
                                                {
                                                    currentSetT = w1;
                                                }
                                                if (logIO)
                                                    LogInfo(String.Format(
                                                        "{0:0.00}: PID Controller - /70A{1:0.00}R - Change Manual Temperature",
                                                                  CurrentTimeSec(), w1));
                                            }
                                            else
                                            {
                                                Console.WriteLine("Requested temperature out of range!");
                                            }
                                        }
                                    }
                                }
                                break;
                            case 68:
                                Instrument.Temp_Ctl.ManualControlOn();
                                Console.WriteLine("Manual Temper controller turned on");
                                if (logIO)
                                    LogInfo(String.Format("{0:0.00}: PID Controller - Manual Temper controller turned on",
                                          CurrentTimeSec()));
                                break;
                            case 69:
                                Instrument.Temp_Ctl.ManualControlOff();
                                Console.WriteLine("Manual Temper controller turned off");
                                if (logIO)
                                    LogInfo(String.Format("{0:0.00}: PID Controller - Manual Temper controller turned off",
                                          CurrentTimeSec()));
                                break;

                            case 80:
                                Instrument.GetPIDProgramHeaders(out programHeaders);
                                if (programHeaders.Count > 0)
                                {
                                    Console.WriteLine("Program Headers for Temperature Controller");
                                    for (int n = 0; n < programHeaders.Count; n++)
                                        Console.WriteLine(programHeaders[n]);
                                }
                                else
                                {
                                    Console.WriteLine("No programs saved for Temperature controller");
                                }
                                break;
                            case 81:
                                Instrument.GetPIDProgramHeaders(out programHeaders);
                                if (words.Length > 1)
                                {
                                    if (Int32.TryParse(words[1], out int prog))
                                    {
                                        if (prog >= 0 && prog < programHeaders.Count)
                                        {
                                            if (!Instrument.LoadPIDProgram(prog, out errorMessage))
                                            {
                                                Console.WriteLine(String.Format("Problem Loading Rotor Program {0}", prog));
                                                Console.WriteLine(String.Format("     {0}", errorMessage));
                                            }
                                        }
                                    }
                                }
                                break;
                            case 82:
                                if (words.Length > 1 && Instrument.TempProgramController.ProgramLoaded
                                    && !Instrument.TempProgramController.ProgramRunning)
                                {
                                    if (Int32.TryParse(words[1], out int w1))
                                    {
                                        if (w1 > 499)
                                        {
                                            Instrument.TempProgramController.StartProgram(0, w1, true);
                                            tempProgOn = true;
                                        }

                                    }
                                }
                                break;
                            case 83:
                                if (Instrument.TempProgramController.ProgramRunning)
                                {
                                    Instrument.TempProgramController.StopProgram();
                                    tempProgOn = false;
                                }
                                break;
                            case 84:
                                if (Instrument.TempProgramController.ProgramRunning)
                                {
                                    Instrument.TempProgramController.StopProgram(false);
                                    tempProgOn = false;
                                }
                                break;
                            case 85:
                                if (Instrument.TempProgramController.ProgramLoaded)
                                {
                                    Console.WriteLine(Instrument.TempProgramController.CurrentStep);
                                }
                                break;
                            case 86:
                                if (Instrument.TempProgramController.ProgramLoaded)
                                {
                                    Console.WriteLine(Instrument.TempProgramController.CurrentState);
                                }
                                break;
                            case 87:
                                if (Instrument.TempProgramController.ProgramLoaded)
                                {
                                    List<TempProgUnit> prog = Instrument.TempProgramController.TempProgram;
                                    Console.WriteLine("Temp - Time - Description");
                                    for (int n = 0; n < prog.Count; n++)
                                    {
                                        Console.WriteLine(String.Format("{0} - {1} - {2}",
                                            prog[n].Temperature, prog[n].Time, prog[n].Description));
                                    }
                                    Console.WriteLine("----------");
                                }
                                break;
                            case 88:
                                if (words.Length > 1 && Instrument.TempProgramController.GetOutput(out pidProgramLog))
                                {
                                    list1.Clear();
                                    list1_Name.Clear();
                                    list1_Name.Append(String.Format("{0}{1}_tempCtl_{2}.csv",
                                        outputDir, words[1], Instrument.BoardTime()
                                        ));
                                    if (IsValidFilename(list1_Name.ToString()) && !File.Exists(list1_Name.ToString()))
                                        System.IO.File.WriteAllLines(@list1_Name.ToString(), pidProgramLog);
                                    
                                    Console.WriteLine("----------");
                                }
                                break;
                            case 90:
                                if (words.Length > 1 && !Instrument.ThermMonitoring.ProgramRunning)
                                {
                                    if (Int32.TryParse(words[1], out int timeInterval))
                                    {
                                        if (timeInterval > 199)
                                        {
                                            Instrument.ThermMonitoring.StartMonitoring(timeInterval);
                                            thermTimerOn = true;
                                        }
                                    }
                                }
                                break;
                            case 91:
                                if (Instrument.ThermMonitoring.ProgramRunning)
                                {
                                    Instrument.ThermMonitoring.StopMonitoring();
                                    thermTimerOn = false;
                                }
                                break;
                            case 92:
                                if (words.Length > 1 && Instrument.ThermMonitoring.GetOutput(out thermProgramLog))
                                {
                                    list1.Clear();
                                    list1_Name.Clear();
                                    list1_Name.Append(String.Format("{0}{1}_thermistorMonitoring_{2}.csv",
                                        outputDir, words[1], Instrument.BoardTime()
                                        ));
                                    if (IsValidFilename(list1_Name.ToString()) && !File.Exists(list1_Name.ToString()))
                                        System.IO.File.WriteAllLines(@list1_Name.ToString(), thermProgramLog);
                                }
                                break;

                            case 95:
                                Instrument.GetPositionProgramHeaders((int)MotorNumber.Rotor, out programHeaders);
                                if (programHeaders.Count > 0)
                                {
                                    Console.WriteLine("Program Headers for Rotor");
                                    for (int n = 0; n < programHeaders.Count; n++)
                                        Console.WriteLine(programHeaders[n]);
                                }
                                else
                                {
                                    Console.WriteLine("No programs saved for Rotor");
                                }
                                break;
                            case 96:
                                Instrument.GetPositionProgramHeaders((int)MotorNumber.Rotor, out programHeaders);
                                if (words.Length > 1)
                                {
                                    int prog;
                                    if (Int32.TryParse(words[1], out prog))
                                    {
                                        if (prog >= 0 && prog < programHeaders.Count)
                                        {
                                            if (!Instrument.LoadPositionProgram((int)MotorNumber.Rotor, prog, out errorMessage))
                                            {
                                                Console.WriteLine(String.Format("Problem Loading Rotor Program {0}", prog));
                                                Console.WriteLine(String.Format("     {0}", errorMessage));
                                            }
                                        }
                                    }
                                }
                                break;
                            case 97:
                                if (words.Length > 2 && Instrument.RotorPositionSequencer.ProgramLoaded && !Instrument.RotorPositionSequencer.ProgramRunning)
                                {
                                    int w1;
                                    int w2;
                                    if (Int32.TryParse(words[1], out w1))
                                    {
                                        if (w1 >= 0 && Int32.TryParse(words[2], out w2))
                                        {
                                            if (w2 > 2)
                                            {
                                                Instrument.RotorPositionSequencer.StartPositionProgram(w1, w2);
                                                motorTimerOn = true;
                                            }
                                        }
                                    }
                                }
                                break;
                            case 98:
                                if (Instrument.RotorPositionSequencer.ProgramRunning)
                                {
                                    Instrument.RotorPositionSequencer.StopPositionProgram();
                                    motorTimerOn = false;
                                }
                                break;
                            case 99:
                                List<string> progOut;
                                if (Instrument.RotorPositionSequencer.GetPositionProgram(out progOut))
                                {
                                    Console.WriteLine("Program in Rotor object");
                                    for (int n = 0; n < progOut.Count; n++)
                                        Console.WriteLine(progOut[n]);
                                }
                                break;
                            default:
                                Console.WriteLine("Command not implemented");
                                break;
                        }
                    }
                }
            }

            Console.WriteLine("\nShutting down program");
            if (logIO)
                StopLogging();

            Console.WriteLine("\nPress any key to exit");
            Console.ReadLine();
            Instrument.SDPCRProcessor.Dispose();
            if (timer0 != null)
            {
                timer0.Stop();
                timer0.Dispose();
            }

        }
        private static void OnTimer0Event(Object source, ElapsedEventArgs e)
        {
            ResponseItem resp;
            while (Instrument.SDPCRProcessor.CheckResponseList(out resp))
            {
                Console.WriteLine(String.Format("{0}: Processor: {1}", Instrument.BoardTime(), resp.ToString()));
                if (logIO)
                    LogInfo(resp.ToString());
            }

            while (Instrument.LED_Ctl.CheckResponseList(out resp))
            {
                Console.WriteLine(String.Format("{0}: LED Controller: {1}", Instrument.BoardTime(), resp.ToString()));
                if (logIO)
                    LogInfo(resp.ToString());
            }

            while (Instrument.Rotor.CheckResponseList(out resp))
            {
                Console.WriteLine(String.Format("{0}: Rotor: {1}", Instrument.BoardTime(), resp.ToString()));
                if (logIO)
                    LogInfo(resp.ToString());
            }
            while (Instrument.XStage.CheckResponseList(out resp))
            {
                Console.WriteLine(String.Format("{0}: XStage: {1}", Instrument.BoardTime(), resp.ToString()));
                if (logIO)
                    LogInfo(resp.ToString());
            }
            while (Instrument.YStage.CheckResponseList(out resp))
            {
                Console.WriteLine(String.Format("{0}: YStage: {1}", Instrument.BoardTime(), resp.ToString()));
                if (logIO)
                    LogInfo(resp.ToString());
            }
            while (Instrument.FilterWheel.CheckResponseList(out resp))
            {
                Console.WriteLine(String.Format("{0}: Filter Wheel: {1}", Instrument.BoardTime(), resp.ToString()));
                if (logIO)
                    LogInfo(resp.ToString());
            }
            while (Instrument.Thermistor_Ctl.CheckResponseList(out resp))
            {
                Console.WriteLine(String.Format("{0}: Thermistors: {1}", Instrument.BoardTime(), resp.ToString()));
                if (logIO)
                    LogInfo(resp.ToString());
            }

            while (Instrument.Temp_Ctl.CheckResponseList(out resp))
            {
                Console.WriteLine(String.Format("{0}: PID Controller: {1}", Instrument.BoardTime(), resp.ToString()));
                if (logIO)
                    LogInfo(resp.ToString());
            }

            while (Instrument.TempProgramController.CheckErrorList(out resp))
            {
                Console.WriteLine(String.Format("{0}: PID Program Error: {1}", Instrument.BoardTime(), resp.ToString()));
                if (logIO)
                    LogInfo(String.Format("{0}: PID Program Error: {1}", Instrument.BoardTime(), resp.ToString()));
            }

            while (Instrument.ThermMonitoring.CheckErrorList(out resp))
            {
                Console.WriteLine(String.Format("{0}: Thermistor Monitoring Error: {1}", Instrument.BoardTime(), resp.ToString()));
                if (logIO)
                    LogInfo(String.Format("{0}: Thermistor Monitoring Error: {1}", Instrument.BoardTime(), resp.ToString()));
            }

            while ( Instrument.RotorPositionSequencer.CheckErrorList(out resp))
            {
                Console.WriteLine(String.Format("{0}: Rotor Sequence Error: {1}", Instrument.BoardTime(), resp.ToString()));
                if (logIO)
                    LogInfo(String.Format("{0}: Rotor Sequence Error: {1}", Instrument.BoardTime(), resp.ToString()));
            }

            if (thermTimerOn)
            {
                thermTimerOn = Instrument.ThermMonitoring.ProgramRunning;
                if (!thermTimerOn)
                    Console.WriteLine("Temperature Monitoring has finished");
            }

            if (tempProgOn)
            {
                tempProgOn = Instrument.TempProgramController.ProgramRunning;
                if (!tempProgOn)
                    Console.WriteLine("PID Program has finished");
            }

            if (motorTimerOn)
            {
                motorTimerOn = Instrument.RotorPositionSequencer.ProgramRunning;
                if (!motorTimerOn)
                    Console.WriteLine("Rotor program has finished");

            }
        }
        private static double CurrentTimeSec()
        {
            return (Instrument.BoardTime()) * 0.001;
        }
        private static void StopLogging()
        {
            if (logIO)
            {
                logIO = false;
                DateTime date = DateTime.Now;
                logList0.Add(String.Format("Day: {0:d} Time: {1:g}", date.Date, date.TimeOfDay));

                if (InfoQueue.Count > 0)
                {
                    string[] Ilines = new string[InfoQueue.Count];
                    InfoQueue.CopyTo(Ilines, 0);
                    logList0.Add("");
                    logList0.Add("======================================");
                    for (int n = 0; n < Ilines.Length; n++)
                        logList0.Add(Ilines[n]);
                }
                string[] lines = new string[logList0.Count];
                logList0.CopyTo(lines, 0);
                for (int n = 0; n < lines.Length; n++)
                {
                    lines[n] = lines[n].TrimEnd(whiteSpace);
                    lines[n] = lines[n].TrimStart(whiteSpace);
                }
                System.IO.File.WriteAllLines(@logList0_Name.ToString(), lines);
                Console.WriteLine("Program has stopped logging IO");
            }
        }

        public static bool IsValidFilename(string testName)
        {
            System.Text.RegularExpressions.Regex containsABadCharacter =
                new System.Text.RegularExpressions.Regex("["
                  + System.Text.RegularExpressions.Regex.Escape(new string(System.IO.Path.GetInvalidPathChars())) + "]");
            if (containsABadCharacter.IsMatch(testName)) { return false; };

            // other checks for UNC, drive-path format, etc

            return true;
        }
        static void LoadCommandList(int CmdSize, ref List<string> cmds)
        {
            for (int n = 0; n < CmdSize; n++)
                cmds.Add("");
            cmds[0] = "Print Preloaded Commands";
            cmds[1] = "Log IO {enter as 1 BaseName}";
            cmds[2] = "Stop logging IO";
            cmds[3] = "Get Firmware version [/0FR]";

            cmds[5] = "Save Board Log {Enter as 5 board log name}[from SDPCRProcessor.ReadLogInfo(out string[] lines)]";

            cmds[9] = "Print current Elapsed Time, Temperature, Set Temperature";
            cmds[11] = "Motor 1 (Home) [/1ZR]";
            cmds[12] = "Motor 2 (Home) [/2ZR]";
            cmds[13] = "Motor 3 (Home) [/3ZR]";
            cmds[14] = "Motor 4 (Home) [/4ZR]";
            cmds[15] = "Motor 1 to Absolute Position [/1AXXR] {Enter as 15 XX, where XX is the position >= 0}";
            cmds[16] = "Motor 2 to Absolute Position[/ 2AXXR] {Enter as 16 XX, where XX is the position >= 0}";
            cmds[17] = "Motor 3 to Absolute Position [/3AXXR] {Enter as 17 XX, where XX is the position >= 0}";
            cmds[18] = "Motor 4 to Absolute Position [/4AXXR] {Enter as 18 XX, where XX is the position >= 0}";
           
            cmds[20] = "Start Rotor at 60 rpm, Profile 1 [/1TR /1hR /1V1R /1v60R /P0R]";
            cmds[21] = "Stop Rotor [/1TR]";
            cmds[22] = "Stop X stage [/2TR]";
            cmds[23] = "Stop Y stage [/3TR]";
            cmds[24] = "Stop Filter Wheel [/4TR]";
            cmds[25] = "Motor 1 (Profile 1), velocity X {enter as 25 X, where X is velocity} [/1TR /1hR /1V1R /1vXR]";

            cmds[27] = "Set motor 1 in positive velocity mode [/1P0R]";
            cmds[28] = "Read motor register {Enter as 28 X, where X is the motor number} [/XrR]";
            cmds[29] = "Check if motor busy {Enter as 29 X, where X is the motor number} [/XQR]";

            cmds[30] = "Set up for Absolute Position Loop for motor 1\r\n"
                + "     {Enter as 30 X Y, where X and Y are the velocity mode and velocity [/1TR /1hR /1VXR /1vYR]";
            cmds[31] = "Start Absolute Position Loop for motor 1\r\n"
                + "     {Enter as 31 X Y, where X and Y are the two absolute positions, use /1TR to terminate}\r\n"
                + "     [/1AXAYGR]";
            cmds[39] = "Check home flag {Enter as 39 X, where X is the motor number} [/6XER]";

            cmds[41] = "Set LED levels\r\n     {enter as 41 X Y, where X = 0, ... 5 and 0 < Y <= 100} [/2XlYR ...]";
            cmds[42] = "Turn LED X on {enter as 42 X, where X = 0, ... 5} [/2XS1R]";
            cmds[43] = "Turn LED X off {enter as 43 X, where X = 0, ... 5} [/2XS0R]";
            cmds[50] = "Set All Thremistor wgts [/1xxWyyR]"
                + "     (xx=0... 23) with preloaded weights (yy)";
            cmds[51] = "Set Thermistor wgt [/1xxWyyR].  (enter as 51 xx yy, qhwew xx is the thermistor and yy >= 0 is the weight)";
            cmds[52] = "Read Thermistor Weights [/1XXWR, for XX = 00 .. 23]";
            cmds[53] = "Set reference temp & flashes biases\r\n" +
                "     {enter as 53 X.XX, where X.XX is the reference temperature}\r\n     [/100TXX.XR]";
            cmds[54] = "Clear All Thermistor Bias [/100ZR]";
            cmds[55] = "Read All Thermistor Bias [/100DR]";
            cmds[56] = "Read All calibrated temperatures [/100AR]";

            cmds[60] = "Read Current Temperature (weighted average) [/70R0R]";
            cmds[61] = "Read Target Temperature [/70QR]";
            cmds[62] = "Turn PID controller on [/70C1R]";
            cmds[63] = "Turn PID controller off [/70C0R]";
            cmds[64] = "Get status of PID controller [/70S0R]";

            cmds[67] = "Set Manual Temperature \r\n     {enter as 67 YY.Y, where YY.Y = temperature [/70AYY.YR]";
            cmds[68] = "Turn Manual Temp control on [/70M1R]";
            cmds[69] = "Turn Manual Temp Control off [/70M0R]";
            cmds[80] = "Print PID program headers";
            cmds[81] = "Load program into PID object {enter as 81 N, where N >= 0 is the program number}";
            cmds[82] = "Start PID program {enter as 82 N, where N > 499, is the time interval in ms to record the temperature}";
            cmds[83] = "Stop PID program, use preset to determine if PID controller should be left on";
            cmds[84] = "Stop PID program, leave PID controller on at last temperature";
            cmds[85] = "Print current step of Temp Control Program";
            cmds[86] = "Print current Temp Control Program";
            cmds[87] = "Print current Temp Control Program";
            cmds[88] = "Save output from Temp Control Program (entered as 83 Basename)";

            cmds[90] = "Start monitoring temperature (timestep set by command 7";
            cmds[91] = "Stop monitoring temperature";
            cmds[92] = "Save output from Temp Monitoring (enter as 93 BaseName)";

            cmds[95] = "Print Rotor program headers";
            cmds[96] = "Load Program into Rotor object {enter as 96 N, where N >= 0 is the program number}";
            cmds[97] = "Start Rotor Program {enter as 97 X Y, where X is the time interval (ms)\r\n"
                + " and Y is the number of steps";
            cmds[98] = "Stop Rotor Timer Program";
            cmds[99] = "Print Current Rotor Program";
        }
    }
}
