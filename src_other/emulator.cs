﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics;
//using System.Timers;
using System.IO;


namespace emulator
{// Use this code inside a project created with the Visual C# > Windows Desktop > Console Application template.
 // Replace the code in Program.cs with this code.

    using System;
    using System.IO.Ports;
    using System.Threading;

    public class Emulator
    {
        static char[] whiteSpace = { ' ', '\n', '\r', '\t', ',' };  // space, new line, carriage return, tab, comma
        public static Stopwatch stopW = new Stopwatch();
        static bool _continue;
        static protected bool logIO = false;
        static SerialPort _serialPort;
        static SynchronizedCollection<string> logList0 = new SynchronizedCollection<string>();
        static StringBuilder logList0_Name = new StringBuilder();
        static StringBuilder outputStr = new StringBuilder();
        static string outputDir = "C:\\instrument\\";
        static int delay0 = 5; // milliseconds
        static char[] delimiterChars = { ' ', ',', '\t' }; // space, comma, and tab
        // Stopwatch stopW = new Stopwatch();
        static double temp1 = 10.0;
        static double temp2 = 40.0;
        static string tString =
            "01.1,02.2,03.3,04.4,05.5,06.6,07.7,08.8,09.9,10.0,11.1,12.2,13.3,14.4,15.5,16.6,17.7,18.8,19.9,20.0,21.1,22.2,23.3";

        static int[] weights = {100, 100, 100, 100, 0, 0, 0, 0, 0, 0, 0, 0,
                                    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

        static double[] biases = {0.01, 0.02, 0.03, 0.04, 0.05, 0.06, 0.07, 0.08, 0.09, 0.10, 0.11, 0.12,
                                    -0.01, -0.02, -0.03, -0.04, -0.05, -0.06, -0.07, -0.08, -0.09, -0.10, -0.11, -0.12};
        static int stringListSize = 21;
        //static List<string> inputStrings = new List<string>(stringListSize);
        static List<string> outputStrings = new List<string>(stringListSize);
        static bool[] homeFlag = { true, false, false, false, false };
        static bool[] busyFlag = { true, false, false, false, false };
        static double idleT = 40.5;
        static double lowT = 60.5;
        static double highT = 90.5;
        static double pidT = 41.5;
        static double manualT = 42.5;
        static bool pidOn;
        static bool manualOn;
        static double setPointTemp = idleT;

        public static string RespondOK()
        {
            delay0 += 4;
            delay0 = delay0 % 25 + 2;
            System.Threading.Thread.Sleep(delay0);
            Console.WriteLine(String.Format("RespondOK - delay {0}", delay0));
            _serialPort.Write("/0'\n\r");
            return ",/0'";
        }
        public static string RespondOK(string Msg)
        {
            delay0 += 4;
            delay0 = delay0 % 25 + 2;
            System.Threading.Thread.Sleep(delay0);
            Console.WriteLine(String.Format("RespondOK({0}) - delay {1}", Msg, delay0));
            string res = String.Format("/0{0}", Msg);
            _serialPort.Write(String.Format("{0}\n\r", res));
            return res;
        }
        public static string RespondBad()
        {
            //delay0 += 4;
            //delay0 = delay0 % 25 + 2;
            //System.Threading.Thread.Sleep(delay0);
            _serialPort.Write("/0B\n\r");
            return ",/0B";
        }
        public static string RespondBadOp()
        {
            //delay0 += 4;
            //delay0 = delay0 % 25 + 2;
            //System.Threading.Thread.Sleep(delay0);
            _serialPort.Write("/0C\n\r");
            return ",/0C";
        }
        public static string RespondBusy()
        {
            //delay0 += 4;
            //delay0 = delay0 % 25 + 2;
            //System.Threading.Thread.Sleep(delay0);
            _serialPort.Write("/0S\n\r");
            return ",/0S";
        }
        public static void Main()
        {
            string message;

            int[] motorDirection = new int[5];
            int cmdSize = 100;
            List<string> cmds = new List<string>();
            for (int n = 0; n < cmdSize; n++)
                cmds.Add("");
            //cmds[n] = "";
            long[] lastPositionCommand = new long[5];
            manualOn = false;
            pidOn = false;
            cmds[1] = "Send /0'";
            cmds[2] = "Send /0B";
            cmds[3] = "Send /0C";
            cmds[4] = "Send /0E01";
            cmds[5] = "Send /0E<Code> (enter as 5 <Code>, where Code > 0";
            cmds[6] = "Send /0E<Code> (enter as 6 <Code>, where Code is a string";
            cmds[7] = "Send <string>";
            cmds[8] = "Send empty string (enter as 8, or 8 <int>, where <int> is the number of spaces before the response termination";
            cmds[9] = "Start logging commands received";
            cmds[10] = "Stop logging commands received";


            for (int n = 0; n < stringListSize; n++)
            {
                //inputStrings.Add("");
                outputStrings.Add("");
            }

            outputStrings[0] = "/00.10,0.11,0.12,0.13,0.14,0.15,0.16,0.17,0.18,0.19,"
                + "-0.10,-0.11,-0.12,-0.13,-0.14,-0.15,-0.16,-0.17,-0.18,-0.19,0.00,0.01,0.02,0.03\n\r";
            outputStrings[1] = "/0{0:0.00},01.1,02.2,03.3,04.4,05.5,06.6,07.7,08.8,"
                + "09.9,10.0,11.1,12.2,13.3,14.4,15.5,16.6,17.7,18.8,19.9,20.0,21.1,22.2,23.3\n\r";

            StringComparer stringComparer = StringComparer.OrdinalIgnoreCase;
            //Thread readThread = new Thread(Read);
            // Thread tempMonitorThread = new Thread(TempMontor);
            // Thread cycleThread = new Thread(CycleTemp);
            // Thread program1Thread = new Thread(TempProgram1);
            // SynchronizedCollection<bool> dVelocity = new SynchronizedCollection<bool>();

            // Create a new SerialPort object with default settings.
            _serialPort = new SerialPort();


            // Allow the user to set the appropriate properties.
            _serialPort.Encoding = System.Text.Encoding.GetEncoding(1252);
            _serialPort.PortName = SetPortName(_serialPort.PortName);
            _serialPort.BaudRate = 57600; // SetPortBaudRate(_serialPort.BaudRate);
            _serialPort.Parity = (Parity)Enum.Parse(typeof(Parity), "None", true);
            // SetPortParity(_serialPort.Parity);
            _serialPort.DataBits = 8;
            // int.Parse(dataBits.ToUpperInvariant()); SetPortDataBits(_serialPort.DataBits);
            _serialPort.StopBits = (StopBits)Enum.Parse(typeof(StopBits), "One", true);
            //SetPortStopBits(_serialPort.StopBits);
            _serialPort.Handshake = (Handshake)Enum.Parse(typeof(Handshake), "None", true);
            //SetPortHandshake(_serialPort.Handshake);
            _serialPort.NewLine = "\n";
            _serialPort.DataReceived += new SerialDataReceivedEventHandler(DataReceivedHandler);
            // Set the read/write timeouts
            _serialPort.ReadTimeout = 100;
            _serialPort.WriteTimeout = 200;

            if (!_serialPort.IsOpen)
            {
                _serialPort.Open();
                //Console.WriteLine("Port open now");
                //if (!_serialPort.IsOpen)
                //    Console.WriteLine("Except it still isn't");
            }
            else
                Console.WriteLine("Port is opened");

            if (!_serialPort.IsOpen)
            {
                Console.WriteLine("Cannot open Port");
            }
            stopW.Start();
            Console.WriteLine(_serialPort.PortName);
            Console.WriteLine(_serialPort.BaudRate);
            Console.WriteLine(_serialPort.Parity);
            Console.WriteLine(_serialPort.DataBits);
            Console.WriteLine(_serialPort.StopBits);
            Console.WriteLine(_serialPort.Handshake);

            _continue = true;
            //readThread.Start();

            for (int n = 0; n < 5; n++)
                motorDirection[n] = 0;

            //Console.Write("Name: ");
            //name = Console.ReadLine();
            if (!Directory.Exists(outputDir))
            {
                Console.WriteLine("");
                Console.WriteLine(String.Format(
                    "!!! Directory {0} does not exist. Create before logging information!!!", outputDir));
                Console.WriteLine("");
            }
            Console.WriteLine("Type QUIT to exit");
            //int serialWait1 = 200;
            //int serialWait2 = 100;
            string msg;
            while (_continue)
            {
                message = Console.ReadLine();

                if (stringComparer.Equals("quit", message))
                {
                    _continue = false;
                }
                else
                {
                    int index = message.IndexOf("/");
                    int mLength = message.Length;
                    string str0;
                    if (index == 0 && stringComparer.Equals("R", message.Substring(mLength - 1)))
                    {
                        str0 = String.Format("{0}", message);
                        if (logIO)
                            logList0.Add(String.Format(">> {0}\n", str0));
                        _serialPort.Write(String.Format("{0}\n\r", str0));
                        // String.Format("<{0}>: {1}", name, message));
                    }
                    else if (message.Length > 0)
                    {
                        string[] words = message.Split(delimiterChars);
                        bool isInt = false;
                        int sw;
                        Console.WriteLine(String.Format(" {0}, {1}", words.Length, words[0]));
                        while (words.Length > 0 && !isInt)
                        {
                            isInt = int.TryParse(words[0], out sw);
                            if (!isInt)
                            {
                                //Console.WriteLine(String.Format(" {0}, {1}", words.Length, words[0]));
                                List<string> newWords = new List<string>();
                                if (words.Length > 1)
                                {
                                    for (int m = 1; m < words.Length; m++)
                                        newWords.Add(words[m]);
                                }
                                words = new string[newWords.Count];
                                for (int m = 0; m < newWords.Count; m++)
                                    words[m] = newWords[m];
                            }
                        }
                        if (words.Length > 0)
                        {
                            switch (Convert.ToInt32(words[0]))
                            {
                                //cmds[1] = "aaa<CR><LF>bbb<LF><CR>";
                                //cmds[2] = "aaa<CR><LF>bbb<LF><CR>ccc<CR><LF>ddd<LF><CR>";
                                //cmds[3] = "aaa<CR><LF>bbb<CR><LF>ccc<CR><LF>ddd<LF><CR>";
                                //cmds[4] = "aaa<CR><LF>bbb<CR><LF>ccc<CR><LF>ddd<LF><CR>eee<LF><CR>";
                                //cmds[5] = "aaa<CR><LF>bbb<CR><LF>ccc<CR><LF>ddd<LF><CR>eee<CR><LF>fffff<LF><CR>";
                                case 0:
                                    Console.WriteLine("==========");
                                    for (int n = 0; n < cmdSize; n++)
                                    {
                                        if (cmds[n].Length > 0)
                                            Console.Write(String.Format("{0} - {1}\r\n", n.ToString(" 0"), cmds[n]));
                                    }
                                    break;
                                case 1:
                                    msg = "/0'";
                                    Console.WriteLine(msg);
                                    _serialPort.Write(String.Format("{0}\n\r", msg));
                                    break;
                                case 2:
                                    msg = "/0B";
                                    Console.WriteLine(msg);
                                    _serialPort.Write(String.Format("{0}\n\r", msg));
                                    break;
                                case 3:
                                    msg = "/0C";
                                    Console.WriteLine(msg);
                                    _serialPort.Write(String.Format("{0}\n\r", msg));
                                    break;
                                case 4:
                                    msg = "/0E01";
                                    Console.WriteLine(msg);
                                    _serialPort.Write(String.Format("{0}\n\r", msg));
                                    break;
                                case 5:
                                    if (words.Length > 1)
                                    {
                                        int code;
                                        if (Int32.TryParse(words[1], out code))
                                        {
                                            if (code > 0)
                                            {
                                                msg = String.Format("/0E{0}", code);
                                                Console.WriteLine(msg);
                                                _serialPort.Write(String.Format("{0}\n\r", msg));
                                            }
                                        }
                                    }
                                    break;
                                case 6:
                                    if (words.Length > 1)
                                    {
                                        msg = String.Format("/0E{0}", words[1]);
                                        Console.WriteLine(msg);
                                        _serialPort.Write(String.Format("{0}\n\r", msg));
                                    }
                                    break;
                                case 7:
                                    if (words.Length > 1)
                                    {
                                        msg =words[1];
                                        Console.WriteLine(msg);
                                        _serialPort.Write(String.Format("{0}\n\r", msg));
                                    }
                                    break;
                                case 8:
                                    if (words.Length > 1)
                                    {
                                        msg = words[1];
                                        int w1;
                                        if (Int32.TryParse(msg, out w1) && w1 > 0)
                                        {
                                            StringBuilder sb = new StringBuilder();
                                            for (int nn = 0; nn <= w1; nn++)
                                                sb.Append(" ");
                                            sb.Append("\n\r");
                                            msg = sb.ToString();
                                        }
                                        else
                                            msg = "\n\r";
                                    }
                                    else
                                        msg = "\n\r";
                                        Console.WriteLine("Sending empty string");
                                        _serialPort.Write(msg);
                                    break;
                                case 9:
                                    if (!logIO && words.Length > 1)
                                    {
                                        logList0_Name.Clear();
                                        logList0_Name.Append(String.Format("{0}{1}_log.csv", outputDir, words[1]));
                                        if (IsValidFilename(logList0_Name.ToString()) && !File.Exists(logList0_Name.ToString()))
                                        {
                                            logList0.Clear();
                                            Console.WriteLine(String.Format("IO logging started, output to {0}",
                                                logList0_Name.ToString()));
                                            DateTime date = DateTime.Now;
                                            logList0.Add(String.Format("Day: {0:d} Time: {1:g}", date.Date, date.TimeOfDay));
                                            logIO = true;
                                        }
                                        else if (File.Exists(logList0_Name.ToString()))
                                            Console.WriteLine(String.Format("{0} already exists, not overwritten",
                                                logList0_Name.ToString()));
                                        else
                                            Console.WriteLine(String.Format("{0} is not a valid file name",
                                                logList0_Name.ToString()));
                                    }
                                    else if (logIO)
                                    {
                                        Console.WriteLine("Program is already logging IO, no change");
                                    }
                                    else
                                    {
                                        Console.WriteLine("No log file named");
                                    }
                                    break;
                                case 10:
                                    if (logIO)
                                        StopLogging();
                                    else
                                    {
                                        Console.WriteLine("Program was not logging commands, no change");
                                    }
                                    break;
                                //case 10:
                                //    temp1 += 0.1;
                                //    msg = String.Format("/70R0R\r\n/0{0}", temp1);
                                //    Console.WriteLine(msg);
                                //    _serialPort.Write(String.Format("{0}\n\r", msg));
                                //    break;
                                //case 11:
                                //    temp2 += 0.1;
                                //    msg = String.Format("/100AR\r\n/0{0},{1}", temp2, tString);
                                //    Console.WriteLine(msg);
                                //    _serialPort.Write(String.Format("{0}\n\r", msg));
                                //    break;

                                default:
                                    Console.WriteLine("No command defined for that number");
                                    break;
                            }
                        }
                    }
                }
            }
            //readThread.Join();
            _serialPort.Close();
        }

        private static void DataReceivedHandler(object sender,
            SerialDataReceivedEventArgs e)
        {
            SerialPort spL = (SerialPort)sender;
            string message;
            try
            {
                message = spL.ReadLine();
                //message = message.Replace("\r", " | ");
                string st;

                if (message.Length > 0)
                {
                    string logStr = String.Format("{0:0.00},", (double)stopW.ElapsedMilliseconds / 1000.0);
                    Console.WriteLine(String.Format("message.Length>0, {0}", message));
                    //Console.WriteLine("");
                    _serialPort.Write(String.Format("{0}\r\n", message));
                    //bool foundString = false;

                    int idx = message.IndexOf("/");
                    int idy;
                    int idz;
                    Console.WriteLine(String.Format("1st idx = {0}", idx));
                    Console.Out.Flush();
                    if (idx >= 0)
                    {
                        Console.WriteLine(String.Format("idx >= 0 : {0}", message));
                        //if (message.Length - idx > 5 && (idy = message.IndexOf("/1")) >= 0 && (idz = message.IndexOf("W")) == idy + 4)
                        //{
                        //    string t = message.Substring(idy + 1, 3);
                        //    Console.WriteLine(String.Format("     Thermistor string: {0}", t));
                        //}
                        if (message.Length - idx > 5 && (idy = message.IndexOf("/1")) >= 0 && (idz = message.IndexOf("WR")) == idy + 4)
                        {
                            string t = message.Substring(idy + 1, 3);
                            Console.WriteLine(String.Format("1     Thermistor string: {0}", t));
                            int w1;
                            if (Int32.TryParse(t, out w1))
                            {
                                w1 -= 100;
                                Console.WriteLine(String.Format("w1 = {0}", w1));
                                if (w1 >= 0 && w1 < 24)
                                {
                                    delay0 += 4;
                                    delay0 = delay0 % 25 + 1;
                                    System.Threading.Thread.Sleep(delay0);
                                    _serialPort.Write(String.Format("/0{0}\n\r", weights[w1]));
                                    Console.WriteLine(String.Format("/0{0}", weights[w1]));
                                    Console.Out.Flush();
                                    logStr += String.Format("/{0}WR", w1 + 100) + String.Format(",/0{0}", weights[w1]);
                                }
                                else
                                {
                                    Console.WriteLine("/0C");
                                    logStr += message + RespondBadOp();
                                }
                            }
                            else
                            {
                                Console.WriteLine("/0B");
                                logStr += message + RespondBad();
                            }
                        }
                        else if (message.Length - idx > 5 && (idy = message.IndexOf("/1")) >= 0 && (idz = message.IndexOf("W")) == idy + 4)
                        {
                            string t = message.Substring(idy + 1, 3);
                            Console.WriteLine(String.Format("2     Thermistor string: {0}", t));
                            int w1;
                            if (Int32.TryParse(t, out w1))
                            {
                                w1 -= 100;
                                Console.WriteLine(String.Format("w1 = {0}", w1));
                                int d2 = message.Length;
                                if (w1 >= 0 && w1 < 24 && d2 - idy - 6 > 0)
                                {
                                    int w2;
                                    t = message.Substring(idy + 5, d2 - idy - 6);
                                    Console.WriteLine(String.Format("idy: {0} / d2: {1} / t: {2} / message: {3} / length: {4}",
                                        idy, d2, t, message, message.Length));
                                    if (Int32.TryParse(t, out w2))
                                    {
                                        weights[w1] = w2;
                                        delay0 += 4;
                                        delay0 = delay0 % 25 + 1;
                                        System.Threading.Thread.Sleep(delay0);
                                        _serialPort.Write("/0'\n\r");
                                        Console.WriteLine("/0'");
                                        Console.Out.Flush();
                                        logStr += message.Substring(idy) + ",/0'";
                                    }
                                    else
                                    {
                                        Console.WriteLine("/0B");
                                        logStr += message + RespondBad();
                                    }
                                }
                                else
                                {
                                    Console.WriteLine("/0C");
                                    logStr += message + RespondBadOp();
                                }
                            }
                            else
                            {
                                Console.WriteLine("/0B");
                                logStr += message + RespondBad();
                            }
                        }
                        else if ((idx = message.IndexOf("/100")) >= 0)
                        {
                            if (message.IndexOf("/100DR") == idx)
                            {
                                delay0 += 4;
                                delay0 = delay0 % 25 + 1;
                                System.Threading.Thread.Sleep(delay0);
                                _serialPort.Write(outputStrings[0]);
                                Console.Write(outputStrings[0]);
                                Console.Out.Flush();
                                logStr += "/100DR," + outputStrings[0];
                            }
                            else if (message.IndexOf("/100AR") == idx)
                            {
                                delay0 += 4;
                                delay0 = delay0 % 25 + 1;
                                System.Threading.Thread.Sleep(delay0);
                                temp2 += 0.1;
                                string msg2 = String.Format(outputStrings[1], temp2);
                                _serialPort.Write(msg2);
                                Console.Write(msg2);
                                Console.Out.Flush();
                                logStr += "/100AR," + msg2.TrimEnd(whiteSpace);
                            }
                            else if (message.IndexOf("/100T") == idx)
                            {
                                int idR = message.IndexOf("R");
                                string m = message.Substring(idx + 5, idR - idx - 5);
                                double refT;
                                if (Double.TryParse(m, out refT))
                                {
                                    double delta = 0.01;
                                    int c = (int)refT % 20 + 10;
                                    if (refT > 0.0)
                                    {
                                        outputStr.Clear();
                                        outputStr.Append("/0");
                                        for (int n = 0; n < biases.Length; n++)
                                        {
                                            if (n > c)
                                            {
                                                biases[n] = delta;
                                                delta += 0.01;
                                            }
                                            else
                                            {
                                                biases[n] = -delta;
                                                delta -= 0.01;
                                            }
                                            outputStr.Append(String.Format("{0:0.00}", biases[n]));
                                            if (n < biases.Length - 1)
                                                outputStr.Append(",");
                                            else
                                                outputStr.Append("\n\r");
                                        }
                                        outputStrings[0] = outputStr.ToString();
                                    }
                                }
                            }
                            else if (message.IndexOf("/100ZR") == idx)
                            {
                                outputStr.Clear();
                                outputStr.Append("/0");
                                for (int n = 0; n < biases.Length; n++)
                                {
                                    biases[n] = 0.0;
                                    if (n < biases.Length - 1)
                                        outputStr.Append("0.00,");
                                    else
                                        outputStr.Append("0.00\n\r");
                                }
                            }
                            else
                            {
                                Console.Write("000 - /0B\n\r");
                                logStr += message + RespondBad();
                            }
                        }
                        else if ((idx = message.IndexOf("/70")) >= 0)
                        {

                            Console.WriteLine(String.Format("00- 70/{0}, idx = {1}", message, idx));
                            Console.Out.Flush();
                            st = message.Substring(idx + 3);
                            if (message.IndexOf("/70R0R") == idx)
                            {
                                temp1 += 0.1;
                                string msg1 = String.Format("/0{0:0.00}\n\r", temp1);
                                //Console.Write(msg1);
                                //Console.Out.Flush();
                                delay0 += 4;
                                delay0 = delay0 % 25 + 1;
                                System.Threading.Thread.Sleep(50);
                                _serialPort.Write(msg1);
                                //_serialPort.Write(String.Format("{0}\n\r", msg1));
                                Console.Write(msg1);
                                logStr += "/70R0R," + msg1.TrimEnd(whiteSpace);
                            }
                            else if (message.IndexOf("/70M0R") == idx)
                            {
                                RespondOK();
                                logStr += "70M0R,/0'";
                                manualOn = false;
                                setPointTemp = pidT;
                            }
                            else if (message.IndexOf("/70M1R") == idx)
                            {
                                RespondOK();
                                logStr += "70M1R,/0'";
                                manualOn = true;
                                setPointTemp = manualT;
                            }
                            else if (message.IndexOf("/70S0R") == idx)
                            {
                                //RespondOK();
                                if (pidOn)
                                {
                                    _serialPort.Write("/01\n\r");
                                    logStr += "70S0R,/01";
                                }
                                else
                                {
                                    _serialPort.Write("/00\n\r");
                                    logStr += "70S0R,/00";
                                }

                            }
                            else if (message.IndexOf("/70C0R") == idx)
                            {
                                RespondOK();
                                logStr += "70C0R,/0'";
                                pidOn = false;
                                manualOn = false;
                            }
                            else if (message.IndexOf("/70C1R") == idx)
                            {
                                RespondOK();
                                logStr += "70C1R,/0'";
                                pidOn = true;
                            }
                            else if (message.IndexOf("/70T0R") == idx)
                            {
                                RespondOK();
                                pidT = idleT;
                                if ( !manualOn)
                                    setPointTemp = idleT;
                                logStr += "70T0R,/0'";
                            }
                            else if (message.IndexOf("/70T1R") == idx)
                            {
                                RespondOK();
                                pidT = lowT;
                                if ( !manualOn)
                                    setPointTemp = lowT;
                                logStr += "70T1R,/0'";
                            }
                            else if (message.IndexOf("/70T2R") == idx)
                            {
                                RespondOK();
                                pidT = highT;
                                if ( !manualOn)
                                    setPointTemp = highT;
                                logStr += "70T2R,/0'";
                            }
                            else if (message.IndexOf("/70F0T") == idx)
                            {
                                delay0 += 4;
                                delay0 = delay0 % 25 + 1;
                                int id = message.IndexOf("/70F0T");
                                int ln = message.Length - (id + 6) - 1;
                                Console.Write(String.Format("/70F0T idx:{0}, ln:{1}, {2}", idx, ln, message));
                                System.Threading.Thread.Sleep(delay0);
                                string st1 = "";
                                double d1;
                                if (ln > 0)
                                {
                                    st1 = message.Substring(id + 6, ln);
                                    if (Double.TryParse(st1, out d1))
                                    {
                                        logStr += String.Format("/70F0T{0:0.00}", d1);
                                        if (d1 > 25.0)
                                        {
                                            //_serialPort.Write("/0'\n\r");
                                            Console.WriteLine(" - Idle Temp set to {0}", d1);
                                            idleT = d1;
                                            logStr += RespondOK();
                                        }
                                        else
                                        {
                                            //_serialPort.Write("/0C\n\r");
                                            logStr += RespondBadOp();
                                        }
                                    }
                                    else
                                    {
                                        //_serialPort.Write("/0C\n\r");
                                        logStr += RespondBadOp();
                                    }
                                }
                                else
                                {
                                    //_serialPort.Write("/0B\n\r");
                                    logStr += RespondBad();
                                }
                            }
                            else if (message.IndexOf("/70F1T") == idx)
                            {
                                delay0 += 4;
                                delay0 = delay0 % 25 + 1;
                                int id = message.IndexOf("/70F1T");
                                int ln = message.Length - (id + 6) - 1;
                                Console.Write(String.Format("/70F1T idx:{0}, ln:{1}, {2}", idx, ln, message));
                                System.Threading.Thread.Sleep(delay0);
                                string st1 = "";
                                double d1;
                                if (ln > 0)
                                {
                                    st1 = message.Substring(id + 6, ln);
                                    if (Double.TryParse(st1, out d1))
                                    {
                                        logStr += String.Format("/70F1T{0:0.00}", d1);
                                        if (d1 > 25.0)
                                        {
                                            //_serialPort.Write("/0'\n\r");
                                            Console.WriteLine(" - Low Temp set to {0}", d1);
                                            lowT = d1;
                                            logStr += RespondOK();
                                        }
                                        else
                                        {
                                            //_serialPort.Write("/0C\n\r");
                                            logStr += RespondBadOp();
                                        }
                                    }
                                    else
                                    {
                                        //_serialPort.Write("/0C\n\r");
                                        logStr += RespondBadOp();
                                    }
                                }
                                else
                                {
                                    //_serialPort.Write("/0B\n\r");
                                    logStr += RespondBad();
                                }
                            }
                            else if (message.IndexOf("/70F2T") == idx)
                            {
                                delay0 += 4;
                                delay0 = delay0 % 25 + 1;
                                int id = message.IndexOf("/70F2T");
                                int ln = message.Length - (id + 6) - 1;
                                Console.Write(String.Format("/70F2T idx:{0}, ln:{1}, {2}", idx, ln, message));
                                System.Threading.Thread.Sleep(delay0);
                                string st1 = "";
                                double d1;
                                if (ln > 0)
                                {
                                    st1 = message.Substring(id + 6, ln);
                                    if (Double.TryParse(st1, out d1))
                                    {
                                        logStr += String.Format("/70F2T{0:0.00}", d1);
                                        if (d1 > 25.0)
                                        {
                                            //_serialPort.Write("/0'\n\r");
                                            Console.WriteLine(" - High Temp set to {0}", d1);
                                            highT = d1;
                                            logStr += RespondOK();
                                        }
                                        else
                                        {
                                            //_serialPort.Write("/0C\n\r");
                                            logStr += RespondBadOp();
                                        }
                                    }
                                    else
                                    {
                                        //_serialPort.Write("/0C\n\r");
                                        logStr += RespondBadOp();
                                    }
                                }
                                else
                                {
                                    //_serialPort.Write("/0B\n\r");
                                    logStr += RespondBadOp();
                                }
                            }

                            else if (message.IndexOf("/70F0R") == idx)
                            {
                                string msg = String.Format("{0:0.00}", idleT);
                                RespondOK(msg);
                                logStr += String.Format("70F0R,/0{0}", msg);
                            }
                            else if (message.IndexOf("/70F1R") == idx)
                            {
                                string msg = String.Format("{0:0.00}", lowT);
                                RespondOK(msg);
                                logStr += String.Format("70F1R,/0{0}", msg);
                            }
                            else if (message.IndexOf("/70F2R") == idx)
                            {
                                string msg = String.Format("{0:0.00}", highT);
                                RespondOK(msg);
                                logStr += String.Format("70F2R,/0{0}", msg);
                            }
                            else if (message.IndexOf("/70A") == idx)
                            {
                                delay0 += 4;
                                delay0 = delay0 % 25 + 1;
                                int id = message.IndexOf("/70A");
                                int ln = message.Length - (id + 4) - 1;
                                Console.WriteLine(String.Format("idx:{0}, ln:{1}, {2}", idx, ln, message));
                                Console.Out.Flush();
                                System.Threading.Thread.Sleep(50);
                                string st1 = "";
                                double d1;
                                if (ln > 0)
                                {
                                    st1 = message.Substring(id + 4, ln);
                                    if (Double.TryParse(st1, out d1))
                                    {
                                        logStr += String.Format("/70A{0:0.00}R", d1);
                                        if (d1 > 25.0)
                                        {
                                            //_serialPort.Write("/0'\n\r");
                                            manualT = d1;
                                            if (manualOn)
                                                setPointTemp = manualT;
                                            logStr += RespondOK();
                                        }
                                        else
                                        {
                                            //_serialPort.Write("/0C\n\r");
                                            logStr += RespondBadOp();
                                        }
                                    }
                                    else
                                    {
                                        //_serialPort.Write("/0C\n\r");
                                        logStr += RespondBadOp();
                                    }
                                }
                                else
                                {
                                    //_serialPort.Write("/0B\n\r");
                                    logStr += RespondBad();
                                }
                            }
                            else if (message.IndexOf("/70QR") == idx)
                            {
                                delay0 += 4;
                                delay0 = delay0 % 25 + 1;
                                System.Threading.Thread.Sleep(delay0);
                                string msg = String.Format("{0:0.00}", setPointTemp);
                                RespondOK(msg);
                                logStr += "/70QR," + String.Format("/0{0}\n\r", msg);
                            }
                            else
                            {
                                RespondBad();
                                logStr += message + ",/0B";
                            }
                        }
                        else if ((idx = message.IndexOf("/20")) >= 0)
                        {
                            st = message.Substring(idx + 3);
                            logStr += message.Substring(idx);
                            Console.WriteLine(String.Format("00- 20/{0}", st));
                            if (message.IndexOf("/20L") == idx)
                            {
                                //RespondOK();
                                logStr += RespondOK();
                            }
                            else if (message.IndexOf("/20l") == idx)
                            {
                                //RespondOK();
                                logStr += RespondOK();
                            }
                            else if (message.IndexOf("/20S") == idx)
                            {
                                //RespondOK();
                                logStr += RespondOK();
                            }
                            else
                            {
                                //RespondBad();
                                logStr += RespondBad();
                            }
                        }
                        else if ((idx = message.IndexOf("/21")) >= 0)
                        {
                            st = message.Substring(idx + 3);
                            logStr += message.Substring(idx);
                            Console.WriteLine(String.Format("00- 21/{0}", st));
                            if (message.IndexOf("/21L") == idx)
                            {
                                //RespondOK();
                                logStr += RespondOK();
                            }
                            else if (message.IndexOf("/21l") == idx)
                            {
                                //RespondOK();
                                logStr += RespondOK();
                            }
                            else if (message.IndexOf("/21S") == idx)
                            {
                                //RespondOK();
                                logStr += RespondOK();
                            }
                            else
                            {
                                //RespondBad();
                                logStr += RespondBad();
                            }
                        }
                        else if ((idx = message.IndexOf("/22")) >= 0)
                        {
                            st = message.Substring(idx + 3);
                            logStr += message.Substring(idx);
                            Console.WriteLine(String.Format("00- 22/{0}", st));
                            if (message.IndexOf("/22L") == idx)
                            {
                                //RespondOK();
                                logStr += RespondOK();
                            }
                            else if (message.IndexOf("/22l") == idx)
                            {
                                //RespondOK();
                                logStr += RespondOK();
                            }
                            else if (message.IndexOf("/22S") == idx)
                            {
                                //RespondOK();
                                logStr += RespondOK();
                            }
                            else
                            {
                                //RespondOK();
                                logStr += RespondBad();
                            }
                        }
                        else if ((idx = message.IndexOf("/23")) >= 0)
                        {
                            st = message.Substring(idx + 3);
                            logStr += message.Substring(idx);
                            Console.WriteLine(String.Format("00- 23/{0}", st));
                            if (message.IndexOf("/23L") == idx)
                            {
                                //RespondOK();
                                logStr += RespondOK();
                            }
                            else if (message.IndexOf("/23l") == idx)
                            {
                                //RespondOK();
                                logStr += RespondOK();
                            }
                            else if (message.IndexOf("/23S") == idx)
                            {
                                //RespondOK();
                                logStr += RespondOK();
                            }
                            else
                            {
                                //RespondOK();
                                logStr += RespondBad();
                            }
                        }
                        else if ((idx = message.IndexOf("/24")) >= 0)
                        {
                            st = message.Substring(idx + 3);
                            logStr += message.Substring(idx);
                            Console.WriteLine(String.Format("00- 24/{0}", st));
                            if (message.IndexOf("/24L") == idx)
                            {
                                //RespondOK();
                                logStr += RespondOK();
                            }
                            else if (message.IndexOf("/24l") == idx)
                            {
                                //RespondOK();
                                logStr += RespondOK();
                            }
                            else if (message.IndexOf("/24S") == idx)
                            {
                                //RespondOK();
                                logStr += RespondOK();
                            }
                            else
                            {
                                //RespondOK();
                                logStr += RespondBad();
                            }
                        }
                        else if ((idx = message.IndexOf("/25")) >= 0)
                        {
                            st = message.Substring(idx + 3);
                            logStr += message.Substring(idx);
                            Console.WriteLine(String.Format("00- 25/{0}", st));
                            if (message.IndexOf("/25L") == idx)
                            {
                                //RespondOK();
                                logStr += RespondOK();
                            }
                            else if (message.IndexOf("/25l") == idx)
                            {
                                //RespondOK();
                                logStr += RespondOK();
                            }
                            else if (message.IndexOf("/25S") == idx)
                            {
                                //RespondOK();
                                logStr += RespondOK();
                            }
                            else
                            {
                                //RespondOK();
                                logStr += RespondBad();
                            }
                        }
                        else if ((idx = message.IndexOf("/6")) >= 0)
                        {
                            if (message.IndexOf("/61ER") == idx)
                            {
                                if (homeFlag[1])
                                {
                                    _serialPort.Write("/01\n\r");
                                    logStr += "/61ER,/01";
                                }
                                else
                                {
                                    _serialPort.Write("/00\n\r");
                                    logStr += "/61ER,/00";
                                }
                                homeFlag[1] = !homeFlag[1];
                            }
                            else if (message.IndexOf("/62ER") == idx)
                            {
                                if (homeFlag[2])
                                {
                                    _serialPort.Write("/01\n\r");
                                    logStr += "/62ER,/01";
                                }
                                else
                                {
                                    _serialPort.Write("/00\n\r");
                                    logStr += "/62ER,/00";
                                }
                                homeFlag[2] = !homeFlag[2];
                            }
                            else if (message.IndexOf("/63ER") == idx)
                            {
                                if (homeFlag[3])
                                {
                                    _serialPort.Write("/01\n\r");
                                    logStr += "/63ER,/01";
                                }
                                else
                                {
                                    _serialPort.Write("/00\n\r");
                                    logStr += "/63ER,/00";
                                }
                                homeFlag[3] = !homeFlag[3];
                            }
                            else if (message.IndexOf("/64ER") == idx)
                            {
                                if (homeFlag[4])
                                {
                                    _serialPort.Write("/01\n\r");
                                    logStr += "/64ER,/01";
                                }
                                else
                                {
                                    _serialPort.Write("/00\n\r");
                                    logStr += "/64ER,/00";
                                }
                                homeFlag[4] = !homeFlag[4];
                            }
                            else
                            {
                                logStr += message.Substring(idx) + RespondBadOp();
                            }
                        }
                        else if ((idx = message.IndexOf("/4")) >= 0)
                        {
                            logStr += message.Substring(idx);
                            if (message.IndexOf("/4IR") == idx)
                            {
                                //RespondOK();
                                logStr += RespondOK();
                            }
                            else if (message.IndexOf("/4hR") == idx)
                            {
                                //RespondOK();
                                logStr += RespondOK();
                            }
                            else if (message.IndexOf("/4V") == idx)
                            {
                                //RespondOK();
                                logStr += RespondOK();
                            }
                            else if (message.IndexOf("/4v") == idx)
                            {
                                //RespondOK();
                                logStr += RespondOK();
                            }
                            else if (message.IndexOf("/4ZR") == idx)
                            {
                                //RespondOK();
                                homeFlag[4] = true;
                                logStr += RespondOK();
                            }
                            else if (message.IndexOf("/4TR") == idx)
                            {
                                //RespondOK();
                                homeFlag[4] = false;
                                logStr += RespondOK();
                            }
                            else if (message.IndexOf("/4f") == idx)
                            {
                                //RespondOK();
                                logStr += RespondOK();
                            }
                            else if (message.IndexOf("/4A") == idx)
                            {
                                //RespondOK();
                                homeFlag[4] = false;
                                logStr += RespondOK();
                            }
                            else if (message.IndexOf("/4B") == idx)
                            {
                                //RespondOK();
                                logStr += RespondOK();
                            }
                            else if (message.IndexOf("/4P0R") == idx)
                            {
                                //RespondOK();
                                homeFlag[4] = false;
                                logStr += RespondOK();
                            }
                            else if (message.IndexOf("/4D0R") == idx)
                            {
                                //RespondOK();
                                homeFlag[4] = false;
                                logStr += RespondOK();
                            }
                            else if (message.IndexOf("/4rR") == idx)
                            {
                                _serialPort.Write("Registers for motor 4\r\n");
                                _serialPort.Write("=====================\r\n");
                                _serialPort.Write("lines of motor 4 info\r\n");
                                _serialPort.Write("/0'\n\r");
                                logStr += "Registers for motor 4,/0'";
                            }
                            else if (message.IndexOf("/4QR") == idx)
                            {
                                if (busyFlag[4])
                                {
                                    _serialPort.Write("/01\n\r");
                                    logStr += "/4QR,/01";
                                }
                                else
                                {
                                    _serialPort.Write("/00\n\r");
                                    logStr += "/4QR,/00";
                                }
                                busyFlag[4] = !busyFlag[4];
                            }
                            else
                            {
                                logStr += RespondBad();
                            }
                        }
                        else if ((idx = message.IndexOf("/3")) >= 0)
                        {
                            logStr += message.Substring(idx);
                            if (message.IndexOf("/3IR") == idx)
                            {
                                //RespondOK();
                                logStr += RespondOK();
                            }
                            else if (message.IndexOf("/3hR") == idx)
                            {
                                //RespondOK();
                                logStr += RespondOK();
                            }
                            else if (message.IndexOf("/3V") == idx)
                            {
                                //RespondOK();
                                logStr += RespondOK();
                            }
                            else if (message.IndexOf("/3v") == idx)
                            {
                                //RespondOK();
                                logStr += RespondOK();
                            }
                            else if (message.IndexOf("/3ZR") == idx)
                            {
                                //RespondOK();
                                homeFlag[3] = true;
                                logStr += RespondOK();
                            }
                            else if (message.IndexOf("/3TR") == idx)
                            {
                                //RespondOK();
                                homeFlag[3] = false;
                                logStr += RespondOK();
                            }
                            else if (message.IndexOf("/3f") == idx)
                            {
                                //RespondOK();
                                logStr += RespondOK();
                            }
                            else if (message.IndexOf("/3A") == idx)
                            {
                                //RespondOK();
                                homeFlag[3] = false;
                                logStr += RespondOK();
                            }
                            else if (message.IndexOf("/3B") == idx)
                            {
                                //RespondOK();
                                logStr += RespondOK();
                            }
                            else if (message.IndexOf("/3P0R") == idx)
                            {
                                //RespondOK();
                                homeFlag[3] = false;
                                logStr += RespondOK();
                            }
                            else if (message.IndexOf("/3D0R") == idx)
                            {
                                //RespondOK();
                                homeFlag[3] = false;
                                logStr += RespondOK();
                            }
                            else if (message.IndexOf("/3rR") == idx)
                            {
                                _serialPort.Write("Registers for motor 3\r\n");
                                _serialPort.Write("=====================\r\n");
                                _serialPort.Write("lines of motor 3 info\r\n");
                                _serialPort.Write("/0'\n\r");
                                logStr += "Registers for motor 3,/0'";
                            }
                            else if (message.IndexOf("/3QR") == idx)
                            {
                                if (busyFlag[3])
                                {
                                    _serialPort.Write("/01\n\r");
                                    logStr += "/2QR,/01";
                                }
                                else
                                {
                                    _serialPort.Write("/00\n\r");
                                    logStr += "/3QR,/00";
                                }
                                busyFlag[3] = !busyFlag[3];
                            }
                            else
                            {
                                logStr += RespondBad();
                            }
                        }
                        else if ((idx = message.IndexOf("/2")) >= 0)
                        {
                            logStr += message.Substring(idx);
                            if (message.IndexOf("/2IR") == idx)
                            {
                                //RespondOK();
                                logStr += RespondOK();
                            }
                            else if (message.IndexOf("/2hR") == idx)
                            {
                                //RespondOK();
                                logStr += RespondOK();
                            }
                            else if (message.IndexOf("/2V") == idx)
                            {
                                //RespondOK();
                                logStr += RespondOK();
                            }
                            else if (message.IndexOf("/2v") == idx)
                            {
                                //RespondOK();
                                logStr += RespondOK();
                            }
                            else if (message.IndexOf("/2ZR") == idx)
                            {
                                //RespondOK();
                                homeFlag[2] = true;
                                logStr += RespondOK();
                            }
                            else if (message.IndexOf("/2TR") == idx)
                            {
                                //RespondOK();
                                homeFlag[2] = false;
                                logStr += RespondOK();
                            }
                            else if (message.IndexOf("/2f") == idx)
                            {
                                //RespondOK();
                                logStr += RespondOK();
                            }
                            else if (message.IndexOf("/2A") == idx)
                            {
                                //RespondOK();
                                homeFlag[2] = false;
                                logStr += RespondOK();
                            }
                            else if (message.IndexOf("/2B") == idx)
                            {
                                //RespondOK();
                                logStr += RespondOK();
                            }
                            else if (message.IndexOf("/2P0R") == idx)
                            {
                                //RespondOK();
                                homeFlag[2] = false;
                                logStr += RespondOK();
                            }
                            else if (message.IndexOf("/2D0R") == idx)
                            {
                                //RespondOK();
                                homeFlag[2] = false;
                                logStr += RespondOK();
                            }
                            else if (message.IndexOf("/2rR") == idx)
                            {
                                _serialPort.Write("Registers for motor 2\r\n");
                                _serialPort.Write("=====================\r\n");
                                _serialPort.Write("lines of motor 2 info\r\n");
                                _serialPort.Write("/0'\n\r");
                                logStr += "Registers for motor 2,/0'";
                            }
                            else if (message.IndexOf("/2QR") == idx)
                            {
                                if (busyFlag[2])
                                {
                                    _serialPort.Write("/01\n\r");
                                    logStr += "/2QR,/01";
                                }
                                else
                                {
                                    _serialPort.Write("/00\n\r");
                                    logStr += "/2QR,/00";
                                }
                                busyFlag[2] = !busyFlag[2];
                            }
                            else
                            {
                                logStr += RespondBad();
                            }
                        }
                        else if ((idx = message.IndexOf("/1")) >= 0)
                        {
                            logStr += message.Substring(idx);
                            if (message.IndexOf("/1IR") == idx)
                            {
                                //RespondOK();
                                logStr += RespondOK();
                            }
                            else if (message.IndexOf("/1hR") == idx)
                            {
                                //RespondOK();
                                logStr += RespondOK();
                            }
                            else if (message.IndexOf("/1V") == idx)
                            {
                                //RespondOK();
                                logStr += RespondOK();
                            }
                            else if (message.IndexOf("/1v") == idx)
                            {
                                //RespondOK();
                                logStr += RespondOK();
                            }
                            else if (message.IndexOf("/1ZR") == idx)
                            {
                                //RespondOK();
                                homeFlag[1] = true;
                                logStr += RespondOK();
                            }
                            else if (message.IndexOf("/1TR") == idx)
                            {
                                //RespondOK();
                                homeFlag[1] = false;
                                logStr += RespondOK();
                            }
                            else if (message.IndexOf("/1f") == idx)
                            {
                                //RespondOK();
                                logStr += RespondOK();
                            }
                            else if (message.IndexOf("/1A") == idx)
                            {
                                //RespondOK();
                                homeFlag[1] = false;
                                logStr += RespondOK();
                            }
                            else if (message.IndexOf("/1B") == idx)
                            {
                                //RespondOK();
                                logStr += RespondOK();
                            }
                            else if (message.IndexOf("/1P0R") == idx)
                            {
                                //RespondOK();
                                homeFlag[1] = false;
                                logStr += RespondOK();
                            }
                            else if (message.IndexOf("/1D0R") == idx)
                            {
                                //RespondOK();
                                homeFlag[1] = false;
                                logStr += RespondOK();
                            }
                            else if (message.IndexOf("/1rR") == idx)
                            {
                                _serialPort.Write("Registers for motor 1\r\n");
                                _serialPort.Write("=====================\r\n");
                                _serialPort.Write("lines of motor 1 info\r\n");
                                _serialPort.Write("/0'\n\r");
                                logStr += "Registers for motor 1,/0'";
                            }
                            else if (message.IndexOf("/1QR") == idx)
                            {
                                if (busyFlag[1])
                                {
                                    _serialPort.Write("/01\n\r");
                                    logStr += "/1QR,/01";
                                }
                                else
                                {
                                    _serialPort.Write("/00\n\r");
                                    logStr += "/1QR,/00";
                                }
                                busyFlag[1] = !busyFlag[1];
                            }
                            else
                            {
                                logStr += RespondBad();
                            }
                        }
                        else if ((idx = message.IndexOf("/0")) >= 0)
                        {
                            logStr += message.Substring(idx);
                            if (message.IndexOf("/0FR") == idx)
                            {
                                delay0 += 4;
                                delay0 = delay0 % 25 + 1;
                                System.Threading.Thread.Sleep(delay0);
                                _serialPort.Write("/01.0.9\n\r");
                                logStr += ",/01.0.9";
                            }
                            else
                            {
                                logStr += RespondBad();
                            }
                        }
                        else
                        {
                            Console.WriteLine(String.Format("No Match: {0}", message));
                        }
                    }
                    else
                    {
                        logStr += message + ",?";
                    }

                    if (logIO)
                    {
                        logList0.Add(logStr);
                    }
                }
            }
            catch (TimeoutException)
            {
                message = spL.ReadExisting();
                Console.WriteLine(String.Format("? {0}", message));
            }
        }

        // Display Port values and prompt user to enter a port.
        public static string SetPortName(string defaultPortName)
        {
            string portName;

            Console.WriteLine("Available Ports:");
            foreach (string s in SerialPort.GetPortNames())
            {
                Console.WriteLine("   {0}", s);
            }

            Console.Write("Enter COM port value (Default: {0}): ", defaultPortName);
            portName = Console.ReadLine();

            if (portName == "" || !(portName.ToLower()).StartsWith("com"))
            {
                portName = defaultPortName;
            }
            return portName;
        }
        public static bool IsValidFilename(string testName)
        {
            System.Text.RegularExpressions.Regex containsABadCharacter =
                new System.Text.RegularExpressions.Regex("["
                  + System.Text.RegularExpressions.Regex.Escape(new string(System.IO.Path.GetInvalidPathChars())) + "]");
            if (containsABadCharacter.IsMatch(testName)) { return false; };

            // other checks for UNC, drive-path format, etc

            return true;
        }
        private static void StopLogging()
        {
            if (logIO)
            {
                logIO = false;
                DateTime date = DateTime.Now;
                logList0.Add(String.Format("Day: {0:d} Time: {1:g}", date.Date, date.TimeOfDay));
                string[] lines = new string[logList0.Count];
                logList0.CopyTo(lines, 0);
                for (int n = 0; n < lines.Length; n++)
                {
                    lines[n] = lines[n].TrimEnd(whiteSpace);
                    lines[n] = lines[n].TrimStart(whiteSpace);
                }
                System.IO.File.WriteAllLines(@logList0_Name.ToString(), lines);
                Console.WriteLine("Program has stopped logging IO");
            }
        }
    }
}

